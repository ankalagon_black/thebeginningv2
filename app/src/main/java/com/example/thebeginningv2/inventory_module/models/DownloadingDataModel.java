package com.example.thebeginningv2.inventory_module.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_module.messages.DownloadingFinished;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.ref.WeakReference;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 12.07.2017.
 */

public class DownloadingDataModel extends BaseModel {
    private ViewInterface viewinterface;
    private DomainInterface domainInterface;

    private Disposable download;

    public DownloadingDataModel(ViewInterface viewinterface, DomainInterface domainInterface){
        this.viewinterface = viewinterface;
        this.domainInterface = domainInterface;
    }

    public void download(final SmbTransmissionInfo transmissionInfo){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                viewinterface.onProcessFinished();
                EventBus.getDefault().post(new DownloadingFinished());
            }

            @Override
            public void onError(Throwable e) {
                viewinterface.onError(e);
            }
        };

        download = observer;

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domainInterface.download(transmissionInfo, new WeakReference<CallbackInterface>(viewinterface));
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void cancel(){
        download.dispose();
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(download != null && !download.isDisposed()) download.dispose();
    }

    public interface ViewInterface extends CallbackInterface{
        void onProcessFinished();
        void onError(Throwable e);
    }

    public interface CallbackInterface{
        void onDownloadingStarted(int files);

        void onFileStartedDownloading(String name, long size);
        void onFileProgressChanged(long progress, long size);
        void onFileFinishedDownloading();
    }

    public interface DomainInterface{
        void download(SmbTransmissionInfo transmissionInfo, WeakReference<CallbackInterface> reference) throws Exception;
    }
}
