package com.example.thebeginningv2.inventory_module.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 19.06.2017.
 */

public class InventoryModuleModel extends BaseModel {
    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable getInventoryCategories;

    public InventoryModuleModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public boolean isPathCorrect(){
        return domainInterface.isPathCorrect();
    }

    public void initialize(){
        domainInterface.initialize();
    }

    public void moveForward(String name){
        domainInterface.moveForward(name);
    }

    public void getInventoryCategories(){
        DisposableSingleObserver<List<String>> observer = new DisposableSingleObserver<List<String>>() {
            @Override
            public void onSuccess(List<String> value) {
                viewInterface.onInventoryCategoriesReceived(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getInventoryCategories = observer;

        Single.fromCallable(new Callable<List<String>>() {
            @Override
            public List<String> call() throws Exception {
                return domainInterface.getInventoryCategories();
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(getInventoryCategories != null && !getInventoryCategories.isDisposed()) getInventoryCategories.dispose();
    }

    public interface ViewInterface{
        void onInventoryCategoriesReceived(List<String> inventoryCategories);
        void onError(Throwable e);
    }

    public interface DomainInterface{
        boolean isPathCorrect();
        void initialize();
        void moveForward(String name);

        List<String> getInventoryCategories();
    }

}
