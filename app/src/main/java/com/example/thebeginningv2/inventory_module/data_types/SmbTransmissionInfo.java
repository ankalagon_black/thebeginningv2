package com.example.thebeginningv2.inventory_module.data_types;

/**
 * Created by Константин on 22.06.2017.
 */

public class SmbTransmissionInfo {
    private String ip;
    private String root;
    private String login, password;

    public SmbTransmissionInfo(String ip, String root, String login, String password){
        this.ip = ip;
        this.root = root;
        this.login = login;
        this.password = password;
    }

    public String getRoot() {
        return root;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSmbProtocolUrl() {
        return "smb://" + ip + root + "/";
    }
}
