package com.example.thebeginningv2.inventory_module.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.ModelDialog;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_module.domain.SettingsDomain;
import com.example.thebeginningv2.inventory_module.models.SynchronizationModel;

/**
 * Created by Константин on 22.06.2017.
 */

public class SynchronizationDialog extends ModelDialog<SettingsDomain, SynchronizationModel>
        implements  SynchronizationModel.ViewInterface{

    private AlertDialog alertDialog;
    private View progressBar;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        progressBar = LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        alertDialog = builder.setTitle(R.string.synchronization_in_progress).
                setView(progressBar).
                setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().cancel();
                    }
                }).create();

        return alertDialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().synchronize(getTransmissionInfo());
    }

    private SmbTransmissionInfo getTransmissionInfo(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String ip = preferences.getString(getString(R.string.smb_ip_key), getString(R.string.smb_ip_default));
        String catalog = preferences.getString(getString(R.string.smb_forward_catalog_key), getString(R.string.smb_catalog_forward_default));
        String login = preferences.getString(getString(R.string.smb_login_key), getString(R.string.smb_login_default));
        String password = preferences.getString(getString(R.string.smb_password_key), getString(R.string.smb_password_default));

        return new SmbTransmissionInfo(ip, catalog, login, password);
    }

    @Override
    protected SynchronizationModel createModel() {
        return new SynchronizationModel(this, getDomain());
    }

    @Override
    public void onSynchronizationFinished() {
        progressBar.setVisibility(View.INVISIBLE);

        alertDialog.setTitle(R.string.synchronization_finished);
        Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        button.setText(R.string.ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SynchronizationDialog.this.dismiss();
            }
        });
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SynchronizationDialog.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().synchronize(getTransmissionInfo());
                    }
                }).
                setCancelable(false).
                create().show();
    }
}
