package com.example.thebeginningv2.inventory_module;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.activities.AppCompatDomainActivity;
import com.example.thebeginningv2.inventories.InventoriesActivity;
import com.example.thebeginningv2.inventory_module.adapters.InventoriesCategoriesAdapter;
import com.example.thebeginningv2.inventory_module.data_types.AppSettings;
import com.example.thebeginningv2.inventory_module.domain.InventoryModuleDomain;
import com.example.thebeginningv2.inventory_module.models.InventoryModuleModel;
import com.example.thebeginningv2.repository.managers.FileManager;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 19.06.2017.
 */

public class InventoryModuleActivity extends AppCompatDomainActivity<InventoryModuleDomain>
        implements InventoryModuleModel.ViewInterface, InventoriesCategoriesAdapter.Interface {
    public static final int GET_PERMISSIONS = 0;

    private RecyclerView recyclerView;
    private TextView currentMode;

    private InventoryModuleModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_module_first_level);

        recyclerView = (RecyclerView) findViewById(R.id.first_level_catalogs_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        currentMode = (TextView) findViewById(R.id.current_mode);

        onAppSettingsSelected(getAppSettings());

        Button changeMode = (Button) findViewById(R.id.change_mode);
        changeMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(InventoryModuleActivity.this);
                builder.setTitle(R.string.choose_mode).
                        setItems(R.array.modes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i){
                                    case 0:
                                        editAppSettings(new AppSettings(AppSettings.INVENTORY_MODE));
                                        break;
                                }
                            }
                        })
                        .setNegativeButton(R.string.cancel, null).
                        create().
                        show();
            }
        });

        model = new InventoryModuleModel(this, getDomain());
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissionsToGrant = new ArrayList<>();

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                permissionsToGrant.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                permissionsToGrant.add(Manifest.permission.ACCESS_FINE_LOCATION);

            List<String> toGrant = new ArrayList<>();
            for (String i: permissionsToGrant){
                if(!ActivityCompat.shouldShowRequestPermissionRationale(this, i)){
                    toGrant.add(i);
                }
            }

            if(toGrant.size() != 0){
                ActivityCompat.requestPermissions(this,
                        toGrant.toArray(new String[toGrant.size()]),
                        GET_PERMISSIONS);
            }
            else createFolders();
        }
        else createFolders();
    }

    private void createFolders(){
        if (model.isPathCorrect()) {
            model.initialize();
            model.getInventoryCategories();
        } else Toast.makeText(this, R.string.path_is_wrong, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GET_PERMISSIONS:
                boolean success = true;

                for (Integer i: grantResults){
                    if(i == PackageManager.PERMISSION_DENIED){
                        success = false;
                        break;
                    }
                }

                if(!success) Toast.makeText(this, R.string.permissions_are_not_granted_warning, Toast.LENGTH_SHORT).show();
                else createFolders();
                break;

            default:
                break;
        }
    }

    @Override
    protected void initializeRepository() {
        super.initializeRepository();

        PathManager pathManager = PathManager.getIntance();

        if(!pathManager.hasRoot()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String storage = preferences.getString(getString(R.string.storage_directory_key), null);

            if (storage == null) {
                FileManager fileManager = new FileManager();
                List<String> externalDirectories = fileManager.getExternalDirectories(this);

                if (externalDirectories.size() == 0) {
                    storage = fileManager.getDefaultDirectory();
                    Toast.makeText(this, R.string.default_directory_is_chosen, Toast.LENGTH_SHORT).show();
                } else {
                    storage = externalDirectories.get(0);
                    Toast.makeText(this, R.string.external_directory_is_chosen, Toast.LENGTH_SHORT).show();
                }

                preferences.edit().putString(getString(R.string.storage_directory_key), storage).apply();
            }

            String targetFolder = preferences.getString(getString(R.string.app_catalog_key), getString(R.string.app_catalog_default));

            pathManager.setRoot(storage + targetFolder);
        }
    }

    @Override
    protected InventoryModuleDomain createDomain() {
        return new InventoryModuleDomain(PathManager.getIntance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.inventory_module_first_level, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    private AppSettings getAppSettings(){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_settings_key), MODE_PRIVATE);
        int mode = preferences.getInt(getString(R.string.app_mode_key), AppSettings.INVENTORY_MODE);

        return new AppSettings(mode);
    }

    private void editAppSettings(AppSettings appSettings){
        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_settings_key), MODE_PRIVATE);
        preferences.edit().putInt(getString(R.string.app_mode_key), appSettings.getProgramMode()).apply();
    }

    private void onAppSettingsSelected(AppSettings appSettings){
        switch (appSettings.getProgramMode()){
            case AppSettings.INVENTORY_MODE:
                currentMode.setText(R.string.mode_inventory);
                break;
        }
    }

    @Override
    public void onInventoryCategoriesReceived(List<String> inventoryCategories) {
        InventoriesCategoriesAdapter adapter = new InventoriesCategoriesAdapter(inventoryCategories, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(String name) {
        model.moveForward(name);
        Intent intent = new Intent(this, InventoriesActivity.class);
        startActivity(intent);
    }
}