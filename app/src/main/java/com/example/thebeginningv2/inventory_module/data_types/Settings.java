package com.example.thebeginningv2.inventory_module.data_types;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Константин on 03.08.2017.
 */

public class Settings {
    @SerializedName("НомерТерминала")
    @Expose
    private String dctNumber;

    @SerializedName("ШлюзКоманд")
    @Expose
    private String commandsGateway;

    @SerializedName("ШлюзКомандКод")
    @Expose
    private String commandsGatewayKey;

    @SerializedName("КаталогПрямойСервер")
    @Expose
    private String smbForwardCatalog;

    @SerializedName("КаталогОбратныйСервер")
    @Expose
    private String smbBackwardsCatalog;

    @SerializedName("Сервер")
    @Expose
    private String smbIpAddress;

    @SerializedName("Логин")
    @Expose
    private String smbLogin;

    @SerializedName("Пароль")
    @Expose
    private String smbPassword;

    public String getDctNumber() {
        return dctNumber;
    }

    public void setDctNumber(String dctNumber) {
        this.dctNumber = dctNumber;
    }

    public boolean hasTcdNumber(){
        return dctNumber != null;
    }

    public String getCommandsGateway() {
        return commandsGateway;
    }

    public void setCommandsGateway(String commandsGateway) {
        this.commandsGateway = commandsGateway;
    }

    public boolean hasCommandsGateway(){
        return commandsGateway != null;
    }

    public String getCommandsGatewayKey() {
        return commandsGatewayKey;
    }

    public void setCommandsGatewayKey(String commandsGatewayKey) {
        this.commandsGatewayKey = commandsGatewayKey;
    }

    public boolean hasCommandsGatewayKey(){
        return commandsGatewayKey != null;
    }

    public String getSmbForwardCatalog() {
        return smbForwardCatalog;
    }

    public void setSmbForwardCatalog(String smbForwardCatalog) {
        this.smbForwardCatalog = smbForwardCatalog;
    }

    public boolean hasSmbForwardCatalog(){
        return smbForwardCatalog != null;
    }

    public String getSmbBackwardsCatalog() {
        return smbBackwardsCatalog;
    }

    public void setSmbBackwardsCatalog(String smbBackwardsCatalog) {
        this.smbBackwardsCatalog = smbBackwardsCatalog;
    }

    public boolean hasSmbBackwardsCatalog(){
        return smbBackwardsCatalog != null;
    }

    public String getSmbIpAddress() {
        return smbIpAddress;
    }

    public void setSmbIpAddress(String smbIpAddress) {
        this.smbIpAddress = smbIpAddress;
    }

    public boolean hasSmbIpAddress(){
        return smbIpAddress != null;
    }

    public String getSmbLogin() {
        return smbLogin;
    }

    public void setSmbLogin(String smbLogin) {
        this.smbLogin = smbLogin;
    }

    public boolean hasSmbLogin(){
        return smbLogin != null;
    }

    public String getSmbPassword() {
        return smbPassword;
    }

    public void setSmbPassword(String smbPassword) {
        this.smbPassword = smbPassword;
    }

    public boolean hasSmbPassword(){
        return smbPassword != null;
    }
}
