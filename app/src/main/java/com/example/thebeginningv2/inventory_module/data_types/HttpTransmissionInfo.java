package com.example.thebeginningv2.inventory_module.data_types;

/**
 * Created by Константин on 12.07.2017.
 */

public class HttpTransmissionInfo {
    private String ip;
    private String commandsGateway, settingGateway;
    private String commandsGatewayCode, settingsGatewayCode;

    public HttpTransmissionInfo(String ip, String commandsGateway, String settingGateway,
                                String commandsGatewayCode, String settingsGatewayCode){
        this.ip = ip;
        this.commandsGateway = commandsGateway;
        this.settingGateway = settingGateway;
        this.commandsGatewayCode = commandsGatewayCode;
        this.settingsGatewayCode = settingsGatewayCode;
    }

    public String getIp() {
        return ip;
    }

    public String getCommandsGateway() {
        return commandsGateway;
    }

    public String getSettingGateway() {
        return settingGateway;
    }

    public String getCommandsGatewayCode() {
        return commandsGatewayCode;
    }

    public String getSettingsGatewayCode() {
        return settingsGatewayCode;
    }

    public String getHttpsUrl(){
        return "https://" + ip + "/";
    }
}
