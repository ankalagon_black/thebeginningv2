package com.example.thebeginningv2.inventory_module.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.ModelDialog;
import com.example.thebeginningv2.inventory_module.data_types.HttpTransmissionInfo;
import com.example.thebeginningv2.inventory_module.domain.SettingsDomain;
import com.example.thebeginningv2.inventory_module.models.GetSettingsModel;

/**
 * Created by Константин on 12.07.2017.
 */

public class GetSettingsDialog extends ModelDialog<SettingsDomain, GetSettingsModel> implements GetSettingsModel.ViewInterface {
    private View progressBar;
    private AlertDialog alertDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        progressBar = LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        alertDialog = builder.setTitle(R.string.http_get_settings).
                setView(progressBar).
                setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().cancel();
                    }
                }).create();

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().getSettings(getTransmissionInfo(), getActivity());
    }

    @Override
    protected GetSettingsModel createModel() {
        return new GetSettingsModel(this, getDomain());
    }

    private HttpTransmissionInfo getTransmissionInfo(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String ip = preferences.getString(getString(R.string.http_settings_ip_key), getString(R.string.http_settings_ip_default));
        String commandGateway = preferences.getString(getString(R.string.commands_gateway_key), getString(R.string.commands_gateway_default));
        String settingGateway = preferences.getString(getString(R.string.settings_gateway_key), getString(R.string.settings_gateway_default));
        String commandGatewayCode = preferences.getString(getString(R.string.commands_gateway_code_key), getString(R.string.commands_gateway_code_default));
        String settingsGatewayCode = preferences.getString(getString(R.string.settings_gateway_code_key), getString(R.string.settings_gateway_code_default));

        return new HttpTransmissionInfo(ip, commandGateway, settingGateway, commandGatewayCode, settingsGatewayCode);
    }

    @Override
    public void onTestFinished(String response) {
        if(response == null) response = getString(R.string.result_is_empty);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.settings_received).
                setMessage(response).
                setPositiveButton(R.string.ok, null).
                create().show();

        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        GetSettingsDialog.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().getSettings(getTransmissionInfo(), getActivity());
                    }
                }).
                setCancelable(false).
                create().show();
    }
}
