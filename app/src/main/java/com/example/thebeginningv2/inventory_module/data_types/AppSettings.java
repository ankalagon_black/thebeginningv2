package com.example.thebeginningv2.inventory_module.data_types;

/**
 * Created by Константин on 19.06.2017.
 */

public class AppSettings {
    public static final int INVENTORY_MODE = 0;

    private int programMode;

    public AppSettings(int programMode){
        this.programMode = programMode;
    }

    public int getProgramMode() {
        return programMode;
    }

    public void setProgramMode(int programMode) {
        this.programMode = programMode;
    }
}
