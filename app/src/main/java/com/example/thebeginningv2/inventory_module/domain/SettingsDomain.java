package com.example.thebeginningv2.inventory_module.domain;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_module.data_types.HttpTransmissionInfo;
import com.example.thebeginningv2.inventory_module.data_types.Settings;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_module.models.GetSettingsModel;
import com.example.thebeginningv2.inventory_module.models.SynchronizationModel;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.network.CertificateManager;
import com.example.thebeginningv2.repository.managers.network.GetSettingsInterface;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Константин on 11.07.2017.
 */

public class SettingsDomain extends DownloadingDataDomain implements SynchronizationModel.DomainInterface, GetSettingsModel.DomainInterface{
    private CertificateManager certificateManager;
    private Activity activity;

    public SettingsDomain(PathManager pathManager, CertificateManager certificateManager, Activity activity) {
        super(pathManager);
        this.certificateManager = certificateManager;
        this.activity = activity;
    }

    @Override
    public void synchronize(SmbTransmissionInfo transmissionInfo) throws MalformedURLException, SmbException {
        NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication(null, transmissionInfo.getLogin(),
                transmissionInfo.getPassword());

        SmbFile smbFile = new SmbFile(transmissionInfo.getSmbProtocolUrl(), authentication);
        SmbFile[] files = smbFile.listFiles();
        if(files != null){
            for (SmbFile i: files){
                File catalog = new File(pathManager.getDataPath(), i.getName());
                if(!catalog.exists()) catalog.mkdir();
            }
        }
    }

    @Override
    public String getSettings(HttpTransmissionInfo info, Context context) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        OkHttpClient okHttp = new OkHttpClient.Builder()
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return s.equals(sslSession.getPeerHost());
                    }
                })
                .sslSocketFactory(getSSLContext(context).getSocketFactory())
                .connectTimeout(1200, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(info.getHttpsUrl())
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetSettingsInterface anInterface = retrofit.create(GetSettingsInterface.class);

        Response<JsonObject> response = anInterface.getSettings(info.getSettingGateway(),
                RequestBody.create(MediaType.parse("text/plain"), "N1=XXXX \n N2=НастройкиТерминала \n N3=" + getDCTNumber())).execute();

        JsonObject object = response.body();
        if(object != null) {
            JsonElement element = object.get("Результат");
            Settings settings = new Gson().fromJson(element, Settings.class);
            setSettings(settings);

            return object.toString();
        }
        else return null;
    }

    private SSLContext getSSLContext(Context context) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        KeyStore keyStore;

        if(certificateManager.isCertificateCreated()) keyStore = certificateManager.getCertificate();
        else keyStore = certificateManager.createCertificate(context);

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustManagerFactory.getTrustManagers(), null);

        return sslContext;
    }

    private String getDCTNumber(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        return preferences.getString(activity.getString(R.string.dct_number_key), activity.getString(R.string.dct_number_default));
    }

    private void setSettings(Settings settings){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(activity).edit();

        if(settings.hasTcdNumber()) editor.putString(activity.getString(R.string.dct_number_key), settings.getDctNumber());
        if(settings.hasCommandsGateway()) editor.putString(activity.getString(R.string.commands_gateway_key), settings.getCommandsGateway());
        if(settings.hasCommandsGatewayKey())editor.putString(activity.getString(R.string.commands_gateway_code_key), settings.getCommandsGatewayKey());
        if(settings.hasSmbForwardCatalog()) editor.putString(activity.getString(R.string.smb_forward_catalog_key), settings.getSmbForwardCatalog());
        if(settings.hasSmbBackwardsCatalog()) editor.putString(activity.getString(R.string.smb_backward_catalog_key), settings.getSmbBackwardsCatalog());
        if(settings.hasSmbIpAddress()) editor.putString(activity.getString(R.string.smb_ip_key), settings.getSmbIpAddress());
        if(settings.hasSmbLogin()) editor.putString(activity.getString(R.string.smb_login_key), settings.getSmbLogin());
        if(settings.hasSmbPassword()) editor.putString(activity.getString(R.string.smb_password_key), settings.getSmbPassword());

        editor.apply();
    }

}
