package com.example.thebeginningv2.inventory_module.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.ModelDialog;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_module.domain.DownloadingDataDomain;
import com.example.thebeginningv2.inventory_module.models.DownloadingDataModel;

import java.util.Locale;

/**
 * Created by Константин on 07.06.2017.
 */

public class DownloadingDataDialog extends ModelDialog<DownloadingDataDomain,DownloadingDataModel>
        implements DownloadingDataModel.ViewInterface{
    private TextView file;
    private ProgressBar filesBar, fileBar;
    private TextView filesProgress, fileProgress;

    private AlertDialog alertDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.transmission_process, null);

        TextView files = (TextView) view.findViewById(R.id.files);
        files.setText(R.string.dowloaded_files);

        filesBar = (ProgressBar) view.findViewById(R.id.files_bar);
        filesProgress = (TextView) view.findViewById(R.id.files_progress);

        file = (TextView) view.findViewById(R.id.file);
        fileBar = (ProgressBar) view.findViewById(R.id.file_bar);
        fileProgress = (TextView) view.findViewById(R.id.file_progress);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        alertDialog = builder.setCancelable(false).
                setTitle(R.string.downloading_title).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().cancel();
                        DownloadingDataDialog.this.dismiss();
                    }
                }).create();

        return alertDialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().download(getTransmissionInfo());
    }

    private SmbTransmissionInfo getTransmissionInfo(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        String ip = preferences.getString(getString(R.string.smb_ip_key), getString(R.string.smb_ip_default));
        String catalog = preferences.getString(getString(R.string.smb_backward_catalog_key), getString(R.string.smb_catalog_backward_default));
        String login = preferences.getString(getString(R.string.smb_login_key), getString(R.string.smb_login_default));
        String password = preferences.getString(getString(R.string.smb_password_key), getString(R.string.smb_password_default));

        return new SmbTransmissionInfo(ip, catalog, login, password);
    }

    @Override
    protected DownloadingDataModel createModel() {
        return new DownloadingDataModel(this, getDomain());
    }

    @Override
    public void onProcessFinished() {
        getDialog().setTitle(R.string.downloaded_title);

        Button button = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);

        button.setText(R.string.ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadingDataDialog.this.dismiss();
            }
        });
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                        DownloadingDataDialog.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       getModel().download(getTransmissionInfo());
                    }
                }).
                setCancelable(false).
                create().show();
    }

    @Override
    public void onDownloadingStarted(int files) {
        filesBar.setMax(files);
        filesBar.setProgress(0);

        filesProgress.setText(String.format(Locale.US, getString(R.string.out_of), 0, files));
    }

    @Override
    public void onFileStartedDownloading(String name, long size) {
        file.setText(name);

        float sizeF = (float) size / 1024;

        fileBar.setMax((int) sizeF);
        fileBar.setProgress(0);

        fileProgress.setText(String.format(Locale.US, getString(R.string.out_of_kbytes), 0.0f, sizeF));
    }

    @Override
    public void onFileProgressChanged(long progress, long size) {
        float sizeF = (float) size / 1024, progressF = (float) progress / 1024;

        fileBar.setProgress((int) progressF);

        fileProgress.setText(String.format(Locale.US, getString(R.string.out_of_kbytes), progressF, sizeF));
    }

    @Override
    public void onFileFinishedDownloading() {
        int progress = filesBar.getProgress() + 1;
        filesBar.setProgress(progress);

        filesProgress.setText(String.format(Locale.US, getString(R.string.out_of), progress, filesBar.getMax()));
    }
}
