package com.example.thebeginningv2.inventory_module;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.activities.PreferenceDomainActivity;
import com.example.thebeginningv2.inventory_module.dialogs.DownloadingDataDialog;
import com.example.thebeginningv2.inventory_module.dialogs.GetSettingsDialog;
import com.example.thebeginningv2.inventory_module.dialogs.SynchronizationDialog;
import com.example.thebeginningv2.inventory_module.domain.SettingsDomain;
import com.example.thebeginningv2.repository.managers.FileManager;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.network.CertificateManager;

import java.util.List;

/**
 * Created by Константин on 08.06.2017.
 */

public class SettingsActivity extends PreferenceDomainActivity<SettingsDomain> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_module_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.settings);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsActivity.this.finish();
            }
        });

        getFragmentManager().beginTransaction().replace(R.id.settings_container, new SettingsFragment()).commit();
    }

    @Override
    protected SettingsDomain createDomain() {
        PathManager pathManager = PathManager.getIntance();
        return new SettingsDomain(pathManager, new CertificateManager(pathManager), this);
    }

    public static class SettingsFragment extends PreferenceFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            Preference storagePreference = findPreference(getString(R.string.storage_directory_key));
            storagePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    FileManager fileManager = new FileManager();

                    final List<String> directories = fileManager.getDirectories(getActivity());

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setTitle(R.string.choose_directory).
                            setItems(directories.toArray(new String[directories.size()]), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onStorageChosen(directories.get(i));
                                }
                            }).setNegativeButton(R.string.cancel, null).create().show();

                    return true;
                }
            });

            EditTextPreference preference = (EditTextPreference) findPreference(getString(R.string.app_catalog_key));
            preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    String storage = preferences.getString(getString(R.string.storage_directory_key), null);
                    if(storage != null) PathManager.getIntance().setRoot(storage + o);
                    return true;
                }
            });

            Preference backwardSynchronization = findPreference(getString(R.string.backward_synchronization_key));
            backwardSynchronization.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                    if (wifi.isConnected()) {
                        DownloadingDataDialog dialog = new DownloadingDataDialog();
                        dialog.show(getFragmentManager(), dialog.getTag());
                    }
                    else Toast.makeText(getActivity(), R.string.wifi_is_not_enabled, Toast.LENGTH_SHORT).show();

                    return true;
                }
            });

            Preference synchronization = findPreference(getString(R.string.synchronization_key));
            synchronization.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                    if (wifi.isConnected()) {
                        SynchronizationDialog dialog = new SynchronizationDialog();
                        dialog.show(getFragmentManager(), dialog.getTag());
                    }
                    else Toast.makeText(getActivity(), R.string.wifi_is_not_enabled, Toast.LENGTH_SHORT).show();

                    return true;
                }
            });

            Preference getSettings = findPreference(getString(R.string.get_settings_key));
            getSettings.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GetSettingsDialog dialog = new GetSettingsDialog();
                    dialog.show(getFragmentManager(), dialog.getTag());
                    return true;
                }
            });
        }

        public void onStorageChosen(String storage){
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(getString(R.string.storage_directory_key), storage);
            editor.apply();

            String targetFolder = preferences.getString(getString(R.string.app_catalog_key), getString(R.string.app_catalog_default));

            PathManager.getIntance().setRoot(storage + targetFolder);
        }
    }
}
