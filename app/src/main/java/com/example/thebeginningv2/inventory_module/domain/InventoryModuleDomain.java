package com.example.thebeginningv2.inventory_module.domain;

import com.example.thebeginningv2.inventory_module.models.InventoryModuleModel;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 11.07.2017.
 */

public class InventoryModuleDomain implements InventoryModuleModel.DomainInterface {
    private PathManager pathManager;

    public InventoryModuleDomain(PathManager pathManager){
        this.pathManager = pathManager;
    }

    @Override
    public boolean isPathCorrect() {
        if(pathManager.getLevel() == PathManager.FIRST_LEVEL) {
            File root = new File(pathManager.getRoot());
            if (!root.exists() && !root.mkdirs()) return false;

            File dataRoot = new File(pathManager.getDataRoot());
            if (!dataRoot.exists() && !dataRoot.mkdir()) return false;

            File settingsRoot = new File(pathManager.getSettingsRoot());
            if(!settingsRoot.exists() && !settingsRoot.mkdir()) return false;

            File systemRoot = new File(pathManager.getSystemRoot());
            if(!systemRoot.exists() && !systemRoot.mkdir()) return false;

            File distrRoot = new File(pathManager.getDistrFolder());
            return distrRoot.exists() || distrRoot.mkdir();
        }
        else return true;
    }

    @Override
    public void initialize() {
        if(pathManager.getLevel() == PathManager.SECOND_LEVEL) pathManager.moveBackward();
    }

    @Override
    public void moveForward(String name) {
        pathManager.moveForward(name);
    }

    @Override
    public List<String> getInventoryCategories() {
        File file = new File(pathManager.getDataPath());
        File[] files = file.listFiles();

        if(files == null) {
            return new ArrayList<>();
        }
        else {
            List<String> inventoryCategories = new ArrayList<>(files.length);
            for (File i : files) inventoryCategories.add(i.getName());

            return inventoryCategories;
        }
    }
}
