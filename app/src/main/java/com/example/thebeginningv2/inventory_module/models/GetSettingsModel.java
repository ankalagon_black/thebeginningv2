package com.example.thebeginningv2.inventory_module.models;

import android.content.Context;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventory_module.data_types.HttpTransmissionInfo;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 12.07.2017.
 */

public class GetSettingsModel extends BaseModel {
    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable getSettings;

    public GetSettingsModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void getSettings(final HttpTransmissionInfo info, final Context context){
        DisposableSingleObserver<String> observer = new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String value) {
                viewInterface.onTestFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getSettings = observer;

        Single.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return domainInterface.getSettings(info, context);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void cancel(){
        getSettings.dispose();
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(getSettings != null && !getSettings.isDisposed()) getSettings.dispose();
    }

    public interface ViewInterface{
        void onTestFinished(String response);
        void onError(Throwable e);
    }

    public interface DomainInterface{
        String getSettings(HttpTransmissionInfo info, Context context) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException;
    }
}
