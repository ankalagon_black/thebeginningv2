package com.example.thebeginningv2.inventory_module.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;

import java.net.MalformedURLException;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import jcifs.smb.SmbException;

/**
 * Created by Константин on 22.06.2017.
 */

public class SynchronizationModel extends BaseModel {
    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable synchronize;

    public SynchronizationModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void synchronize(final SmbTransmissionInfo transmissionInfo){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                viewInterface.onSynchronizationFinished();
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        synchronize = observer;

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domainInterface.synchronize(transmissionInfo);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void cancel(){
        synchronize.dispose();
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(synchronize != null && !synchronize.isDisposed()) synchronize.dispose();
    }

    public interface ViewInterface{
        void onSynchronizationFinished();
        void onError(Throwable e);
    }

    public interface DomainInterface{
        void synchronize(SmbTransmissionInfo transmissionInfo) throws MalformedURLException, SmbException;
    }
}
