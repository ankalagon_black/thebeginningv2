package com.example.thebeginningv2.inventory_module.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thebeginningv2.R;

import java.util.List;

/**
 * Created by Константин on 19.06.2017.
 */

public class InventoriesCategoriesAdapter extends RecyclerView.Adapter<InventoriesCategoriesAdapter.Holder> {

    private List<String> list;

    private Interface anInterface;

    public InventoriesCategoriesAdapter(List<String> list, Interface anInterface) {
        this.list = list;
        this.anInterface = anInterface;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_first_level_item, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        private TextView name;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
        }

        public void setItem(final String name){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onClick(name);
                }
            });
            this.name.setText(name);
        }
    }

    public interface Interface{
        void onClick(String name);
    }
}
