package com.example.thebeginningv2.inventory_module.domain;

import android.os.Handler;
import android.os.Looper;

import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_module.models.DownloadingDataModel;
import com.example.thebeginningv2.repository.managers.FileManager;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

/**
 * Created by Константин on 12.07.2017.
 */

public class DownloadingDataDomain implements DownloadingDataModel.DomainInterface{
    protected PathManager pathManager;

    public DownloadingDataDomain(PathManager pathManager){
        this.pathManager = pathManager;
    }

    @Override
    public void download(SmbTransmissionInfo transmissionInfo, final WeakReference<DownloadingDataModel.CallbackInterface> reference) throws Exception {
        NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication("", transmissionInfo.getLogin(),
                transmissionInfo.getPassword());

        SmbFile root = new SmbFile(transmissionInfo.getSmbProtocolUrl(), authentication);

        if(root.exists() && root.isDirectory()) {
            SmbFile[] smbFiles = root.listFiles();
            if(smbFiles != null) {
                Handler handler = new Handler(Looper.getMainLooper());

                if (pathManager.getLevel() == PathManager.FIRST_LEVEL) {
                    final int count = getFilesCount(root);

                    if(reference.get() != null) handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onDownloadingStarted(count);
                        }
                    });

                    File settings = new File(pathManager.getSettingsRoot()),
                            distr = new File(pathManager.getDistrFolder());

                    FileManager fileManager = new FileManager();
                    fileManager.deleteContent(settings);
                    fileManager.deleteContent(distr);

                    for (SmbFile i: smbFiles){
                        String name = i.getName();
                        name = i.isDirectory() ? name.substring(0, name.length() - 1) : name;

                        if(name.equals(PathManager.SETTINGS)) download(settings, i, reference, handler);
                        else{
                            File file = new File(distr.getPath(), name);
                            if(i.isDirectory()) file.mkdir();

                            download(file, i, reference, handler);
                        }
                    }
                }
                else {
                    SmbFile destination = new SmbFile(root.getPath() + PathManager.SETTINGS + "/" + pathManager.getFirstLevelCatalog() + "/", authentication);

                    final int count = getFilesCount(destination);

                    if(reference.get() != null) handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onDownloadingStarted(count);
                        }
                    });

                    File settings = new File(pathManager.getSettingsPath());

                    FileManager fileManager = new FileManager();
                    fileManager.deleteContent(settings);

                    download(settings,
                            destination,
                            reference, handler);
                }
            }
        }
        else throw new Exception("No root");
    }

    private int getFilesCount(SmbFile smbFile) throws SmbException {
        if(smbFile.isDirectory()){
            int count = 0;
            SmbFile[] smbFiles = smbFile.listFiles();
            if(smbFiles != null){
                for (SmbFile i: smbFiles){
                    count += getFilesCount(i);
                }
            }

            return count;
        }
        else return 1;
    }

    private void download(final File file, SmbFile smbFile, final WeakReference<DownloadingDataModel.CallbackInterface> reference, Handler handler) throws IOException {
        if(smbFile.isDirectory()){
            SmbFile[] smbFiles = smbFile.listFiles();

            if(smbFiles != null) {
                for (SmbFile i: smbFiles){
                    File child = new File(file.getPath(), i.getName());

                    if(i.isDirectory()) child.mkdir();

                    download(child, i, reference, handler);
                }
            }
        }
        else {
            final long size = smbFile.length();

            if(reference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        reference.get().onFileStartedDownloading(file.getName(), size);
                    }
                });
            }

            SmbFileInputStream inputStream = new SmbFileInputStream(smbFile);
            FileOutputStream outputStream = new FileOutputStream(file);

            byte[] buffer = new byte[12288];
            int n;
            int downloadedBytes = 0;
            while ((n = inputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, n);
                downloadedBytes += n;
                if(reference.get() != null){
                    final int finalDownloadedBytes = downloadedBytes;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onFileProgressChanged(finalDownloadedBytes, size);
                        }
                    });
                }
            }

            outputStream.flush();
            outputStream.close();

            inputStream.close();

            if(reference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        reference.get().onFileFinishedDownloading();
                    }
                });
            }
        }
    }
}
