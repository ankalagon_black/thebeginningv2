//package com.example.thebeginningv2.domain;
//
//
//import com.example.thebeginningv2.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_module.data_types.DataMode;
//import com.example.thebeginningv2.inventory_module.data_types.ModeSettings;
//import com.example.thebeginningv2.inventory_module.data_types.InventoryParameter;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//import com.example.thebeginningv2.repository.data_types.xml.DataPakModeCommon;
//import com.example.thebeginningv2.repository.data_types.xml.DataPakModeField;
//import com.example.thebeginningv2.repository.data_types.xml.DataPakModeRoot;
//import com.example.thebeginningv2.repository.data_types.xml.DataPakRoot;
//import com.example.thebeginningv2.repository.data_types.xml.Element;
//import com.example.thebeginningv2.repository.data_types.xml.InventoryModeAdditionalData;
//import com.example.thebeginningv2.repository.data_types.xml.InventoryModeRoot;
//import com.example.thebeginningv2.repository.data_types.xml.InventoryModeSystemData;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by Константин on 24.06.2017.
// */
//
//public class CreateInventoryUseCase extends SingleSingleUseCase<DataMode, String> {
//    private SecondLevelRepositoryInterface repositoryInterface;
//
//    public CreateInventoryUseCase(Executor executor, SecondLevelRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<String> buildUseCaseSingle(final DataMode params) {
//        return Single.fromCallable(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                String name = "";
//                List<InventoryParameter> inventoryParameters = params.getInventoryParameters();
//
//                List<Element> mainData = new ArrayList<>(), paketsData = new ArrayList<>();
//
//                for(InventoryParameter i: inventoryParameters) {
//                    String value = i.getValueAsString();
//                    if(value == null || value.equals("")) value = "-";
//
//                    if(i.isPartOfName()) name += value;
//
//                    Element element = new Element(i.getTag(), value);
//                    if(i.isInPakets()) paketsData.add(element);
//                    else mainData.add(element);
//                }
//
//                if(name.replace(" ", "").length() == 0) name = "Инвентаризация";
//
//                Date date = Calendar.getInstance().getTime();
//
//                name += ", " + new SimpleDateFormat("dd-MM-yyyy").format(date);
//
//                InventoryModeSystemData systemData = new InventoryModeSystemData(name, new SimpleDateFormat("dd-MM-yyyy HH-mm").format(date));
//
//                ModeSettings modeSettings = params.getModeSettings();
//
//                String dbPatternFile = modeSettings.getDbPatternFile();
//
//                InventoryModeAdditionalData additionalData = new InventoryModeAdditionalData(dbPatternFile,
//                        modeSettings.isDivideByDates() ? "1" : "0",
//                        modeSettings.isRewriteCatalogMode() ? "1" : "0");
//
//                repositoryInterface.createInventoryFolder(name, new InventoryModeRoot(additionalData, mainData, systemData, paketsData));
//
//                DataPakModeRoot pakModeRoot = repositoryInterface.getDataPakMode(additionalData.getDbPatternFile());
//
//                DataPakModeCommon pakModeCommon = pakModeRoot.getCommon();
//                List<DataPakModeField> pakModeFields = pakModeRoot.getFields();
//
//                List<String> tags = new ArrayList<>(pakModeFields.size());
//
//                for (DataPakModeField i: pakModeFields) tags.add(i.getTag());
//                if(pakModeCommon.getGroupField() != null){
//                    tags.add(pakModeCommon.getGroupField());
//                    tags.addAll(pakModeCommon.getGroupBarcodeFields());
//                    tags.addAll(pakModeCommon.getGroupRFIDFields());
//                }
//
//                DataPakRoot pakRoot = repositoryInterface.getDataPak(pakModeCommon.getDbDataFile(), tags);
//
//                repositoryInterface.createInventoryDatabase(name, pakModeRoot, pakRoot);
//
//                return name;
//            }
//        });
//    }
//}
