package com.example.thebeginningv2.core_v4.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.core_v3.models.Model;


/**
 * Created by Константин on 16.07.2017.
 */

public abstract class SupportModelDialog<M extends BaseModel> extends DialogFragment {
    private M model;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = createModel();
        if (model instanceof Model) ((Model) model).registerEventBus();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (model instanceof Model) ((Model) model).unregisterEventBus();
        model.disposeAllBackgroundTasks();
    }

    protected abstract M createModel();

    protected M getModel() {
        return model;
    }
}
