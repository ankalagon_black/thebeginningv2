package com.example.thebeginningv2.core_v4.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.thebeginningv2.core_v3.presenters.BasePresenter;
import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public abstract class SupportPresenterFragment<R extends Repository,P extends BasePresenter> extends Fragment {
    private P presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter = createPresenter();
        presenter.onActivityCreated(getArguments(), savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroyView();
    }

    protected abstract P createPresenter();

    protected P getPresenter(){
        return presenter;
    }
}
