package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by apple on 30.06.17.
 */

public class PakModeField {
    public static final String FIELD = "Field";
    public static final String TAG = "Tag";
    public static final String TYPE = "Type";

    public static final String TITLE = "TitleFull";
    public static final String TITLE_ROW = "TitleRow";
    public static final String ORDER = "Order";

    public static final String DATA_BACKGROUND_COLOR = "DataBackgroundColor";
    public static final String DATA_USE_BOLD_FONT = "DataUseBoldFont";
    public static final String DATA_TEXT_COLOR = "DataTextColor ";
    public static final String DATA_TEXT_SIZE = "DataTextSize";

    public static final String TITLE_BACKGROUND_COLOR = "TitleBackgroundColor";
    public static final String TITLE_USE_BOLD_FONT = "TitleUseBoldFont";
    public static final String TITLE_TEXT_COLOR = "TitleTextColor";
    public static final String TITLE_TEXT_SIZE = "TitleTextSize";

    public static final String STRING = "String", INTEGER = "Integer", BOOLEAN = "Boolean", FLOAT = "Double", DATE = "Date";

    private String tag, type;
    private String title, titleRow, order;
    private String dataBackgroundColor, dataUseBoldFont, dataTextColor, dataTextSize;
    private String titleBackgroundColor, titleUseBoldFont, titleTextColor, titleTextSize;

    public PakModeField(String tag, String type,
                        String title, String titleRow, String order,
                        String dataBackgroundColor, String dataUseBoldFont, String dataTextColor, String dataTextSize,
                        String titleBackgroundColor, String titleUseBoldFont, String titleTextColor, String titleTextSize){
        this.tag = tag;
        this.type = type;

        this.title = title;
        this.titleRow = titleRow;
        this.order = order;

        this.dataBackgroundColor = dataBackgroundColor;
        this.dataUseBoldFont = dataUseBoldFont;
        this.dataTextColor = dataTextColor;
        this.dataTextSize = dataTextSize;

        this.titleBackgroundColor = titleBackgroundColor;
        this.titleUseBoldFont = titleUseBoldFont;
        this.titleTextColor = titleTextColor;
        this.titleTextSize = titleTextSize;
    }

    public String getTag() {
        return tag;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleRow() {
        return titleRow;
    }

    public String getOrder() {
        return order;
    }

    public String getDataBackgroundColor() {
        return dataBackgroundColor;
    }

    public String getDataUseBoldFont() {
        return dataUseBoldFont;
    }

    public String getDataTextColor() {
        return dataTextColor;
    }

    public String getDataTextSize() {
        return dataTextSize;
    }

    public String getTitleBackgroundColor() {
        return titleBackgroundColor;
    }

    public String getTitleUseBoldFont() {
        return titleUseBoldFont;
    }

    public String getTitleTextColor() {
        return titleTextColor;
    }

    public String getTitleTextSize() {
        return titleTextSize;
    }
}
