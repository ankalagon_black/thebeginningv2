package com.example.thebeginningv2.repository.managers.xml_managers;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Константин on 24.06.2017.
 */

public abstract class XmlCreator<T> {

    public void create(T root, File file) throws IOException{
        XmlSerializer serializer = Xml.newSerializer();
        OutputStream outputStream = new FileOutputStream(file);
        serializer.setOutput(outputStream, "UTF-8");

        serializer.startDocument("UTF-8", true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        createRoot(serializer, root);

        serializer.endDocument();

        serializer.flush();
        outputStream.close();
    }

    protected abstract void createRoot(XmlSerializer serializer, T root) throws IOException;
}
