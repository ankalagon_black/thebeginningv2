package com.example.thebeginningv2.repository.data_types;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 06.07.2017.
 */

public class Columns {
    private String barcodeColumn;
    private List<String> searchColumns;
    private String quantityColumn;
    private List<String> listColumns, resultColumns;
    private String groupColumn;
    private List<String> groupBarcodeColumns, groupRFIDColumns;

    public Columns(String barcodeColumn, List<String> searchColumns, String quantityColumn,
                               List<String> listColumns, List<String> resultColumns, String groupColumn, List<String> groupBarcodeColumns, List<String> groupRFIDColumns){
        this.barcodeColumn = barcodeColumn;
        this.searchColumns = searchColumns;
        this.quantityColumn = quantityColumn;
        this.listColumns = listColumns;
        this.resultColumns = resultColumns;
        this.groupColumn = groupColumn;
        this.groupBarcodeColumns = groupBarcodeColumns;
        this.groupRFIDColumns = groupRFIDColumns;
    }

    public String getBarcodeColumn() {
        return barcodeColumn;
    }

    public List<String> getSearchColumns() {
        List<String> columns = new ArrayList<>(searchColumns.size() + 1);
        columns.add(barcodeColumn);
        columns.addAll(searchColumns);
        return columns;
    }

    public String getQuantityColumn() {
        return quantityColumn;
    }

    public List<String> getListColumns() {
        return listColumns;
    }

    public List<String> getResultColumns() {
        return resultColumns;
    }

    public boolean hasGroupColumn(){
        return groupColumn != null;
    }

    public String getGroupColumn() {
        return groupColumn;
    }

    public List<String> getGroupBarcodeColumns() {
        return groupBarcodeColumns;
    }

    public List<String> getGroupRFIDColumns() {
        return groupRFIDColumns;
    }
}
