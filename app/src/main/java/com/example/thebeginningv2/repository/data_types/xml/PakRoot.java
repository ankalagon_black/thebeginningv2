package com.example.thebeginningv2.repository.data_types.xml;

import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakRoot {
    public static final String ROOT = "root";
    public static final String COMMON = "Common";
    public static final String FIELDS = "Fields";

    private PakCommon common;
    private List<PakField> fields;

    public PakRoot(PakCommon common, List<PakField> fields){
        this.common = common;
        this.fields = fields;
    }

    public PakCommon getCommon() {
        return common;
    }

    public List<PakField> getFields() {
        return fields;
    }
}
