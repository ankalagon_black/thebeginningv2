package com.example.thebeginningv2.repository.data_types.tables;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class ColumnsTable {
    public static final String TABLE_NAME = "Columns";

    public static final String NAME_COLUMN = "Name";
    public static final String SEMANTICS_COLUMN = "Semantics";
    public static final String AS_LIST_COLUMN = "AsList";
    public static final String AS_RESULT_COLUMN = "AsResult";

    public static final String TITLE_COLUMN = "Title";
    public static final String ORDER_COLUMN = "OutputOrder";

    public static final String DATA_BACKGROUND_COLOR_COLUMN = "DataBackgroundColor";
    public static final String DATA_USE_BOLD_FONT_COLUMN = "DataUseBoldFont";
    public static final String DATA_TEXT_COLOR_COLUMN = "DataTextColor";
    public static final String DATA_TEXT_SIZE_COLUMN = "DataTextSize";

    public static final String TITLE_BACKGROUND_COLOR_COLUMN = "TitleBackgroundColor";
    public static final String TITLE_USE_BOLD_FONT_COLUMN = "TitleUseBoldFont";
    public static final String TITLE_TEXT_COLOR_COLUMN = "TitleTextColor";
    public static final String TITLE_TEXT_SIZE_COLUMN = "TitleTextSize";

    public static final int BARCODE = 0, RFID = 1, NAME = 2, NUMBER = 3, QUANTITY = 4, GROUP = 5, GROUP_BARCODE = 6, GROUP_RFID = 7, NONE = 8;

    public static String[] getInfoColumns(){
        List<String> columns = new ArrayList<>();
        columns.add(TITLE_COLUMN);
        columns.add(DATA_BACKGROUND_COLOR_COLUMN);
        columns.add(DATA_USE_BOLD_FONT_COLUMN);
        columns.add(DATA_TEXT_COLOR_COLUMN);
        columns.add(DATA_TEXT_SIZE_COLUMN);
        columns.add(TITLE_BACKGROUND_COLOR_COLUMN);
        columns.add(TITLE_USE_BOLD_FONT_COLUMN);
        columns.add(TITLE_TEXT_COLOR_COLUMN);
        columns.add(TITLE_TEXT_SIZE_COLUMN);
        return columns.toArray(new String[columns.size()]);
    }
}
