package com.example.thebeginningv2.repository.managers;

/**
 * Created by Константин on 21.06.2017.
 */

public class PathManager {
    public static final int FIRST_LEVEL = 1, SECOND_LEVEL = 2, THIRD_LEVEL = 3;

    public static final String MODE = "mode.xml", PAK_MODE = "pak_mode.xml", PAK = "pak.xml";
    public static final String DB = "db.db";

    public static final String SETTINGS = "Mode";
    public static final String INVENTORY_RESULTS = "inventory_results.tbf";

    public static final String CERTIFICATE = "certificate";

    private final String appFolder = "TheBeginning_v2";
    private final String dataFolder = "Data", settingsFolder = "Settings", systemFolder = "System", distrFolder = "Distr";

    private String root = null;

    private int level = FIRST_LEVEL;

    private String firstLevelCatalog = null, secondLevelCatalog = null;

    private PathManager(){
    }

    public boolean hasRoot(){
        return root != null;
    }

    public void setRoot(String pathOnSDCard){
        root = pathOnSDCard + "/" + appFolder;
    }

    public String getRoot(){
        return root;
    }

    public int getLevel() {
        return level;
    }

    public String getDataRoot(){
        return root + "/" + dataFolder;
    }

    public String getSettingsRoot(){
        return root + "/" + settingsFolder;
    }

    public String getSystemRoot(){
        return root + "/" + systemFolder;
    }

    public String getDistrFolder(){
        return root + "/" + distrFolder;
    }

    public String getFirstLevelCatalog() {
        return firstLevelCatalog;
    }

    public String getSecondLevelCatalog() {
        return secondLevelCatalog;
    }

    public void moveForward(String catalog){
        switch (level){
            case FIRST_LEVEL:
                firstLevelCatalog = catalog;
                level = SECOND_LEVEL;
                break;
            case SECOND_LEVEL:
                secondLevelCatalog = catalog;
                level = THIRD_LEVEL;
                break;
        }
    }

    public void moveBackward(){
        switch (level){
            case SECOND_LEVEL:
                firstLevelCatalog = null;
                level = FIRST_LEVEL;
                break;
            case THIRD_LEVEL:
                secondLevelCatalog = null;
                level = SECOND_LEVEL;
                break;
        }
    }

    public String getDataPath(){
        switch (level){
            case FIRST_LEVEL:
                return getDataRoot();
            case SECOND_LEVEL:
                return getDataRoot() + "/" + firstLevelCatalog;
            case THIRD_LEVEL:
                return getDataRoot() + "/" + firstLevelCatalog + "/" + secondLevelCatalog;
            default:
                return null;
        }
    }

    public String getDataChildPath(String child){
        switch (level){
            case FIRST_LEVEL:
                return root + "/" + dataFolder + "/" + child;
            case SECOND_LEVEL:
                return root + "/" + dataFolder + "/" + firstLevelCatalog + "/" + child;
            case THIRD_LEVEL:
                return root + "/" + dataFolder + "/" + firstLevelCatalog + "/" + secondLevelCatalog + "/" + child;
            default:
                return null;
        }
    }

    public String getDataChildContentPath(String child, String content){
        switch (level){
            case FIRST_LEVEL:
                return root + "/" + dataFolder + "/" + child + "/" + content;
            case SECOND_LEVEL:
                return root + "/" + dataFolder + "/" + firstLevelCatalog + "/" + child + "/" + content;
            case THIRD_LEVEL:
                return root + "/" + dataFolder + "/" + firstLevelCatalog + "/" + secondLevelCatalog + "/" + child + "/" + content;
            default:
                return null;
        }
    }

    public String getSettingsPath(){
       switch (level){
           case FIRST_LEVEL:
               return getSettingsRoot();
           case SECOND_LEVEL:
           case THIRD_LEVEL:
               return getSettingsRoot() + "/" + firstLevelCatalog;
           default:
               return null;
       }
    }

    public String getSettingsPathChild(String child){
        switch (level){
            case FIRST_LEVEL:
                return getSettingsRoot() + "/" + child;
            case SECOND_LEVEL:
            case THIRD_LEVEL:
                return getSettingsRoot() + "/" + firstLevelCatalog + "/" + child;
            default:
                return null;
        }
    }

    public static class PathManagerHolder{
        public static final PathManager pathManager = new PathManager();
    }

    public static PathManager getIntance(){
        return PathManagerHolder.pathManager;
    }
}
