package com.example.thebeginningv2.repository.data_types.xml;

import com.example.thebeginningv2.repository.data_types.tables.ColumnsTable;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseCreator;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakModeRoot {
    public static final String ROOT = "root";
    public static final String COMMON = "Common";
    public static final String FIELDS = "Fields";

    private PakModeCommon common;
    private List<PakModeField> fields;

    public PakModeRoot(PakModeCommon common, List<PakModeField> fields){
        this.common = common;
        this.fields = fields;
    }

    public PakModeCommon getCommon() {
        return common;
    }

    public List<PakModeField> getFields() {
        return fields;
    }

    public List<DatabaseCreator.ColumnRow> getColumns(){
        List<DatabaseCreator.ColumnRow> columns = new ArrayList<>();

        for (PakModeField i: fields){
            String order = i.getOrder();

            String dataUseBoldFont = i.getDataUseBoldFont();
            String dataTextSize  = i.getDataTextSize();

            String titleUseBoldFont = i.getTitleUseBoldFont();
            String titleTextSize = i.getTitleTextSize();

            columns.add(new DatabaseCreator.ColumnRow(i.getTag(), common.getSemantics(i.getTag()),
                    common.isAsList(i.getTag()) ? 1 : 0, common.isAsResults(i.getTag()) ? 1 : 0,
                    i.getTitle(),
                    (order == null || !NumberUtils.isCreatable(order)) ? Integer.MAX_VALUE : Integer.valueOf(order),
                    i.getDataBackgroundColor(),
                    i.getDataTextColor(),
                    dataUseBoldFont != null && dataUseBoldFont.equals("1") ? 1 : 0,
                    (dataTextSize == null || !NumberUtils.isCreatable(dataTextSize)) ? -1 : Float.valueOf(dataTextSize),
                    i.getTitleBackgroundColor(),
                    i.getTitleTextColor(),
                    titleUseBoldFont != null && titleUseBoldFont.equals("1") ? 1 : 0,
                    (titleTextSize == null || !NumberUtils.isCreatable(titleTextSize)) ? -1 : Float.valueOf(titleTextSize)));
        }

        String groupField = common.getGroupField();
        if (groupField != null) {
            columns.add(new DatabaseCreator.ColumnRow(groupField, ColumnsTable.GROUP, 0, 0,
                    null, -1, null, null, 0, -1, null, null, 0, -1));

            List<String> groupBarcodeFields = common.getGroupBarcodeFields();
            for (String i : groupBarcodeFields) {
                columns.add(new DatabaseCreator.ColumnRow(i, ColumnsTable.GROUP_BARCODE, 0, 0,
                        null, -1, null, null, 0, -1, null, null, 0, -1));
            }

            List<String> groupRFIDFields = common.getGroupRFIDFields();
            for (String i : groupRFIDFields) {
                columns.add(new DatabaseCreator.ColumnRow(i, ColumnsTable.GROUP_RFID, 0, 0,
                        null, -1, null, null, 0, -1, null, null, 0, -1));
            }
        }

        return columns;
    }
}
