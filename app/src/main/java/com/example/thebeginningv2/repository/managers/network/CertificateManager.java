package com.example.thebeginningv2.repository.managers.network;

import android.content.Context;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * Created by Константин on 12.07.2017.
 */

public class CertificateManager {
    private static final char[] PASSWORD = new char[] {'f', 'G', '1', 's', 'd', '7', 'L', 'A', '3'};

    private PathManager pathManager;

    public CertificateManager(PathManager pathManager){
        this.pathManager = pathManager;
    }

    public boolean isCertificateCreated(){
        File file = new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE);
        return file.exists();
    }

    public KeyStore createCertificate(Context context) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException {
        CertificateFactory factory = CertificateFactory.getInstance("X.509");

        Certificate certificate;
        InputStream in = context.getResources().openRawResource(R.raw.certificate);
        certificate = factory.generateCertificate(in);
        in.close();

        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null, null);
        keyStore.setCertificateEntry("certificate", certificate);

        OutputStream out = new FileOutputStream(new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE));
        keyStore.store(out, PASSWORD);
        out.close();

        return keyStore;
    }

    public KeyStore getCertificate() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

        InputStream inputStream = new FileInputStream(new File(pathManager.getSystemRoot() + "/" + PathManager.CERTIFICATE));
        keyStore.load(inputStream, PASSWORD);
        inputStream.close();

        return keyStore;
    }
}
