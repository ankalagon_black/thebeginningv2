package com.example.thebeginningv2.repository.managers.xml_managers;

import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.Element;
import com.example.thebeginningv2.repository.data_types.xml.DataModeAdditionalData;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 24.06.2017.
 */

public class DataModeCreator extends XmlCreator<DataModeRoot>{

    @Override
    protected void createRoot(XmlSerializer serializer, DataModeRoot root) throws IOException {
        serializer.startTag(null, DataModeRoot.ROOT);

        createAdditionalData(serializer, root.getAdditionalData());
        createElements(serializer, DataModeRoot.MAIN_DATA, root.getMainData());
        createSystemData(serializer, root.getSystemData());
        createElements(serializer, DataModeRoot.PAKETS_DATA, root.getPaketsData());

        serializer.endTag(null, DataModeRoot.ROOT);
    }

    private void createAdditionalData(XmlSerializer serializer, DataModeAdditionalData additionalData) throws IOException {
        serializer.startTag(null, DataModeRoot.ADDITIONAL_DATA);

        List<Element> elements = new ArrayList<>();
        elements.add(new Element(DataModeAdditionalData.DB_PATTERN_FILE, additionalData.getDbPatternFile()));
        elements.add(new Element(DataModeAdditionalData.DIVIDE_BY_DATES, additionalData.getDivideByDates()));
        elements.add(new Element(DataModeAdditionalData.TIME_OF_SHIFT, additionalData.getTimeOfShift()));
        elements.add(new Element(DataModeAdditionalData.REWRITE_CATALOG_MODE, additionalData.getRewriteCatalogMode()));

        for (Element i: elements) createElement(serializer, i);

        serializer.endTag(null, DataModeRoot.ADDITIONAL_DATA);
    }

    private void createSystemData(XmlSerializer serializer, DataModeSystemData systemData) throws IOException {
        serializer.startTag(null, DataModeRoot.SYSTEM_DATA);

        List<Element> elements = new ArrayList<>();
        elements.add(new Element(DataModeSystemData.STATE, systemData.getState()));
        elements.add(new Element(DataModeSystemData.MODE, systemData.getMode()));
        elements.add(new Element(DataModeSystemData.CATALOG_OF_CREATION, systemData.getCatalogOfCreation()));
        elements.add(new Element(DataModeSystemData.DATE_OF_CREATION, systemData.getDateOfCreation()));
        elements.add(new Element(DataModeSystemData.CATALOG_OF_SYNC, systemData.getCatalogOfSync()));
        elements.add(new Element(DataModeSystemData.DATE_OF_SYNC, systemData.getDateOfSync()));
        for (Element i: elements) createElement(serializer, i);

        serializer.endTag(null, DataModeRoot.SYSTEM_DATA);
    }

    private void createElements(XmlSerializer serializer, String tag, List<Element> elements) throws IOException {
        serializer.startTag(null, tag);

        for (Element i: elements) createElement(serializer, i);

        serializer.endTag(null, tag);
    }

    private void createElement(XmlSerializer serializer, Element element) throws IOException {
        serializer.startTag(null, element.getTag());
        serializer.text(element.getValue());
        serializer.endTag(null, element.getTag());
    }
}
