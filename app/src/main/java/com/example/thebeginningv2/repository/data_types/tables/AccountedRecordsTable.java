package com.example.thebeginningv2.repository.data_types.tables;

/**
 * Created by apple on 30.06.17.
 */

public class AccountedRecordsTable {
    public static final String TABLE_NAME = "AccountedRecords";

    public static final String ID = "id";
    public static final String NAME = "Name";
    public static final String QUANTITY = "Quantity";
    public static final String GPS_MARK = "GpsMark";
    public static final String TIMESTAMP_MARK = "TimeStampMark";
}
