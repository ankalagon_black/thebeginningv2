package com.example.thebeginningv2.repository.managers.database_managers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thebeginningv2.inventories.data_types.Result;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnInfo;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.repository.data_types.Columns;
import com.example.thebeginningv2.repository.data_types.tables.AccountedRecordsTable;
import com.example.thebeginningv2.repository.data_types.tables.AccountingDataTable;
import com.example.thebeginningv2.repository.data_types.tables.ColumnsTable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class DatabaseManager {
    public static final String COUNT_RES = "countRes";
    public static final String SUM_RES = "sumRes";

    private SQLiteDatabase db;

    private Columns columns;

    public DatabaseManager(SQLiteDatabase db){
        this.db = db;
        if(hasColumns()) initializeColumns();
    }

    private boolean hasColumns(){
        Cursor cursor = db.rawQuery("PRAGMA table_info(" + ColumnsTable.TABLE_NAME + ")", null);
        boolean result = cursor.moveToFirst();

        cursor.close();

        return result;
    }

    private void initializeColumns(){
        Cursor cursor =  db.query(ColumnsTable.TABLE_NAME, null, null, null, null, null, ColumnsTable.SEMANTICS_COLUMN);

        String barcodeColumn;
        List<String> searchColumns = new ArrayList<>();
        String quantityColumn = null;
        List<String> listColumns = new ArrayList<>(), resultColumns = new ArrayList<>();
        String groupColumn = null;
        List<String> groupBarcodeColumns = new ArrayList<>(), groupRFIDColumns = new ArrayList<>();

        cursor.moveToFirst();

        int count = cursor.getCount();

        int nameColumn = cursor.getColumnIndex(ColumnsTable.NAME_COLUMN),
                semanticsColumn = cursor.getColumnIndex(ColumnsTable.SEMANTICS_COLUMN),
                asListColumn = cursor.getColumnIndex(ColumnsTable.AS_LIST_COLUMN),
                asResultColumn = cursor.getColumnIndex(ColumnsTable.AS_RESULT_COLUMN);

        barcodeColumn = cursor.getString(nameColumn);

        for (int i = 1; i < count; i++){
            cursor.moveToNext();

            int semantics = cursor.getInt(semanticsColumn);

            if(semantics != ColumnsTable.NONE){
                if(semantics == ColumnsTable.QUANTITY) quantityColumn = cursor.getString(nameColumn);
                else if(semantics == ColumnsTable.GROUP) groupColumn = cursor.getString(nameColumn);
                else if(semantics == ColumnsTable.GROUP_BARCODE) groupBarcodeColumns.add(cursor.getString(nameColumn));
                else if(semantics == ColumnsTable.GROUP_RFID) groupRFIDColumns.add(cursor.getString(nameColumn));
                else searchColumns.add(cursor.getString(nameColumn));
            }

            if(cursor.getInt(asListColumn) == 1) listColumns.add(cursor.getString(nameColumn));
            if(cursor.getInt(asResultColumn) == 1) resultColumns.add(cursor.getString(nameColumn));
        }

        cursor.close();

        columns = new Columns(barcodeColumn, searchColumns, quantityColumn,
                listColumns, resultColumns, groupColumn, groupBarcodeColumns, groupRFIDColumns);
    }

    public void close(){
        db.close();
    }

    public int getAccountedRecordsCount(){
        Cursor cursor = db.rawQuery("SELECT COUNT(" + AccountedRecordsTable.ID + ") AS " + COUNT_RES
                + " FROM " + AccountedRecordsTable.TABLE_NAME, null);

        cursor.moveToFirst();
        int accountedRecordsCount = cursor.getInt(cursor.getColumnIndex(DatabaseManager.COUNT_RES));
        cursor.close();

        return accountedRecordsCount;
    }

    public int getRemainedRecordsCount(){
        Cursor cursor =  db.rawQuery("SELECT COUNT(" + AccountingDataTable.ID + ") AS " + COUNT_RES + " FROM " + AccountingDataTable.TABLE_NAME
                + " WHERE " + AccountingDataTable.IS_ACCOUNTED + " = ?", new String[] {"0"});

        cursor.moveToFirst();
        int remainedRecordsCount = cursor.getInt(cursor.getColumnIndex(DatabaseManager.COUNT_RES));
        cursor.close();

        return remainedRecordsCount;
    }

    public float getAccountedRecordSum(){
        Cursor cursor = db.rawQuery("SELECT SUM(" + AccountedRecordsTable.QUANTITY + ") AS "
                + SUM_RES + " FROM " + AccountedRecordsTable.TABLE_NAME, null);
        cursor.moveToFirst();

        float sum = cursor.getFloat(cursor.getColumnIndex(DatabaseManager.SUM_RES));

        cursor.close();

        return sum;
    }

    public float getStatedRecordsSum(){
        Cursor cursor =  db.rawQuery("SELECT SUM(" + columns.getQuantityColumn() + ") AS "
                + SUM_RES + " FROM " + AccountingDataTable.TABLE_NAME, null);
        cursor.moveToFirst();

        float sum = cursor.getFloat(cursor.getColumnIndex(DatabaseManager.SUM_RES));

        cursor.close();

        return sum;
    }

    public float getResidueQuantity(){
        String quantityColumn = columns.getQuantityColumn();
        Cursor cursor = db.rawQuery("SELECT SUM(" + quantityColumn + " - " + AccountedRecordsTable.QUANTITY
                + ") AS " + SUM_RES + " FROM " + AccountedRecordsTable.TABLE_NAME + " a" + " INNER JOIN "
                + AccountingDataTable.TABLE_NAME + " b" + " ON " + "a." + AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID +
                " WHERE " +  quantityColumn + " > " + AccountedRecordsTable.QUANTITY, null);

        cursor.moveToFirst();

        float sum = cursor.getFloat(cursor.getColumnIndex(DatabaseManager.SUM_RES));

        cursor.close();

        return sum;
    }

    public float getSurplusQuantity(){
        String quantityColumn = columns.getQuantityColumn();
        Cursor cursor = db.rawQuery("SELECT SUM(" + AccountedRecordsTable.QUANTITY + " - " + quantityColumn
                + ") AS " + SUM_RES + " FROM " + AccountedRecordsTable.TABLE_NAME + " a" + " INNER JOIN "
                + AccountingDataTable.TABLE_NAME + " b" + " ON " + "a." + AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID +
                " WHERE " + AccountedRecordsTable.QUANTITY + " > " + quantityColumn , null);
        cursor.moveToFirst();
        float sum = cursor.getFloat(cursor.getColumnIndex(DatabaseManager.SUM_RES));
        cursor.close();

        return sum;
    }

    public float getUnidentifiedRecordsSum(){
        Cursor cursor =  db.rawQuery("SELECT SUM(" + AccountedRecordsTable.QUANTITY + ") AS "
                + SUM_RES + " FROM " + AccountedRecordsTable.TABLE_NAME + " WHERE "
                + AccountedRecordsTable.ID + " = ?", new String[]{ String.valueOf(AccountedRecord.NO_ID) });
        cursor.moveToFirst();

        float sum = cursor.getFloat(cursor.getColumnIndex(DatabaseManager.SUM_RES));

        cursor.close();

        return sum;
    }


    private List<Record> retrieveIdentifiedRecords(Cursor cursor){
        String barcodeColumn = columns.getBarcodeColumn();
        String quantityColumn = columns.getQuantityColumn();
        List<String> listColumns = columns.getListColumns();
        String groupColumn = columns.getGroupColumn();

        int count = cursor.getCount();
        List<Record> records = new ArrayList<>(count);

        cursor.moveToFirst();

        int idIndex = cursor.getColumnIndex(AccountingDataTable.ID),
                barcodeIndex = cursor.getColumnIndex(barcodeColumn);

        List<Integer> subtitleIndices = new ArrayList<>(listColumns.size());
        for (String i: listColumns) subtitleIndices.add(cursor.getColumnIndex(i));

        int accQuantityIndex = cursor.getColumnIndex(AccountedRecordsTable.QUANTITY),
                stQuantityIndex = cursor.getColumnIndex(quantityColumn);

        int isAccountedIndex = cursor.getColumnIndex(AccountingDataTable.IS_ACCOUNTED);

        int groupIndex = -1;
        if(columns.hasGroupColumn()) groupIndex = cursor.getColumnIndex(groupColumn);
        int gpsIndex = cursor.getColumnIndex(AccountedRecordsTable.GPS_MARK);

        for (int i = 0; i < count; i++){
            int id = cursor.getInt(idIndex);
            if(id != AccountedRecord.NO_ID){
                int isAccounted = cursor.getInt(isAccountedIndex);

                if(isAccounted == 1)
                    records.add(retrieveIdentifiedAccountedRecord(cursor, idIndex, barcodeIndex,
                            subtitleIndices, accQuantityIndex, stQuantityIndex, groupIndex, gpsIndex));
                else
                    records.add(retrieveRemainedRecord(cursor, idIndex, barcodeIndex, subtitleIndices, stQuantityIndex, groupIndex));
            }
            else records.add(retrieveUnidentifiedAccountedRecord(cursor, barcodeIndex, accQuantityIndex, gpsIndex));

            cursor.moveToNext();
        }

        return records;
    }

    private List<AccountedRecord> retrieveAccountedRecords(Cursor cursor){
        String quantityColumn = columns.getQuantityColumn();
        List<String> listColumns = columns.getListColumns();
        String groupColumn = columns.getGroupColumn();

        int count = cursor.getCount();
        List<AccountedRecord> accountedRecords = new ArrayList<>(count);

        cursor.moveToFirst();

        int idIndex = cursor.getColumnIndex(AccountedRecordsTable.ID),
                barcodeIndex = cursor.getColumnIndex(AccountedRecordsTable.NAME);

        List<Integer> subtitleIndices = new ArrayList<>(listColumns.size());
        for (String i: listColumns) subtitleIndices.add(cursor.getColumnIndex(i));

        int accQuantityIndex = cursor.getColumnIndex(AccountedRecordsTable.QUANTITY),
                stQuantityIndex = cursor.getColumnIndex(quantityColumn);

        int groupIndex = -1;
        if(columns.hasGroupColumn()) groupIndex = cursor.getColumnIndex(groupColumn);
        int gpsIndex = cursor.getColumnIndex(AccountedRecordsTable.GPS_MARK);

        for (int i = 0; i < count; i++){
            int id = cursor.getInt(idIndex);

            accountedRecords.add(id != AccountedRecord.NO_ID ?
                    retrieveIdentifiedAccountedRecord(cursor, idIndex, barcodeIndex, subtitleIndices, accQuantityIndex, stQuantityIndex, groupIndex, gpsIndex) :
                    retrieveUnidentifiedAccountedRecord(cursor, barcodeIndex, accQuantityIndex, gpsIndex));

            cursor.moveToNext();
        }

        return accountedRecords;
    }

    private List<AccountedRecord> retrieveUnidentifiedAccountedRecords(Cursor cursor){
        int count = cursor.getCount();
        List<AccountedRecord> accountedRecords = new ArrayList<>(count);

        cursor.moveToFirst();

        int barcodeIndex = cursor.getColumnIndex(AccountedRecordsTable.NAME),
                accQuantityIndex = cursor.getColumnIndex(AccountedRecordsTable.QUANTITY);
        int gpsIndex = cursor.getColumnIndex(AccountedRecordsTable.GPS_MARK);

        for (int i = 0; i < count; i++){
            accountedRecords.add(retrieveUnidentifiedAccountedRecord(cursor, barcodeIndex, accQuantityIndex, gpsIndex));

            cursor.moveToNext();
        }

        return accountedRecords;
    }

    private List<RemainedRecord> retrieveRemainedRecords(Cursor cursor){
        String barcodeString = columns.getBarcodeColumn();
        String quantityColumn = columns.getQuantityColumn();
        List<String> listColumns = columns.getListColumns();
        String groupColumn = columns.getGroupColumn();

        int count = cursor.getCount();
        List<RemainedRecord> remainedRecords = new ArrayList<>(count);

        cursor.moveToFirst();

        int idIndex = cursor.getColumnIndex(AccountingDataTable.ID),
                barcodeIndex = cursor.getColumnIndex(barcodeString);

        List<Integer> subtitleIndices = new ArrayList<>(listColumns.size());
        for (String i: listColumns){
            subtitleIndices.add(cursor.getColumnIndex(i));
        }

        int stQuantityIndex = cursor.getColumnIndex(quantityColumn);

        int groupIndex = -1;
        if(columns.hasGroupColumn()) groupIndex = cursor.getColumnIndex(groupColumn);

        for (int i = 0; i < count; i++) {
            remainedRecords.add(retrieveRemainedRecord(cursor, idIndex, barcodeIndex, subtitleIndices, stQuantityIndex, groupIndex));
            cursor.moveToNext();
        }

        return remainedRecords;
    }

    private AccountedRecord retrieveIdentifiedAccountedRecord(Cursor cursor, int idIndex, int barcodeIndex, List<Integer> subtitleIndices,
                                                    int accQuantityIndex, int stQuantityIndex, int groupIndex, int gpsIndex){
        int id;
        id = cursor.getInt(idIndex);

        String barcode, subtitle, gpsMark;
        float accountedQuantity, statedQuantity;
        boolean isGroup = false;

        barcode = cursor.getString(barcodeIndex);
        accountedQuantity = cursor.getFloat(accQuantityIndex);
        subtitle = "";

        for (Integer j : subtitleIndices) {
            subtitle += cursor.getString(j) + " ";
        }

        statedQuantity = cursor.getFloat(stQuantityIndex);

        if (columns.hasGroupColumn()) isGroup = cursor.getInt(groupIndex) == 1;

        gpsMark = cursor.getString(gpsIndex);

        return new AccountedRecord(id, barcode, subtitle, accountedQuantity, statedQuantity, isGroup, gpsMark);
    }

    private AccountedRecord retrieveUnidentifiedAccountedRecord(Cursor cursor, int barcodeIndex, int accQuantityIndex, int gpsIndex){
        String barcode = cursor.getString(barcodeIndex), gpsMark = cursor.getString(gpsIndex);
        float quantity = cursor.getFloat(accQuantityIndex);

        return new AccountedRecord(barcode, quantity, gpsMark);
    }

    private RemainedRecord retrieveRemainedRecord(Cursor cursor, int idIndex, int barcodeIndex,
                                                  List<Integer> subtitleIndices, int stQuantityIndex,
                                                  int groupIndex){
        int id;
        String barcode, subtitle = "";
        float statedQuantity;
        boolean isGroup = false;

        id = cursor.getInt(idIndex);
        barcode = cursor.getString(barcodeIndex);

        for (Integer j: subtitleIndices){
            subtitle += cursor.getString(j) + " ";
        }

        statedQuantity = cursor.getFloat(stQuantityIndex);

        if(columns.hasGroupColumn()) isGroup = cursor.getInt(groupIndex) == 1;



        return new RemainedRecord(id, barcode, subtitle, statedQuantity, isGroup);
    }


    public List<AccountedRecord> getAccountedRecords(){
        List<String> columns = new ArrayList<>();
        columns.add("a." + AccountedRecordsTable.ID);
        columns.add(AccountedRecordsTable.NAME);
        columns.add(AccountedRecordsTable.QUANTITY);
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());
        columns.add(AccountedRecordsTable.GPS_MARK);

        if(this.columns.hasGroupColumn()) columns.add(this.columns.getGroupColumn());

        Cursor cursor =  db.query(AccountedRecordsTable.TABLE_NAME + " a LEFT JOIN " + AccountingDataTable.TABLE_NAME + " b ON a." +
                AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID,
                columns.toArray(new String[columns.size()]),
                null,
                null,
                null,
                null,
                "a." + AccountedRecordsTable.TIMESTAMP_MARK + " DESC");

        List<AccountedRecord> accountedRecords = cursor.getCount() != 0 ? retrieveAccountedRecords(cursor) : new ArrayList<AccountedRecord>();

        cursor.close();

        return accountedRecords;
    }

    public List<AccountedRecord> getAccountedRecordsAsUnidentified(){

        Cursor cursor = db.query(AccountedRecordsTable.TABLE_NAME,
                new String[] { AccountedRecordsTable.NAME, AccountedRecordsTable.QUANTITY, AccountedRecordsTable.GPS_MARK },
                null,
                null,
                null,
                null,
                AccountedRecordsTable.TIMESTAMP_MARK);

        List<AccountedRecord> accountedRecords = cursor.getCount() != 0 ? retrieveUnidentifiedAccountedRecords(cursor) : new ArrayList<AccountedRecord>();

        cursor.close();

        return accountedRecords;
    }

    public List<RemainedRecord> getRemainedRecords(){
        List<String> columns = new ArrayList<>();
        columns.add(AccountingDataTable.ID);
        columns.add(this.columns.getBarcodeColumn());
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());

        if(this.columns.hasGroupColumn()) columns.add(this.columns.getGroupColumn());

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                columns.toArray(new String[columns.size()]),
                AccountingDataTable.IS_ACCOUNTED + " = ? ",
                new String[] { "0" },
                null,
                null,
                AccountingDataTable.TIMESTAMP_MARK + " DESC");

        List<RemainedRecord> remainedRecords = cursor.getCount() != 0 ? retrieveRemainedRecords(cursor) : new ArrayList<RemainedRecord>();

        cursor.close();

        return remainedRecords;
    }


    public void updateAccountedRecordId(int id, String barcode, int newId){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AccountedRecordsTable.ID, newId);
        contentValues.put(AccountedRecordsTable.TIMESTAMP_MARK, Calendar.getInstance().getTimeInMillis());

        String where;
        String[] whereArgs;

        if(newId != AccountedRecord.NO_ID){
            where = AccountedRecordsTable.ID + " = ?";
            whereArgs = new String[] { String.valueOf(id) };
        }
        else {
            where = AccountedRecordsTable.NAME + " = ?";
            whereArgs = new String[] { barcode };
        }

        db.update(AccountedRecordsTable.TABLE_NAME, contentValues, where, whereArgs);
    }

    public void updateAccountedRecordIdWithoutTimestamp(int id, String barcode, int newId){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AccountedRecordsTable.ID, newId);

        String where;
        String[] whereArgs;

        if(newId != AccountedRecord.NO_ID){
            where = AccountedRecordsTable.ID + " = ?";
            whereArgs = new String[] { String.valueOf(id) };
        }
        else {
            where = AccountedRecordsTable.NAME + " = ?";
            whereArgs = new String[] { barcode };
        }

        db.update(AccountedRecordsTable.TABLE_NAME, contentValues, where, whereArgs);
    }


    private void markAs(int id, boolean isAccounted){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AccountingDataTable.IS_ACCOUNTED, isAccounted ? 1 : 0);
        contentValues.put(AccountingDataTable.TIMESTAMP_MARK, Calendar.getInstance().getTimeInMillis());

        db.update(AccountingDataTable.TABLE_NAME, contentValues, AccountingDataTable.ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    public void markAsWithoutTimestamp(int id, boolean isAccounted){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AccountingDataTable.IS_ACCOUNTED, isAccounted ? 1 : 0);

        db.update(AccountingDataTable.TABLE_NAME, contentValues, AccountingDataTable.ID + " = ?",
                new String[] { String.valueOf(id) });
    }


    public void addToAccountedRecords(int id, String barcode, float quantity, String gpsMark){
        ContentValues contentValues = new ContentValues();

        contentValues.put(AccountedRecordsTable.ID, id);
        contentValues.put(AccountedRecordsTable.NAME, barcode);
        contentValues.put(AccountedRecordsTable.QUANTITY, quantity);
        contentValues.put(AccountedRecordsTable.TIMESTAMP_MARK, Calendar.getInstance().getTimeInMillis());
        if(gpsMark != null) contentValues.put(AccountedRecordsTable.GPS_MARK, gpsMark);

        db.insert(AccountedRecordsTable.TABLE_NAME, null, contentValues);
        if(id != AccountedRecord.NO_ID) markAsWithoutTimestamp(id, true);
    }

    public void updateAccountedRecord(int id, String barcode, float quantity){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AccountedRecordsTable.QUANTITY, quantity);
        contentValues.put(AccountedRecordsTable.TIMESTAMP_MARK, Calendar.getInstance().getTimeInMillis());

        String where;
        String[] whereArgs;

        if(id != AccountedRecord.NO_ID){
            where = AccountedRecordsTable.ID + " = ?";
            whereArgs = new String[] { String.valueOf(id) };
        }
        else {
            where = AccountedRecordsTable.NAME + " = ?";
            whereArgs = new String[] { barcode };
        }

        db.update(AccountedRecordsTable.TABLE_NAME, contentValues, where, whereArgs);
    }

    public void deleteFromAccountedRecords(int id, String barcode){
        String where;
        String[] whereArgs;

        if(id != AccountedRecord.NO_ID){
            where = AccountedRecordsTable.ID + " = ? ";
            whereArgs = new String[] { String.valueOf(id) };

            markAs(id, false);
        }
        else {
            where = AccountedRecordsTable.NAME + " = ?";
            whereArgs = new String[] { barcode };
        }

        db.delete(AccountedRecordsTable.TABLE_NAME, where, whereArgs);
    }

    public void deleteAllAccountedRecords(List<Integer> identifiedIds){
        for (Integer i: identifiedIds) markAs(i, false);

        db.delete(AccountedRecordsTable.TABLE_NAME, null, null);
    }


    public void deleteAllAccountedRecords(){
        db.delete(AccountedRecordsTable.TABLE_NAME, null, null);
    }


    public List<Integer> findIdentifiedRecordIds(String keyWord, boolean remainedOnly, String limit){
        List<String> searchColumns = this.columns.getSearchColumns();

        String selection = "";
        int size = searchColumns.size();
        List<String> selectionArgs = new ArrayList<>(size);

        if(remainedOnly){
            selection += AccountingDataTable.IS_ACCOUNTED + " = ? AND";
            selectionArgs.add("0");
        }

        selection += "(" + searchColumns.get(0) + " = ?";
        selectionArgs.add(keyWord);

        for (int i = 1; i < size; i++) {
            selection += " OR " + searchColumns.get(i) + " = ?";
            selectionArgs.add(keyWord);
        }

        selection += ")";

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                new String[] { AccountingDataTable.ID },
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                AccountingDataTable.TIMESTAMP_MARK + " DESC",
                limit);
        cursor.moveToFirst();

        int count = cursor.getCount();
        List<Integer> ids = new ArrayList<>(count);

        if(count > 0){
            int idIndex = cursor.getColumnIndex(AccountingDataTable.ID);
            for (int i = 0; i < count; i++){
                ids.add(cursor.getInt(idIndex));
                cursor.moveToNext();
            }
        }

        cursor.close();

        return ids;
    }

    public List<Record> findIdentifiedRecords(String keyWord, boolean useLike){
        List<String> columns = new ArrayList<>();

        String prefix = "a.";

        columns.add(prefix + AccountingDataTable.ID);
        columns.add(this.columns.getBarcodeColumn());
        columns.add(AccountedRecordsTable.QUANTITY);
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());
        columns.add(AccountingDataTable.IS_ACCOUNTED);
        columns.add(AccountedRecordsTable.GPS_MARK);

        if(this.columns.hasGroupColumn()) columns.add(this.columns.getGroupColumn());

        List<String> searchColumns = this.columns.getSearchColumns();

        String selection = "";
        int size = searchColumns.size();
        List<String> selectionArgs = new ArrayList<>(size);

        if(!useLike) {
            selection += searchColumns.get(0) + " = ?";
            selectionArgs.add(keyWord);

            for (int i = 1; i < size; i++) {
                selection += " OR " + searchColumns.get(i) + " = ?";
                selectionArgs.add(keyWord);
            }
        }
        else {
            selection += searchColumns.get(0) + " LIKE ?";
            selectionArgs.add("%" + keyWord + "%");

            for (int i = 1; i < size; i++){
                selection += " OR " + searchColumns.get(i) + " LIKE ?";
                selectionArgs.add("%" + keyWord + "%");
            }
        }

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME + " a LEFT JOIN " + AccountedRecordsTable.TABLE_NAME  + " b ON a." +
                        AccountingDataTable.ID + " = b." + AccountedRecordsTable.ID,
                columns.toArray(new String[columns.size()]),
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                "a." + AccountingDataTable.TIMESTAMP_MARK + " DESC");

        List<Record> identifiedRecords = cursor.getCount() != 0 ? retrieveIdentifiedRecords(cursor) : new ArrayList<Record>();

        cursor.close();

        return identifiedRecords;
    }

    public List<AccountedRecord> findUnidentifiedRecords(String keyWord, boolean useLike){

        String selection = AccountedRecordsTable.ID + " = ? AND ";
        String[] selectionArgs = new String[2];

        selectionArgs[0] = String.valueOf(AccountedRecord.NO_ID);

        if(!useLike){
            selection += AccountedRecordsTable.NAME + " = ?";
            selectionArgs[1] = keyWord;
        }
        else {
            selection += AccountedRecordsTable.NAME + " LIKE ?";
            selectionArgs[1] = "%" + keyWord + "%";
        }

        Cursor cursor = db.query(AccountedRecordsTable.TABLE_NAME,
                new String[] { AccountedRecordsTable.NAME,  AccountedRecordsTable.QUANTITY, AccountedRecordsTable.GPS_MARK},
                selection,
                selectionArgs, null, null,
                AccountedRecordsTable.TIMESTAMP_MARK);

        List<AccountedRecord> accountedRecords = cursor.getCount() != 0 ? retrieveUnidentifiedAccountedRecords(cursor) : new ArrayList<AccountedRecord>();

        cursor.close();

        return accountedRecords;
    }


    public List<ColumnValue> getGroupBarcodes(int id){
        List<String> groupBarcodeColumns = columns.getGroupBarcodeColumns();

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                groupBarcodeColumns.toArray(new String[groupBarcodeColumns.size()]),
                AccountingDataTable.ID + " = ? ",
                new String[] { String.valueOf(id) },
                null,
                null,
                null);
        cursor.moveToFirst();

        List<ColumnValue> groupBarcodes = new ArrayList<>();

        for (String i: groupBarcodeColumns){
            String groupBarcode = cursor.getString(cursor.getColumnIndex(i));
            if(groupBarcode != null && groupBarcode.replace(" ", "").length() != 0) groupBarcodes.add(new ColumnValue(i, groupBarcode));
        }

        cursor.close();

        return groupBarcodes;
    }

    public List<AccountedRecord> findGroupAccountedRecords(List<ColumnValue> groupBarcodes){
        List<String> columns = new ArrayList<>();
        columns.add("a." + AccountedRecordsTable.ID);
        columns.add(AccountedRecordsTable.NAME);
        columns.add(AccountedRecordsTable.QUANTITY);
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());
        columns.add(this.columns.getGroupColumn());
        columns.add(AccountedRecordsTable.GPS_MARK);

        int size = groupBarcodes.size();

        String selection = this.columns.getGroupColumn() + " = ? AND (";
        List<String> selectionArgs = new ArrayList<>(size);
        selectionArgs.add("1");

        for (int i = 0; i < size; i++){
            ColumnValue columnValue = groupBarcodes.get(i);
            if(i != 0){
                selection += " OR " + columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
            else {
                selection += columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
        }

        selection += ")";

        Cursor cursor =  db.query(AccountedRecordsTable.TABLE_NAME + " a INNER JOIN " + AccountingDataTable.TABLE_NAME + " b ON a." +
                        AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID,
                columns.toArray(new String[columns.size()]),
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                "a." + AccountedRecordsTable.TIMESTAMP_MARK + " DESC");

        List<AccountedRecord> accountedRecords = cursor.getCount() != 0 ? retrieveAccountedRecords(cursor) : new ArrayList<AccountedRecord>();

        cursor.close();

        return accountedRecords;
    }

    public List<RemainedRecord> findGroupRemainedRecords(List<ColumnValue> groupBarcodes){
        List<String> columns = new ArrayList<>();
        columns.add(AccountingDataTable.ID);
        columns.add(this.columns.getBarcodeColumn());
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());

        columns.add(this.columns.getGroupColumn());

        int size = groupBarcodes.size();

        String selection = AccountingDataTable.IS_ACCOUNTED + " = ? AND " + this.columns.getGroupColumn() + " = ? AND (";
        List<String> selectionArgs = new ArrayList<>(size);
        selectionArgs.add("0");
        selectionArgs.add("1");

        for (int i = 0; i < size; i++){
            ColumnValue columnValue = groupBarcodes.get(i);
            if(i != 0){
                selection += " OR " + columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
            else {
                selection += columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
        }

        selection += ")";

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                columns.toArray(new String[columns.size()]),
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                AccountingDataTable.TIMESTAMP_MARK + " DESC");

        List<RemainedRecord> remainedRecords = cursor.getCount() != 0 ? retrieveRemainedRecords(cursor) : new ArrayList<RemainedRecord>();

        cursor.close();

        return remainedRecords;
    }

    public List<Record> findGroupRecords(List<ColumnValue> groupBarcodes){
        List<String> columns = new ArrayList<>();

        String prefix = "a.";

        columns.add(prefix + AccountingDataTable.ID);
        columns.add(this.columns.getBarcodeColumn());
        columns.add(AccountedRecordsTable.QUANTITY);
        columns.add(this.columns.getQuantityColumn());
        columns.addAll(this.columns.getListColumns());
        columns.add(AccountingDataTable.IS_ACCOUNTED);
        columns.add(this.columns.getGroupColumn());
        columns.add(AccountedRecordsTable.GPS_MARK);

        int size = groupBarcodes.size();

        String selection = this.columns.getGroupColumn() + " = ? AND (";
        List<String> selectionArgs = new ArrayList<>(size);
        selectionArgs.add("1");

        for (int i = 0; i < size; i++){
            ColumnValue columnValue = groupBarcodes.get(i);
            if(i != 0){
                selection += " OR " + columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
            else {
                selection += columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
        }

        selection += ")";


        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME + " a LEFT JOIN " + AccountedRecordsTable.TABLE_NAME  + " b ON a." +
                        AccountingDataTable.ID + " = b." + AccountedRecordsTable.ID,
                columns.toArray(new String[columns.size()]),
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                "a." + AccountingDataTable.TIMESTAMP_MARK + " DESC");

        List<Record> identifiedRecords = cursor.getCount() != 0 ? retrieveIdentifiedRecords(cursor) : new ArrayList<Record>();

        cursor.close();

        return identifiedRecords;
    }

    public boolean hasGroupAccountedRecords(List<ColumnValue> groupBarcodes){
        int size = groupBarcodes.size();

        String selection = this.columns.getGroupColumn() + " = ? AND (";
        List<String> selectionArgs = new ArrayList<>(size);
        selectionArgs.add("1");

        for (int i = 0; i < size; i++){
            ColumnValue columnValue = groupBarcodes.get(i);
            if(i != 0){
                selection += " OR " + columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
            else {
                selection += columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
        }

        selection += ")";

        Cursor cursor =  db.query(AccountedRecordsTable.TABLE_NAME + " a INNER JOIN " + AccountingDataTable.TABLE_NAME + " b ON a." +
                        AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID,
                new String[] { "COUNT(a." + AccountedRecordsTable.ID + ")" + " AS " + COUNT_RES },
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                null,
                null);

        cursor.moveToFirst();

        int count = cursor.getInt(cursor.getColumnIndex(COUNT_RES));

        cursor.close();

        return count != 0;
    }

    public boolean hasGroupRemainedRecords(List<ColumnValue> groupBarcodes){

        int size = groupBarcodes.size();

        String selection = AccountingDataTable.IS_ACCOUNTED + " = ? AND " + this.columns.getGroupColumn() + " = ? AND (";
        List<String> selectionArgs = new ArrayList<>(size);
        selectionArgs.add("0");
        selectionArgs.add("1");

        for (int i = 0; i < size; i++){
            ColumnValue columnValue = groupBarcodes.get(i);
            if(i != 0){
                selection += " OR " + columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
            else {
                selection += columnValue.getColumn() + " = ? ";
                selectionArgs.add(columnValue.getValue());
            }
        }

        selection += ")";

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                new String[] { "COUNT(" + AccountingDataTable.ID + ")" + " AS " + COUNT_RES },
                selection,
                selectionArgs.toArray(new String[selectionArgs.size()]),
                null,
                null,
                null);

        cursor.moveToFirst();

        int count = cursor.getInt(cursor.getColumnIndex(COUNT_RES));

        cursor.close();

        return count != 0;
    }


    public boolean isAccounted(int id){
        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                new String[] { AccountingDataTable.IS_ACCOUNTED },
                AccountingDataTable.ID + " = ? ",
                new String[] { String.valueOf(id) },
                null,
                null,
                null);

        cursor.moveToFirst();
        boolean isAccounted = cursor.getInt(cursor.getColumnIndex(AccountingDataTable.IS_ACCOUNTED)) == 1;
        cursor.close();

        return isAccounted;
    }

    public AccountedRecord getIdentifiedAccountedRecord(int id){
        String quantityColumn = columns.getQuantityColumn();
        List<String> listColumns = columns.getListColumns();
        String groupColumn = columns.getGroupColumn();

        List<String> columns = new ArrayList<>();
        columns.add(AccountedRecordsTable.NAME);
        columns.add(AccountedRecordsTable.QUANTITY);
        columns.add(quantityColumn);
        columns.addAll(listColumns);
        columns.add(AccountedRecordsTable.GPS_MARK);

        if(this.columns.hasGroupColumn()) columns.add(groupColumn);

        Cursor cursor = db.query(AccountedRecordsTable.TABLE_NAME + " a LEFT JOIN " + AccountingDataTable.TABLE_NAME + " b ON a." +
                AccountedRecordsTable.ID + " = b." + AccountingDataTable.ID,
                columns.toArray(new String[columns.size()]),
                " b." + AccountingDataTable.ID + " = ?",
                new String[] { String.valueOf(id) },
                null,
                null,
                null);

        cursor.moveToFirst();

        String barcode, subtitle;
        float accountedQuantity, statedQuantity;
        boolean isGroup = false;
        String gpsMark;

        barcode = cursor.getString(cursor.getColumnIndex(AccountedRecordsTable.NAME));

        subtitle = "";

        for (String i: listColumns){
            subtitle += cursor.getString(cursor.getColumnIndex(i)) + " ";
        }

        statedQuantity = cursor.getFloat(cursor.getColumnIndex(quantityColumn));

        accountedQuantity = cursor.getFloat(cursor.getColumnIndex(AccountedRecordsTable.QUANTITY));

        if(this.columns.hasGroupColumn()) isGroup = cursor.getInt(cursor.getColumnIndex(groupColumn)) == 1;

        gpsMark = cursor.getString(cursor.getColumnIndex(AccountedRecordsTable.GPS_MARK));

        AccountedRecord accountedRecord = new AccountedRecord(id, barcode, subtitle, accountedQuantity, statedQuantity, isGroup, gpsMark);

        cursor.close();

        return accountedRecord;
    }

    public RemainedRecord getRemainedRecord(int id){
        String barcodeColumn = columns.getBarcodeColumn();
        String quantityColumn = columns.getQuantityColumn();
        List<String> listColumns = columns.getListColumns();
        String groupColumn = columns.getGroupColumn();

        List<String> columns = new ArrayList<>(listColumns.size() + 2);
        columns.add(barcodeColumn);
        columns.add(quantityColumn);
        columns.addAll(listColumns);
        if(this.columns.hasGroupColumn()) columns.add(groupColumn);

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                columns.toArray(new String[columns.size()]),
                AccountingDataTable.IS_ACCOUNTED + " = ? AND " + AccountingDataTable.ID + " = ?",
                new String[] {"0", String.valueOf(id)}, null, null, null);
        cursor.moveToFirst();

        String barcode = cursor.getString(cursor.getColumnIndex(barcodeColumn));
        float quantity = cursor.getFloat(cursor.getColumnIndex(quantityColumn));
        boolean isGroup = false;

        String subtitle = "";

        for (String i: listColumns){
            subtitle += cursor.getString(cursor.getColumnIndex(i)) + " ";
        }

        if(this.columns.hasGroupColumn()) isGroup = cursor.getInt(cursor.getColumnIndex(groupColumn)) == 1;

        RemainedRecord remainedRecord = new RemainedRecord(id, barcode, subtitle, quantity, isGroup);

        cursor.close();

        return remainedRecord;
    }


    private List<String> getColumns(){
        Cursor columnsCursor = db.query(ColumnsTable.TABLE_NAME, null, null, null, null, null, null);
        int size = columnsCursor.getCount();
        List<String> columns = new ArrayList<>(size);

        columnsCursor.moveToFirst();

        int nameIndex = columnsCursor.getColumnIndex(ColumnsTable.NAME_COLUMN);

        for (int i = 0; i < size; i++){
            columns.add(columnsCursor.getString(nameIndex));
            columnsCursor.moveToNext();
        }

        columnsCursor.close();

        return columns;
    }

    public List<ColumnInfo> getColumnValues(int id){
       List<String> columns = getColumns();

        Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                columns.toArray(new String[columns.size()]),
                AccountingDataTable.ID + " = ? ",
                new String[] { String.valueOf(id) }, null, null, null);

        List<ColumnInfo> columnInfos = new ArrayList<>(columns.size());

        cursor.moveToFirst();

        for (String i: columns){
            String value = cursor.getString(cursor.getColumnIndex(i));

            String[] infoColumns = ColumnsTable.getInfoColumns();

            Cursor info = db.query(ColumnsTable.TABLE_NAME,
                    infoColumns,
                    ColumnsTable.NAME_COLUMN + " = ?",
                    new String[] { i }, null, null,
                    ColumnsTable.ORDER_COLUMN);

            info.moveToFirst();

            String title = info.getString(info.getColumnIndex(ColumnsTable.TITLE_COLUMN));
            String dataBackgroundColor = info.getString(info.getColumnIndex(ColumnsTable.DATA_BACKGROUND_COLOR_COLUMN));
            String dataTextColor = info.getString(info.getColumnIndex(ColumnsTable.DATA_TEXT_COLOR_COLUMN));
            boolean dataUseBoldFont = info.getInt(info.getColumnIndex(ColumnsTable.DATA_USE_BOLD_FONT_COLUMN)) == 1;
            float dataTextSize = info.getFloat(info.getColumnIndex(ColumnsTable.DATA_TEXT_SIZE_COLUMN));

            String titleBackgroundColor = info.getString(info.getColumnIndex(ColumnsTable.TITLE_BACKGROUND_COLOR_COLUMN));
            String titleTextColor = info.getString(info.getColumnIndex(ColumnsTable.TITLE_TEXT_COLOR_COLUMN));
            boolean titleUseBoldFont = info.getInt(info.getColumnIndex(ColumnsTable.TITLE_USE_BOLD_FONT_COLUMN)) == 1;
            float titleTextSize = info.getFloat(info.getColumnIndex(ColumnsTable.TITLE_TEXT_SIZE_COLUMN));

            info.close();

            columnInfos.add(new ColumnInfo(title, value,
                    dataBackgroundColor, dataTextColor, dataUseBoldFont, dataTextSize,
                    titleBackgroundColor, titleTextColor, titleUseBoldFont, titleTextSize));
        }

        cursor.close();

        return columnInfos;
    }

    public List<Result> getResults(){
        List<String> resultColumns = this.columns.getResultColumns();
        int size = resultColumns.size();
        if(size > 0){
            List<String> columns = new ArrayList<>(size);
            for (int i = 0; i < size; i++) columns.add("SUM(" + resultColumns.get(i) + ") AS " + SUM_RES + String.valueOf(i));

            Cursor cursor = db.query(AccountingDataTable.TABLE_NAME,
                columns.toArray(new String[columns.size()]),
                null,
                null,
                null,
                null,
                null);

            cursor.moveToFirst();

            List<Result> results = new ArrayList<>(size);
            for (int i = 0; i < size; i++){
                String column = resultColumns.get(i);
                float sum = cursor.getFloat(cursor.getColumnIndex(SUM_RES + String.valueOf(i)));
                results.add(new Result(column, sum));
            }

            cursor.close();

            return results;
        }
        return new ArrayList<>();
    }
}
