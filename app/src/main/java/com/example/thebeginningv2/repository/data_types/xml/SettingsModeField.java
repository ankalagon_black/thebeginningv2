package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by Константин on 24.06.2017.
 */

public class SettingsModeField {
    public static final String FIELD = "Field";
    public static final String TITLE = "Title";
    public static final String TAG = "Tag";
    public static final String DEFAULT_VALUE = "DefaultValue";
    public static final String PART_OF_NAME = "PartOfName";
    public static final String REQUIRED = "Required";
    public static final String EDITABLE = "Editable";
    public static final String TYPE = "Type";

    public static final String STRING = "String", INTEGER = "Integer", BOOLEAN = "Boolean", FLOAT = "Double", DATE = "Date";

    private String title, tag, defaultValue;
    private String partOfName, required, editable;
    private String type;

    public SettingsModeField(String title, String tag, String defaultValue, String partOfName, String required, String editable, String type){
        this.title = title;
        this.tag = tag;
        this.defaultValue = defaultValue;
        this.partOfName = partOfName;
        this.required = required;
        this.editable = editable;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getTag() {
        return tag;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue){
        this.defaultValue = defaultValue;
    }

    public String getPartOfName() {
        return partOfName;
    }

    public String getRequired() {
        return required;
    }

    public String getEditable() {
        return editable;
    }

    public String getType() {
        return type;
    }
}
