package com.example.thebeginningv2.repository.managers.network;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Константин on 12.07.2017.
 */

public interface GetSettingsInterface {
    @POST("{type}")
    Call<JsonObject> getSettings(@Path("type") String path, @Body RequestBody requestBody);
}
