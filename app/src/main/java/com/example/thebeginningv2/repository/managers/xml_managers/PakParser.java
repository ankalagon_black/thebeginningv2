package com.example.thebeginningv2.repository.managers.xml_managers;

import android.util.Xml;

import com.example.thebeginningv2.repository.data_types.xml.PakCommon;
import com.example.thebeginningv2.repository.data_types.xml.PakField;
import com.example.thebeginningv2.repository.data_types.xml.PakRoot;
import com.example.thebeginningv2.repository.data_types.xml.Element;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakParser extends Parser{

    public PakParser(File file) {
        super(file);
    }

    public PakRoot getPakRoot(List<String> tags) throws XmlPullParserException, IOException {
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPakRoot(parser, tags);
        } finally {
            in.close();
        }
    }

    public PakCommon getPakCommon() throws XmlPullParserException, IOException{
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPakRoot(parser);
        } finally {
            in.close();
        }
    }

    private PakRoot readPakRoot(XmlPullParser parser, List<String> tags) throws IOException, XmlPullParserException {
        PakCommon common = null;
        List<PakField> fields = null;

        parser.require(XmlPullParser.START_TAG, null, PakRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case PakRoot.COMMON:
                    common = readCommon(parser);
                    break;
                case PakRoot.FIELDS:
                    fields = readFields(parser, tags);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new PakRoot(common, fields);
    }

    private PakCommon readPakRoot(XmlPullParser parser) throws IOException, XmlPullParserException{
        PakCommon common = null;

        boolean flag = false;

        parser.require(XmlPullParser.START_TAG, null, PakRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG && !flag) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();

            if(name.equals(PakRoot.COMMON)){
                common = readCommon(parser);
                flag = true;
            }
            else skip(parser);
        }

        return common;
    }

    private PakCommon readCommon(XmlPullParser parser) throws IOException, XmlPullParserException{
        String title = null, dateOfDownload = null;

        parser.require(XmlPullParser.START_TAG, null, PakRoot.COMMON);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case PakCommon.TITLE:
                    title = readText(parser);
                    break;
                case PakCommon.DATE_OF_DOWNLOAD:
                    dateOfDownload = readText(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new PakCommon(title, dateOfDownload);
    }

    private List<PakField> readFields(XmlPullParser parser, List<String> tags) throws IOException, XmlPullParserException{
        List<PakField> fields = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, PakRoot.FIELDS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name) {
                case PakField.FIELD:
                    fields.add(readField(parser, tags));
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return fields;
    }

    private PakField readField(XmlPullParser parser, List<String> tags) throws IOException, XmlPullParserException{
        List<Element> elements = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, PakField.FIELD);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            boolean found = false;

            for (String i: tags){
                if(i.equals(name)){
                    elements.add(new Element(i, readText(parser)));
                    found = true;
                    break;
                }
            }

            if(!found) skip(parser);
        }

        return new PakField(elements);
    }
}
