package com.example.thebeginningv2.repository;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Константин on 06.08.2017.
 */

public class BarcodeScannerReceiver extends BroadcastReceiver {
    public static final String INTENT = "android.intent.ACTION_DECODE_DATA";
    public static final String EXTRA = "barcode_string";

    private ReceiveInterface receiveInterface;

    public BarcodeScannerReceiver(ReceiveInterface receiveInterface){
        this.receiveInterface = receiveInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        receiveInterface.onBarcodeScanned(intent.getStringExtra(EXTRA));
    }

    public interface ReceiveInterface{
        void onBarcodeScanned(String barcode);
    }
}
