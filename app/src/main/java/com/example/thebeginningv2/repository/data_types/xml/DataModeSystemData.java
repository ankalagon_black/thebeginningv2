package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by Константин on 24.06.2017.
 */

public class DataModeSystemData {
    public static final String STATE = "State";
    public static final String MODE = "Mode";
    public static final String CATALOG_OF_CREATION = "КаталогСоздания";
    public static final String DATE_OF_CREATION = "ДатаСоздания";
    public static final String CATALOG_OF_SYNC = "КаталогСинхронизации";
    public static final String DATE_OF_SYNC = "ДатаСинхронизации";

    public static final String IN_PROGRESS = "0", SENT = "1";
    public static final String WITHOUT_ACCOUNTING_DATA = "0", WITH_ACCOUNTING_DATA = "1";

    private String state, mode;
    private String catalogOfCreation, dateOfCreation;
    private String catalogOfSync, dateOfSync;

    public DataModeSystemData(String state, String mode, String catalogOfCreation, String dateOfCreation,
                              String catalogOfSync, String dateOfSync) {
        this.state = state;
        this.mode = mode;
        this.catalogOfCreation = catalogOfCreation;
        this.dateOfCreation = dateOfCreation;
        this.catalogOfSync = catalogOfSync;
        this.dateOfSync = dateOfSync;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMode() {
        return mode;
    }

    public void setCatalogOfSync(String catalogOfSync) {
        this.catalogOfSync = catalogOfSync;
    }

    public String getCatalogOfCreation() {
        return catalogOfCreation;
    }

    public void setDateOfSync(String dateOfSync){
        this.dateOfSync = dateOfSync;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public String getCatalogOfSync() {
        return catalogOfSync;
    }

    public String getDateOfSync() {
        return dateOfSync;
    }
}
