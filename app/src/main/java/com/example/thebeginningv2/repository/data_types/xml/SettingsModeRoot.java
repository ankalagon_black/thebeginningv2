package com.example.thebeginningv2.repository.data_types.xml;

import java.util.List;

/**
 * Created by Константин on 24.06.2017.
 */

public class SettingsModeRoot {
    public static final String ROOT = "root";
    public static final String COMMON = "Common";
    public static final String FIELDS = "Fields";
    public static final String PAKETS = "Pakets";

    private SettingsModeCommon common;
    private List<SettingsModeField> fields;
    private List<SettingsModeField> pakets;

    public SettingsModeRoot(SettingsModeCommon common, List<SettingsModeField> fields, List<SettingsModeField> pakets){
        this.common = common;
        this.fields = fields;
        this.pakets = pakets;
    }

    public SettingsModeCommon getCommon() {
        return common;
    }

    public List<SettingsModeField> getFields() {
        return fields;
    }

    public List<SettingsModeField> getPakets() {
        return pakets;
    }
}
