package com.example.thebeginningv2.repository.managers.database_managers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thebeginningv2.repository.data_types.tables.AccountedRecordsTable;
import com.example.thebeginningv2.repository.data_types.tables.AccountingDataTable;
import com.example.thebeginningv2.repository.data_types.tables.ColumnsTable;
import com.example.thebeginningv2.repository.data_types.xml.PakField;
import com.example.thebeginningv2.repository.data_types.xml.PakModeCommon;
import com.example.thebeginningv2.repository.data_types.xml.PakModeField;
import com.example.thebeginningv2.repository.data_types.xml.Element;

import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class DatabaseCreator {
    private static final String TEXT = "TEXT", INTEGER = "INTEGER", REAL = "REAL";

    private SQLiteDatabase db;

    public DatabaseCreator(String path){
        db = SQLiteDatabase.openOrCreateDatabase(path, null);
    }

    public void createColumnsTable(List<ColumnRow> rows){

        String query = "CREATE TABLE " + ColumnsTable.TABLE_NAME + "(";
        query += ColumnsTable.NAME_COLUMN + " " + TEXT + ", " +
                ColumnsTable.SEMANTICS_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.AS_LIST_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.AS_RESULT_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.TITLE_COLUMN + " " + TEXT + ", " +
                ColumnsTable.ORDER_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.DATA_BACKGROUND_COLOR_COLUMN + " " + TEXT + ", " +
                ColumnsTable.DATA_USE_BOLD_FONT_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.DATA_TEXT_COLOR_COLUMN + " " + TEXT + ", " +
                ColumnsTable.DATA_TEXT_SIZE_COLUMN + " " + REAL + ", " +
                ColumnsTable.TITLE_BACKGROUND_COLOR_COLUMN + " " + TEXT + ", " +
                ColumnsTable.TITLE_USE_BOLD_FONT_COLUMN + " " + INTEGER + ", " +
                ColumnsTable.TITLE_TEXT_COLOR_COLUMN + " " + TEXT + ", " +
                ColumnsTable.TITLE_TEXT_SIZE_COLUMN + " " + REAL + ");";

        db.execSQL(query);

        for (ColumnRow i: rows){
            ContentValues contentValues = new ContentValues();

            contentValues.put(ColumnsTable.NAME_COLUMN, i.getName() );
            contentValues.put(ColumnsTable.SEMANTICS_COLUMN, i.getSemantics());
            contentValues.put(ColumnsTable.AS_LIST_COLUMN, i.getAsList());
            contentValues.put(ColumnsTable.AS_RESULT_COLUMN, i.getAsResult());

            contentValues.put(ColumnsTable.TITLE_COLUMN, i.getTitle());
            contentValues.put(ColumnsTable.ORDER_COLUMN, i.getOrder());

            contentValues.put(ColumnsTable.DATA_BACKGROUND_COLOR_COLUMN, i.getDataBackgroundColor());
            contentValues.put(ColumnsTable.DATA_USE_BOLD_FONT_COLUMN, i.getDataUseBoldFont());
            contentValues.put(ColumnsTable.DATA_TEXT_COLOR_COLUMN, i.getDataTextColor());
            contentValues.put(ColumnsTable.DATA_TEXT_SIZE_COLUMN, i.getDataTextSize());

            contentValues.put(ColumnsTable.TITLE_BACKGROUND_COLOR_COLUMN, i.getTitleBackgroundColor());
            contentValues.put(ColumnsTable.TITLE_USE_BOLD_FONT_COLUMN, i.getTitleUseBoldFont());
            contentValues.put(ColumnsTable.TITLE_TEXT_COLOR_COLUMN, i.getTitleTextColor());
            contentValues.put(ColumnsTable.TITLE_TEXT_SIZE_COLUMN, i.getTitleTextSize());

            db.insert(ColumnsTable.TABLE_NAME, null, contentValues);
        }
    }

    public void createAccountingDataTable(List<PakModeField> fields, PakModeCommon common, List<PakField> dataFields){
        String query = "CREATE TABLE " + AccountingDataTable.TABLE_NAME + "(" + AccountingDataTable.ID +
                " " + INTEGER + " PRIMARY KEY AUTOINCREMENT";
        for (PakModeField i: fields ){
            query += ", " + i.getTag() + " " + getAffinity(i.getType());
        }

        String group = common.getGroupField();
        if(group != null) {
            query += ", " + group + " " + INTEGER;

            List<String> groupBarcode = common.getGroupBarcodeFields(), groupRFID = common.getGroupRFIDFields();
            for (String i: groupBarcode){
                query += ", " + i + " " + TEXT;
            }

            for (String i: groupRFID){
                query += ", " + i + " " + TEXT;
            }
        }

        query += ", " + AccountingDataTable.IS_ACCOUNTED + " " + INTEGER +
                ", " + AccountingDataTable.TIMESTAMP_MARK + " " + INTEGER + ");";

        db.execSQL(query);

        for (PakField i: dataFields){
            ContentValues contentValues = new ContentValues();

            List<Element> elements = i.getElements();
            for (Element j: elements){
                contentValues.put(j.getTag(), j.getValue());
            }

            contentValues.put(AccountingDataTable.IS_ACCOUNTED, 0);
            contentValues.put(AccountingDataTable.TIMESTAMP_MARK, 0);

            db.insert(AccountingDataTable.TABLE_NAME, null, contentValues);
        }
    }

    public void createAccountedRecordsTable(){
        String query = "CREATE TABLE " + AccountedRecordsTable.TABLE_NAME +
                "(" + AccountedRecordsTable.ID + " " + INTEGER + ", " +
                AccountedRecordsTable.NAME + " " + TEXT + ", " +
                AccountedRecordsTable.QUANTITY + " " + REAL + ", " +
                AccountedRecordsTable.GPS_MARK + " " + TEXT + ", " +
                AccountedRecordsTable.TIMESTAMP_MARK + " " + INTEGER + ");";
        db.execSQL(query);
    }

    public boolean tableExists(String tableName){
        Cursor cursor = db.rawQuery("PRAGMA table_info(" + tableName + ")", null);

        boolean result = cursor.getCount() > 0;

        cursor.close();

        return result;
    }

    public void deleteTable(String tableName){
        db.execSQL("DROP TABLE " + tableName);
    }

    public void close(){
        db.close();
    }

    private String getAffinity(String type){
        switch (type){
            case PakModeField.STRING:
            case PakModeField.DATE:
                return TEXT;
            case PakModeField.INTEGER:
            case PakModeField.BOOLEAN:
                return INTEGER;
            case PakModeField.FLOAT:
                return REAL;
            default:
                return TEXT;
        }
    }

    public static class ColumnRow{
        private String name;
        private int semantics;
        private int asList, asResult;

        private String title;
        private int order;

        private String dataBackgroundColor, dataTextColor;
        private int dataUseBoldFont;
        private float dataTextSize;

        private String titleBackgroundColor, titleTextColor;
        private int titleUseBoldFont;
        private float titleTextSize;

        public ColumnRow(String name,
                         int semantics, int asList, int asResult,
                         String title, int order,
                         String dataBackgroundColor, String dataTextColor, int dataUseBoldFont, float dataTextSize,
                         String titleBackgroundColor, String titleTextColor, int titleUseBoldFont, float titleTextSize){
            this.name = name;
            this.semantics = semantics;
            this.asList = asList;
            this.asResult = asResult;

            this.title = title;
            this.order = order;

            this.dataBackgroundColor = dataBackgroundColor;
            this.dataTextColor = dataTextColor;
            this.dataUseBoldFont = dataUseBoldFont;
            this.dataTextSize = dataTextSize;

            this.titleBackgroundColor = titleBackgroundColor;
            this.titleTextColor = titleTextColor;
            this.titleUseBoldFont = titleUseBoldFont;
            this.titleTextSize = titleTextSize;
        }

        public String getName() {
            return name;
        }

        public int getSemantics() {
            return semantics;
        }

        public int getAsList() {
            return asList;
        }

        public int getAsResult() {
            return asResult;
        }

        public String getTitle() {
            return title;
        }

        public int getOrder() {
            return order;
        }

        public String getDataBackgroundColor() {
            return dataBackgroundColor;
        }

        public String getDataTextColor() {
            return dataTextColor;
        }

        public int getDataUseBoldFont() {
            return dataUseBoldFont;
        }

        public float getDataTextSize() {
            return dataTextSize;
        }

        public String getTitleBackgroundColor() {
            return titleBackgroundColor;
        }

        public String getTitleTextColor() {
            return titleTextColor;
        }

        public int getTitleUseBoldFont() {
            return titleUseBoldFont;
        }

        public float getTitleTextSize() {
            return titleTextSize;
        }
    }
}
