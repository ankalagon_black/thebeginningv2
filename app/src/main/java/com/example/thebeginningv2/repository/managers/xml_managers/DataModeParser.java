package com.example.thebeginningv2.repository.managers.xml_managers;

import android.util.Xml;

import com.example.thebeginningv2.repository.data_types.xml.DataModeAdditionalData;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
import com.example.thebeginningv2.repository.data_types.xml.Element;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 04.07.2017.
 */

public class DataModeParser extends Parser {

    public DataModeParser(File file) {
        super(file);
    }

    public DataModeRoot getDataModeRoot() throws IOException, XmlPullParserException {
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readDataModeRoot(parser);
        } finally {
            in.close();
        }
    }

    public DataModeSystemData geDataModeSystemData() throws XmlPullParserException, IOException{
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readDataModeRoot_v2(parser);
        } finally {
            in.close();
        }
    }

    private DataModeRoot readDataModeRoot(XmlPullParser parser) throws IOException, XmlPullParserException {
        DataModeAdditionalData additionalData = null;
        List<Element> mainData = null;
        DataModeSystemData systemData = null;
        List<Element> paketsData = null;

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case DataModeRoot.ADDITIONAL_DATA:
                    additionalData = readAdditionalData(parser);
                    break;
                case DataModeRoot.MAIN_DATA:
                    mainData = readMainData(parser);
                    break;
                case DataModeRoot.SYSTEM_DATA:
                    systemData = readSystemData(parser);
                    break;
                case DataModeRoot.PAKETS_DATA:
                    paketsData = readPaketsData(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new DataModeRoot(additionalData, mainData, systemData, paketsData);
    }

    private DataModeSystemData readDataModeRoot_v2(XmlPullParser parser) throws IOException, XmlPullParserException{
        DataModeSystemData systemData = null;

        boolean flag = false;

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG && !flag) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();

            if(name.equals(DataModeRoot.SYSTEM_DATA)) {
                systemData = readSystemData(parser);
                flag = true;
            }
            else skip(parser);
        }

        return systemData;
    }

    private DataModeAdditionalData readAdditionalData(XmlPullParser parser) throws IOException, XmlPullParserException{
        String dbPatternFile = null;
        String divideByDates = null, timeOfShift = null, rewriteCatalogMode = null;

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.ADDITIONAL_DATA);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case DataModeAdditionalData.DB_PATTERN_FILE:
                    dbPatternFile = readText(parser);
                    break;
                case DataModeAdditionalData.DIVIDE_BY_DATES:
                    divideByDates = readText(parser);
                    break;
                case DataModeAdditionalData.TIME_OF_SHIFT:
                    timeOfShift = readText(parser);
                    break;
                case DataModeAdditionalData.REWRITE_CATALOG_MODE:
                    rewriteCatalogMode = readText(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new DataModeAdditionalData(dbPatternFile, divideByDates, timeOfShift, rewriteCatalogMode);
    }

    private List<Element> readMainData(XmlPullParser parser) throws IOException, XmlPullParserException{
        List<Element> mainData = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.MAIN_DATA);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            mainData.add(new Element(name, readText(parser)));
        }

        return mainData;
    }

    private DataModeSystemData readSystemData(XmlPullParser parser) throws IOException, XmlPullParserException{
        String state = null, mode = null;
        String catalogOfCreation = null, dateOfCreation = null;
        String catalogOfSync = null, dateOfSync = null;

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.SYSTEM_DATA);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case DataModeSystemData.STATE:
                    state = readText(parser);
                    break;
                case DataModeSystemData.MODE:
                    mode = readText(parser);
                    break;
                case DataModeSystemData.CATALOG_OF_CREATION:
                    catalogOfCreation = readText(parser);
                    break;
                case DataModeSystemData.DATE_OF_CREATION:
                    dateOfCreation = readText(parser);
                    break;
                case DataModeSystemData.CATALOG_OF_SYNC:
                    catalogOfSync = readText(parser);
                    break;
                case DataModeSystemData.DATE_OF_SYNC:
                    dateOfSync = readText(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new DataModeSystemData(state, mode, catalogOfCreation, dateOfCreation, catalogOfSync, dateOfSync);
    }

    private List<Element> readPaketsData(XmlPullParser parser) throws IOException, XmlPullParserException{
        List<Element> paketsData = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, DataModeRoot.PAKETS_DATA);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            paketsData.add(new Element(name, readText(parser)));
        }

        return paketsData;
    }
}
