package com.example.thebeginningv2.repository.managers.xml_managers;

import android.util.Xml;

import com.example.thebeginningv2.repository.data_types.xml.PakModeCommon;
import com.example.thebeginningv2.repository.data_types.xml.PakModeField;
import com.example.thebeginningv2.repository.data_types.xml.PakModeRoot;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakModeParser extends Parser {

    public PakModeParser(File file) {
        super(file);
    }

    public PakModeRoot getPakModeRoot() throws IOException, XmlPullParserException {
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPakModeRoot(parser);
        } finally {
            in.close();
        }
    }

    public PakModeCommon getPakModeCommon() throws XmlPullParserException, IOException{
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPakModeRoot_v2(parser);
        } finally {
            in.close();
        }
    }

    private PakModeRoot readPakModeRoot(XmlPullParser parser) throws IOException, XmlPullParserException {
        PakModeCommon common = null;
        List<PakModeField> fields = null;

        parser.require(XmlPullParser.START_TAG, null, PakModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case PakModeRoot.COMMON:
                    common = readCommon(parser);
                    break;
                case PakModeRoot.FIELDS:
                    fields = readFields(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new PakModeRoot(common, fields);
    }

    private PakModeCommon readPakModeRoot_v2(XmlPullParser parser) throws IOException, XmlPullParserException{
        PakModeCommon common = null;

        boolean flag = false;

        parser.require(XmlPullParser.START_TAG, null, PakModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG && !flag) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();

            if(name.equals(PakModeRoot.COMMON)){
                common = readCommon(parser);
                flag = true;
            }
            else skip(parser);
        }

        return common;
    }

    private PakModeCommon readCommon(XmlPullParser parser) throws IOException, XmlPullParserException{
        String dbDataFile = null;
        List<String> barcodeFields = new ArrayList<>(), RFIDFields = new ArrayList<>(), nameFields = new ArrayList<>(), numberFields = new ArrayList<>();
        String quantityField = null;
        List<String> listFields = new ArrayList<>(), resultFields = new ArrayList<>();
        String groupField = null;
        List<String> groupBarcodeFields = new ArrayList<>(), groupRFIDFields = new ArrayList<>();


        parser.require(XmlPullParser.START_TAG, null, PakModeRoot.COMMON);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();

            if(name.equals(PakModeCommon.DB_DATA_FILE)){
                dbDataFile = readText(parser);
            }
            else if(name.contains(PakModeCommon.FIELD_BARCODE)){
                barcodeFields.add(readText(parser));
            }
            else if(name.contains(PakModeCommon.FIELD_RFID)){
                RFIDFields.add(readText(parser));
            }
            else if(name.contains(PakModeCommon.FIELD_NAME)){
                nameFields.add(readText(parser));
            }
            else if(name.contains(PakModeCommon.FIELD_NUMBER)){
                numberFields.add(readText(parser));
            }
            else if(name.equals(PakModeCommon.FIELD_QUANTITY)){
                quantityField = readText(parser);
            }
            else if(name.contains(PakModeCommon.FIELD_LIST)){
                listFields.add(readText(parser));
            }
            else if(name.contains(PakModeCommon.FIELD_RESULT)){
                resultFields.add(readText(parser));
            }
            else if(name.equals(PakModeCommon.FIELD_GROUP)){
                groupField = readText(parser);
            }
            else if(name.contains(PakModeCommon.FIELD_GROUP_BARCODE)){
                groupBarcodeFields.add(readText(parser));
            }
            else if(name.contains(PakModeCommon.FIELD_GROUP_RFID)){
                groupRFIDFields.add(readText(parser));
            }
            else {
                skip(parser);
            }
        }

        return new PakModeCommon(dbDataFile,
                barcodeFields, RFIDFields, nameFields, numberFields,
                quantityField,
                listFields, resultFields,
                groupField,
                groupBarcodeFields, groupRFIDFields);
    }

    private List<PakModeField> readFields(XmlPullParser parser) throws IOException, XmlPullParserException{
        List<PakModeField> fields = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, PakModeRoot.FIELDS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name) {
                case PakModeField.FIELD:
                    fields.add(readField(parser));
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return fields;
    }

    private PakModeField readField(XmlPullParser parser) throws IOException, XmlPullParserException{
        String tag = null, type = null;
        String title = null, titleRow = null, order = null;
        String dataBackgroundColor = null, dataUseBoldFont = null, dataTextColor = null, dataTextSize = null;
        String titleBackgroundColor = null, titleUseBoldFont = null, titleTextColor = null, titleTextSize = null;

        parser.require(XmlPullParser.START_TAG, null, PakModeField.FIELD);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case PakModeField.TAG:
                    tag = readText(parser);
                    break;
                case PakModeField.TYPE:
                    type = readText(parser);
                    break;

                case PakModeField.TITLE:
                    title = readText(parser);
                    break;
                case PakModeField.TITLE_ROW:
                    titleRow = readText(parser);
                    break;
                case PakModeField.ORDER:
                    order = readText(parser);
                    break;

                case PakModeField.DATA_BACKGROUND_COLOR:
                    dataBackgroundColor = readText(parser);
                    break;
                case PakModeField.DATA_USE_BOLD_FONT:
                    dataUseBoldFont = readText(parser);
                    break;
                case PakModeField.DATA_TEXT_COLOR:
                    dataTextColor = readText(parser);
                    break;
                case PakModeField.DATA_TEXT_SIZE:
                    dataTextSize = readText(parser);
                    break;

                case PakModeField.TITLE_BACKGROUND_COLOR:
                    titleBackgroundColor = readText(parser);
                    break;
                case PakModeField.TITLE_USE_BOLD_FONT:
                    titleUseBoldFont = readText(parser);
                    break;
                case PakModeField.TITLE_TEXT_COLOR:
                    titleTextColor = readText(parser);
                    break;
                case PakModeField.TITLE_TEXT_SIZE:
                    titleTextSize = readText(parser);
                    break;

                default:
                    skip(parser);
                    break;
            }
        }

        return new PakModeField(tag, type,
                title, titleRow, order,
                dataBackgroundColor, dataUseBoldFont, dataTextColor, dataTextSize,
                titleBackgroundColor, titleUseBoldFont, titleTextColor, titleTextSize);
    }

}
