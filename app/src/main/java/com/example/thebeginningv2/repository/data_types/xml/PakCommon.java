package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by apple on 30.06.17.
 */

public class PakCommon {
    public static final String TITLE = "Title";
    public static final String DATE_OF_DOWNLOAD = "DateOfDownload";

    private String title, dateOfDownload;

    public PakCommon(String title, String dateOfDownload){
        this.title = title;
        this.dateOfDownload = dateOfDownload;
    }

    public String getTitle() {
        return title;
    }

    public String getDateOfDownload() {
        return dateOfDownload;
    }
}
