package com.example.thebeginningv2.repository.data_types.xml;

import com.example.thebeginningv2.repository.data_types.tables.ColumnsTable;

import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakModeCommon {
    public static final String DB_DATA_FILE = "DBDataFile";
    public static final String FIELD_BARCODE = "FieldBarcode";
    public static final String FIELD_RFID = "FieldRFID";
    public static final String FIELD_NAME = "FieldName";
    public static final String FIELD_NUMBER = "FieldNumber";
    public static final String FIELD_QUANTITY = "FieldQuantity";
    public static final String FIELD_LIST = "FieldList";
    public static final String FIELD_RESULT = "FieldResult";
    public static final String FIELD_GROUP = "FieldGroup";
    public static final String FIELD_GROUP_BARCODE = "FieldGroupBarcode";
    public static final String FIELD_GROUP_RFID = "FieldGroupRFID";

    private String dbDataFile;
    private List<String> barcodeFields, RFIDFields, nameFields, numberFields;
    private String quantityField;
    private List<String> listFields, resultFields;
    private String groupField;
    private List<String> groupBarcodeFields, groupRFIDFields;

    public PakModeCommon(String dbDataFile,
                         List<String> barcodeFields, List<String> RFIDFields, List<String> nameFields, List<String> numberFields,
                         String quantityField,
                         List<String> listFields, List<String> resultFields,
                         String groupField,
                         List<String> groupBarcodeFields, List<String> groupRFIDFields){
        this.dbDataFile = dbDataFile;
        this.barcodeFields = barcodeFields;
        this.RFIDFields = RFIDFields;
        this.nameFields = nameFields;
        this.numberFields = numberFields;
        this.quantityField = quantityField;
        this.listFields = listFields;
        this.resultFields = resultFields;
        this.groupField = groupField;
        this.groupBarcodeFields = groupBarcodeFields;
        this.groupRFIDFields = groupRFIDFields;
    }

    public String getDbDataFile() {
        return dbDataFile;
    }

    public List<String> getBarcodeFields() {
        return barcodeFields;
    }

    public List<String> getRFIDFields() {
        return RFIDFields;
    }

    public List<String> getNameFields() {
        return nameFields;
    }

    public List<String> getNumberFields() {
        return numberFields;
    }

    public String getQuantityField() {
        return quantityField;
    }

    public List<String> getListFields() {
        return listFields;
    }

    public List<String> getResultFields() {
        return resultFields;
    }

    public String getGroupField() {
        return groupField;
    }

    public List<String> getGroupBarcodeFields() {
        return groupBarcodeFields;
    }

    public List<String> getGroupRFIDFields() {
        return groupRFIDFields;
    }

    public int getSemantics(String tag){
        for (String i: barcodeFields){
            if(tag.equals(i)) return ColumnsTable.BARCODE;
        }

        for (String i: RFIDFields){
            if(tag.equals(i)) return ColumnsTable.RFID;
        }

        for (String i: nameFields){
            if(tag.equals(i)) return ColumnsTable.NAME;
        }

        for (String i: numberFields){
            if(tag.equals(i)) return ColumnsTable.NUMBER;
        }

        if(quantityField != null && tag.equals(quantityField)) return ColumnsTable.QUANTITY;

        return ColumnsTable.NONE;
    }

    public boolean isAsList(String tag){
        for (String i: listFields){
            if(i.equals(tag)) return true;
        }
        return false;
    }

    public boolean isAsResults(String tag){
        for (String i: resultFields){
            if(i.equals(tag)) return true;
        }
        return false;
    }

    public boolean containsQuantityField(){
        return quantityField != null;
    }

    public boolean containsSearchFileds(){
        return barcodeFields.size() > 0 || RFIDFields.size() > 0 || nameFields.size() > 0 || numberFields.size() > 0;
    }
}
