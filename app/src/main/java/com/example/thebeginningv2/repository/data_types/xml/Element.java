package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by Константин on 24.06.2017.
 */

public class Element {
    private String tag;
    private String value;

    public Element(String tag, String value){
        this.tag = tag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public String getValue() {
        return value;
    }
}
