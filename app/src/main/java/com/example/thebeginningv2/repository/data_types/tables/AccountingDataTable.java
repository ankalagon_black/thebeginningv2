package com.example.thebeginningv2.repository.data_types.tables;

import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class AccountingDataTable {
    public static final String TABLE_NAME = "AccountingData";

    public static final String ID = "id";
    public static final String IS_ACCOUNTED = "IsAccounted";
    public static final String TIMESTAMP_MARK = "TimestampMark";
}
