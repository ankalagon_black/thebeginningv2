package com.example.thebeginningv2.repository.data_types.xml;

import java.util.List;

/**
 * Created by Константин on 24.06.2017.
 */

public class DataModeRoot {
    public static final String ROOT = "root";
    public static final String ADDITIONAL_DATA = "AdditionalData";
    public static final String MAIN_DATA = "MainData";
    public static final String SYSTEM_DATA = "SystemData";
    public static final String PAKETS_DATA = "PaketsData";

    private DataModeAdditionalData additionalData;
    private List<Element> mainData;
    private DataModeSystemData systemData;
    private List<Element> paketsData;

    public DataModeRoot(DataModeAdditionalData additionalData, List<Element> mainData, DataModeSystemData systemData, List<Element> paketsData){
        this.additionalData = additionalData;
        this.mainData = mainData;
        this.systemData = systemData;
        this.paketsData = paketsData;
    }

    public DataModeAdditionalData getAdditionalData() {
        return additionalData;
    }

    public List<Element> getMainData() {
        return mainData;
    }

    public DataModeSystemData getSystemData() {
        return systemData;
    }

    public List<Element> getPaketsData() {
        return paketsData;
    }
}
