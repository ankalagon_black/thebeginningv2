package com.example.thebeginningv2.repository.data_types.xml;

/**
 * Created by Константин on 24.06.2017.
 */

public class SettingsModeCommon {
    public static final String TITLE = "Title";
    public static final String DB_PATTERN_FILE = "DBPatternFile";
    public static final String DIVIDE_BY_DATES = "DivideByDates";
    public static final String TIME_OF_SHIFT = "TimeOfShift";
    public static final String REWRITE_CATALOG_MODE = "RewriteCatalogMode";

    private String title;
    private String dbPatternFile;
    private String divideByDates, timeOfShift, rewriteCatalogMode;

    public SettingsModeCommon(String title, String dbPatternFile, String divideByDates, String timeOfShift, String rewriteCatalogMode){
        this.title = title;
        this.dbPatternFile = dbPatternFile;
        this.divideByDates = divideByDates;
        this.timeOfShift = timeOfShift;
        this.rewriteCatalogMode = rewriteCatalogMode;
    }

    public String getTitle() {
        return title;
    }

    public String getDbPatternFile() {
        return dbPatternFile;
    }

    public String getDivideByDates() {
        return divideByDates;
    }

    public String getRewriteCatalogMode() {
        return rewriteCatalogMode;
    }

    public String getTimeOfShift() {
        return timeOfShift;
    }
}
