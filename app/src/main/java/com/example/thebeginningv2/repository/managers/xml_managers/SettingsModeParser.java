package com.example.thebeginningv2.repository.managers.xml_managers;

import android.util.Xml;

import com.example.thebeginningv2.repository.data_types.xml.SettingsModeCommon;
import com.example.thebeginningv2.repository.data_types.xml.SettingsModeField;
import com.example.thebeginningv2.repository.data_types.xml.SettingsModeRoot;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 24.06.2017.
 */

public class SettingsModeParser extends Parser{

    public SettingsModeParser(File file) {
        super(file);
    }

    public SettingsModeRoot getSettingsModeRoot() throws IOException, XmlPullParserException {
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readSettingsModeRoot(parser);
        } finally {
            in.close();
        }
    }

    public SettingsModeCommon getSettingsModeCommon() throws XmlPullParserException, IOException{
        InputStream in = new FileInputStream(file);

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readSeetingsModeRoot_v2(parser);
        } finally {
            in.close();
        }
    }

    private SettingsModeRoot readSettingsModeRoot(XmlPullParser parser) throws IOException, XmlPullParserException {
        SettingsModeCommon common = null;
        List<SettingsModeField> fields = null, pakets = null;

        parser.require(XmlPullParser.START_TAG, null, SettingsModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case SettingsModeRoot.COMMON:
                    common = readCommon(parser);
                    break;
                case SettingsModeRoot.FIELDS:
                    fields = readFields(parser);
                    break;
                case SettingsModeRoot.PAKETS:
                    pakets = readPakets(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new SettingsModeRoot(common, fields, pakets);
    }

    private SettingsModeCommon readSeetingsModeRoot_v2(XmlPullParser parser) throws IOException, XmlPullParserException{
        SettingsModeCommon common = null;

        boolean flag = false;

        parser.require(XmlPullParser.START_TAG, null, SettingsModeRoot.ROOT);
        while (parser.next() != XmlPullParser.END_TAG && !flag) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();

            if(name.equals(SettingsModeRoot.COMMON)){
                common = readCommon(parser);
                flag = true;
            }
            else skip(parser);
        }

        return common;
    }

    private SettingsModeCommon readCommon(XmlPullParser parser) throws IOException, XmlPullParserException {
        String title = null;
        String dbPatternFile = null;
        String divideByDates = null, timeOfShift = null, rewriteCatalogMode = null;

        parser.require(XmlPullParser.START_TAG, null, SettingsModeRoot.COMMON);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case SettingsModeCommon.TITLE:
                    title = readValue(parser, SettingsModeCommon.TITLE);
                    break;
                case SettingsModeCommon.DB_PATTERN_FILE:
                    dbPatternFile = readValue(parser, SettingsModeCommon.DB_PATTERN_FILE);
                    break;
                case SettingsModeCommon.DIVIDE_BY_DATES:
                    divideByDates = readValue(parser, SettingsModeCommon.DIVIDE_BY_DATES);
                    break;
                case SettingsModeCommon.TIME_OF_SHIFT:
                    timeOfShift = readValue(parser, SettingsModeCommon.TIME_OF_SHIFT);
                    break;
                case SettingsModeCommon.REWRITE_CATALOG_MODE:
                    rewriteCatalogMode = readValue(parser, SettingsModeCommon.REWRITE_CATALOG_MODE);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new SettingsModeCommon(title, dbPatternFile, divideByDates, timeOfShift, rewriteCatalogMode);
    }

    private List<SettingsModeField> readFields(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<SettingsModeField> fields = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, SettingsModeRoot.FIELDS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name) {
                case SettingsModeField.FIELD:
                    fields.add(readField(parser));
                break;
                default:
                    skip(parser);
                    break;
            }
        }

        return fields;
    }

    private List<SettingsModeField> readPakets(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<SettingsModeField> pakets = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, SettingsModeRoot.PAKETS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name) {
                case SettingsModeField.FIELD:
                    pakets.add(readField(parser));
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return pakets;
    }

    private SettingsModeField readField(XmlPullParser parser) throws IOException, XmlPullParserException {
        String title = null, tag = null, defaultValue = null;
        String partOfName = null, required = null, editable = null;
        String type = null;

        parser.require(XmlPullParser.START_TAG, null, SettingsModeField.FIELD);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) continue;

            String name = parser.getName();
            switch (name){
                case SettingsModeField.TITLE:
                    title = readValue(parser, SettingsModeField.TITLE);
                    break;
                case SettingsModeField.TAG:
                    tag = readValue(parser, SettingsModeField.TAG);
                    break;
                case SettingsModeField.DEFAULT_VALUE:
                    defaultValue = readValue(parser, SettingsModeField.DEFAULT_VALUE);
                    break;
                case SettingsModeField.PART_OF_NAME:
                    partOfName = readValue(parser, SettingsModeField.PART_OF_NAME);
                    break;
                case SettingsModeField.REQUIRED:
                    required = readValue(parser, SettingsModeField.REQUIRED);
                    break;
                case SettingsModeField.EDITABLE:
                    editable = readValue(parser, SettingsModeField.EDITABLE);
                    break;
                case SettingsModeField.TYPE:
                    type = readValue(parser, SettingsModeField.TYPE);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }

        return new SettingsModeField(title, tag, defaultValue, partOfName, required, editable, type);
    }

    private String readValue(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tag);
        String value = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, tag);
        return value;
    }
}
