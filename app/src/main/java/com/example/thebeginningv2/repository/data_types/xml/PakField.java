package com.example.thebeginningv2.repository.data_types.xml;

import java.util.List;

/**
 * Created by apple on 30.06.17.
 */

public class PakField {
    public static final String FIELD = "Field";

    private List<Element> elements;

    public PakField(List<Element> elements){
        this.elements = elements;
    }

    public List<Element> getElements() {
        return elements;
    }
}
