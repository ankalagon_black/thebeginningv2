package com.example.thebeginningv2.inventories.domains;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeCreator;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

/**
 * Created by apple on 24.07.17.
 */

public class SendingDomain {
    private Activity activity;
    private PathManager pathManager;

    public SendingDomain(Activity activity, PathManager pathManager){
        this.pathManager = pathManager;
        this.activity = activity;
    }

    public SmbTransmissionInfo getSendingInfo(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);

        String ip = preferences.getString(activity.getString(R.string.smb_ip_key),
                activity.getString(R.string.smb_ip_default));
        String catalog = preferences.getString(activity.getString(R.string.smb_forward_catalog_key),
                activity.getString(R.string.smb_catalog_forward_default));
        String login = preferences.getString(activity.getString(R.string.smb_login_key),
                activity.getString(R.string.smb_login_default));
        String password = preferences.getString(activity.getString(R.string.smb_password_key),
                activity.getString(R.string.smb_password_default));

        return new SmbTransmissionInfo(ip, catalog, login, password);
    }

    public List<Inventory> getUnsentInventories(){
        File[] files = new File(pathManager.getDataPath()).listFiles();

        List<Inventory> inventories = new ArrayList<>();
        if(files != null){
            for (File i: files){
                String name = i.getName();
                int state, mode;

                try {
                    File file = new File(pathManager.getDataChildContentPath(name, PathManager.MODE));
                    DataModeSystemData systemData = new DataModeParser(file).geDataModeSystemData();

                    state = systemData.getState().equals(DataModeSystemData.SENT) ? Inventory.SENT : Inventory.IN_PROGRESS;
                    if(state == Inventory.SENT) continue;

                    mode = systemData.getMode().equals(DataModeSystemData.WITH_ACCOUNTING_DATA) ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA;
                }
                catch (Exception e){
                    continue;
                }

                inventories.add(new Inventory(name, state, mode));
            }
        }

        return inventories;
    }

    public DataModeRoot getDataModeRoot(String inventoryName) throws IOException, XmlPullParserException {
        File file = new File(pathManager.getDataChildContentPath(inventoryName, PathManager.MODE));

        return new DataModeParser(file).getDataModeRoot();
    }

    public void setDataModeRoot(String inventoryName, DataModeRoot dataModeRoot) throws IOException {
        File file = new File(pathManager.getDataChildContentPath(inventoryName, PathManager.MODE));

        new DataModeCreator().create(dataModeRoot, file);
    }

    public String getFirstLevelFolder(){
        return pathManager.getFirstLevelCatalog();
    }

    public void deleteContent(SmbFile smbFile) throws SmbException {
        deleteRecursive(smbFile);
    }

    private void deleteRecursive(SmbFile smbFile) throws SmbException {
        if(smbFile.isDirectory()){
            SmbFile[] files  = smbFile.listFiles();
            if(files != null){
                for (SmbFile i: files) deleteRecursive(i);
            }
        }
        else smbFile.delete();
    }

    public String getInventoryModePath(String inventoryName){
        return pathManager.getDataChildContentPath(inventoryName, PathManager.MODE);
    }

    public void sendMode(File modeFile, SmbFile smbFile) throws IOException {
        SmbFileOutputStream outputStream = new SmbFileOutputStream(smbFile);

        FileInputStream inputStream = new FileInputStream(modeFile);

        byte[] buffer = new byte[12288];
        int n;

        while ((n = inputStream.read(buffer)) != -1){
            outputStream.write(buffer, 0, n);
        }

        inputStream.close();

        outputStream.flush();
        outputStream.close();
    }

    public void sendRecords(List<AccountedRecord> accountedRecords, SmbFile smbFile) throws IOException {
        SmbFileOutputStream outputStream = new SmbFileOutputStream(smbFile);

        StringBuilder builder = new StringBuilder();

        for (AccountedRecord i: accountedRecords){
            if(i.hasGPSMark())
                builder.append(String.format(Locale.US,
                        activity.getString(R.string.output_format_with_gps_mark),
                        i.getBarcode(), i.getAccountedQuantity(), i.getGPSMark()));
            else builder.append(String.format(Locale.US,
                    activity.getString(R.string.output_format),
                    i.getBarcode(), i.getAccountedQuantity()));

            builder.append("\r\n");
        }

        InputStream inputStream = new ByteArrayInputStream(builder.toString().getBytes("UTF-8"));

        byte[] buffer = new byte[12288];
        int n;

        while ((n = inputStream.read(buffer)) != -1){
            outputStream.write(buffer, 0, n);
        }

        inputStream.close();

        outputStream.flush();
        outputStream.close();
    }

    public DatabaseManager getDatabaseManager(String inventoryName){
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildContentPath(inventoryName, PathManager.DB), null);
        return new DatabaseManager(db);
    }
}
