package com.example.thebeginningv2.inventories.domains;

import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.data_types.Result;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by apple on 25.07.17.
 */

public class AboutInventoryDomain {
    private PathManager pathManager;
    private DatabaseManager databaseManager;

    public AboutInventoryDomain(PathManager pathManager, DatabaseManager databaseManager){
        this.pathManager = pathManager;
        this.databaseManager = databaseManager;
    }

    public Inventory getInventory() throws IOException, XmlPullParserException {
        File file = new File(pathManager.getDataChildPath(PathManager.MODE));

        DataModeSystemData systemData = new DataModeParser(file).geDataModeSystemData();

        int state = systemData.getState().equals(DataModeSystemData.SENT) ? Inventory.SENT : Inventory.IN_PROGRESS;
        int mode = systemData.getMode().equals(DataModeSystemData.WITH_ACCOUNTING_DATA) ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA;

        return new Inventory(pathManager.getSecondLevelCatalog(), state, mode);
    }

    public ShortStatistics getShortStatistics(){
        return new ShortStatistics(databaseManager.getAccountedRecordSum(), databaseManager.getStatedRecordsSum(), databaseManager.getResidueQuantity(),
                databaseManager.getSurplusQuantity(), databaseManager.getUnidentifiedRecordsSum());
    }

    public RecordsCount getRecordsCount(){
        return new RecordsCount(databaseManager.getAccountedRecordsCount(), databaseManager.getRemainedRecordsCount());
    }

    public List<Result> getResults(){
        return databaseManager.getResults();
    }
}
