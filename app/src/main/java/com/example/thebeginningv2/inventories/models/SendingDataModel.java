package com.example.thebeginningv2.inventories.models;

import android.os.Handler;
import android.os.Looper;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.domains.SendingDomain;
import com.example.thebeginningv2.inventories.messages.InventorySentMessage;
import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.repository.data_types.xml.DataModeAdditionalData;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;

/**
 * Created by apple on 24.07.17.
 */

public class SendingDataModel extends BaseModel {
    public static final String INVENTORY_KEY = "InventoryKey";

    private Inventory inventory;
    private SendingDomain domain;
    private ViewInterface viewInterface;

    private Disposable send;

    public SendingDataModel(Inventory inventory, SendingDomain domain, ViewInterface viewInterface){
        this.inventory = inventory;
        this.domain = domain;
        this.viewInterface = viewInterface;
    }

    public void send(){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                viewInterface.onSendingFinished();
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        send = observer;

        Completable completable = Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                send(new WeakReference<>(viewInterface));
            }
        });

        subscribe(completable, observer);
    }

    public void cancel(){
        send.dispose();
    }

    private void send(final WeakReference<ViewInterface> weakReference) throws IOException, XmlPullParserException {
        SmbTransmissionInfo sendingInfo = domain.getSendingInfo();
        String root = sendingInfo.getSmbProtocolUrl();
        NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication("", sendingInfo.getLogin(), sendingInfo.getPassword());
        Handler handler = new Handler(Looper.getMainLooper());
        if(inventory == null){
            final List<Inventory> inventories = domain.getUnsentInventories();

            final int size = inventories.size();
            if(weakReference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        weakReference.get().onSendingStarted(size);
                    }
                });
            }

            for (int i = 0; i < size; i++){
                sendInventory(inventories.get(i), root, authentication);

                if(weakReference.get() != null){
                    final int finalI = i;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            notifyInventoryIsSent(inventories.get(finalI));
                            weakReference.get().onInventorySent(finalI);
                        }
                    });
                }
            }
        }
        else{
            if(weakReference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        weakReference.get().onSendingStarted(1);
                    }
                });
            }

            sendInventory(inventory, root, authentication);

            if(weakReference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        notifyInventoryIsSent(inventory);
                        weakReference.get().onInventorySent(1);
                    }
                });
            }
        }
    }

    private void notifyInventoryIsSent(Inventory inventory){

        EventBus.getDefault().post(new InventorySentMessage(inventory));
    }

    private void sendInventory(Inventory inventory, String root, NtlmPasswordAuthentication authentication) throws IOException, XmlPullParserException {
        DataModeRoot dataModeRoot = domain.getDataModeRoot(inventory.getName());

        DataModeAdditionalData additionalData = dataModeRoot.getAdditionalData();

        String folderName = inventory.getName();

        Calendar calendar = Calendar.getInstance();
        String syncDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(calendar.getTime());

        if(additionalData.getDivideByDates().equals("1")){
//            String timeOfShift = additionalData.getTimeOfShift();
//
//            int hours = calendar.get(Calendar.HOUR_OF_DAY), minutes = calendar.get(Calendar.MINUTE);
//            int shiftHours = Integer.parseInt(timeOfShift.substring(0, 2)),
//                    shiftMinutes = Integer.parseInt(timeOfShift.substring(3, 5));
//
//            if(shiftHours > hours || (shiftHours == hours && shiftMinutes > minutes)) calendar.add(Calendar.DATE, -1);
//
//            folderName = new SimpleDateFormat("yyyy/MM/dd").format(calendar.getTime()) + "/" + folderName;
            try {
                Date creationDate = new SimpleDateFormat("dd-MM-yyyy HH-mm").parse(dataModeRoot.getSystemData().getDateOfCreation());
                folderName = new SimpleDateFormat("yyyy/MM/dd").format(creationDate) + "/" + folderName;
            }
            catch (ParseException ignored) {
            }
        }

        String innerPath = domain.getFirstLevelFolder() + "/" + folderName + "/";

        DataModeSystemData systemData = dataModeRoot.getSystemData();
        systemData.setCatalogOfSync(folderName);
        systemData.setDateOfSync(syncDate);

        domain.setDataModeRoot(inventory.getName(), dataModeRoot);

        SmbFile destinationFolder = new SmbFile(root + innerPath, authentication);
        if(!destinationFolder.exists()) destinationFolder.mkdirs();

        if(additionalData.getRewriteCatalogMode().equals("1")) domain.deleteContent(destinationFolder);

        domain.sendMode(new File(domain.getInventoryModePath(inventory.getName())),
                new SmbFile(destinationFolder.getPath(), PathManager.MODE, authentication));

        DatabaseManager databaseManager = domain.getDatabaseManager(inventory.getName());
        List<AccountedRecord> records = databaseManager.getAccountedRecordsAsUnidentified();
        domain.sendRecords(records, new SmbFile(destinationFolder.getPath(), PathManager.INVENTORY_RESULTS, authentication));

        systemData.setState(DataModeSystemData.SENT);
        domain.setDataModeRoot(inventory.getName(), dataModeRoot);
    }

    public interface ViewInterface{
        void onSendingStarted(int inventories);
        void onInventorySent(int index);

        void onSendingFinished();

        void onError(Throwable e);
    }
}
