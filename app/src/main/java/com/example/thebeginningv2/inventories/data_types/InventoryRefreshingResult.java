package com.example.thebeginningv2.inventories.data_types;

import java.util.List;

/**
 * Created by Константин on 07.07.2017.
 */

public class InventoryRefreshingResult {
    private List<String> errors;
    private List<String> ambiguousRecords;

    public InventoryRefreshingResult(List<String> errors, List<String> ambiguousRecords){
        this.errors = errors;
        this.ambiguousRecords = ambiguousRecords;
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getAmbiguousRecords() {
        return ambiguousRecords;
    }
}
