package com.example.thebeginningv2.inventories.data_types;

/**
 * Created by Константин on 13.07.2017.
 */

public class Result {
    private String column;
    private float sum;

    public Result(String column, float sum){
        this.column = column;
        this.sum = sum;
    }

    public String getColumn() {
        return column;
    }

    public float getSum() {
        return sum;
    }
}
