package com.example.thebeginningv2.inventories.dialogs;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.adapters.ResultsAdapter;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.data_types.Result;
import com.example.thebeginningv2.inventories.domains.AboutInventoryDomain;
import com.example.thebeginningv2.inventories.models.AboutInventoryModel;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 08.06.2017.
 */

public class AboutInventoryDialog extends SupportModelDialog<AboutInventoryModel> implements AboutInventoryModel.ViewInterface{

    private TextView inventoryName, inventoryState;
    private TextView accountedRecordsCount, remainedRecordsCount;
    private TextView accountedCount, withResidue, withSurplus, statedCount, unidentifiedCount;
    private RecyclerView recyclerView;

    public static AboutInventoryDialog getInstance(Inventory inventory){
        AboutInventoryDialog dialog = new AboutInventoryDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(AboutInventoryModel.INVENTORY_KEY, inventory);

        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_module_about_inventory, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.about_inventory);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AboutInventoryDialog.this.dismiss();
            }
        });

        inventoryName = (TextView) view.findViewById(R.id.inventory_name);

        inventoryState = (TextView) view.findViewById(R.id.inventory_state);

        accountedRecordsCount = (TextView) view.findViewById(R.id.accounted_records_count);
        remainedRecordsCount = (TextView) view.findViewById(R.id.remained_records_count);

        accountedCount = (TextView) view.findViewById(R.id.accounted_count);
        withResidue = (TextView) view.findViewById(R.id.with_residue);
        withSurplus = (TextView) view.findViewById(R.id.with_surplus);
        statedCount = (TextView) view.findViewById(R.id.stated_count);
        unidentifiedCount = (TextView) view.findViewById(R.id.unidentified_count);

        recyclerView = (RecyclerView) view.findViewById(R.id.results);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().getInventory();
        getModel().getRecordsCount();
        getModel().getShortStatistics();
        getModel().getResults();
    }

    @Override
    protected AboutInventoryModel createModel() {
        Inventory inventory = null;
        PathManager pathManager = PathManager.getIntance();

        SQLiteDatabase sqLiteDatabase;

        if(pathManager.getLevel() == PathManager.SECOND_LEVEL){
            inventory = getArguments().getParcelable(AboutInventoryModel.INVENTORY_KEY);
            sqLiteDatabase = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildContentPath(inventory.getName(), PathManager.DB), null);
        }
        else {
            sqLiteDatabase = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildPath(PathManager.DB), null);
        }

        AboutInventoryDomain domain = new AboutInventoryDomain(pathManager, new DatabaseManager(sqLiteDatabase));

        return new AboutInventoryModel(this, domain, inventory);
    }

    @Override
    public void onInventory(Inventory inventory) {
        inventoryName.setText(inventory.getName());

        switch (inventory.getState()){
            case Inventory.IN_PROGRESS:
                inventoryState.setText(getActivity().getString(R.string.inventory_state_in_progress));
                break;
            case Inventory.SENT:
                inventoryState.setText(getActivity().getString(R.string.inventory_state_sent));
                break;
        }
    }

    @Override
    public void onRecordsCount(RecordsCount recordsCount) {
        accountedRecordsCount.setText(String.valueOf(recordsCount.getAccountedRecordsCount()));
        remainedRecordsCount.setText(String.valueOf(recordsCount.getRemainedRecordsCount()));
    }

    @Override
    public void onShortStatistics(ShortStatistics shortStatistics) {
        accountedCount.setText(String.format(Locale.US, getString(R.string.float_2_decimal), shortStatistics.getAccountedQuantity()));
        withResidue.setText(String.format(Locale.US, getString(R.string.float_2_decimal), shortStatistics.getResidueQuantity()));
        withSurplus.setText(String.format(Locale.US, getString(R.string.float_2_decimal), shortStatistics.getSurplusQuantity()));
        statedCount.setText(String.format(Locale.US, getString(R.string.float_2_decimal), shortStatistics.getStatedQuantity()));
        unidentifiedCount.setText(String.format(Locale.US, getString(R.string.float_2_decimal), shortStatistics.getUnidentifiedQuantity()));
    }

    @Override
    public void onResults(List<Result> results) {
        ResultsAdapter adapter = new ResultsAdapter(results);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
