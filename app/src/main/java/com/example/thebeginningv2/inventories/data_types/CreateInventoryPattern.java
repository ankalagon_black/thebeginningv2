package com.example.thebeginningv2.inventories.data_types;

import java.util.List;

/**
 * Created by Константин on 24.06.2017.
 */

public class CreateInventoryPattern {
    private CreateInventorySettings inventorySettings;
    private List<InventoryParameter> inventoryParameters;

    public CreateInventoryPattern(CreateInventorySettings inventorySettings, List<InventoryParameter> inventoryParameters){
        this.inventorySettings = inventorySettings;
        this.inventoryParameters = inventoryParameters;
    }

    public CreateInventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public List<InventoryParameter> getInventoryParameters() {
        return inventoryParameters;
    }
}
