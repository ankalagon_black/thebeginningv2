package com.example.thebeginningv2.inventories.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventories.data_types.EditInventoryPattern;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 06.07.2017.
 */

public class EditInventoryModel extends BaseModel {
    public static final String INVENTORY_NAME_KEY = "InventoryName";

    private EditInventoryPattern inventoryPattern;
    private String inventoryName;

    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable getInventoryPattern, editInventoryPattern;

    public EditInventoryModel(String inventoryName, ViewInterface viewInterface, DomainInterface domainInterface){
        this.inventoryName = inventoryName;
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void getInventoryPattern(){
        DisposableSingleObserver<EditInventoryPattern> observer = new DisposableSingleObserver<EditInventoryPattern>() {
            @Override
            public void onSuccess(EditInventoryPattern value) {
                inventoryPattern = value;
                viewInterface.onEditInventoryPatternReceived(inventoryPattern);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getInventoryPattern = observer;

        Single.fromCallable(new Callable<EditInventoryPattern>() {
            @Override
            public EditInventoryPattern call() throws Exception {
                return domainInterface.getEditInventoryPattern(inventoryName);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void editInventoryPattern(){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                viewInterface.onInventoryPatternEdited();
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        editInventoryPattern = observer;

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domainInterface.editInventoryPattern(inventoryName, inventoryPattern);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(getInventoryPattern != null && !getInventoryPattern.isDisposed()) getInventoryPattern.dispose();
        if(editInventoryPattern != null && !editInventoryPattern.isDisposed()) editInventoryPattern.dispose();
    }

    public interface ViewInterface{
        void onEditInventoryPatternReceived(EditInventoryPattern inventoryPattern);
        void onInventoryPatternEdited();

        void onError(Throwable e);
    }

    public interface DomainInterface{
        EditInventoryPattern getEditInventoryPattern(String inventoryName) throws Exception;
        void editInventoryPattern(String inventoryName, EditInventoryPattern inventoryPattern) throws IOException;
    }
}
