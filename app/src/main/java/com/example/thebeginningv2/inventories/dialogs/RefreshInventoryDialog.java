package com.example.thebeginningv2.inventories.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.data_types.InventoryRefreshingResult;
import com.example.thebeginningv2.inventories.domains.InventoriesDomain;
import com.example.thebeginningv2.inventories.models.RefreshInventoryModel;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 06.07.2017.
 */

public class RefreshInventoryDialog extends SupportModelDialog<InventoriesDomain, RefreshInventoryModel> implements RefreshInventoryModel.ViewInterface{

    private AlertDialog processDialog;
    private View progressBar;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        progressBar = LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null);

        processDialog = builder.setCancelable(false).setTitle(R.string.accounting_data_update).setView(progressBar).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getModel().stopRefreshing();
            }
        }).create();

        return processDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getModel().refreshInventory();
    }

    @Override
    protected RefreshInventoryModel createModel() {
        return new RefreshInventoryModel(getArguments().getString(RefreshInventoryModel.INVENTORY_NAME_KEY), this, getDomain());
    }

    @Override
    public void onRefreshingFinished(InventoryRefreshingResult result) {
        processDialog.setTitle(R.string.refreshing_finished);
        progressBar.setVisibility(View.GONE);

        List<String> errors = result.getErrors();

        if(errors == null){
            List<String> ambiguousRecords = result.getAmbiguousRecords();

            if(ambiguousRecords.size() != 0){
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.inventory_module_inventory_creation_issues, null);

                TextView message = (TextView) view.findViewById(R.id.message);
                message.setText(R.string.refreshing_finished_succesfully_but_has_ambiguous_records);

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, ambiguousRecords);

                ListView listView = (ListView) view.findViewById(R.id.list);
                listView.setAdapter(arrayAdapter);

                processDialog.setView(view);
            }
            else processDialog.setMessage(getString(R.string.refreshing_finished_succesfully));
        }
        else {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.inventory_module_inventory_creation_issues, null);

            TextView message = (TextView) view.findViewById(R.id.message);
            message.setText(R.string.refreshing_finished_with_errors);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, errors);

            ListView listView = (ListView) view.findViewById(R.id.list);
            listView.setAdapter(arrayAdapter);

            processDialog.setView(view);
        }

        Button button = processDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        button.setText(R.string.ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RefreshInventoryDialog.this.dismiss();
            }
        });
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), String.format(Locale.US, getString(R.string.refreshing_failed), e.getMessage()), Toast.LENGTH_SHORT).show();
        dismiss();
    }
}
