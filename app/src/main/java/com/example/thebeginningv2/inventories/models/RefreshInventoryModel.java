package com.example.thebeginningv2.inventories.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventories.data_types.InventoryRefreshingResult;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 06.07.2017.
 */

public class RefreshInventoryModel extends BaseModel {
    public static final String INVENTORY_NAME_KEY = "InventoryName";

    private String inventoryName;

    private ViewInterface viewinterface;
    private DomainInterface domainInterface;

    private Disposable refreshInventory;

    public RefreshInventoryModel(String inventoryName, ViewInterface viewInterface, DomainInterface domainInterface){
        this.inventoryName = inventoryName;
        this.viewinterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void refreshInventory(){
        DisposableSingleObserver<InventoryRefreshingResult> observer = new DisposableSingleObserver<InventoryRefreshingResult>() {
            @Override
            public void onSuccess(InventoryRefreshingResult value) {
                viewinterface.onRefreshingFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewinterface.onError(e);
            }
        };

        refreshInventory = observer;

        Single.fromCallable(new Callable<InventoryRefreshingResult>() {
            @Override
            public InventoryRefreshingResult call() throws Exception {
                return domainInterface.refreshInventory(inventoryName);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void stopRefreshing(){
        refreshInventory.dispose();
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(refreshInventory != null && !refreshInventory.isDisposed()) refreshInventory.dispose();
    }

    public interface ViewInterface{
        void onRefreshingFinished(InventoryRefreshingResult result);
        void onError(Throwable e);
    }

    public interface DomainInterface{
        InventoryRefreshingResult refreshInventory(String inventoryName) throws IOException, XmlPullParserException;
    }
}