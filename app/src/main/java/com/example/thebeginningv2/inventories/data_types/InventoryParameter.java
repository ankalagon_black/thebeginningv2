package com.example.thebeginningv2.inventories.data_types;

/**
 * Created by Константин on 21.06.2017.
 */

public class InventoryParameter {
    public static final int STRING = 0, INTEGER = 1, DATE = 2, BOOLEAN = 3, FLOAT = 4;

    private String title, tag, defaultValue;
    private String value = null;
    private int type;
    private boolean partOfName, required, editable;
    private boolean isInPakets;

    public InventoryParameter(String title, String tag, String defaultValue, int type, boolean partOfName, boolean required, boolean editable, boolean isInPakets){
        this.title = title;
        this.tag = tag;
        this.defaultValue = defaultValue;
        this.type = type;
        this.partOfName = partOfName;
        this.required = required;
        this.editable = editable;
        this.isInPakets = isInPakets;
    }

    public String getTitle() {
        return title;
    }

    public String getTag() {
        return tag;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public int getType(){
        return type;
    }

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }

    public boolean isPartOfName() {
        return partOfName;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isInPakets() {
        return isInPakets;
    }
}
