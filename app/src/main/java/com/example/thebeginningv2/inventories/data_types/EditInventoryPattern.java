package com.example.thebeginningv2.inventories.data_types;

import java.util.List;

/**
 * Created by Константин on 06.07.2017.
 */

public class EditInventoryPattern {
    private EditInventorySettings settings;
    private List<InventoryParameter> inventoryParameters;

    public EditInventoryPattern(EditInventorySettings settings, List<InventoryParameter> inventoryParameters){
        this.settings = settings;
        this.inventoryParameters = inventoryParameters;
    }

    public EditInventorySettings getSettings() {
        return settings;
    }

    public List<InventoryParameter> getInventoryParameters() {
        return inventoryParameters;
    }
}
