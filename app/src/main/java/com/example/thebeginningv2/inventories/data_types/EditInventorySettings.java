package com.example.thebeginningv2.inventories.data_types;

/**
 * Created by Константин on 06.07.2017.
 */

public class EditInventorySettings {
    private String dbPatternFile, timeOfShift;
    private boolean divideByDates, rewriteCatalogMode;
    private int state, mode;
    private String catalogOfCreation, dateOfCreation;
    private String catalogOfSync, dateOfSync;

    public EditInventorySettings(String dbPatternFile, String timeOfShift, boolean divideByDates, boolean rewriteCatalogMode, int state, int mode,
                                 String catalogOfCreation, String dateOfCreation, String catalogOfSync, String dateOfSync){
        this.dbPatternFile = dbPatternFile;
        this.timeOfShift = timeOfShift;
        this.divideByDates = divideByDates;
        this.rewriteCatalogMode = rewriteCatalogMode;
        this.state = state;
        this.mode = mode;
        this.catalogOfCreation = catalogOfCreation;
        this.dateOfCreation = dateOfCreation;
        this.catalogOfSync = catalogOfSync;
        this.dateOfSync = dateOfSync;
    }

    public String getDbPatternFile() {
        return dbPatternFile;
    }

    public boolean isDivideByDates() {
        return divideByDates;
    }

    public boolean isRewriteCatalogMode() {
        return rewriteCatalogMode;
    }

    public int getState() {
        return state;
    }

    public int getMode() {
        return mode;
    }

    public String getCatalogOfCreation() {
        return catalogOfCreation;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public String getCatalogOfSync() {
        return catalogOfSync;
    }

    public String getDateOfSync() {
        return dateOfSync;
    }

    public String getTimeOfShift() {
        return timeOfShift;
    }
}
