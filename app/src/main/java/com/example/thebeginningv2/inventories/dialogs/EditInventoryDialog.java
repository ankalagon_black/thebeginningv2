package com.example.thebeginningv2.inventories.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.domains.InventoriesDomain;
import com.example.thebeginningv2.inventories.adapters.InventoryParametersAdapter;
import com.example.thebeginningv2.inventories.adapters.InventorySettingsItemsAdapter;
import com.example.thebeginningv2.inventories.data_types.EditInventoryPattern;
import com.example.thebeginningv2.inventories.data_types.EditInventorySettings;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.models.EditInventoryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 06.07.2017.
 */

public class EditInventoryDialog extends SupportModelDialog<InventoriesDomain, EditInventoryModel> implements EditInventoryModel.ViewInterface{
    private ListView listView;
    private RecyclerView recyclerView;

    private InventoryParametersAdapter adapter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_module_edit_inventory, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.parameters_edit);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditInventoryDialog.this.dismiss();
            }
        });

        listView = (ListView) view.findViewById(R.id.inventory_settings);
        recyclerView = (RecyclerView) view.findViewById(R.id.inventory_parameters);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Button button = (Button) view.findViewById(R.id.apply);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter.allRequiredParametersFilled()){
                    getModel().editInventoryPattern();
                }
                else Toast.makeText(getActivity(), R.string.inventory_parameters_are_not_valid, Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().getInventoryPattern();
    }

    @Override
    protected EditInventoryModel createModel() {
        return new EditInventoryModel(getArguments().getString(EditInventoryModel.INVENTORY_NAME_KEY), this, getDomain());
    }

    @Override
    public void onEditInventoryPatternReceived(EditInventoryPattern inventoryPattern) {
        EditInventorySettings inventorySettings = inventoryPattern.getSettings();

        List<InventorySettingsItemsAdapter.Item> items = new ArrayList<>(7);

        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.db_pattern_file), inventorySettings.getDbPatternFile()));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.divide_by_dates), inventorySettings.isDivideByDates() ?
                getString(R.string.yes) : getString(R.string.no)));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.time_of_shift), inventorySettings.getTimeOfShift()));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.rewrite_catalog_mode), inventorySettings.isRewriteCatalogMode() ?
                getString(R.string.yes) : getString(R.string.no)));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.state), inventorySettings.getState() == Inventory.IN_PROGRESS ?
                getString(R.string.in_progress) : getString(R.string.sent)));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.mode), inventorySettings.getMode() == Inventory.WITH_ACCOUNTING_DATA ?
                getString(R.string.with_accounting_data) : getString(R.string.without_accounting_data)));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.catalog_of_creation), inventorySettings.getCatalogOfCreation()));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.date_of_creation), inventorySettings.getDateOfCreation()));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.catalog_of_sync), inventorySettings.getCatalogOfSync()));
        items.add(new InventorySettingsItemsAdapter.Item(getString(R.string.date_of_sync), inventorySettings.getDateOfSync()));

        InventorySettingsItemsAdapter settingsItemsAdapter = new InventorySettingsItemsAdapter(getActivity(), R.layout.inventory_module_inventory_settings_item, items);
        listView.setAdapter(settingsItemsAdapter);

        adapter = new InventoryParametersAdapter(inventoryPattern.getInventoryParameters(), InventoryParametersAdapter.EDIT);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInventoryPatternEdited() {
        Toast.makeText(getActivity(), R.string.editing_finished, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), String.format(Locale.US, getString(R.string.editing_finished_with_error), e.getMessage()), Toast.LENGTH_SHORT).show();
        dismiss();
    }
}
