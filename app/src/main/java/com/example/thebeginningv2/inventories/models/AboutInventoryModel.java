package com.example.thebeginningv2.inventories.models;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.data_types.Result;
import com.example.thebeginningv2.inventories.domains.AboutInventoryDomain;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 13.07.2017.
 */

public class AboutInventoryModel extends BaseModel {
    public static final String INVENTORY_KEY = "InventoryKey";

    private ViewInterface viewInterface;
    private AboutInventoryDomain domain;

    private Inventory inventory = null;

    public AboutInventoryModel(ViewInterface viewInterface, AboutInventoryDomain domain, Inventory inventory){
        this.viewInterface = viewInterface;
        this.domain = domain;
        this.inventory = inventory;
    }

    public void getInventory(){
        if(inventory != null){
            viewInterface.onInventory(inventory);
        }
        else {
            DisposableSingleObserver<Inventory> observer = new DisposableSingleObserver<Inventory>() {
                @Override
                public void onSuccess(Inventory value) {
                    viewInterface.onInventory(value);
                }

                @Override
                public void onError(Throwable e) {
                    viewInterface.onError(e);
                }
            };

            Single<Inventory> single = Single.fromCallable(new Callable<Inventory>() {
                @Override
                public Inventory call() throws Exception {
                    return domain.getInventory();
                }
            });

            subscribe(single, observer);
        }
    }

    public void getRecordsCount(){
        DisposableSingleObserver<RecordsCount> observer = new DisposableSingleObserver<RecordsCount>() {
            @Override
            public void onSuccess(RecordsCount value) {
                viewInterface.onRecordsCount(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<RecordsCount> single = Single.fromCallable(new Callable<RecordsCount>() {
            @Override
            public RecordsCount call() throws Exception {
                return domain.getRecordsCount();
            }
        });

        subscribe(single, observer);
    }

    public void getShortStatistics() {
        DisposableSingleObserver<ShortStatistics> observer = new DisposableSingleObserver<ShortStatistics>() {
            @Override
            public void onSuccess(ShortStatistics value) {
                viewInterface.onShortStatistics(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<ShortStatistics> single = Single.fromCallable(new Callable<ShortStatistics>() {
            @Override
            public ShortStatistics call() throws Exception {
                return domain.getShortStatistics();
            }
        });

       subscribe(single, observer);
    }

    public void getResults(){
        DisposableSingleObserver<List<Result>> observer = new DisposableSingleObserver<List<Result>>() {
            @Override
            public void onSuccess(List<Result> value) {
                viewInterface.onResults(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<Result>> single = Single.fromCallable(new Callable<List<Result>>() {
            @Override
            public List<Result> call() throws Exception {
                return domain.getResults();
            }
        });

        subscribe(single, observer);
    }

    public interface ViewInterface{
        void onInventory(Inventory inventory);
        void onRecordsCount(RecordsCount recordsCount);
        void onShortStatistics(ShortStatistics shortStatistics);
        void onResults(List<Result> results);

        void onError(Throwable e);
    }
}
