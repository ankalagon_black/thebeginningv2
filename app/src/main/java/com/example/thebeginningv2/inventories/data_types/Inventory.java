package com.example.thebeginningv2.inventories.data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Константин on 20.06.2017.
 */

public class Inventory implements Parcelable{
    public static final int IN_PROGRESS = 0, SENT = 1;
    public static final int WITHOUT_ACCOUNTING_DATA = 0, WITH_ACCOUNTING_DATA = 1;

    private String name;
    private int state, mode;

    public Inventory(String name, int state, int mode){
        this.name = name;
        this.state = state;
        this.mode = mode;
    }

    protected Inventory(Parcel in) {
        name = in.readString();
        state = in.readInt();
        mode = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(state);
        dest.writeInt(mode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Inventory> CREATOR = new Creator<Inventory>() {
        @Override
        public Inventory createFromParcel(Parcel in) {
            return new Inventory(in);
        }

        @Override
        public Inventory[] newArray(int size) {
            return new Inventory[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}
