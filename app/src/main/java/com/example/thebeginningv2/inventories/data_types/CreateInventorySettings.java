package com.example.thebeginningv2.inventories.data_types;

/**
 * Created by Константин on 21.06.2017.
 */

public class CreateInventorySettings {
    private String title, dbPatternFile, timeOfShift;
    private boolean divideByDates, rewriteCatalogMode;
    private String catalogOfSync, dateOfSync;

    public CreateInventorySettings(String title, String dbPatternFile, String timeOfShift,
                                   boolean divideByDates, boolean rewriteCatalogMode, String catalogOfSync, String dateOfSync){
        this.title = title;
        this.dbPatternFile = dbPatternFile;
        this.timeOfShift = timeOfShift;
        this.divideByDates = divideByDates;
        this.rewriteCatalogMode = rewriteCatalogMode;
        this.catalogOfSync = catalogOfSync;
        this.dateOfSync = dateOfSync;
    }

    public String getTitle() {
        return title;
    }

    public String getDbPatternFile() {
        return dbPatternFile;
    }

    public boolean isDivideByDates() {
        return divideByDates;
    }

    public boolean isRewriteCatalogMode() {
        return rewriteCatalogMode;
    }

    public String getTimeOfShift() {
        return timeOfShift;
    }

    public String getCatalogOfSync() {
        return catalogOfSync;
    }

    public String getDateOfSync() {
        return dateOfSync;
    }
}
