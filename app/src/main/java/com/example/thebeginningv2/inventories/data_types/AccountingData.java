package com.example.thebeginningv2.inventories.data_types;

/**
 * Created by Константин on 04.07.2017.
 */

public class AccountingData {
    public static final int NO_ACCOUNTING_DATA = 0, ACCOUNTING_DATA_DOWNLOADED = 1, ACCOUNTING_DATA_DOWNLOADED_WITH_DATE = 2;

    private int state;
    private String date;

    public AccountingData(int state, String date){
        this.state = state;
        this.date = date;
    }


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
