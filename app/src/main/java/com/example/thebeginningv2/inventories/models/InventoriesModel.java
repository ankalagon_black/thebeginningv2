package com.example.thebeginningv2.inventories.models;

import com.example.thebeginningv2.core_v2.models.Model;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventories.data_types.AccountingData;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.messages.InventorySentMessage;
import com.example.thebeginningv2.inventory_module.messages.DownloadingFinished;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 04.07.2017.
 */

public class InventoriesModel extends Model{
    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private List<Inventory> inventories;
    private AccountingData accountingData;

    private Disposable getInventories, getState, toInProgress;

    public InventoriesModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void initialize(){
        domainInterface.initialize();
    }

    public void getInventories(){
        DisposableSingleObserver<List<Inventory>> observer = new DisposableSingleObserver<List<Inventory>>() {
            @Override
            public void onSuccess(List<Inventory> value) {
                inventories = value;
                viewInterface.onInventoriesReceived(inventories);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getInventories = observer;

        Single.fromCallable(new Callable<List<Inventory>>() {
            @Override
            public List<Inventory> call() throws Exception {
                return domainInterface.getInventories();
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void delete(Inventory inventory){
        int index = inventories.indexOf(inventory);
        inventories.remove(index);

        domainInterface.delete(inventory.getName());

        viewInterface.onInventoryRemoved(index);
    }

    public void getAccountingData(){
        DisposableSingleObserver<AccountingData> observer = new DisposableSingleObserver<AccountingData>() {
            @Override
            public void onSuccess(AccountingData value) {
                accountingData = value;
                viewInterface.onAccountingDataReceived(accountingData);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        getState = observer;

        Single.fromCallable(new Callable<AccountingData>() {
            @Override
            public AccountingData call() throws Exception {
                return domainInterface.getAccountingData();
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void deleteAccountingData(){
        accountingData.setState(AccountingData.NO_ACCOUNTING_DATA);
        domainInterface.deleteAccountingData();
        viewInterface.onAccountingDataReceived(accountingData);
    }

    public void toInProgress(final Inventory inventory){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                int index = inventories.indexOf(inventory);
                if(index != -1){
                    inventory.setState(Inventory.IN_PROGRESS);
                    viewInterface.onInventoryUpdated(index);
                }
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        toInProgress = observer;

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domainInterface.toInProgress(inventory.getName());
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void openInventory(Inventory inventory){
        domainInterface.openInventory(inventory.getName());
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(getInventories != null && !getInventories.isDisposed()) getInventories.dispose();
        if(getState != null && !getState.isDisposed()) getState.dispose();
        if(toInProgress != null && !toInProgress.isDisposed()) toInProgress.dispose();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInventoryCreated(Inventory inventory){
        inventories.add(inventory);

        viewInterface.onInventoryAdded(inventories.size() - 1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInventoryIsSent(InventorySentMessage message){
        Inventory inventory = message.getInventory();

        int index  = findInventoryByName(inventory.getName());
        if(index != -1){
            inventories.get(index).setState(Inventory.SENT);
            viewInterface.onInventoryUpdated(index);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDownloadingFinished(DownloadingFinished downloadingFinished){
        getAccountingData();
    }

    private int findInventoryByName(String inventoryName){
        int size = inventories.size();
        for (int i = 0; i < size; i++){
            if(inventories.get(i).getName().equals(inventoryName)){
                return i;
            }
        }

        return -1;
    }

    public interface ViewInterface{
        void onInventoriesReceived(List<Inventory> inventories);
        void onInventoryAdded(int position);
        void onInventoryRemoved(int position);
        void onInventoryUpdated(int position);

        void onAccountingDataReceived(AccountingData accountingData);

        void onError(Throwable e);
    }

    public interface DomainInterface{
        void initialize();

        List<Inventory> getInventories();
        void toInProgress(String inventoryName) throws IOException, XmlPullParserException;
        void delete(String inventoryName);

        AccountingData getAccountingData() throws Exception;
        void deleteAccountingData();

        void openInventory(String inventoryName);
    }
}
