package com.example.thebeginningv2.inventories.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thebeginningv2.R;

import java.util.List;

/**
 * Created by Константин on 06.07.2017.
 */

public class InventorySettingsItemsAdapter extends ArrayAdapter<InventorySettingsItemsAdapter.Item> {

    private List<Item> items;

    public InventorySettingsItemsAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Item> objects) {
        super(context, resource, objects);
        items = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.inventory_module_inventory_settings_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();

            viewHolder.settings = (TextView) convertView.findViewById(R.id.settings);
            viewHolder.value = (TextView) convertView.findViewById(R.id.value);

            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) convertView.getTag();

        Item item = items.get(position);

        viewHolder.settings.setText(item.getSettings());
        viewHolder.value.setText(item.getValue());

        return convertView;
    }

    public static class Item{
        private String settings, value;

        public Item(String settings, String value){
            this.settings = settings;
            this.value = value;
        }

        public String getSettings() {
            return settings;
        }

        public String getValue() {
            return value;
        }
    }

    public class ViewHolder{
        public TextView settings, value;
    }
}
