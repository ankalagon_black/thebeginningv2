package com.example.thebeginningv2.inventories.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.domains.SendingDomain;
import com.example.thebeginningv2.inventories.models.SendingDataModel;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.util.Locale;

/**
 * Created by apple on 24.07.17.
 */

public class SendingDataDialog extends SupportModelDialog<SendingDataModel>
        implements SendingDataModel.ViewInterface{
    private ProgressBar inventoriesBar;
    private TextView inventoriesProgress;

    public static SendingDataDialog getInstance(Inventory inventory){
        SendingDataDialog sendingDataDialog = new SendingDataDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(SendingDataModel.INVENTORY_KEY, inventory);

        sendingDataDialog.setArguments(bundle);
        return sendingDataDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.sending_inventory, null);

        inventoriesBar = (ProgressBar) view.findViewById(R.id.inventories_bar);
        inventoriesProgress = (TextView) view.findViewById(R.id.inventories_progress);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return builder.setCancelable(false).
                setTitle(R.string.sending_inventories).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().cancel();
                    }
                }).create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().send();
    }

    @Override
    protected SendingDataModel createModel() {
        Inventory inventory = null;
        Bundle bundle = getArguments();
        if(bundle != null) inventory = bundle.getParcelable(SendingDataModel.INVENTORY_KEY);

        return new SendingDataModel(inventory, new SendingDomain(getActivity(), PathManager.getIntance()), this);
    }

    @Override
    public void onError(Throwable e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.transmission_error).
                setMessage(R.string.transmission_error_message).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SendingDataDialog.this.dismiss();
                    }
                }).
                setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getModel().send();
                    }
                }).
                setCancelable(false).
                create().show();
    }

    @Override
    public void onSendingStarted(int files) {
        inventoriesBar.setMax(files);
        inventoriesBar.setProgress(0);

        inventoriesProgress.setText(String.format(Locale.US, getString(R.string.out_of), 0, files));
    }

    @Override
    public void onInventorySent(int index) {
        inventoriesBar.setProgress(index);

        inventoriesProgress.setText(String.format(Locale.US, getString(R.string.out_of), index,
                inventoriesBar.getMax()));
    }

    @Override
    public void onSendingFinished() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.inventories_sent).
                setMessage(R.string.inventories_sent_message).
                setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SendingDataDialog.this.dismiss();
                    }
                }).
                create().
                show();
    }
}
