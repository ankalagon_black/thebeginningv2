package com.example.thebeginningv2.inventories.adapters;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventories.data_types.InventoryParameter;

import org.apache.commons.lang3.math.NumberUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Константин on 21.06.2017.
 */

public class InventoryParametersAdapter extends RecyclerView.Adapter<InventoryParametersAdapter.Holder> {
    public static int CREATE = 0, EDIT = 1;

    private List<InventoryParameter> inventoryParameters;
    private int mode;

    public InventoryParametersAdapter(List<InventoryParameter> inventoryParameters, int mode){
        this.inventoryParameters = inventoryParameters;
        this.mode = mode;
    }

    @Override
    public int getItemViewType(int position) {
        return inventoryParameters.get(position).getType();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case InventoryParameter.STRING:
                return new StringHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory_parameter_string, parent, false));
            case InventoryParameter.INTEGER:
                return new NumberHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory_parameter_integer, parent, false));
            case InventoryParameter.BOOLEAN:
                return new BooleanHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory_parameter_boolean, parent, false));
            case InventoryParameter.DATE:
                return new DateHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory_parameter_date, parent, false));
            case InventoryParameter.FLOAT:
                return new NumberHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory_parameter_float, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setData(inventoryParameters.get(position));
    }

    @Override
    public int getItemCount() {
        return inventoryParameters.size();
    }

    public boolean allRequiredParametersFilled(){
        for (InventoryParameter i: inventoryParameters){
            if(i.isRequired()){
                switch (i.getType()){
                    case InventoryParameter.STRING:
                        String value = i.getValue();
                        if(value == null || value.replace(" ", "").length() == 0) return false;
                        break;
                    case InventoryParameter.DATE:
                        value = i.getValue();
                        if(value == null) return false;
                        break;
                    case InventoryParameter.INTEGER:
                    case InventoryParameter.FLOAT:
                        value = i.getValue();
                        if(value == null || !NumberUtils.isCreatable(value)) return false;
                        break;
                }
            }
        }
        return true;
    }

    public class Holder extends RecyclerView.ViewHolder{
        private TextView name;
        protected EditText editValue;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.parameter_name);
            editValue = (EditText) itemView.findViewById(R.id.parameter_value);
        }

        public void setData(InventoryParameter inventoryParameter){
            String title = inventoryParameter.getTitle();
            if(title != null) name.setText(title);
            else name.setText(R.string.inventory_parameter_default_title);

            if(inventoryParameter.isRequired()) name.setTextColor(itemView.getResources().getColor(R.color.colorAccent));

            if(inventoryParameter.getDefaultValue() != null) editValue.setText(inventoryParameter.getDefaultValue());
        }
    }

    public class StringHolder extends Holder{

        public StringHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setData(final InventoryParameter inventoryParameter) {
            super.setData(inventoryParameter);

            if(mode == EDIT && !inventoryParameter.isEditable()) editValue.setEnabled(false);
            else {
                editValue.setEnabled(true);
                editValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        inventoryParameter.setValue(editable.toString());
                    }
                });
            }
        }
    }

    public class NumberHolder extends Holder{

        public NumberHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setData(final InventoryParameter inventoryParameter) {
            super.setData(inventoryParameter);
            if(mode == EDIT && !inventoryParameter.isEditable()) editValue.setEnabled(false);
            else {
                editValue.setEnabled(true);
                editValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String string = editable.toString();
                        if (NumberUtils.isCreatable(string)) inventoryParameter.setValue(string);
                    }
                });
            }
        }
    }

    public class DateHolder extends Holder{
        private ImageButton setCurrentDate, chooseDate;

        public DateHolder(View itemView) {
            super(itemView);
            setCurrentDate = (ImageButton) itemView.findViewById(R.id.set_current_date);
            chooseDate = (ImageButton) itemView.findViewById(R.id.choose_date);
        }

        @Override
        public void setData(final InventoryParameter inventoryParameter) {
            super.setData(inventoryParameter);

            if(mode == EDIT && !inventoryParameter.isEditable()){
                setCurrentDate.setEnabled(false);
                chooseDate.setEnabled(false);
            }
            else {
                setCurrentDate.setEnabled(true);
                chooseDate.setEnabled(true);

                setCurrentDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String dateStr = df.format(Calendar.getInstance().getTime());

                        inventoryParameter.setValue(dateStr);
                        editValue.setText(dateStr);
                    }
                });

                final Context context = itemView.getContext();

                chooseDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String defaultValue = inventoryParameter.getDefaultValue();
                        Date date = null;
                        if (defaultValue != null) {
                            DateFormat dateFormat = new SimpleDateFormat();
                            try {
                                date = dateFormat.parse(defaultValue);
                            } catch (ParseException ignored) {
                            }
                        }

                        final Calendar calendar = Calendar.getInstance();

                        if (date != null) calendar.setTime(date);


                        final DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DatePicker datePicker = dialog.getDatePicker();
                                calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                                String date = dateFormat.format(calendar.getTime());

                                inventoryParameter.setValue(date);
                                editValue.setText(date);
                            }
                        });
                        dialog.show();
                    }
                });
            }
        }
    }

    public class BooleanHolder extends Holder{
        private CheckBox yes;

        public BooleanHolder(View itemView) {
            super(itemView);
            yes = (CheckBox) itemView.findViewById(R.id.yes);
        }

        @Override
        public void setData(final InventoryParameter inventoryParameter) {
            super.setData(inventoryParameter);

            if(mode == EDIT && !inventoryParameter.isEditable()){
                yes.setEnabled(false);
            }
            else {
                yes.setEnabled(true);
                yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        inventoryParameter.setValue(b ? "1" : "0");

                        if (b) editValue.setText("1");
                        else editValue.setText("0");
                    }
                });

                String defaultValue = inventoryParameter.getDefaultValue();
                if (defaultValue == null || defaultValue.equals("0")) yes.setChecked(false);
                else yes.setChecked(true);
            }
        }
    }
}
