package com.example.thebeginningv2.inventories.domains;

import android.database.sqlite.SQLiteDatabase;

import com.example.thebeginningv2.inventories.data_types.AccountingData;
import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
import com.example.thebeginningv2.inventories.data_types.CreateInventorySettings;
import com.example.thebeginningv2.inventories.data_types.EditInventoryPattern;
import com.example.thebeginningv2.inventories.data_types.EditInventorySettings;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.data_types.InventoryCreationResult;
import com.example.thebeginningv2.inventories.data_types.InventoryRefreshingResult;
import com.example.thebeginningv2.inventories.data_types.Result;
import com.example.thebeginningv2.inventories.models.AboutInventoryModel;
import com.example.thebeginningv2.inventories.models.CreateInventoryModel;
import com.example.thebeginningv2.inventories.models.EditInventoryModel;
import com.example.thebeginningv2.inventories.models.InventoriesModel;
import com.example.thebeginningv2.inventories.data_types.InventoryParameter;
import com.example.thebeginningv2.inventories.models.RefreshInventoryModel;
import com.example.thebeginningv2.inventory_module.domain.DownloadingDataDomain;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.repository.managers.FileManager;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.data_types.tables.AccountingDataTable;
import com.example.thebeginningv2.repository.data_types.tables.ColumnsTable;
import com.example.thebeginningv2.repository.data_types.xml.DataModeAdditionalData;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
import com.example.thebeginningv2.repository.data_types.xml.Element;
import com.example.thebeginningv2.repository.data_types.xml.PakCommon;
import com.example.thebeginningv2.repository.data_types.xml.PakModeCommon;
import com.example.thebeginningv2.repository.data_types.xml.PakModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.PakRoot;
import com.example.thebeginningv2.repository.data_types.xml.SettingsModeCommon;
import com.example.thebeginningv2.repository.data_types.xml.SettingsModeField;
import com.example.thebeginningv2.repository.data_types.xml.SettingsModeRoot;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseCreator;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeCreator;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;
import com.example.thebeginningv2.repository.managers.xml_managers.PakModeParser;
import com.example.thebeginningv2.repository.managers.xml_managers.PakParser;
import com.example.thebeginningv2.repository.managers.xml_managers.SettingsModeParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Константин on 04.07.2017.
 */

public class InventoriesDomain extends DownloadingDataDomain implements InventoriesModel.DomainInterface, CreateInventoryModel.DomainInterface,
        EditInventoryModel.DomainInterface, RefreshInventoryModel.DomainInterface{
    private FileManager fileManager;

    public InventoriesDomain(PathManager pathManager, FileManager fileManager){
        super(pathManager);
        this.fileManager = fileManager;
    }

    @Override
    public void initialize() {
        if(pathManager.getLevel() == PathManager.THIRD_LEVEL) pathManager.moveBackward();
    }

    @Override
    public List<Inventory> getInventories() {
        File[] files = new File(pathManager.getDataPath()).listFiles();

        List<Inventory> inventories = new ArrayList<>();
        if(files != null){
            for (File i: files){
                String name = i.getName();
                int state, mode;

                try {
                    File file = new File(pathManager.getDataChildContentPath(name, PathManager.MODE));
                    DataModeSystemData systemData = new DataModeParser(file).geDataModeSystemData();
                    state = systemData.getState().equals(DataModeSystemData.SENT) ? Inventory.SENT : Inventory.IN_PROGRESS;
                    mode = systemData.getMode().equals(DataModeSystemData.WITH_ACCOUNTING_DATA) ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA;
                }
                catch (Exception e){
                    continue;
                }

                inventories.add(new Inventory(name, state, mode));
            }
        }

        return inventories;
    }

    @Override
    public void toInProgress(String inventoryName) throws IOException, XmlPullParserException {
        File file = new File(pathManager.getDataChildContentPath(inventoryName, PathManager.MODE));

        DataModeRoot dataModeRoot = new DataModeParser(file).getDataModeRoot();
        dataModeRoot.getSystemData().setState(DataModeSystemData.IN_PROGRESS);

        new DataModeCreator().create(dataModeRoot, file);
    }

    @Override
    public void delete(String inventoryName) {
        File file = new File(pathManager.getDataChildPath(inventoryName));
        fileManager.delete(file);
    }

    @Override
    public AccountingData getAccountingData() {
        File mode = new File(pathManager.getSettingsPathChild(PathManager.MODE));
        if(mode.exists()){
            try {
                File pakMode = new File(pathManager.getSettingsPathChild(PathManager.PAK_MODE));
                if(!pakMode.exists()){
                    SettingsModeCommon settingsModeCommon = new SettingsModeParser(mode).getSettingsModeCommon();
                    pakMode = new File(pathManager.getSettingsPathChild(settingsModeCommon.getDbPatternFile()));
                    if(!pakMode.exists()) return new AccountingData(AccountingData.NO_ACCOUNTING_DATA, null);
                }

                File pak = new File(pathManager.getSettingsPathChild(PathManager.PAK));
                if(!pak.exists()){
                    PakModeCommon pakModeCommon = new PakModeParser(pakMode).getPakModeCommon();
                    pak = new File(pathManager.getSettingsPathChild(pakModeCommon.getDbDataFile()));
                    if(!pak.exists()) return new AccountingData(AccountingData.NO_ACCOUNTING_DATA, null);
                }

                PakCommon pakCommon = new PakParser(pak).getPakCommon();
                String date = pakCommon.getDateOfDownload();
                if(date == null) return new AccountingData(AccountingData.ACCOUNTING_DATA_DOWNLOADED, null);
                else return new AccountingData(AccountingData.ACCOUNTING_DATA_DOWNLOADED_WITH_DATE, date);
            }
            catch (Exception e){
                return new AccountingData(AccountingData.NO_ACCOUNTING_DATA, null);
            }

        }
        else return new AccountingData(AccountingData.NO_ACCOUNTING_DATA, null);
    }

    @Override
    public void deleteAccountingData() {
        File file = new File(pathManager.getSettingsPath());
        fileManager.deleteContent(file);
    }

    @Override
    public void openInventory(String inventoryName) {
        pathManager.moveForward(inventoryName);
    }

    @Override
    public CreateInventoryPattern getCreateInventoryPattern() {
        try {
            SettingsModeRoot root = new SettingsModeParser(new File(pathManager.getSettingsPathChild(PathManager.MODE))).getSettingsModeRoot();

            SettingsModeCommon common = root.getCommon();
            String dbPatternFile = common.getDbPatternFile(), timeOfShift = common.getTimeOfShift();

            String divideByDates = common.getDivideByDates(), rewriteCatalogMode = common.getRewriteCatalogMode();
            String catalogOfSync = "-", dateOfSync = "-";

            CreateInventorySettings inventorySettings = new CreateInventorySettings(
                    common.getTitle(),
                    dbPatternFile != null ? dbPatternFile : PathManager.PAK_MODE,
                    timeOfShift != null ? timeOfShift : "00:00",
                    divideByDates != null && divideByDates.equals("1"),
                    rewriteCatalogMode != null && rewriteCatalogMode.equals("1"),
                    catalogOfSync,
                    dateOfSync);

            List<SettingsModeField> fields = root.getFields(), pakets = root.getPakets();
            List<InventoryParameter> inventoryParameters = new ArrayList<>(fields.size() + pakets.size());

            for (SettingsModeField i: fields) inventoryParameters.add(getInventoryParameter(i, false));
            for (SettingsModeField i: pakets) inventoryParameters.add(getInventoryParameter(i, true));

            return new CreateInventoryPattern(inventorySettings, inventoryParameters);
        }
        catch (Exception e){
            CreateInventorySettings inventorySettings = new CreateInventorySettings("Создание инвентаризации",
                    "-",
                    "00:00",
                    false,
                    false,
                    "-",
                    "-");

            List<InventoryParameter> inventoryParameters = new ArrayList<>();
            inventoryParameters.add(new InventoryParameter("Название инвентаризации", "name",
                    null, InventoryParameter.STRING, true, true, false, false));

            return new CreateInventoryPattern(inventorySettings, inventoryParameters);
        }
    }

    private InventoryParameter getInventoryParameter(SettingsModeField field, boolean isInPakets) throws Exception {
        String tag = field.getTag();

        if (tag == null) throw new Exception("No tag");

        String title = field.getTitle(), defaultValue = field.getDefaultValue();

        String partOfName = field.getPartOfName(), required = field.getRequired(), editable = field.getEditable();

        String type = field.getType();

        return new InventoryParameter(title, tag, defaultValue, getType(type),
                partOfName != null && partOfName.equals("1"), required != null && required.equals("1"), editable != null && editable.equals("1"), isInPakets);
    }

    private int getType(String type){
        if(type != null){
            switch (type){
                case SettingsModeField.STRING:
                    return InventoryParameter.STRING;
                case SettingsModeField.INTEGER:
                    return InventoryParameter.INTEGER;
                case SettingsModeField.BOOLEAN:
                    return InventoryParameter.BOOLEAN;
                case SettingsModeField.DATE:
                    return InventoryParameter.DATE;
                case SettingsModeField.FLOAT:
                    return InventoryParameter.FLOAT;
                default:
                    return InventoryParameter.STRING;
            }
        }
        else return InventoryParameter.STRING;
    }

    @Override
    public InventoryCreationResult createInventory(CreateInventoryPattern inventoryPattern) throws IOException, XmlPullParserException {
        CreateInventorySettings inventorySettings = inventoryPattern.getInventorySettings();

        String name = "";
        List<InventoryParameter> inventoryParameters = inventoryPattern.getInventoryParameters();

        List<Element> mainData = new ArrayList<>(), paketsData = new ArrayList<>();
        for(InventoryParameter i: inventoryParameters) {
            String value = i.getValue();
            if(value == null || value.replace(" ", "").length() == 0) value = "-";

            if(i.isPartOfName()) name += value;

            Element element = new Element(i.getTag(), value);

            if(i.isInPakets()) paketsData.add(element);
            else mainData.add(element);
        }

        Date date = Calendar.getInstance().getTime();

        name += ", " + new SimpleDateFormat("dd-MM-yyyy").format(date);
        new File(pathManager.getDataPath(), name).mkdir();

        String dbPatternFile = inventorySettings.getDbPatternFile();
        List<String> errors = createDatabase(name, dbPatternFile);

        DataModeAdditionalData additionalData = new DataModeAdditionalData(dbPatternFile,
                inventorySettings.isDivideByDates() ? "1" : "0",
                inventorySettings.getTimeOfShift(),
                inventorySettings.isRewriteCatalogMode() ? "1" : "0");

        DataModeSystemData systemData = new DataModeSystemData(DataModeSystemData.IN_PROGRESS, errors == null ? DataModeSystemData.WITH_ACCOUNTING_DATA : DataModeSystemData.WITHOUT_ACCOUNTING_DATA
                , name, new SimpleDateFormat("dd-MM-yyyy HH-mm").format(date),
                inventorySettings.getCatalogOfSync(), inventorySettings.getDateOfSync());

        DataModeRoot dataModeRoot = new DataModeRoot(additionalData, mainData, systemData, paketsData);
        new DataModeCreator().create(dataModeRoot, new File(pathManager.getDataChildContentPath(name, PathManager.MODE)));

        return new InventoryCreationResult(new Inventory(name, Inventory.IN_PROGRESS, errors == null ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA), errors);
    }

    private List<String> createDatabase(String name, String dbPatternFile) throws IOException, XmlPullParserException {
        File pakMode = new File(pathManager.getSettingsPathChild(dbPatternFile));
        if(!pakMode.exists()) pakMode = new File(pathManager.getSettingsPathChild(PathManager.PAK_MODE));

        DatabaseCreator databaseCreator = new DatabaseCreator(pathManager.getDataChildContentPath(name, PathManager.DB));

        databaseCreator.createAccountedRecordsTable();

        PakModeRoot pakModeRoot;
        PakModeCommon pakModeCommon;

        try {
            pakModeRoot = new PakModeParser(pakMode).getPakModeRoot();
            pakModeCommon = pakModeRoot.getCommon();
        }
        catch (Exception e){
            List<String> errors = new ArrayList<>();

            errors.add(e.getMessage());

            return errors;
        }

        boolean containsQuantityField = pakModeCommon.containsQuantityField(),
                containsSearchFields = pakModeCommon.containsSearchFileds();

        if(!containsQuantityField || !containsSearchFields){
            List<String> errors = new ArrayList<>();

            if(!containsQuantityField) errors.add("No quantity field found. pak_mode.xml must contain one copy of it.");
            if(!containsSearchFields) errors.add("No search fields found. pak_mode.xml must contain at least on copy of any of those fields: barcode, RFID, name or number.");

            databaseCreator.close();

            return errors;
        }

        List<DatabaseCreator.ColumnRow> columnRows = pakModeRoot.getColumns();
        databaseCreator.createColumnsTable(columnRows);

        File pak = new File(pathManager.getSettingsPathChild(pakModeCommon.getDbDataFile()));
        if(!pak.exists()) pak = new File(pathManager.getSettingsPathChild(PathManager.PAK));

        List<String> columns = new ArrayList<>(columnRows.size());
        for (DatabaseCreator.ColumnRow i: columnRows) columns.add(i.getName());

        PakRoot pakRoot = new PakParser(pak).getPakRoot(columns);
        databaseCreator.createAccountingDataTable(pakModeRoot.getFields(), pakModeCommon, pakRoot.getFields());

        databaseCreator.close();

        return null;
    }

    @Override
    public EditInventoryPattern getEditInventoryPattern(String inventoryName) throws Exception {
        DataModeRoot dataModeRoot = new DataModeParser(new File(pathManager.getDataChildContentPath(inventoryName, PathManager.MODE))).getDataModeRoot();

        DataModeAdditionalData additionalData = dataModeRoot.getAdditionalData();
        DataModeSystemData systemData = dataModeRoot.getSystemData();

        EditInventorySettings editInventorySettings = new EditInventorySettings(additionalData.getDbPatternFile(),
                additionalData.getTimeOfShift(),
                additionalData.getDivideByDates().equals("1"),
                additionalData.getRewriteCatalogMode().equals("1"),
                systemData.getState().equals(DataModeSystemData.SENT) ? Inventory.SENT : Inventory.IN_PROGRESS,
                systemData.getMode().equals(DataModeSystemData.WITH_ACCOUNTING_DATA) ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA,
                systemData.getCatalogOfCreation(),
                systemData.getDateOfCreation(),
                systemData.getCatalogOfSync(),
                systemData.getDateOfSync());

        SettingsModeRoot settingsModeRoot = new SettingsModeParser(new File(pathManager.getSettingsPathChild(PathManager.MODE))).getSettingsModeRoot();

        List<SettingsModeField> fields = settingsModeRoot.getFields(), pakets = settingsModeRoot.getPakets();

        List<Element> mainData = dataModeRoot.getMainData(), paketsData = dataModeRoot.getPaketsData();

        for (SettingsModeField i: fields){
            for (Element j: mainData){
                if(i.getTag().equals(j.getTag())) i.setDefaultValue(j.getValue());
            }
        }

        for (SettingsModeField i: pakets){
            for (Element j: paketsData){
                if(i.getTag().equals(j.getTag())) i.setDefaultValue(j.getValue());
            }
        }

        List<InventoryParameter> inventoryParameters = new ArrayList<>(fields.size() + pakets.size());

        for (SettingsModeField i: fields) inventoryParameters.add(getInventoryParameter(i, false));
        for (SettingsModeField i: pakets) inventoryParameters.add(getInventoryParameter(i, true));

        for (InventoryParameter i: inventoryParameters) i.setValue(i.getDefaultValue());

        return new EditInventoryPattern(editInventorySettings, inventoryParameters);
    }

    @Override
    public void editInventoryPattern(String inventoryName, EditInventoryPattern editInventoryPattern) throws IOException {
        EditInventorySettings settings = editInventoryPattern.getSettings();

        DataModeAdditionalData additionalData = new DataModeAdditionalData(settings.getDbPatternFile(),
                settings.isDivideByDates() ? "1" : "0",
                settings.getTimeOfShift(),
                settings.isRewriteCatalogMode() ? "1" : "0");

        DataModeSystemData systemData = new DataModeSystemData(settings.getState() == Inventory.SENT ? DataModeSystemData.SENT : DataModeSystemData.IN_PROGRESS,
                settings.getMode() == Inventory.WITH_ACCOUNTING_DATA ? DataModeSystemData.WITH_ACCOUNTING_DATA : DataModeSystemData.WITHOUT_ACCOUNTING_DATA,
                settings.getDateOfCreation(),
                settings.getCatalogOfCreation(),
                settings.getCatalogOfSync(),
                settings.getDateOfSync());

        List<InventoryParameter> inventoryParameters = editInventoryPattern.getInventoryParameters();

        List<Element> mainData = new ArrayList<>(), paketsData = new ArrayList<>();
        for (InventoryParameter i: inventoryParameters){
            String value = i.getValue();
            if(value == null || value.replace(" ", "").length() == 0) value = "-";

            Element element = new Element(i.getTag(), value);

            if(i.isInPakets()) paketsData.add(element);
            else mainData.add(element);
        }

        DataModeRoot dataModeRoot = new DataModeRoot(additionalData, mainData, systemData, paketsData);

        new DataModeCreator().create(dataModeRoot, new File(pathManager.getDataChildContentPath(inventoryName, PathManager.MODE)));
    }

    @Override
    public InventoryRefreshingResult refreshInventory(String inventoryName) throws IOException, XmlPullParserException {
        File mode = new File(pathManager.getSettingsPathChild(PathManager.MODE));
        SettingsModeCommon common = new SettingsModeParser(mode).getSettingsModeCommon();

        File pakMode = new File(pathManager.getSettingsPathChild(common.getDbPatternFile()));
        if(!pakMode.exists()) pakMode = new File(pathManager.getSettingsPathChild(PathManager.PAK_MODE));

        PakModeRoot pakModeRoot = new PakModeParser(pakMode).getPakModeRoot();
        PakModeCommon pakModeCommon = pakModeRoot.getCommon();

        boolean containsQuantityField = pakModeCommon.containsQuantityField(),
                containsSearchFields = pakModeCommon.containsSearchFileds();

        if(!containsQuantityField || !containsSearchFields){
            List<String> errors = new ArrayList<>();

            if(!containsQuantityField) errors.add("No quantity field found. pak_mode.xml must contain one copy of it.");
            if(!containsSearchFields) errors.add("No search fields found. pak_mode.xml must contain at least on copy of any of those fields: barcode, RFID, name or number.");

            return new InventoryRefreshingResult(errors, null);
        }

        DatabaseCreator databaseCreator = new DatabaseCreator(pathManager.getDataChildContentPath(inventoryName, PathManager.DB));

        List<DatabaseCreator.ColumnRow> columnRows = pakModeRoot.getColumns();
        if(databaseCreator.tableExists(ColumnsTable.TABLE_NAME)) databaseCreator.deleteTable(ColumnsTable.TABLE_NAME);
        databaseCreator.createColumnsTable(columnRows);

        File pak = new File(pathManager.getSettingsPathChild(pakModeCommon.getDbDataFile()));
        if (!pak.exists()) pak = new File(pathManager.getSettingsPathChild(PathManager.PAK));

        List<String> columns = new ArrayList<>(columnRows.size());
        for (DatabaseCreator.ColumnRow i: columnRows) columns.add(i.getName());

        PakRoot pakRoot = new PakParser(pak).getPakRoot(columns);

        if(databaseCreator.tableExists(AccountingDataTable.TABLE_NAME)) databaseCreator.deleteTable(AccountingDataTable.TABLE_NAME);
        databaseCreator.createAccountingDataTable(pakModeRoot.getFields(), pakModeCommon, pakRoot.getFields());

        databaseCreator.close();

        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildContentPath(inventoryName, PathManager.DB), null);
        DatabaseManager databaseManager = new DatabaseManager(db);

        List<String> ambiguousRecords = new ArrayList<>();

        List<AccountedRecord> accountedRecords = databaseManager.getAccountedRecordsAsUnidentified();
        for (AccountedRecord i: accountedRecords){
            List<Integer> ids = databaseManager.findIdentifiedRecordIds(i.getBarcode(), true, "2");
            if(ids.size() != 0){
                if(ids.size() != 1){
                    ambiguousRecords.add(i.getBarcode());
                    databaseManager.updateAccountedRecordId(i.getId(), i.getBarcode(), AccountedRecord.NO_ID);
                }
                else {
                    int newId = ids.get(0);
                    databaseManager.updateAccountedRecordIdWithoutTimestamp(i.getId(), i.getBarcode(), newId);
                    databaseManager.markAsWithoutTimestamp(newId, true);
                }
            }
            else databaseManager.updateAccountedRecordIdWithoutTimestamp(i.getId(), i.getBarcode(), AccountedRecord.NO_ID);
        }

        databaseManager.close();

        return new InventoryRefreshingResult(null, ambiguousRecords);
    }
}
