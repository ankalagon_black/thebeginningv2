package com.example.thebeginningv2.inventories.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.domains.InventoriesDomain;
import com.example.thebeginningv2.inventories.adapters.InventoryParametersAdapter;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.data_types.InventoryCreationResult;
import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
import com.example.thebeginningv2.inventories.models.CreateInventoryModel;

/**
 * Created by Константин on 05.07.2017.
 */

public class CreateInventoryDialog extends SupportModelDialog<InventoriesDomain, CreateInventoryModel> implements CreateInventoryModel.ViewInterface{
    private Toolbar toolbar;
    private RecyclerView recyclerView;

    private InventoryParametersAdapter adapter;

    private View progressBar;
    private AlertDialog processDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventories_create_inventory, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateInventoryDialog.this.dismiss();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.inventory_parameters);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Button button = (Button) view.findViewById(R.id.create);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter.allRequiredParametersFilled()){

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(false);

                    progressBar = LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null);

                    processDialog = builder.setTitle(R.string.inventory_creation_process).setView(progressBar).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getModel().stopCreation();
                        }
                    }).create();

                    processDialog.show();

                    getModel().createInventory();
                }
                else Toast.makeText(getActivity(), R.string.inventory_parameters_are_not_valid, Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    @Override
    protected CreateInventoryModel createModel() {
        return new CreateInventoryModel(this, getDomain());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getModel().getInventoryPattern();
    }

    @Override
    public void onInventoryPatternReceived(CreateInventoryPattern inventoryPattern) {
        String title = inventoryPattern.getInventorySettings().getTitle();
        if(title != null) toolbar.setTitle(title);
        else toolbar.setTitle(R.string.create_inventory_default_title);

        adapter = new InventoryParametersAdapter(inventoryPattern.getInventoryParameters(), InventoryParametersAdapter.CREATE);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onGetInventoryPatternError(Throwable e) {
        Toast.makeText(getActivity(), R.string.mode_file_loading_error, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onInventoryCreationFinished(InventoryCreationResult inventoryCreationResult) {
        dismiss();
        processDialog.dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder = builder.setTitle(R.string.inventory_creation_completed);


        if(inventoryCreationResult.getMode() == Inventory.WITH_ACCOUNTING_DATA) builder = builder.setMessage(getString(R.string.inventory_created_message));
        else {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.inventory_module_inventory_creation_issues, null);

            TextView message = (TextView) view.findViewById(R.id.message);
            message.setText(R.string.inventory_created_with_errors);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, inventoryCreationResult.getErrors());

            ListView listView = (ListView) view.findViewById(R.id.list);
            listView.setAdapter(arrayAdapter);

            builder = builder.setView(view);
        }

        builder.setPositiveButton(R.string.ok, null).
                create().
                show();
    }

    @Override
    public void onInventoryCreationError(Throwable e) {
        processDialog.setTitle(R.string.inventory_creation_failed);
        //// TODO: 05.07.2017 getSettings
        progressBar.setVisibility(View.GONE);
        processDialog.setMessage(e.getMessage());
        Button button = processDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        button.setText(R.string.ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processDialog.dismiss();
                CreateInventoryDialog.this.dismiss();
            }
        });
    }
}
