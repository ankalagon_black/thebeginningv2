package com.example.thebeginningv2.inventories.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventories.data_types.Result;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 13.07.2017.
 */

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ViewHolder> {
    private List<Result> results;

    public ResultsAdapter(List<Result> results){
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_result_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(results.get(position));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView column, sum;

        public ViewHolder(View itemView) {
            super(itemView);
            column = (TextView) itemView.findViewById(R.id.title);
            sum = (TextView) itemView.findViewById(R.id.sum);
        }

        public void setData(Result result){
            column.setText(result.getColumn());
            sum.setText(String.format(Locale.US, itemView.getContext().getString(R.string.float_2_decimal), result.getSum()));
        }
    }
}
