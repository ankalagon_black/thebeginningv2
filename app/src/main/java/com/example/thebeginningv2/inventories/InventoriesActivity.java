package com.example.thebeginningv2.inventories;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v2.activities.AppCompatDomainActivity;
import com.example.thebeginningv2.inventories.dialogs.AboutInventoryDialog;
import com.example.thebeginningv2.inventories.dialogs.RefreshInventoryDialog;
import com.example.thebeginningv2.inventories.dialogs.SendingDataDialog;
import com.example.thebeginningv2.inventories.domains.InventoriesDomain;
import com.example.thebeginningv2.inventories.adapters.InventoryAdapter;
import com.example.thebeginningv2.inventories.data_types.AccountingData;
import com.example.thebeginningv2.inventories.data_types.Inventory;
import com.example.thebeginningv2.inventories.dialogs.CreateInventoryDialog;
import com.example.thebeginningv2.inventories.dialogs.EditInventoryDialog;
import com.example.thebeginningv2.inventories.models.EditInventoryModel;
import com.example.thebeginningv2.inventories.models.InventoriesModel;
import com.example.thebeginningv2.inventories.models.RefreshInventoryModel;
import com.example.thebeginningv2.inventory_module.dialogs.DownloadingDataDialog;
import com.example.thebeginningv2.inventory_process.WithAccountingDataInventoryActivity;
import com.example.thebeginningv2.inventory_process_without_data.WithoutAccountingDataInventoryActivity;
import com.example.thebeginningv2.repository.managers.FileManager;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 04.07.2017.
 */

public class InventoriesActivity extends AppCompatDomainActivity<InventoriesDomain>
        implements InventoriesModel.ViewInterface, InventoryAdapter.Interface{

    private RecyclerView recyclerView;
    private InventoryAdapter adapter;
    
    private TextView state;
    private Button download, delete;
    
    private InventoriesModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.inventory_module_inventories);

        setTitle(R.string.inventories_title);
        getSupportActionBar().setHomeButtonEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.inventories_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final Button createInventory = (Button) findViewById(R.id.create_inventory);
        createInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateInventoryDialog createInventoryDialog = new CreateInventoryDialog();
                createInventoryDialog.show(getSupportFragmentManager(), createInventoryDialog.getTag());
            }
        });
        
        state = (TextView) findViewById(R.id.state);
        
        download = (Button) findViewById(R.id.download);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadingDataDialog dialog = new DownloadingDataDialog();
                dialog.show(getFragmentManager(), dialog.getTag());
            }
        });
        
        delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.deleteAccountingData();
            }
        });
        
        model = new InventoriesModel(this, getDomain());
        model.subscribe();
    }

    @Override
    protected void onStart() {
        super.onStart();

        model.initialize();

        model.getInventories();
        model.getAccountingData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        model.unsubscribe();
        model.disposeAllBackgroundTasks();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.inventory_module_inventories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.send_all:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.action_confirmation).
                        setMessage(R.string.send_all_confirmation_message).
                        setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                               SendingDataDialog sendingDataDialog = new SendingDataDialog();
                                sendingDataDialog.show(getSupportFragmentManager(), sendingDataDialog.getTag());
                            }
                        }).
                        setNegativeButton(R.string.cancel, null).
                        create().
                        show();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected InventoriesDomain createDomain() {
        return new InventoriesDomain(PathManager.getIntance(), new FileManager());
    }

    @Override
    public void onInventoriesReceived(List<Inventory> inventories) {
        adapter = new InventoryAdapter(inventories, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInventoryAdded(int position) {
        adapter.notifyItemInserted(position);
    }

    @Override
    public void onInventoryRemoved(int position) {
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void onInventoryUpdated(int position) {
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onAccountingDataReceived(AccountingData accountingData) {
        switch (accountingData.getState()){
            case AccountingData.NO_ACCOUNTING_DATA:
                state.setText(R.string.no_accounting_data);
                delete.setVisibility(View.GONE);
                break;
            case AccountingData.ACCOUNTING_DATA_DOWNLOADED:
                state.setText(R.string.accounting_data_downloaded);
                delete.setVisibility(View.VISIBLE);
                break;
            case AccountingData.ACCOUNTING_DATA_DOWNLOADED_WITH_DATE:
                state.setText(String.format(Locale.US, getString(R.string.accounting_data_downloaded_with_date), accountingData.getDate()));
                delete.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPopupMenu(View view, final Inventory inventory) {
        PopupMenu popupMenu = new PopupMenu(this, view);

        if(inventory.getState() == Inventory.IN_PROGRESS){
            if(inventory.getMode() == Inventory.WITH_ACCOUNTING_DATA) popupMenu.getMenuInflater().inflate(R.menu.inventory_in_progress, popupMenu.getMenu());
            else popupMenu.getMenuInflater().inflate(R.menu.inventory_without_accounting_data_in_progress, popupMenu.getMenu());
        }
        else{
            if(inventory.getMode() == Inventory.WITH_ACCOUNTING_DATA) popupMenu.getMenuInflater().inflate(R.menu.inventory_sent, popupMenu.getMenu());
            else popupMenu.getMenuInflater().inflate(R.menu.inventory_without_accounting_data_sent, popupMenu.getMenu());
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.send:
                    case R.id.send_again:
                        AlertDialog.Builder builder = new AlertDialog.Builder(InventoriesActivity.this);
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.send_inventory_confirmation_message).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        SendingDataDialog sendingDataDialog = SendingDataDialog.getInstance(inventory);
                                        sendingDataDialog.show(getSupportFragmentManager(), sendingDataDialog.getTag());
                                    }
                                }).
                                create().
                                show();
                        return true;

                    case R.id.back_to_in_progress:
                        builder = new AlertDialog.Builder(InventoriesActivity.this);
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.back_to_in_progress_message).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        model.toInProgress(inventory);
                                    }
                                }).
                                create().
                                show();
                        return true;

                    case R.id.more_about_inventory:
                        AboutInventoryDialog dialog = AboutInventoryDialog.getInstance(inventory);

                        dialog.show(getSupportFragmentManager(), dialog.getTag());
                        return true;

                    case R.id.inventory_parameters:
                        EditInventoryDialog editInventoryDialog = new EditInventoryDialog();

                        Bundle bundle = new Bundle();
                        bundle.putString(EditInventoryModel.INVENTORY_NAME_KEY, inventory.getName());

                        editInventoryDialog.setArguments(bundle);
                        editInventoryDialog.show(getSupportFragmentManager(), editInventoryDialog.getTag());
                        return true;

                    case R.id.refresh_db:
                        builder = new AlertDialog.Builder(InventoriesActivity.this);
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.refresh_confirmation_message).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.refresh, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        RefreshInventoryDialog refreshInventoryDialog = new RefreshInventoryDialog();

                                        Bundle bundle1 = new Bundle();
                                        bundle1.putString(RefreshInventoryModel.INVENTORY_NAME_KEY, inventory.getName());

                                        refreshInventoryDialog.setArguments(bundle1);
                                        refreshInventoryDialog.show(getSupportFragmentManager(), refreshInventoryDialog.getTag());
                                    }
                                }).
                                create().
                                show();
                        return true;

                    case R.id.remove:
                        builder = new AlertDialog.Builder(InventoriesActivity.this);
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.delete_inventory_confirmation_message).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        model.delete(inventory);
                                    }
                                }).
                                create().
                                show();
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }

    @Override
    public void showInventory(Inventory inventory) {
        model.openInventory(inventory);
        if(inventory.getMode() == Inventory.WITH_ACCOUNTING_DATA) startActivity(new Intent(this, WithAccountingDataInventoryActivity.class));
        else startActivity(new Intent(this, WithoutAccountingDataInventoryActivity.class));
    }
}
