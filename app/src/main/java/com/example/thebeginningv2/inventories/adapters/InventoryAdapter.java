package com.example.thebeginningv2.inventories.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventories.data_types.Inventory;

import java.util.List;

/**
 * Created by Константин on 20.06.2017.
 */

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.Holder> {
    private List<Inventory> inventories;
    private Interface anInterface;

    public InventoryAdapter(List<Inventory> inventories, Interface anInterface) {
        this.inventories = inventories;
        this.anInterface = anInterface;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_module_inventory, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setHolder(inventories.get(position));
    }

    @Override
    public int getItemCount() {
        return inventories.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        private TextView name, state, mode;
        private ImageButton imageButton;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.inventory_name);
            state = (TextView) itemView.findViewById(R.id.inventory_state);
            mode = (TextView) itemView.findViewById(R.id.inventory_mode);
            imageButton = (ImageButton) itemView.findViewById(R.id.functions);
        }

        public void setHolder(final Inventory inventory){
            Context context = itemView.getContext();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showInventory(inventory);
                }
            });

            name.setText(inventory.getName());

            switch (inventory.getState()){
                case Inventory.IN_PROGRESS:
                    state.setText(context.getString(R.string.inventory_state_in_progress));
                    break;
                case Inventory.SENT:
                    state.setText(context.getString(R.string.inventory_state_sent));
                    break;
            }

            switch (inventory.getMode()){
                case Inventory.WITH_ACCOUNTING_DATA:
                    mode.setText(R.string.with_accounting_data);
                    break;
                case Inventory.WITHOUT_ACCOUNTING_DATA:
                    mode.setText(R.string.without_accounting_data);
                    break;
            }

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPopupMenu(imageButton, inventory);
                }
            });
        }
    }

    public interface Interface{
        void showPopupMenu(View view, Inventory inventory);
        void showInventory(Inventory inventory);
    }
}
