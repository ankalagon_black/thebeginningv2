package com.example.thebeginningv2.inventories.data_types;

import java.util.List;

/**
 * Created by Константин on 05.07.2017.
 */

public class InventoryCreationResult {
    private Inventory inventory;
    private List<String> errors = null;

    public InventoryCreationResult(Inventory inventory, List<String> errors){
        this.inventory = inventory;
        this.errors = errors;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public int getMode() {
        return inventory.getMode();
    }

    public List<String> getErrors() {
        return errors;
    }
}
