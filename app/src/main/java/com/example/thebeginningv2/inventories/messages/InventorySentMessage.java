package com.example.thebeginningv2.inventories.messages;

import com.example.thebeginningv2.inventories.data_types.Inventory;

/**
 * Created by apple on 24.07.17.
 */

public class InventorySentMessage {
    private Inventory inventory;

    public InventorySentMessage(Inventory inventory){
        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
