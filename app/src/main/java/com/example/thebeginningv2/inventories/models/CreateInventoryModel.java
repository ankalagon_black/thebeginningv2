package com.example.thebeginningv2.inventories.models;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.threads.TaskExecutor;
import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
import com.example.thebeginningv2.inventories.data_types.InventoryCreationResult;

import org.greenrobot.eventbus.EventBus;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 05.07.2017.
 */

public class CreateInventoryModel extends BaseModel {
    private CreateInventoryPattern inventoryPattern;

    private ViewInterface viewInterface;
    private DomainInterface domainInterface;

    private Disposable getInventoryParameters, createInventory;

    public CreateInventoryModel(ViewInterface viewInterface, DomainInterface domainInterface){
        this.viewInterface = viewInterface;
        this.domainInterface = domainInterface;
    }

    public void getInventoryPattern(){
        DisposableSingleObserver<CreateInventoryPattern> observer = new DisposableSingleObserver<CreateInventoryPattern>() {
            @Override
            public void onSuccess(CreateInventoryPattern value) {
                inventoryPattern = value;
                viewInterface.onInventoryPatternReceived(inventoryPattern);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onGetInventoryPatternError(e);
            }
        };

        getInventoryParameters = observer;

        Single.fromCallable(new Callable<CreateInventoryPattern>() {
            @Override
            public CreateInventoryPattern call() throws Exception {
                return domainInterface.getCreateInventoryPattern();
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void createInventory(){
        DisposableSingleObserver<InventoryCreationResult> observer = new DisposableSingleObserver<InventoryCreationResult>() {
            @Override
            public void onSuccess(InventoryCreationResult value) {
                EventBus.getDefault().post(value.getInventory());
                viewInterface.onInventoryCreationFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onInventoryCreationError(e);
            }
        };

        createInventory = observer;

        Single.fromCallable(new Callable<InventoryCreationResult>() {
            @Override
            public InventoryCreationResult call() throws Exception {
                return domainInterface.createInventory(inventoryPattern);
            }
        }).subscribeOn(Schedulers.from(TaskExecutor.getInstance())).
                observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public void stopCreation(){
        createInventory.dispose();
    }

    @Override
    public void disposeAllBackgroundTasks() {
        if(getInventoryParameters != null && !getInventoryParameters.isDisposed()) getInventoryParameters.dispose();
        if(createInventory != null && !createInventory.isDisposed()) createInventory.dispose();
    }

    public interface ViewInterface{
        void onInventoryPatternReceived(CreateInventoryPattern inventoryPattern);
        void onGetInventoryPatternError(Throwable e);

        void onInventoryCreationFinished(InventoryCreationResult inventoryCreationResult);
        void onInventoryCreationError(Throwable e);
    }

    public interface DomainInterface{
        CreateInventoryPattern getCreateInventoryPattern() throws Exception;
        InventoryCreationResult createInventory(CreateInventoryPattern inventoryPattern) throws IOException, XmlPullParserException;
    }
}
