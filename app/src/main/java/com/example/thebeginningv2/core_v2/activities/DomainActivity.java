package com.example.thebeginningv2.core_v2.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Константин on 12.07.2017.
 */

public abstract class DomainActivity<D> extends Activity implements GetDomain<D>{
    private D domain = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeRepository();
        domain = createDomain();
    }

    protected void initializeRepository(){

    }

    protected abstract D createDomain();

    public D getDomain(){
        return domain;
    }
}
