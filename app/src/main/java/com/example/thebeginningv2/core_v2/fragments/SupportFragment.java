package com.example.thebeginningv2.core_v2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.models.Model;
import com.example.thebeginningv2.core_v2.presenters.BasePresenter;

/**
 * Created by Константин on 08.07.2017.
 */

public abstract class SupportFragment<D,M extends BaseModel,P extends BasePresenter> extends SupportPresenterFragment<D,P> {
    private M model;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = createModel();
        if(model instanceof Model) ((Model) model).subscribe();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(model instanceof Model) ((Model) model).unsubscribe();
        model.disposeAllBackgroundTasks();
    }

    protected abstract M createModel();

    public M getModel() {
        return model;
    }
}
