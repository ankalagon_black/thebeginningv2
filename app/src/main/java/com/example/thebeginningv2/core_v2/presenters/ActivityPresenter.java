package com.example.thebeginningv2.core_v2.presenters;

import android.os.Bundle;
import android.os.Parcelable;


/**
 * Created by Константин on 04.07.2017.
 */

public abstract class ActivityPresenter<S extends Parcelable> extends BaseActivityPresenter {
    private S stateModel = null;

    @Override
    public void onCreate(Bundle bundle, Bundle savedInstanceState) {
        if(savedInstanceState != null) stateModel = savedInstanceState.getParcelable(STATE_MODEL);
        if(!hasStateModel()) createStateModel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(STATE_MODEL, stateModel);
    }

    protected abstract S createStateModel();

    protected boolean hasStateModel(){
        return stateModel != null;
    }

    protected S getStateModel(){
        return stateModel;
    }
}
