package com.example.thebeginningv2.core_v2.models;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class BaseModel {
    abstract public void disposeAllBackgroundTasks();
}
