package com.example.thebeginningv2.core_v2.fragments;

import android.support.v4.app.Fragment;

import com.example.thebeginningv2.core_v2.activities.AppCompatDomainActivity;

/**
 * Created by Константин on 04.07.2017.
 */

public class SupportDomainFragment<D> extends Fragment {
    public D getDomain(){
        return ((AppCompatDomainActivity<D>) getActivity()).getDomain();
    }
}
