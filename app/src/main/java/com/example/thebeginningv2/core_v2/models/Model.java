package com.example.thebeginningv2.core_v2.models;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class Model extends BaseModel {
    public void subscribe(){
        EventBus.getDefault().register(this);
    }

    public void unsubscribe(){
        EventBus.getDefault().unregister(this);
    }
}
