package com.example.thebeginningv2.core_v2.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class AppCompatDomainActivity<D> extends AppCompatActivity implements GetDomain<D>{
    private D domain = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeRepository();
        domain = createDomain();
    }

    protected void initializeRepository(){

    }

    protected abstract D createDomain();

    public D getDomain(){
        return domain;
    }
}
