package com.example.thebeginningv2.core_v2.activities;

/**
 * Created by Константин on 12.07.2017.
 */

public interface GetDomain<D> {
    D getDomain();
}
