package com.example.thebeginningv2.core_v2.presenters;

import android.os.Bundle;
import android.os.Parcelable;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class Presenter<S extends Parcelable> extends BasePresenter {
    private S stateModel = null;

    @Override
    public void onActivityCreated(Bundle bundle, Bundle savedInstanceState) {
        if(savedInstanceState != null) stateModel = savedInstanceState.getParcelable(STATE_MODEL);
        if(!hasStateModel()) stateModel = createStateModel(bundle);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(STATE_MODEL, stateModel);
    }

    protected abstract S createStateModel(Bundle bundle);

    protected boolean hasStateModel(){
        return stateModel != null;
    }

    protected S getStateModel(){
        return stateModel;
    }
}
