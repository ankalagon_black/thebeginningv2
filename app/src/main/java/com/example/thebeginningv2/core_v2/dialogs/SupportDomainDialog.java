package com.example.thebeginningv2.core_v2.dialogs;

import android.support.v4.app.DialogFragment;

import com.example.thebeginningv2.core_v2.activities.AppCompatDomainActivity;
import com.example.thebeginningv2.core_v2.activities.GetDomain;

/**
 * Created by Константин on 05.07.2017.
 */

public class SupportDomainDialog<D> extends DialogFragment {
    public D getDomain(){
        return ((GetDomain<D>) getActivity()).getDomain();
    }
}
