package com.example.thebeginningv2.core_v2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.thebeginningv2.core_v2.presenters.BasePresenter;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class SupportPresenterFragment<D, P extends BasePresenter> extends SupportDomainFragment<D>{
    private P presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter = createPresenter();
        presenter.onActivityCreated(getArguments(), savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroyView();
    }

    protected abstract P createPresenter();

    protected P getPresenter(){
        return presenter;
    }
}
