package com.example.thebeginningv2.core_v2.presenters;

import android.os.Bundle;

/**
 * Created by Константин on 04.07.2017.
 */

public abstract class BaseActivityPresenter {
    public static final String STATE_MODEL = "StateModel";

    public abstract void onCreate(Bundle bundle, Bundle savedInstanceState);

    public abstract void onSaveInstanceState(Bundle outState);

    public abstract void onDestroy();
}
