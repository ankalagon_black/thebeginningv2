package com.example.thebeginningv2.core_v2.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.thebeginningv2.core_v2.fragments.SupportDomainFragment;
import com.example.thebeginningv2.core_v2.models.BaseModel;
import com.example.thebeginningv2.core_v2.models.Model;

/**
 * Created by Константин on 05.07.2017.
 */

public abstract class SupportModelDialog<D, M extends BaseModel> extends SupportDomainDialog<D> {
    private M model;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = createModel();
        if(model instanceof Model) ((Model) model).subscribe();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(model instanceof Model) ((Model) model).unsubscribe();
        model.disposeAllBackgroundTasks();
    }

    protected abstract M createModel();

    protected M getModel(){
        return model;
    }
}
