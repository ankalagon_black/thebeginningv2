package com.example.thebeginningv2.core_v2.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;

/**
 * Created by Константин on 11.07.2017.
 */

public abstract class PreferenceDomainActivity<D> extends PreferenceActivity implements GetDomain<D>{
    private D domain = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeRepository();
        domain = createDomain();
    }

    protected void initializeRepository(){

    }

    protected abstract D createDomain();

    public D getDomain(){
        return domain;
    }
}
