package com.example.thebeginningv2.core_v2.dialogs;

import android.app.DialogFragment;

import com.example.thebeginningv2.core_v2.activities.GetDomain;

/**
 * Created by Константин on 11.07.2017.
 */

public abstract class DomainDialog<D> extends DialogFragment {
    public D getDomain(){
        return ((GetDomain<D>) getActivity()).getDomain();
    }
}
