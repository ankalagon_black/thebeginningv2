package com.example.thebeginningv2.core_v3.models;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 14.07.2017.
 */

public class Model extends BaseModel{
    public void registerEventBus(){
        EventBus.getDefault().register(this);
    }

    public void unregisterEventBus(){
        EventBus.getDefault().unregister(this);
    }
}
