package com.example.thebeginningv2.core_v3.dialogs;

import android.support.v4.app.DialogFragment;

import com.example.thebeginningv2.core_v3.activities.AppCompatRepositoryActivity;
import com.example.thebeginningv2.core_v3.activities.GetRepository;
import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public class SupportRepositoryDialog<R extends Repository> extends DialogFragment{
    public R getRepository(){
        return ((GetRepository<R>) getActivity()).getRepository();
    }
}
