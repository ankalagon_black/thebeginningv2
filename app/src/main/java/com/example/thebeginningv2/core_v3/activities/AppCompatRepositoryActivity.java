package com.example.thebeginningv2.core_v3.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public abstract class AppCompatRepositoryActivity<R extends Repository> extends AppCompatActivity implements GetRepository<R>{

    private R repository = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        repository = createRepository();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        repository.releaseResources();
    }

    protected abstract R createRepository();

    @Override
    public R getRepository() {
        return repository;
    }
}
