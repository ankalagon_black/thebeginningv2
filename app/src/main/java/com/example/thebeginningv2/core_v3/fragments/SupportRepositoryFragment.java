package com.example.thebeginningv2.core_v3.fragments;

import android.support.v4.app.Fragment;

import com.example.thebeginningv2.core_v3.activities.AppCompatRepositoryActivity;
import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public class SupportRepositoryFragment<R extends Repository> extends Fragment {
    public R getRepository(){
        return ((AppCompatRepositoryActivity<R>) getActivity()).getRepository();
    }
}
