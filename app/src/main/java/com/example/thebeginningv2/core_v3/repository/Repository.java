package com.example.thebeginningv2.core_v3.repository;

import android.app.Activity;

/**
 * Created by Константин on 14.07.2017.
 */

public abstract class Repository {
    private Activity activity;

    public Repository(Activity activity){
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    public abstract void releaseResources();
}
