package com.example.thebeginningv2.core_v3.activities;

import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public interface GetRepository<R extends Repository> {
    R getRepository();
}
