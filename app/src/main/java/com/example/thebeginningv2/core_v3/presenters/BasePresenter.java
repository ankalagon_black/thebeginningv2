package com.example.thebeginningv2.core_v3.presenters;

import android.os.Bundle;

/**
 * Created by Константин on 14.07.2017.
 */

public abstract class BasePresenter {
    public static final String STATE_MODEL = "StateModel";

    public abstract void onActivityCreated(Bundle bundle, Bundle savedInstanceState);

    public abstract void onSaveInstanceState(Bundle outState);

    public abstract void onDestroyView();
}
