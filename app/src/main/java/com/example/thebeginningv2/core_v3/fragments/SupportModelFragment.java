package com.example.thebeginningv2.core_v3.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.core_v3.repository.Repository;

/**
 * Created by Константин on 14.07.2017.
 */

public abstract class SupportModelFragment<R extends Repository, M extends BaseModel> extends SupportRepositoryFragment<R> {
    private M model;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        model = createModel();
        if(model instanceof Model) ((Model) model).registerEventBus();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(model instanceof Model) ((Model) model).unregisterEventBus();
        model.disposeAllBackgroundTasks();
    }

    protected abstract M createModel();

    protected M getModel(){
        return model;
    }
}
