package com.example.thebeginningv2.inventory_process.data_types;

import android.os.Parcelable;

/**
 * Created by Константин on 15.06.2017.
 */

public abstract class Record implements Parcelable {
    public abstract boolean isAccounted();

    public abstract int getId();
    public abstract String getBarcode();

    public abstract boolean hasSubtitle();
    public abstract String getSubtitle();

    public abstract float getStatedQuantity();

    public abstract boolean isGroup();
}
