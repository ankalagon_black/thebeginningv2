package com.example.thebeginningv2.inventory_process.models;

import android.widget.Toast;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.domains.DeleteAccountedRecordsDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedRemainedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 17.07.2017.
 */

public class DeleteGroupRecordsModel extends Model {
    public static final String REMAINED_RECORD_KEY = "RemainedRecordKey";

    private InventorySettings inventorySettings;

    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private RemainedRecord remainedRecord;
    private List<AccountedRecord> groupRecords = null;

    private DeleteAccountedRecordsDomain domain;

    public DeleteGroupRecordsModel(ViewInterface viewInterface, DatabaseManager databaseManager, RemainedRecord remainedRecord,
                                   DeleteAccountedRecordsDomain domain){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.remainedRecord = remainedRecord;
        this.domain = domain;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void getGroupBarcodes(){
       DisposableSingleObserver<List<ColumnValue>> observer = new DisposableSingleObserver<List<ColumnValue>>() {
           @Override
           public void onSuccess(List<ColumnValue> value) {
               List<String> groupBarcodes = new ArrayList<>(value.size());

               for (ColumnValue i: value) groupBarcodes.add(i.getValue());

               viewInterface.onGroupBarcodesReceived(groupBarcodes);

               getGroupRecords(value);
           }

           @Override
           public void onError(Throwable e) {
               viewInterface.onError(e);
           }
       };

       Single<List<ColumnValue>> single = Single.fromCallable(new Callable<List<ColumnValue>>() {
           @Override
           public List<ColumnValue> call() throws Exception {
               return databaseManager.getGroupBarcodes(remainedRecord.getId());
           }
       });

        subscribe(single,observer);
    }

    private void getGroupRecords(final List<ColumnValue> values){
        DisposableSingleObserver<List<AccountedRecord>> observer = new DisposableSingleObserver<List<AccountedRecord>>() {
            @Override
            public void onSuccess(List<AccountedRecord> value) {
                groupRecords = value;
                viewInterface.onGroupRecordsReceived(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<AccountedRecord>> single = Single.fromCallable(new Callable<List<AccountedRecord>>() {
            @Override
            public List<AccountedRecord> call() throws Exception {
                return databaseManager.findGroupAccountedRecords(values);
            }
        });

        subscribe(single, observer);
    }

    public void delete(final AccountedRecord accountedRecord){
        DisposableSingleObserver<RemainedRecord> observer = new DisposableSingleObserver<RemainedRecord>() {

            @Override
            public void onSuccess(RemainedRecord value) {
                int index  = groupRecords.indexOf(accountedRecord);
                EventBus.getDefault().post(new DeletedAccountedRecord(accountedRecord));
                EventBus.getDefault().post(new AddedRemainedRecord(value));

                groupRecords.remove(index);
                if(groupRecords.size() == 0) viewInterface.onAllRecordsDeleted();
                else viewInterface.onRecordDeleted(index);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<RemainedRecord> single = Single.fromCallable(new Callable<RemainedRecord>() {
            @Override
            public RemainedRecord call() throws Exception {
                return domain.delete(accountedRecord);
            }
        });

        subscribe(single, observer);
    }

    public void deleteAll(){
        if(groupRecords != null){
            DisposableSingleObserver<List<RemainedRecord>> observer = new DisposableSingleObserver<List<RemainedRecord>>() {
                @Override
                public void onSuccess(List<RemainedRecord> value) {
                    for (AccountedRecord i: groupRecords){
                        EventBus.getDefault().post(new DeletedAccountedRecord(i));
                    }

                    for (RemainedRecord i: value){
                        EventBus.getDefault().post(new AddedRemainedRecord(i));
                    }

                    viewInterface.onAllRecordsDeleted();
                }

                @Override
                public void onError(Throwable e) {
                    viewInterface.onError(e);
                }
            };

            Single<List<RemainedRecord>> single = Single.fromCallable(new Callable<List<RemainedRecord>>() {
                @Override
                public List<RemainedRecord> call() throws Exception {
                    List<RemainedRecord> remainedRecords = new ArrayList<>(groupRecords.size());

                    for (AccountedRecord i: groupRecords){
                        remainedRecords.add(domain.delete(i));
                    }

                    return remainedRecords;
                }
            });

            subscribe(single, observer);
        }
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public RemainedRecord getRemainedRecord() {
        return remainedRecord;
    }

    public interface ViewInterface{
        void onGroupRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> groupRecords);
        void onGroupBarcodesReceived(List<String> groupBarcodes);

        void onRecordDeleted(int position);
        void onAllRecordsDeleted();

        void onError(Throwable e);
    }
}
