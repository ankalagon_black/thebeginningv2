package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.repository.SearchRepository;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.Element;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by apple on 23.07.17.
 */

public class SearchModel extends BaseModel {
    private ViewInterface viewInterface;

    private DatabaseManager databaseManager;
    private SearchRepository repository;

    private InventorySettings inventorySettings = null;

    public SearchModel(ViewInterface viewInterface, DatabaseManager databaseManager, SearchRepository searchRepository){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.repository = searchRepository;
    }

    public void getInventorySettings(){
        try {
            DataModeRoot root = repository.getDataModeParser().getDataModeRoot();
            List<Element> pakets = root.getPaketsData();

            boolean allowDuplicates = true, allowUnidentifiedRecords = true,
                    allowColorIdentification = true, allowGroupInventory = false, allowGPS = false;

            for (Element i: pakets){
                switch (i.getTag()){
                    case InventorySettings.ALLOW_DUPLICATES:
                        allowDuplicates = !i.getValue().equals("0");
                        break;
                    case InventorySettings.ALLOW_UNIDENTIFIED_RECORDS:
                        allowUnidentifiedRecords = !i.getValue().equals("0");
                        break;
                    case InventorySettings.ALLOW_COLOR_IDENTIFICATION:
                        allowColorIdentification = !i.getValue().equals("0");
                        break;
                    case InventorySettings.GROUP_INVENTORY:
                        allowGroupInventory = i.getValue().equals("1");
                        break;
                    case InventorySettings.GPS:
                        allowGPS = i.getValue().equals("1");
                        break;
                }
            }

            inventorySettings = new InventorySettings(allowDuplicates, allowUnidentifiedRecords, allowColorIdentification, allowGroupInventory, allowGPS);

        } catch (IOException | XmlPullParserException e) {
            viewInterface.onError(e);
        }
    }

    public boolean hasInventorySettings(){
        return inventorySettings != null;
    }

    public void search(final String keyWord, final boolean partialMatch){
        DisposableSingleObserver<List<Record>> observer = new DisposableSingleObserver<List<Record>>() {
            @Override
            public void onSuccess(List<Record> value) {
                viewInterface.onSearchResults(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<Record>> single = Single.fromCallable(new Callable<List<Record>>() {
            @Override
            public List<Record> call() throws Exception {
                List<Record> records = databaseManager.findIdentifiedRecords(keyWord, partialMatch);
                records.addAll(databaseManager.findUnidentifiedRecords(keyWord, partialMatch));

                return records;
            }
        });

        subscribe(single, observer);
    }

    public interface ViewInterface{
        void onSearchResults(InventorySettings inventorySettings, List<Record> results);
        void onError(Throwable e);
    }
}
