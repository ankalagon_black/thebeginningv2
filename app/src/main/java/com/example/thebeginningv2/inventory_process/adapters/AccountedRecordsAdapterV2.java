package com.example.thebeginningv2.inventory_process.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordViewHolder;

import java.util.List;

/**
 * Created by Константин on 17.07.2017.
 */

public class AccountedRecordsAdapterV2 extends RecyclerView.Adapter<IdentifiedAccountedRecordViewHolder>{
    public static final int DELETE = 0, EDIT = 1;

    private Interface anInterface;

    private int mode;
    private List<AccountedRecord> accountedRecords;
    private InventorySettings inventorySettings;

    public AccountedRecordsAdapterV2(int mode, List<AccountedRecord> accountedRecords, InventorySettings inventorySettings, Interface anInterface){
        this.mode = mode;
        this.accountedRecords = accountedRecords;
        this.inventorySettings = inventorySettings;
        this.anInterface = anInterface;
    }

    @Override
    public IdentifiedAccountedRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(mode == DELETE) return new DeleteRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.identified_accounted_record, parent, false));
        else return new EditRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.identified_accounted_record, parent, false));
    }

    @Override
    public void onBindViewHolder(IdentifiedAccountedRecordViewHolder holder, int position) {
        holder.setRecord(position, accountedRecords.get(position),
                inventorySettings.isAllowColorIdentification(),
                inventorySettings.isAllowGroupInventory(),
                inventorySettings.isAllowGPS());
    }

    @Override
    public int getItemCount() {
        return accountedRecords.size();
    }

    public class DeleteRecordHolder extends IdentifiedAccountedRecordViewHolder{

        public DeleteRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_action_cancel);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButtonPressed(record);
                }
            });
        }
    }

    public class EditRecordHolder extends IdentifiedAccountedRecordViewHolder{

        public EditRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_check);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButtonPressed(record);
                }
            });
        }
    }

    public interface Interface{
        void onFunctionalButtonPressed(Record record);
    }
}
