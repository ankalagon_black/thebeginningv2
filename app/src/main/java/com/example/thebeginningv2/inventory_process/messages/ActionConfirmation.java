package com.example.thebeginningv2.inventory_process.messages;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Record;

/**
 * Created by Константин on 16.07.2017.
 */

public class ActionConfirmation {
    private Record targetRecord;
    private AccountedRecord result;
    private boolean addGpsMark;

    public ActionConfirmation(Record targetRecord, AccountedRecord result, boolean addGpsMark){
        this.targetRecord = targetRecord;
        this.result = result;
        this.addGpsMark = addGpsMark;
    }

    public Record getTargetRecord() {
        return targetRecord;
    }

    public AccountedRecord getResult() {
        return result;
    }

    public boolean isAddGpsMark() {
        return addGpsMark;
    }
}
