package com.example.thebeginningv2.inventory_process.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.fragments.SupportFragment;
import com.example.thebeginningv2.inventories.dialogs.AboutInventoryDialog;
import com.example.thebeginningv2.inventory_process.SearchRecordActivity;
import com.example.thebeginningv2.inventory_process.adapters.AccountedRecordsAdapter;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.DeletionResult;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Selection;
import com.example.thebeginningv2.inventory_process.dialogs.AboutRecordDialog;
import com.example.thebeginningv2.inventory_process.dialogs.ConvertRecordDialog;
import com.example.thebeginningv2.inventory_process.dialogs.DeleteGroupRecordsDialog;
import com.example.thebeginningv2.inventory_process.dialogs.EditQuantityDialog;
import com.example.thebeginningv2.inventory_process.dialogs.InputSettingsDialog;
import com.example.thebeginningv2.inventory_process.dialogs.ManualInputDialog;
import com.example.thebeginningv2.inventory_process.domains.DeleteAccountedRecordsDomain;
import com.example.thebeginningv2.inventory_process.messages.AllAccountedRecordsDeleted;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;
import com.example.thebeginningv2.inventory_process.models.AccountedRecordsModel;
import com.example.thebeginningv2.inventory_process.models.ConvertRecordModel;
import com.example.thebeginningv2.inventory_process.models.DeleteGroupRecordsModel;
import com.example.thebeginningv2.inventory_process.models.EditQuantityModel;
import com.example.thebeginningv2.inventory_process.presenters.AccountedRecordsPresenter;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by Константин on 10.06.2017.
 */

public class AccountedRecordsFragment extends SupportFragment<InventoryProcessRepository, AccountedRecordsModel, AccountedRecordsPresenter>
        implements AccountedRecordsModel.ViewInterface, AccountedRecordsAdapter.Interface, AccountedRecordsPresenter.Interface{
    private RecyclerView recyclerView;
    private AccountedRecordsAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_accounted_records, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.accounted_records_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(this);

        getModel().getInputSettings();
        getModel().getAccountedRecords();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInputSettings(InputSettings inputSettings){
        setInputType(inputSettings.getInputType());
    }

    @Override
    protected AccountedRecordsPresenter createPresenter() {
        return new AccountedRecordsPresenter(this);
    }

    @Override
    protected AccountedRecordsModel createModel() {
        InventoryProcessRepository repository = getRepository();
        DeleteAccountedRecordsDomain domain = new DeleteAccountedRecordsDomain(repository.getDatabaseManager());
        return new AccountedRecordsModel(this, domain, repository);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.inventory_process_accounted_records, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                Intent intent = new Intent(getContext(), SearchRecordActivity.class);
                startActivity(intent);
                return true;
            case R.id.remove_all:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.action_confirmation).
                        setMessage(R.string.remove_all_records_confirmation).
                        setNegativeButton(R.string.cancel, null).
                        setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DeleteAllAccountedRecordsDialog dialog = new DeleteAllAccountedRecordsDialog();
                                dialog.show(getChildFragmentManager(), dialog.getTag());

                                getModel().deleteAll();
                            }
                        }).
                        create().show();
                return true;
            case R.id.manual_input:
                ManualInputDialog manualInputDialog = new ManualInputDialog();
                manualInputDialog.show(getChildFragmentManager(), manualInputDialog.getTag());
                return true;
            case R.id.more_about_inventory:
                AboutInventoryDialog aboutInventory = new AboutInventoryDialog();
                aboutInventory.show(getChildFragmentManager(), aboutInventory.getTag());
                return true;
            case R.id.input_settings:
                InputSettingsDialog inputSettingsDialog = new InputSettingsDialog();
                inputSettingsDialog.show(getChildFragmentManager(), inputSettingsDialog.getTag());
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onAccountedRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> accountedRecords) {
        adapter = new AccountedRecordsAdapter(accountedRecords, getPresenter().getSelection(),
                inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS(),
                this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAccountedRecordAdded(AccountedRecord accountedRecord) {
        adapter.addRecord(accountedRecord);
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void onAccountedRecordRemoved(AccountedRecord accountedRecord) {
        adapter.removeRecord(accountedRecord);
        Toast.makeText(getActivity(), R.string.record_deleted, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAccountedRecordRemoved(DeletionResult deletionResult) {
        if(deletionResult.isHasGroupRecords()){
            DeleteGroupRecordsDialog dialog = new DeleteGroupRecordsDialog();

            Bundle bundle = new Bundle();
            bundle.putParcelable(DeleteGroupRecordsModel.REMAINED_RECORD_KEY, deletionResult.getRemainedRecord());

            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), dialog.getTag());
        }
    }

    @Override
    public void onAllAccountedRecordsRemoved() {
        adapter.removeAll();
        Toast.makeText(getActivity(), R.string.all_records_deleted, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setInputType(int inputType) {
        FragmentManager fragmentManager = getChildFragmentManager();

        switch (inputType){
            case InputSettings.INPUT_TYPE_1:
                InputType1Fragment inputType1Fragment = (InputType1Fragment) fragmentManager.findFragmentByTag(InputType1Fragment.TAG);
                if(inputType1Fragment == null) inputType1Fragment = new InputType1Fragment();
                fragmentManager.beginTransaction().replace(R.id.input_mode_container, inputType1Fragment, InputType1Fragment.TAG).commit();
                break;
            case InputSettings.INPUT_TYPE_2:
                InputType2Fragment inputType2Fragment = (InputType2Fragment) fragmentManager.findFragmentByTag(InputType2Fragment.TAG);
                if(inputType2Fragment == null) inputType2Fragment = new InputType2Fragment();
                fragmentManager.beginTransaction().replace(R.id.input_mode_container, inputType2Fragment, InputType2Fragment.TAG).commit();
                break;
        }
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPopupMenu(final AccountedRecord accountedRecord, View view) {
        PopupMenu popupMenu = new PopupMenu(getContext(), view);
        if(accountedRecord.isIdentified()) popupMenu.getMenuInflater().inflate(R.menu.inventory_process_identified_record_popup, popupMenu.getMenu());
        else popupMenu.getMenuInflater().inflate(R.menu.inventory_process_unidentified_record_popup, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.edit:
                        EditQuantityDialog editQuantityDialog = new EditQuantityDialog();

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY, accountedRecord);

                        editQuantityDialog.setArguments(bundle);
                        editQuantityDialog.show(getChildFragmentManager(), editQuantityDialog.getTag());
                        return true;

                    case R.id.convert:
                        ConvertRecordDialog convertRecordDialog = new ConvertRecordDialog();

                        bundle = new Bundle();
                        bundle.putParcelable(ConvertRecordModel.UNIDENTIFIED_RECORD_KEY, accountedRecord);

                        convertRecordDialog.setArguments(bundle);
                        convertRecordDialog.show(getChildFragmentManager(), convertRecordDialog.getTag());
                        return true;

                    case R.id.more_about_record:
                        AboutRecordDialog aboutRecordDialog = new AboutRecordDialog();

                        bundle = new Bundle();
                        bundle.putParcelable(AboutRecordModel.RECORD_KEY, accountedRecord);

                        aboutRecordDialog.setArguments(bundle);
                        aboutRecordDialog.show(getChildFragmentManager(), aboutRecordDialog.getTag());
                        return true;

                    case R.id.remove:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.remove_record_confirmation).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        DeleteAccountedRecordDialog deleteAccountedRecordDialog = new DeleteAccountedRecordDialog();
                                        deleteAccountedRecordDialog.show(getChildFragmentManager(), deleteAccountedRecordDialog.getTag());

                                        getModel().delete(accountedRecord);
                                    }
                                }).
                                create().show();
                        return true;

                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }

    @Override
    public void changeSelection(Selection oldSelection, Selection newSelection) {
        adapter.changeSelection(oldSelection, newSelection);
    }

    @Override
    public void openSelection() {
        FragmentManager fragmentManager = getChildFragmentManager();
        SelectionFragment selectionFragment = (SelectionFragment) fragmentManager.findFragmentByTag(SelectionFragment.TAG);
        if(selectionFragment == null){
            selectionFragment = new SelectionFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable(AccountedRecordsPresenter.SELECTION_KEY, getPresenter().getSelection());

            selectionFragment.setArguments(bundle);
        }
        fragmentManager.beginTransaction().replace(R.id.selection_mode_container, selectionFragment, SelectionFragment.TAG).commit();
    }

    @Override
    public void closeSelection() {
        FragmentManager fragmentManager = getChildFragmentManager();
        ShortStatisticsFragment statisticsFragment = (ShortStatisticsFragment) fragmentManager.findFragmentByTag(ShortStatisticsFragment.TAG);
        if(statisticsFragment == null) statisticsFragment = new ShortStatisticsFragment();
        fragmentManager.beginTransaction().replace(R.id.selection_mode_container, statisticsFragment, ShortStatisticsFragment.TAG).commit();
    }

    public static class DeleteAllAccountedRecordsDialog extends DialogFragment{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder.setCancelable(false).
                    setTitle(R.string.delete_all_title).
                    setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                    create();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            EventBus.getDefault().unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onAllAccountedRecordsDeleted(AllAccountedRecordsDeleted allAccountedRecordsDeleted){
            dismiss();
        }
    }

    public static class DeleteAccountedRecordDialog extends DialogFragment{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder.setCancelable(false).
                    setTitle(R.string.delete_record_title).
                    setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                    create();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            EventBus.getDefault().unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onAccountedRecordDeleted(DeletedAccountedRecord deletedAccountedRecord){
            dismiss();
        }
    }
}
