package com.example.thebeginningv2.inventory_process.dialogs;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventories.dialogs.AboutInventoryDialog;
import com.example.thebeginningv2.inventory_process.adapters.AboutRecordAdapter;
import com.example.thebeginningv2.inventory_process.adapters.IdentifiedRecordsAdapter;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnInfo;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordBody;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordBody;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.util.List;

/**
 * Created by Константин on 14.06.2017.
 */

public class AboutRecordDialog extends SupportModelDialog<AboutRecordModel>
        implements AboutRecordModel.ViewInterface, IdentifiedRecordsAdapter.Interface{

    private IdentifiedAccountedRecordBody accountedRecordBody;
    private RemainedRecordBody remainedRecordBody;

    private ListView listView;
    private RecyclerView recyclerView;
    private IdentifiedRecordsAdapter identifiedRecordsAdapter;

    private RecyclerView results;
    private AboutRecordAdapter aboutRecordAdapter;

    public static AboutRecordDialog getInstance(Record record){
        AboutRecordDialog dialog = new AboutRecordDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(AboutRecordModel.RECORD_KEY, record);

        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_record, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.about_record);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AboutRecordDialog.this.dismiss();
            }
        });

        Record record = getArguments().getParcelable(AboutRecordModel.RECORD_KEY);

        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.record_container);

        if(record.isAccounted()) accountedRecordBody = new IdentifiedAccountedRecordBody(inflater.inflate(R.layout.identified_accounted_record_body_v2, frameLayout, true));
        else remainedRecordBody = new RemainedRecordBody(inflater.inflate(R.layout.remained_record_body_v2, frameLayout, true));

        View layout = view.findViewById(R.id.group_records_layout);

        if(record.isGroup()){
            listView = (ListView) view.findViewById(R.id.list);

            TextView groupRecordsDescription = (TextView) view.findViewById(R.id.group_records_description);
            groupRecordsDescription.setText(R.string.list_of_group_records);

            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        else layout.setVisibility(View.GONE);

        results = (RecyclerView) view.findViewById(R.id.detailed_info);
        results.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Record record = getArguments().getParcelable(AboutRecordModel.RECORD_KEY);

        if(savedInstanceState == null){
            InventorySettings inventorySettings = getModel().getInventorySettings();

            if(record.isAccounted()){
                accountedRecordBody.setAccountedRecord((AccountedRecord) record,
                        inventorySettings.isAllowColorIdentification(),
                        inventorySettings.isAllowGroupInventory(),
                        inventorySettings.isAllowGPS());
            }
            else {
                remainedRecordBody.setRemainedRecord((RemainedRecord) record,
                        inventorySettings.isAllowColorIdentification(),
                        inventorySettings.isAllowGroupInventory(),
                        inventorySettings.isAllowGPS());
            }
        }

        if(record.isGroup()){
            getModel().getGroupBarcodes();
        }

        getModel().getColumnValues();
    }

    @Override
    protected AboutRecordModel createModel() {
        Record record = getArguments().getParcelable(AboutRecordModel.RECORD_KEY);

        PathManager pathManager = PathManager.getIntance();
        SQLiteDatabase sqLiteDatabase = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildPath(PathManager.DB), null);

        return new AboutRecordModel(this, record, new DatabaseManager(sqLiteDatabase));
    }

    @Override
    public void onGroupBarcodes(List<String> groupBarcodes) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, groupBarcodes);
        listView.setAdapter(adapter);
    }

    @Override
    public void onGroupRecords(InventorySettings inventorySettings, List<Record> groupRecord) {
        identifiedRecordsAdapter = new IdentifiedRecordsAdapter(groupRecord, this, inventorySettings);
        recyclerView.setAdapter(identifiedRecordsAdapter);
    }

    @Override
    public void onResults(List<ColumnInfo> columnInfos) {
        aboutRecordAdapter = new AboutRecordAdapter(columnInfos);
        results.setAdapter(aboutRecordAdapter);
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(Record record) {
        //// TODO: 17.07.2017 do later
    }

    @Override
    public void onFunctionalButtonPressed(Record record) {
        AboutRecordDialog dialog = getInstance(record);
        dialog.show(getFragmentManager(), dialog.getTag());

        dismiss();
    }

}
