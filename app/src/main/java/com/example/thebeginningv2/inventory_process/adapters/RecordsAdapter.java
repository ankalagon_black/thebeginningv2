package com.example.thebeginningv2.inventory_process.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.RecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.UnidentifiedRecordBody;

import java.util.List;
import java.util.Locale;

/**
 * Created by apple on 23.07.17.
 */

public class RecordsAdapter extends RecyclerView.Adapter<RecordViewHolder> {
    public static final int IDENTIFIED_ACCOUNTED_TYPE = 0, UNIDENTIFIED_TYPE = 1, REMAINED_TYPE = 2;

    private List<Record> records;
    private InventorySettings inventorySettings;

    private Interface anInterface;

    public RecordsAdapter(List<Record> records, InventorySettings inventorySettings, Interface anInterface){
        this.records = records;
        this.inventorySettings = inventorySettings;
        this.anInterface = anInterface;
    }

    @Override
    public int getItemViewType(int position) {
        Record record = records.get(position);
        if(record.isAccounted()){
            if(((AccountedRecord) record).isIdentified()){
                return IDENTIFIED_ACCOUNTED_TYPE;
            }
            else return UNIDENTIFIED_TYPE;
        }
        else return REMAINED_TYPE;
    }

    @Override
    public RecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case IDENTIFIED_ACCOUNTED_TYPE:
                return new IdentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.identified_accounted_record, parent, false));
            case UNIDENTIFIED_TYPE:
                return new UnidentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.unidentified_accounted_record_with_functional_button, parent, false));
            case REMAINED_TYPE:
                return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.remained_record, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecordViewHolder holder, int position) {
        holder.setRecord(position, records.get(position), inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS());
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public class IdentifiedRecordHolder extends IdentifiedAccountedRecordViewHolder {

        public IdentifiedRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_action_search);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButton(record);
                }
            });
        }
    }

    public class UnidentifiedRecordHolder extends RecordViewHolder<AccountedRecord> {
        private TextView index;
        private ImageButton functionalButton;

        private UnidentifiedRecordBody recordBody;

        public UnidentifiedRecordHolder(View itemView) {
            super(itemView);
            recordBody = new UnidentifiedRecordBody(itemView);

            index = (TextView) itemView.findViewById(R.id.index);
            functionalButton = (ImageButton) itemView.findViewById(R.id.functional_button);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            recordBody.setAccountedRecord(record, allowColorIdentification, allowGPS);
            Context context = itemView.getContext();
            index.setText(String.format(Locale.US, context.getString(R.string.index), position + 1));
            functionalButton.setImageResource(R.drawable.ic_action_search);

            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButton(record);
                }
            });
        }
    }

    public class Holder extends RemainedRecordViewHolder {

        public Holder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final RemainedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_action_search);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButton(record);
                }
            });
        }
    }

    public interface Interface{
        void onFunctionalButton(Record record);
    }
}
