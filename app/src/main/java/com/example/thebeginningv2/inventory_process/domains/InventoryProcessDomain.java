//package com.example.thebeginningv2.inventory_process.domain;
//
//import com.example.thebeginningv2.inventories.data_types.Inventory;
//import com.example.thebeginningv2.inventories.data_types.Result;
//import com.example.thebeginningv2.inventories.models.AboutInventoryDomain;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.AddedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
//import com.example.thebeginningv2.inventory_process.data_types.InputValues;
//import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
//import com.example.thebeginningv2.inventory_process.data_types.MovedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.Record;
//import com.example.thebeginningv2.inventory_process.data_types.RecordCreationResult;
//import com.example.thebeginningv2.inventory_process.data_types.RecordDeletionResult;
//import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
//import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
//import com.example.thebeginningv2.inventory_process.data_types.UpdatedRecord;
//import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;
//import com.example.thebeginningv2.inventory_process.models.AccountedRecordsModel;
//import com.example.thebeginningv2.inventory_process.models.ConvertRecordModel;
//import com.example.thebeginningv2.inventory_process.models.DeleteAllAccountedRecordsModel;
//import com.example.thebeginningv2.inventory_process.models.EditQuantityModel;
//import com.example.thebeginningv2.inventory_process.models.InputTypeModel;
//import com.example.thebeginningv2.inventory_process.models.ManualInputModel;
//import com.example.thebeginningv2.inventory_process.models.MoveRecordModel;
//import com.example.thebeginningv2.inventory_process.models.RemainedRecordsModel;
//import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
//import com.example.thebeginningv2.repository.managers.PathManager;
//import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
//import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;
//
//import org.xmlpull.v1.XmlPullParserException;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Константин on 07.07.2017.
// */
//
//public class InventoryProcessDomain implements AccountedRecordsModel.DomainInterface, InputTypeModel.DomainInterface,
//        RemainedRecordsModel.DomainInterface, MoveRecordModel.DomainInterface, EditQuantityModel.DomainInterface, ConvertRecordModel.DomainInterface,
//        ManualInputModel.DomainInterface, DeleteAllAccountedRecordsModel.DomainInterface, AboutRecordModel.DomainInterface, AboutInventoryDomain.DomainInterface {
//
//    private PathManager pathManager;
//    private DatabaseManager databaseManager;
//
//    public InventoryProcessDomain(PathManager pathManager, DatabaseManager databaseManager){
//        this.pathManager = pathManager;
//        this.databaseManager = databaseManager;
//    }
//
//
//    public RecordsCount getRecordsCount() {
//        return new RecordsCount(databaseManager.getAccountedRecordsCount(), databaseManager.getRemainedRecordsCount());
//    }
//
//    public ShortStatistics getShortStatistics() {
//        return new ShortStatistics(databaseManager.getAccountedRecordSum(), databaseManager.getStatedRecordsSum(), databaseManager.getResidueQuantity(),
//                databaseManager.getSurplusQuantity(), databaseManager.getUnidentifiedRecordsSum());
//    }
//
//    @Override
//    public List<AccountedRecord> getAccountedRecords() {
//        return databaseManager.getAccountedRecords();
//    }
//
//    @Override
//    public List<RemainedRecord> getRemainedRecords() {
//        return databaseManager.getRemainedRecords();
//    }
//
//    @Override
//    public RecordCreationResult createRecord(InventorySettings inventorySettings, InputValues inputValues) {
//        ArrayList<Integer> ids = databaseManager.findIdentifiedRecords(inputValues.getValue(), false);
//        if(ids.size() != 0){
//            if(ids.size() != 1){
//                return new AddedRecord(null, null, ids, null, null, null);
//            }
//            else{
//                int id = ids.get(0);
//                return createRecord(inventorySettings, id, inputValues);
//            }
//        }
//        else {
//            if (inventorySettings.isAllowUnidentifiedRecords()) {
//                List<String> names = databaseManager.findUnidentifiedRecord(inputValues.getValue(), false);
//                if (names.size() != 0) {
//                    AccountedRecord oldRecord = databaseManager.getUnidentifiedAccountedRecord(names.get(0));
//                    AccountedRecord newRecord = new AccountedRecord(oldRecord, oldRecord.getAccountedQuantity() + inputValues.getQuantity());
//
//                    if(!inventorySettings.isAllowDuplicates() && newRecord.getAccountedQuantity() > 1.0f) return new RecordCreationResult(RecordCreationResult.DUPLICATES_NOT_ALLOWED);
//                    else {
//
//                        databaseManager.updateAccountedRecord(newRecord.getId(), newRecord.getBarcode(), newRecord.getAccountedQuantity());
//                        return new UpdatedRecord(oldRecord, newRecord);
//                    }
//                }
//                else {
//                    AccountedRecord accountedRecord = new AccountedRecord(inputValues.getValue(), inputValues.getQuantity());
//                    databaseManager.addToAccountedRecords(accountedRecord.getId(), inputValues.getValue(), inputValues.getQuantity());
//                    return new AddedRecord(null, accountedRecord, null, null, null, null);
//                }
//            }
//            else return new RecordCreationResult(RecordCreationResult.UNIDENTIFIED_RECORDS_NOT_ALLOWED);
//        }
//    }
//
//    @Override
//    public RecordCreationResult createRecord(InventorySettings inventorySettings, int id, InputValues inputValues) {
//        if(databaseManager.isAccounted(id)){
//            AccountedRecord oldRecord = databaseManager.getIdentifiedAccountedRecord(id);
//            AccountedRecord newRecord = new AccountedRecord(oldRecord, oldRecord.getAccountedQuantity() + inputValues.getQuantity());
//
//            if(!inventorySettings.isAllowDuplicates() && newRecord.getAccountedQuantity() > 1.0f) return new RecordCreationResult(RecordCreationResult.DUPLICATES_NOT_ALLOWED);
//            else {
//
//                databaseManager.updateAccountedRecord(newRecord.getId(), newRecord.getBarcode(), newRecord.getAccountedQuantity());
//                return new UpdatedRecord(oldRecord, newRecord);
//            }
//        }
//        else {
//            RemainedRecord remainedRecord = databaseManager.getRemainedRecord(id);
//            AccountedRecord accountedRecord = new AccountedRecord(remainedRecord, inputValues.getQuantity());
//
//            databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity());
//
//            if(databaseManager.isGroup(remainedRecord.getId())){
//                String groupBarcode = databaseManager.getGroupBarcode(id);
//                List<Integer> groupIds = databaseManager.findRemainedGroupRecords(groupBarcode);
//
//                if(groupIds.size() != 0){
//                    List<RemainedRecord> remainedRecords = new ArrayList<>(groupIds.size());
//                    List<AccountedRecord> accountedRecords = new ArrayList<>(groupIds.size());
//
//                    for (Integer i: groupIds){
//                        RemainedRecord temp = databaseManager.getRemainedRecord(i);
//                        AccountedRecord temp2 = new AccountedRecord(temp, temp.getStatedQuantity());
//
//                        databaseManager.addToAccountedRecords(temp2.getId(), temp2.getBarcode(), temp2.getAccountedQuantity());
//
//                        remainedRecords.add(temp);
//                        accountedRecords.add(temp2);
//                    }
//
//                    return new AddedRecord(remainedRecord, accountedRecord, null, remainedRecords, accountedRecords, null);
//                }
//                else return new AddedRecord(remainedRecord, accountedRecord, null, null, null, null);
//            }
//            else return new AddedRecord(remainedRecord, accountedRecord, null, null, null, null);
//        }
//    }
//
//    @Override
//    public AddedRecord createRecords(List<Integer> ids) {
//        return null;
//    }
//
//    @Override
//    public void createRecord(AccountedRecord accountedRecord, Record record) {
//        if(record.isAccounted()){
//            databaseManager.updateAccountedRecord(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity());
//        }
//        else {
//            databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity());
//        }
//    }
//
//    @Override
//    public void convert(AccountedRecord result, Record record, AccountedRecord unidentifiedRecord) {
//        databaseManager.deleteFromAccountedRecords(unidentifiedRecord.getId(), unidentifiedRecord.getBarcode());
//        createRecord(result, record);
//    }
//
//    @Override
//    public MovedRecord move(RemainedRecord remainedRecord, float quantity) {
//        AccountedRecord accountedRecord = new AccountedRecord(remainedRecord, quantity);
//        MovedRecord movedRecord = new MovedRecord(remainedRecord, accountedRecord);
//        databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity());
//        return movedRecord;
//    }
//
//    @Override
//    public UpdatedRecord updateQuantity(AccountedRecord accountedRecord, float quantity) {
//        AccountedRecord newRecord = new AccountedRecord(accountedRecord, quantity);
//        databaseManager.updateAccountedRecord(newRecord.getId(), newRecord.getBarcode(), newRecord.getAccountedQuantity());
//        return new UpdatedRecord(accountedRecord, newRecord);
//    }
//
//    @Override
//    public RecordDeletionResult delete(AccountedRecord accountedRecord) {
//        databaseManager.deleteFromAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode());
//        if(accountedRecord.isIdentified()) return new RecordDeletionResult(accountedRecord, databaseManager.getRemainedRecord(accountedRecord.getId()));
//        else return new RecordDeletionResult(accountedRecord, null);
//    }
//
//    @Override
//    public RemainedRecord deleteRecord(AccountedRecord accountedRecord) {
//        databaseManager.deleteFromAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode());
//        return databaseManager.getRemainedRecord(accountedRecord.getId());
//    }
//
//    @Override
//    public List<Record> search(String keyWord, boolean useLike) {
//        List<Integer> ids = databaseManager.findIdentifiedRecords(keyWord, useLike);
//        List<Record> records = new ArrayList<>(ids.size());
//
//        for (Integer i: ids){
//            if(databaseManager.isAccounted(i)) records.add(databaseManager.getIdentifiedAccountedRecord(i));
//            else records.add(databaseManager.getRemainedRecord(i));
//        }
//
//        return records;
//    }
//
//    @Override
//    public List<Record> getRecords(List<Integer> ids) {
//        List<Record> records = new ArrayList<>(ids.size());
//
//        for (Integer i: ids){
//            if(databaseManager.isAccounted(i)) records.add(databaseManager.getIdentifiedAccountedRecord(i));
//            else records.add(databaseManager.getRemainedRecord(i));
//        }
//
//        return records;
//    }
//
//    @Override
//    public String getGroupBarcode(int id) {
//        return databaseManager.getGroupBarcode(id);
//    }
//
//    @Override
//    public List<String> getGroupBarcodes(String groupBarcode) {
//        List<Integer> ids = databaseManager.findGroupRecords(groupBarcode);
//        List<String> list = new ArrayList<>(ids.size());
//
//        for (Integer i: ids){
//            if(databaseManager.isAccounted(i)){
//                AccountedRecord accountedRecord = databaseManager.getIdentifiedAccountedRecord(i);
//                list.add(accountedRecord.getBarcode());
//            }
//            else {
//                RemainedRecord remainedRecord = databaseManager.getRemainedRecord(i);
//                list.add(remainedRecord.getBarcode());
//            }
//        }
//
//        return list;
//    }
//
//    @Override
//    public List<ColumnValue> getColumnValues(int id) {
//        return databaseManager.getColumnValues(id);
//    }
//
//    @Override
//    public Inventory getInventory(String inventory) throws IOException, XmlPullParserException {
//        File file = new File(pathManager.getDataChildPath(PathManager.MODE));
//
//        DataModeSystemData systemData = new DataModeParser().geDataModeSystemData(file);
//
//        int state = systemData.getState().equals(DataModeSystemData.SENT) ? Inventory.SENT : Inventory.IN_PROGRESS;
//        int mode = systemData.getMode().equals(DataModeSystemData.WITH_ACCOUNTING_DATA) ? Inventory.WITH_ACCOUNTING_DATA : Inventory.WITHOUT_ACCOUNTING_DATA;
//
//        return new Inventory(pathManager.getSecondLevelCatalog(), state, mode);
//    }
//
//    @Override
//    public RecordsCount getRecordsCount(String inventory) {
//        return getRecordsCount();
//    }
//
//    @Override
//    public ShortStatistics getShortStatistics(String inventory) {
//        return getShortStatistics();
//    }
//
//    @Override
//    public List<Result> getResults(String inventory) {
//        return databaseManager.getResults();
//    }
//}
