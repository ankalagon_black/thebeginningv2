package com.example.thebeginningv2.inventory_process.models;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Константин on 09.07.2017.
 */

public class ManualInputModel extends Model {
    public static final String INPUT_VALUES_KEY = "InputValuesKey";

    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private InventorySettings inventorySettings;

    private GpsAndGroupInventoryDomain domain;
    private InventoryProcessRepository repository;

    public ManualInputModel(ViewInterface viewInterface, DatabaseManager databaseManager,
                            GpsAndGroupInventoryDomain domain, InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.domain = domain;
        this.repository = repository;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void search(final String keyWord, final boolean useLike){
        DisposableSingleObserver<List<Record>> observer = new DisposableSingleObserver<List<Record>>() {
            @Override
            public void onSuccess(List<Record> value) {
                viewInterface.onSearchResultsReceived(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<Record>> single = Single.fromCallable(new Callable<List<Record>>() {
            @Override
            public List<Record> call() throws Exception {
                return databaseManager.findIdentifiedRecords(keyWord, useLike);
            }
        });

        subscribe(single, observer);
    }

    public void input(final Record targetRecord, final AccountedRecord result, final Location location){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                if(targetRecord.isAccounted()) EventBus.getDefault().post(new DeletedAccountedRecord((AccountedRecord) targetRecord));
                else EventBus.getDefault().post(new DeletedRemainedRecord((RemainedRecord) targetRecord));

                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                viewInterface.onInputFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onInputError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                if(targetRecord.isAccounted()) return domain.updateRecord(inventorySettings, result);
                else return domain.moveRecord(inventorySettings,
                        (RemainedRecord) targetRecord, result.getAccountedQuantity(), location);
            }
        });

        subscribe(single, observer);
    }


    public void input(final Record targetRecord, final AccountedRecord result, final boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    input(targetRecord, result, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else input(targetRecord, result,null);
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public interface ViewInterface{
        void onSearchResultsReceived(InventorySettings inventorySettings, List<Record> records);

        void onInputFinished(Result result);
        void onInputError(Throwable e);

        void onGPSNotAvailable();

        void onError(Throwable e);
    }
}
