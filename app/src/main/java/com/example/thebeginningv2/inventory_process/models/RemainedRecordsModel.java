package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.messages.AddedRemainedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 10.06.2017.
 */

public class RemainedRecordsModel extends Model {
    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private InventorySettings inventorySettings;
    private List<RemainedRecord> remainedRecords;

    public RemainedRecordsModel(ViewInterface viewInterface, DatabaseManager databaseManager){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettingsReceived(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemainedRecordAdded(AddedRemainedRecord addedRemainedRecord){
        RemainedRecord remainedRecord = addedRemainedRecord.getRemainedRecord();

        remainedRecords.add(0, remainedRecord);

        viewInterface.onRemainedRecordAdded();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemainedRecordDeleted(DeletedRemainedRecord deletedRemainedRecord){
        RemainedRecord remainedRecord = deletedRemainedRecord.getRemainedRecord();

        int index = findRecord(remainedRecord.getId());
        if(index != -1){
            remainedRecords.remove(index);
            viewInterface.onRemainedRecordRemoved(index);
        }
    }

    public void getRemainedRecords() {
        DisposableSingleObserver<List<RemainedRecord>> observer = new DisposableSingleObserver<List<RemainedRecord>>() {
            @Override
            public void onSuccess(List<RemainedRecord> value) {
                remainedRecords = value;
                viewInterface.onRemainedRecordsReceived(inventorySettings, remainedRecords);
            }

            @Override
            public void onError(Throwable e) {

            }
        };

        Single<List<RemainedRecord>> single =  Single.fromCallable(new Callable<List<RemainedRecord>>() {
            @Override
            public List<RemainedRecord> call() throws Exception {
                return databaseManager.getRemainedRecords();
            }
        });

        subscribe(single, observer);
    }

    private int findRecord(int id){
        int size = remainedRecords.size();
        for (int i = 0; i < size; i++){
            if(remainedRecords.get(i).getId() == id) return i;
        }
        return -1;
    }

    public interface ViewInterface{
        void onRemainedRecordsReceived(InventorySettings inventorySettings, List<RemainedRecord> remainedRecords);
        void onRemainedRecordAdded();
        void onRemainedRecordRemoved(int position);
        void onError(Throwable e);
    }
}
