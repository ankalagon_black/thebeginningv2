package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.RecordMovementFinished;
import com.example.thebeginningv2.inventory_process.models.MoveRecordModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordV2;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

/**
 * Created by Константин on 14.06.2017.
 */

public class MoveRecordDialog extends SupportModelDialog<InventoryProcessRepository, MoveRecordModel> implements MoveRecordModel.ViewInterface{
    private IdentifiedAccountedRecordV2 identifiedAccountedRecordV2;

    private EditText accountedQuantityEdit;
    private CheckBox addGpsMark;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(R.layout.move_record, null);

        final RemainedRecord remainedRecord = getArguments().getParcelable(MoveRecordModel.REMAINED_RECORD_KEY);

        identifiedAccountedRecordV2 = new IdentifiedAccountedRecordV2(view);

        accountedQuantityEdit = (EditText) view.findViewById(R.id.accounted_quantity_edit);
        accountedQuantityEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentQuantity = editable.toString();
                if (NumberUtils.isCreatable(currentQuantity)) updateDifference(remainedRecord, Float.parseFloat(currentQuantity));
            }
        });

        addGpsMark = (CheckBox) view.findViewById(R.id.add_gps_mark);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.
                setTitle(R.string.move_record).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);
                    }
                }).
                setPositiveButton(R.string.move, null).
                create();


        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String currentQuantity = accountedQuantityEdit.getEditableText().toString();
                        if(NumberUtils.isCreatable(currentQuantity)) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);

                            float quantity = Float.parseFloat(currentQuantity);

                            if (!getModel().getInventorySettings().isAllowDuplicates() && quantity > 1.0f) {
                                Toast.makeText(getActivity(), R.string.duplicates_not_alllowed, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                RemainedRecordMovementDialog dialog = new RemainedRecordMovementDialog();
                                dialog.show(getFragmentManager(), dialog.getTag());

                                getModel().move(remainedRecord, Float.parseFloat(currentQuantity), addGpsMark.isChecked());
                            }
                        }
                        else Toast.makeText(getContext(), R.string.quantity_not_a_number, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            RemainedRecord remainedRecord = getArguments().getParcelable(MoveRecordModel.REMAINED_RECORD_KEY);

            InventorySettings inventorySettings = getModel().getInventorySettings();

            float accountedQuantity = remainedRecord.getStatedQuantity();

            identifiedAccountedRecordV2.setAccountedRecord(new AccountedRecord(remainedRecord, accountedQuantity),
                    inventorySettings.isAllowColorIdentification(),
                    inventorySettings.isAllowGroupInventory(),
                    inventorySettings.isAllowGPS());

            updateDifference(remainedRecord, accountedQuantity);

            accountedQuantityEdit.setText(String.format(Locale.US, getString(R.string.float_3_decimal), accountedQuantity));

            if(inventorySettings.isAllowGPS()) addGpsMark.setVisibility(View.VISIBLE);
            else addGpsMark.setVisibility(View.GONE);
        }
    }

    @Override
    protected MoveRecordModel createModel() {
        return new MoveRecordModel(this, new GpsAndGroupInventoryDomain(getRepository().getDatabaseManager(), getActivity()), getRepository());
    }

    private void updateDifference(RemainedRecord remainedRecord, float accountedQuantity){
        TextView outOf = identifiedAccountedRecordV2.getOutOf(), difference = identifiedAccountedRecordV2.getDifference();

        outOf.setText(String.format(Locale.US, getString(R.string.out_of_stated),
                accountedQuantity, remainedRecord.getStatedQuantity()));

        switch (AccountedRecord.getDifferenceType(accountedQuantity, remainedRecord.getStatedQuantity())){
            case AccountedRecord.MATCHING:
                difference.setVisibility(View.INVISIBLE);

                if(getModel().getInventorySettings().isAllowColorIdentification())
                    outOf.setTextColor(getResources().getColor(R.color.green_color));
                break;

            case AccountedRecord.WITH_RESIDUE:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, getString(R.string.residue_difference),
                        remainedRecord.getStatedQuantity() - accountedQuantity));

                if(getModel().getInventorySettings().isAllowColorIdentification()){
                    int color = getResources().getColor(R.color.blue_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;

            case AccountedRecord.WITH_SURPLUS:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, getString(R.string.surplus_difference),
                        accountedQuantity - remainedRecord.getStatedQuantity()));

                if(getModel().getInventorySettings().isAllowColorIdentification()){
                    int color = getResources().getColor(R.color.red_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;
        }
    }

    @Override
    public void onRecordMoved(AccountedRecord movedRecord, boolean hasGroupRecords) {
        EventBus.getDefault().post(new RecordMovementFinished());
        Toast.makeText(getActivity(), R.string.move_finished, Toast.LENGTH_SHORT).show();

        if(hasGroupRecords){
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(AddGroupRecordsDialog.MOVE, movedRecord);
            dialog.show(getFragmentManager(), dialog.getTag());
        }

        dismiss();
    }

    @Override
    public void onGPSNotAvailable() {
        EventBus.getDefault().post(new RecordMovementFinished());
        Toast.makeText(getContext(), R.string.gps_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new RecordMovementFinished());
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public static class RemainedRecordMovementDialog extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder.setCancelable(false).
                    setTitle(R.string.record_is_moving).
                    setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                    create();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            EventBus.getDefault().unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onAccountedRecordDeleted(RecordMovementFinished recordMovementFinished){
            dismiss();
        }
    }
}
