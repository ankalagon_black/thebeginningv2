package com.example.thebeginningv2.inventory_process.data_types;

import android.os.Parcel;

/**
 * Created by Константин on 14.06.2017.
 */

public class RemainedRecord extends Record{
    private int id;
    private String barcode, subtitle;
    private float statedQuantity;
    private boolean isGroup;

    public RemainedRecord(int id, String barcode, String subtitle, float statedQuantity, boolean isGroup){
        this.id = id;
        this.barcode = barcode;
        this.subtitle = subtitle;
        this.statedQuantity = statedQuantity;
        this.isGroup = isGroup;
    }

    public RemainedRecord(AccountedRecord accountedRecord){
        this(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getSubtitle(), accountedRecord.getStatedQuantity(),
                accountedRecord.isGroup());
    }

    protected RemainedRecord(Parcel in) {
        id = in.readInt();
        barcode = in.readString();
        subtitle = in.readString();
        statedQuantity = in.readFloat();
        isGroup = in.readInt() == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(barcode);
        dest.writeString(subtitle);
        dest.writeFloat(statedQuantity);
        dest.writeInt(isGroup ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RemainedRecord> CREATOR = new Creator<RemainedRecord>() {
        @Override
        public RemainedRecord createFromParcel(Parcel in) {
            return new RemainedRecord(in);
        }

        @Override
        public RemainedRecord[] newArray(int size) {
            return new RemainedRecord[size];
        }
    };

    @Override
    public boolean isAccounted() {
        return false;
    }

    public int getId() {
        return id;
    }

    public String getBarcode() {
        return barcode;
    }

    public boolean hasSubtitle(){
        return subtitle != null;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public float getStatedQuantity() {
        return statedQuantity;
    }

    @Override
    public boolean isGroup() {
        return isGroup;
    }
}
