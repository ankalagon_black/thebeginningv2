package com.example.thebeginningv2.inventory_process.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.dialogs.AboutRecordDialog;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;

/**
 * Created by Константин on 16.07.2017.
 */

public class IdentifiedAccountedRecordV2 extends IdentifiedAccountedRecordBody {
    protected ImageButton functionalButton;

    public IdentifiedAccountedRecordV2(View view) {
        super(view);
        functionalButton = (ImageButton) view.findViewById(R.id.functional_button);
    }

    @Override
    public void setAccountedRecord(final AccountedRecord accountedRecord, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
        super.setAccountedRecord(accountedRecord, allowColorIdentification, allowGroupInventory, allowGPS);

        functionalButton.setImageResource(R.drawable.ic_action_info);
        functionalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AboutRecordDialog aboutRecordDialog = new AboutRecordDialog();

                Bundle bundle = new Bundle();
                bundle.putParcelable(AboutRecordModel.RECORD_KEY, accountedRecord);

                aboutRecordDialog.setArguments(bundle);
                aboutRecordDialog.show(((AppCompatActivity) view.getContext()).getSupportFragmentManager(), aboutRecordDialog.getTag());
            }
        });
    }


}
