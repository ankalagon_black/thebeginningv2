package com.example.thebeginningv2.inventory_process.messages;

/**
 * Created by apple on 25.07.17.
 */

public class AddGpsMark {
    private boolean state;

    public AddGpsMark(boolean state){
        this.state = state;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state){
        this.state = state;
    }
}
