package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.inventory_process.domains.InventoryStatisticsDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.AddedRemainedRecord;
import com.example.thebeginningv2.inventory_process.messages.AllAccountedRecordsDeleted;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.Element;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 07.07.2017.
 */

public class WithAccountingDataInventoryModel extends Model {
    private ViewInterface viewInterface;
    private InventoryProcessRepository repository;

    private InventoryStatisticsDomain statisticsDomain;

    private RecordsCount recordsCount;
    private ShortStatistics shortStatistics;

    public WithAccountingDataInventoryModel(ViewInterface viewInterface, InventoryProcessRepository repository, InventoryStatisticsDomain statisticsDomain){
        this.viewInterface = viewInterface;
        this.repository = repository;
        this.statisticsDomain = statisticsDomain;
    }

    public void getInventorySettings(){
        InventorySettings inventorySettings = null;
        try {
            DataModeRoot root = repository.getDataModeParser().getDataModeRoot();
            List<Element> pakets = root.getPaketsData();

            boolean allowDuplicates = true, allowUnidentifiedRecords = true,
                    allowColorIdentification = true, allowGroupInventory = false, allowGPS = false;

            for (Element i: pakets){
                switch (i.getTag()){
                    case InventorySettings.ALLOW_DUPLICATES:
                        allowDuplicates = !i.getValue().equals("0");
                        break;
                    case InventorySettings.ALLOW_UNIDENTIFIED_RECORDS:
                        allowUnidentifiedRecords = !i.getValue().equals("0");
                        break;
                    case InventorySettings.ALLOW_COLOR_IDENTIFICATION:
                        allowColorIdentification = !i.getValue().equals("0");
                        break;
                    case InventorySettings.GROUP_INVENTORY:
                        allowGroupInventory = i.getValue().equals("1");
                        break;
                    case InventorySettings.GPS:
                        allowGPS = i.getValue().equals("1");
                        break;
                }
            }

            inventorySettings = new InventorySettings(allowDuplicates, allowUnidentifiedRecords, allowColorIdentification, allowGroupInventory, allowGPS);

        } catch (IOException | XmlPullParserException e) {
            viewInterface.onError(e);
        }
        if(inventorySettings != null) EventBus.getDefault().postSticky(inventorySettings);
    }

    public void getRecordsCount(){
        DisposableSingleObserver<RecordsCount> observer = new DisposableSingleObserver<RecordsCount>() {
            @Override
            public void onSuccess(RecordsCount value) {
                recordsCount = value;
                viewInterface.onRecordsCountReceived(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<RecordsCount> single = Single.fromCallable(new Callable<RecordsCount>() {
            @Override
            public RecordsCount call() throws Exception {
                return statisticsDomain.getRecordsCount();
            }
        });

        subscribe(single, observer);
    }

    public void getShortStatistics(){
        DisposableSingleObserver<ShortStatistics> observer = new DisposableSingleObserver<ShortStatistics>() {
            @Override
            public void onSuccess(ShortStatistics value) {
                shortStatistics = value;
                EventBus.getDefault().postSticky(shortStatistics);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<ShortStatistics> single = Single.fromCallable(new Callable<ShortStatistics>() {
            @Override
            public ShortStatistics call() throws Exception {
                return statisticsDomain.getShortStatistics();
            }
        });

        subscribe(single, observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordAdded(AddedAccountedRecord addedAccountedRecord){
        AccountedRecord accountedRecord = addedAccountedRecord.getAccountedRecord();

        recordsCount.addAccountedRecordsCount(1);

        shortStatistics.addAccountedQuantity(accountedRecord.getAccountedQuantity());

        if(accountedRecord.isIdentified()){
            switch (accountedRecord.getDifferenceType()){
                case AccountedRecord.WITH_RESIDUE:
                    shortStatistics.addResidueQuantity(accountedRecord.getDifference());
                    break;
                case AccountedRecord.WITH_SURPLUS:
                    shortStatistics.addSurplusQuantity(accountedRecord.getDifference());
                    break;
            }
        }
        else shortStatistics.addUnidentifiedQuantity(accountedRecord.getAccountedQuantity());

        EventBus.getDefault().post(shortStatistics);

        viewInterface.onRecordsCountUpdated(recordsCount);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordDeleted(DeletedAccountedRecord deletedAccountedRecord){
        AccountedRecord accountedRecord = deletedAccountedRecord.getAccountedRecord();

        recordsCount.addAccountedRecordsCount(-1);

        shortStatistics.addAccountedQuantity(-accountedRecord.getAccountedQuantity());

        if(accountedRecord.isIdentified()){
            switch (accountedRecord.getDifferenceType()){
                case AccountedRecord.WITH_RESIDUE:
                    shortStatistics.addResidueQuantity(-accountedRecord.getDifference());
                    break;
                case AccountedRecord.WITH_SURPLUS:
                    shortStatistics.addSurplusQuantity(-accountedRecord.getDifference());
                    break;
            }
        }
        else shortStatistics.addUnidentifiedQuantity(-accountedRecord.getAccountedQuantity());

        EventBus.getDefault().post(shortStatistics);

        viewInterface.onRecordsCountUpdated(recordsCount);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordsDeleted(AllAccountedRecordsDeleted allAccountedRecordsDeleted){
        shortStatistics.setAccountedQuantity(0);
        shortStatistics.setResidueQuantity(0);
        shortStatistics.setSurplusQuantity(0);
        shortStatistics.setUnidentifiedQuantity(0);

        recordsCount.setAccountedRecordsCount(0);

        EventBus.getDefault().post(shortStatistics);

        viewInterface.onRecordsCountUpdated(recordsCount);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemainedRecordAdded(AddedRemainedRecord addedRemainedRecord){
        recordsCount.addRemainedRecordsCount(1);

        viewInterface.onRecordsCountUpdated(recordsCount);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemainedRecordDeleted(DeletedRemainedRecord deletedRemainedRecord){
        recordsCount.addRemainedRecordsCount(-1);

        viewInterface.onRecordsCountUpdated(recordsCount);
    }

    public interface ViewInterface{
        void onRecordsCountReceived(RecordsCount recordsCount);
        void onRecordsCountUpdated(RecordsCount recordsCount);
        void onError(Throwable e);
    }
}
