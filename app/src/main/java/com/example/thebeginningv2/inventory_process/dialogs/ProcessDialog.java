package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 17.07.2017.
 */

public class ProcessDialog extends DialogFragment {
    public static final String TITLE_KEY = "TitleKey";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();

        String title = bundle == null ? getString(R.string.process_default_title) : bundle.getString(TITLE_KEY);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return builder.setCancelable(false).
                setTitle(title).
                setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStopProcess(StopProcess stopProcess){
        dismiss();
    }
}
