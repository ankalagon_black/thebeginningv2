package com.example.thebeginningv2.inventory_process.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.adapters.IdentifiedRecordsAdapter;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.ActionConfirmation;
import com.example.thebeginningv2.inventory_process.messages.ActionProcessFinished;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;
import com.example.thebeginningv2.inventory_process.models.ManualInputModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 14.06.2017.
 */

public class ManualInputDialog extends SupportModelDialog<InventoryProcessRepository, ManualInputModel>
        implements ManualInputModel.ViewInterface, IdentifiedRecordsAdapter.Interface{

    private EditText valueEdit, quantityEdit;
    private CheckBox partialMatch, addUpQuantities;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.manual_input, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.manual_input);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManualInputDialog.this.dismiss();
            }
        });

        valueEdit = (EditText) view.findViewById(R.id.value_edit);
        quantityEdit = (EditText) view.findViewById(R.id.quantity_edit);

        Button search = (Button) view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = valueEdit.getText().toString();

               if(value.replace(" ", "").length() > 0) getModel().search(value, partialMatch.isChecked());
            }
        });

        partialMatch = (CheckBox) view.findViewById(R.id.partial_match);
        partialMatch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String value = valueEdit.getText().toString();
                if(value.replace(" ", "").length() > 0){
                   getModel().search(value, partialMatch.isChecked());
                }
            }
        });

        addUpQuantities = (CheckBox) view.findViewById(R.id.add_up_quantities);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        if(savedInstanceState == null) {
            Bundle bundle = getArguments();
            if (bundle != null) {
                InputValues inpuValues = bundle.getParcelable(ManualInputModel.INPUT_VALUES_KEY);
                valueEdit.setText(inpuValues.getValue());
                quantityEdit.setText(String.format(Locale.US, getString(R.string.float_3_decimal), inpuValues.getQuantity()));
            }
            else quantityEdit.setText("1");
        }

        String value = valueEdit.getText().toString();
        if(value.replace(" ", "").length() > 0){
            getModel().search(value, partialMatch.isChecked());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected ManualInputModel createModel() {
        return new ManualInputModel(this, getRepository().getDatabaseManager(),
                new GpsAndGroupInventoryDomain(getRepository().getDatabaseManager(), getActivity()), getRepository());
    }

    @Override
    public void onSearchResultsReceived(InventorySettings inventorySettings, List<Record> records) {
        IdentifiedRecordsAdapter adapter = new IdentifiedRecordsAdapter(records, this, inventorySettings);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInputFinished(Result result) {
        EventBus.getDefault().post(new ActionProcessFinished());
        Toast.makeText(getActivity(), R.string.input_finished, Toast.LENGTH_SHORT).show();

        if(result.isHasGroupRecords()) {
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(result.getType() == Result.MOVE ? AddGroupRecordsDialog.MOVE : AddGroupRecordsDialog.UPDATE,
                    result.getAccountedRecord());
            dialog.show(getFragmentManager(), dialog.getTag());
        }

        dismiss();
    }

    @Override
    public void onInputError(Throwable e) {
        EventBus.getDefault().post(new ActionProcessFinished());
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGPSNotAvailable() {
        EventBus.getDefault().post(new ActionProcessFinished());
        Toast.makeText(getContext(), R.string.gps_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActionConfirmation(ActionConfirmation actionConfirmation){
        ActionProcessDialog dialog = new ActionProcessDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(ActionProcessDialog.ACTION_TYPE_KEY, actionConfirmation.getTargetRecord().isAccounted() ? Result.UPDATE : Result.MOVE);

        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), dialog.getTag());

        getModel().input(actionConfirmation.getTargetRecord(), actionConfirmation.getResult(), actionConfirmation.isAddGpsMark());
    }

    @Override
    public void onClick(Record record) {
        String quantityStr = quantityEdit.getEditableText().toString();
        if(NumberUtils.isCreatable(quantityStr)){

            float quantity = Float.parseFloat(quantityStr);
            if(addUpQuantities.isChecked()) quantity += record.isAccounted() ? ((AccountedRecord) record).getAccountedQuantity() : 0;

            if (!getModel().getInventorySettings().isAllowDuplicates() && quantity > 1.0f) {
                Toast.makeText(getActivity(), R.string.duplicates_not_alllowed, Toast.LENGTH_SHORT).show();
            }
            else {
                AccountedRecord result = new AccountedRecord(record, quantity);

                ActionConfirmationDialog dialog = new ActionConfirmationDialog();

                Bundle bundle = new Bundle();
                bundle.putParcelable(ActionConfirmationDialog.TARGET_RECORD_KEY, record);
                bundle.putParcelable(ActionConfirmationDialog.RESULT_RECORD_KEY, result);

                dialog.setArguments(bundle);
                dialog.show(getFragmentManager(), dialog.getTag());
            }

        }
        else Toast.makeText(getContext(), R.string.quantity_not_a_number, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFunctionalButtonPressed(Record record) {
        AboutRecordDialog dialog = new AboutRecordDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(AboutRecordModel.RECORD_KEY, record);

        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), dialog.getTag());
    }
}
