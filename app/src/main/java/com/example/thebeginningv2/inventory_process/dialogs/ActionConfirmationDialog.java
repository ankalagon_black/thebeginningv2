package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.messages.ActionConfirmation;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordV2;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 16.07.2017.
 */

public class ActionConfirmationDialog extends DialogFragment {
    public static final String TARGET_RECORD_KEY = "TargetRecordKey";
    public static final String RESULT_RECORD_KEY = "ResultRecordKey";

    private IdentifiedAccountedRecordV2 recordV2;

    private CheckBox addGpsMark;

    private InventorySettings inventorySettings;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.action_confirmation, null);

        recordV2 = new IdentifiedAccountedRecordV2(view);

        addGpsMark = (CheckBox) view.findViewById(R.id.add_gps_mark);

        return builder.setTitle(R.string.result).
                setView(view).setNegativeButton(R.string.cancel, null).
                setPositiveButton(R.string.convert, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Bundle bundle = getArguments();
                        Record targetRecord = bundle.getParcelable(TARGET_RECORD_KEY);
                        AccountedRecord resultRecord = bundle.getParcelable(RESULT_RECORD_KEY);
                        EventBus.getDefault().post(new ActionConfirmation(targetRecord, resultRecord, addGpsMark.isChecked()));
                    }
                }).
                create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        if(savedInstanceState == null){
            AccountedRecord resultRecord = getArguments().getParcelable(RESULT_RECORD_KEY);

            recordV2.setAccountedRecord(resultRecord,
                    inventorySettings.isAllowColorIdentification(),
                    inventorySettings.isAllowGroupInventory(),
                    inventorySettings.isAllowGPS());

            if(inventorySettings.isAllowGPS()) addGpsMark.setVisibility(View.VISIBLE);
            else addGpsMark.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }
}
