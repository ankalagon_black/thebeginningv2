package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.adapters.RemainedRecordsAdapterV2;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process.models.AddGroupRecordsModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordV2;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Константин on 18.07.2017.
 */

public class AddGroupRecordsDialog extends SupportModelDialog<InventoryProcessRepository, AddGroupRecordsModel>
        implements AddGroupRecordsModel.ViewInterface, RemainedRecordsAdapterV2.Interface{
    public static final String ACTION_TYPE_KEY = "ActionTypeKey";
    public static final int MOVE = 0, UPDATE = 1;

    private ListView listView;
    private RecyclerView recyclerView;
    private RemainedRecordsAdapterV2 adapterV2;

    private CheckBox addGpsMark;

    private IdentifiedAccountedRecordV2 recordV2;

    public static AddGroupRecordsDialog getInstance(int actionType, AccountedRecord accountedRecord){
        AddGroupRecordsDialog dialog = new AddGroupRecordsDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(ACTION_TYPE_KEY, actionType);
        bundle.putParcelable(AddGroupRecordsModel.ACCOUNTED_RECORD_KEY, accountedRecord);

        dialog.setArguments(bundle);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.add_group_records, null);

        listView = (ListView) view.findViewById(R.id.list);

        TextView groupRecordsDescription = (TextView) view.findViewById(R.id.group_records_description);
        groupRecordsDescription.setText(R.string.choose_group_record_to_move);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        addGpsMark = (CheckBox) view.findViewById(R.id.add_gps_mark);

        recordV2 = new IdentifiedAccountedRecordV2(view);

        Bundle bundle = getArguments();

        TextView actionType = (TextView) view.findViewById(R.id.action_type);
        actionType.setText(bundle.getInt(ACTION_TYPE_KEY) == MOVE ? R.string.record_was_moved : R.string.record_was_updated);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.setTitle(R.string.group_records_movement).
                setView(view).
                setNegativeButton(R.string.close, null).
                setPositiveButton(R.string.move_all, null).
                create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProcessDialog dialog = new ProcessDialog();
                        dialog.show(getFragmentManager(), dialog.getTag());

                        getModel().moveAll(addGpsMark.isChecked());
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            InventorySettings inventorySettings = getModel().getInventorySettings();

            recordV2.setAccountedRecord(getModel().getAccountedRecord(),
                    inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS());

            if(inventorySettings.isAllowGPS()) addGpsMark.setVisibility(View.VISIBLE);
            else addGpsMark.setVisibility(View.INVISIBLE);
        }

        getModel().getGroupBarcodes();
    }

    @Override
    protected AddGroupRecordsModel createModel() {
        AccountedRecord accountedRecord = getArguments().getParcelable(AddGroupRecordsModel.ACCOUNTED_RECORD_KEY);

        return new AddGroupRecordsModel(this, getRepository().getDatabaseManager(), accountedRecord, new GpsAndGroupInventoryDomain(getRepository().getDatabaseManager(), getActivity()), getRepository());
    }

    @Override
    public void onGroupRecordsReceived(InventorySettings inventorySettings, List<RemainedRecord> groupRecords) {
        adapterV2 = new RemainedRecordsAdapterV2(groupRecords, this, inventorySettings);
        recyclerView.setAdapter(adapterV2);
    }

    @Override
    public void onGroupBarcodesReceived(List<String> groupBarcodes) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, groupBarcodes);
        listView.setAdapter(adapter);
    }

    @Override
    public void onRecordMoved(int position) {
        EventBus.getDefault().post(new StopProcess());
        adapterV2.notifyItemRemoved(position);
    }

    @Override
    public void onAllRecordMoved() {
        EventBus.getDefault().post(new StopProcess());

        Toast.makeText(getActivity(), R.string.group_records_moved, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onGPSNotAvailable() {
        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), R.string.gps_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFunctionalButtonPressed(Record record) {
        ProcessDialog dialog = new ProcessDialog();
        dialog.show(getFragmentManager(), dialog.getTag());

        getModel().move((RemainedRecord) record, addGpsMark.isChecked());
    }
}
