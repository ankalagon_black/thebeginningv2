package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 17.06.2017.
 */

public class ColumnValue {
    private String column;
    private String value;

    public ColumnValue(String column, String value){
        this.column = column;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public String getValue() {
        return value;
    }
}
