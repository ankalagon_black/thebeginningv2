package com.example.thebeginningv2.inventory_process.repository;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.repository.Repository;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;

import java.io.File;

/**
 * Created by Константин on 14.07.2017.
 */

public class InventoryProcessRepository extends Repository {
    private PathManager pathManager;
    private DatabaseManager databaseManager;

    public InventoryProcessRepository(Activity activity, PathManager pathManager) {
        super(activity);
        this.pathManager = pathManager;
        this.databaseManager = new DatabaseManager(SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildPath(PathManager.DB), null));
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public DataModeParser getDataModeParser() {
        return new DataModeParser(new File(pathManager.getDataChildPath(PathManager.MODE)));
    }

    public InputSettings getInputSettings() {
        Activity activity = getActivity();
        SharedPreferences preferences = activity.getSharedPreferences(activity.getString(R.string.input_settings_key), Context.MODE_PRIVATE);

        int inputType = preferences.getInt(activity.getString(R.string.input_type_key), InputSettings.DEFAULT_INPUT_TYPE);

        return new InputSettings(inputType);
    }

    public void getLocation(LocationListener locationListener) {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Requested permission is missing", Toast.LENGTH_SHORT).show();
        }
        else locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, Looper.getMainLooper());
    }

    @Override
    public void releaseResources() {
        databaseManager.close();
    }
}
