package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.DeletionResult;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.domains.DeleteAccountedRecordsDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.AddedRemainedRecord;
import com.example.thebeginningv2.inventory_process.messages.AllAccountedRecordsDeleted;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 10.06.2017.
 */

public class AccountedRecordsModel extends Model {
    private ViewInterface viewInterface;
    private DeleteAccountedRecordsDomain domain;
    private InventoryProcessRepository repository;

    private InventorySettings inventorySettings;
    private List<AccountedRecord> accountedRecords;

    public AccountedRecordsModel(ViewInterface viewInterface, DeleteAccountedRecordsDomain domain, InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.domain = domain;
        this.repository = repository;
    }

    public void getAccountedRecords(){
        DisposableSingleObserver<List<AccountedRecord>> observer = new DisposableSingleObserver<List<AccountedRecord>>() {
            @Override
            public void onSuccess(List<AccountedRecord> value) {
                accountedRecords = value;
                viewInterface.onAccountedRecordsReceived(inventorySettings, accountedRecords);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

       Single<List<AccountedRecord>> single = Single.fromCallable(new Callable<List<AccountedRecord>>() {
           @Override
           public List<AccountedRecord> call() throws Exception {
               return repository.getDatabaseManager().getAccountedRecords();
           }
       });

        subscribe(single, observer);
    }

    public void getInputSettings(){
        InputSettings inputSettings = repository.getInputSettings();
        viewInterface.setInputType(inputSettings.getInputType());
    }

    public void delete(final AccountedRecord accountedRecord){
        DisposableSingleObserver<DeletionResult> observer = new DisposableSingleObserver<DeletionResult>() {
            @Override
            public void onSuccess(DeletionResult value) {
                EventBus.getDefault().post(new DeletedAccountedRecord(accountedRecord));

                if(value.getRemainedRecord() != null){
                    EventBus.getDefault().post(new AddedRemainedRecord(value.getRemainedRecord()));
                    viewInterface.onAccountedRecordRemoved(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<DeletionResult> single = Single.fromCallable(new Callable<DeletionResult>() {
            @Override
            public DeletionResult call() throws Exception {
                RemainedRecord remainedRecord = domain.delete(accountedRecord);

                if(remainedRecord != null && inventorySettings.isAllowGroupInventory() && remainedRecord.isGroup()){
                    DatabaseManager databaseManager= repository.getDatabaseManager();
                    List<ColumnValue> columnValues = databaseManager.getGroupBarcodes(remainedRecord.getId());

                    boolean hasGroupRecords = repository.getDatabaseManager().hasGroupAccountedRecords(columnValues);

                    return new DeletionResult(remainedRecord, hasGroupRecords);
                }
                else return new DeletionResult(remainedRecord, false);
            }
        });

        subscribe(single, observer);
    }

    public void deleteAll(){
        DisposableSingleObserver<List<RemainedRecord>> observer = new DisposableSingleObserver<List<RemainedRecord>>() {
            @Override
            public void onSuccess(List<RemainedRecord> value) {
                for (RemainedRecord i: value) EventBus.getDefault().post(new AddedRemainedRecord(i));
                EventBus.getDefault().post(new AllAccountedRecordsDeleted());
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<RemainedRecord>> single = Single.fromCallable(new Callable<List<RemainedRecord>>() {
            @Override
            public List<RemainedRecord> call() throws Exception {
                return domain.deleteAll(accountedRecords);
            }
        });

        subscribe(single, observer);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettingsReceived(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordAdded(AddedAccountedRecord addedAccountedRecord){
        AccountedRecord accountedRecord = addedAccountedRecord.getAccountedRecord();

        accountedRecords.add(0, accountedRecord);

        viewInterface.onAccountedRecordAdded(accountedRecord);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordDeleted(DeletedAccountedRecord deletedAccountedRecord){
        AccountedRecord accountedRecord = deletedAccountedRecord.getAccountedRecord();

        int index;
        if(accountedRecord.isIdentified()) index = findRecord(accountedRecord.getId());
        else index = findRecord(accountedRecord.getBarcode());

        if(index != -1) {
            AccountedRecord record = accountedRecords.get(index);
            accountedRecords.remove(index);
            viewInterface.onAccountedRecordRemoved(record);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAccountedRecordsDeleted(AllAccountedRecordsDeleted allAccountedRecordsDeleted){
        accountedRecords.clear();

        viewInterface.onAllAccountedRecordsRemoved();
    }

    private int findRecord(String value){
        int size = accountedRecords.size();
        for(int i = 0; i < size; i++){
            AccountedRecord accountedRecord = accountedRecords.get(i);
            if(!accountedRecord.isIdentified() && accountedRecord.getBarcode().equals(value)) return i;
        }

        return -1;
    }

    private int findRecord(int id){
        int size = accountedRecords.size();
        for(int i = 0; i < size; i++){
            AccountedRecord accountedRecord = accountedRecords.get(i);
            if(accountedRecord.isIdentified() && accountedRecord.getId() == id) return i;
        }

        return -1;
    }

    public interface ViewInterface{
        void onAccountedRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> accountedRecords);

        void onAccountedRecordAdded(AccountedRecord accountedRecord);

        void onAccountedRecordRemoved(AccountedRecord accountedRecord);
        void onAccountedRecordRemoved(DeletionResult deletionResult);
        void onAllAccountedRecordsRemoved();

        void setInputType(int inputType);

        void onError(Throwable e);
    }
}
