package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.messages.ActionProcessFinished;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 16.07.2017.
 */

public class ActionProcessDialog extends DialogFragment {
    public static final String ACTION_TYPE_KEY = "ActionTypeKey";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        int type = getArguments().getInt(ACTION_TYPE_KEY);
        if(type == Result.UPDATE) builder = builder.setTitle(R.string.record_is_updating);
        else builder.setTitle(R.string.record_is_moving);

        return builder.
                setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActionProcessFinished(ActionProcessFinished actionProcessFinished){
        dismiss();
    }
}
