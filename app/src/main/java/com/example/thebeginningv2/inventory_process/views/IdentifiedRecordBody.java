package com.example.thebeginningv2.inventory_process.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Record;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class IdentifiedRecordBody {
    protected View view;

    protected TextView barcode, subtitle, isGroup, gpsMark;

    public IdentifiedRecordBody(View view){
        this.view = view;
        barcode = (TextView) view.findViewById(R.id.barcode);
        subtitle = (TextView) view.findViewById(R.id.subtitle);
        isGroup = (TextView) view.findViewById(R.id.is_group);
        gpsMark = (TextView) view.findViewById(R.id.gps_mark);
    }

    public void setRecord(Record record, boolean allowGroupInventory, boolean allowGPS){
        barcode.setText(record.getBarcode());

        if(record.hasSubtitle()){
            subtitle.setVisibility(View.VISIBLE);
            subtitle.setText(record.getSubtitle());
        }
        else subtitle.setVisibility(View.GONE);

        if(allowGroupInventory && record.isGroup()){
            isGroup.setVisibility(View.VISIBLE);
            Context context = view.getContext();
            isGroup.setText(context.getString(R.string.group_record));

        }
        else isGroup.setVisibility(View.GONE);

        if(allowGPS){
            gpsMark.setVisibility(View.VISIBLE);
            Context context = view.getContext();
            if(record.isAccounted()){
                AccountedRecord accountedRecord = (AccountedRecord) record;
                if(accountedRecord.hasGPSMark())
                    gpsMark.setText(String.format(Locale.US, context.getString(R.string.gps_mark), accountedRecord.getGPSMark()));
                else gpsMark.setText(context.getString(R.string.no_gps_mark));
            }
            else gpsMark.setVisibility(View.GONE);
        }
        else gpsMark.setVisibility(View.GONE);
    }
}
