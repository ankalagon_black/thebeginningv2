package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 17.07.2017.
 */

public class SearchResult {
    public static final int SUCCESS = 0, AMBIGUOUS = 1;

    private int type;
    private Record record;

    public SearchResult(int type, Record record){
        this.type = type;
        this.record = record;
    }

    public int getType() {
        return type;
    }

    public Record getRecord() {
        return record;
    }
}
