package com.example.thebeginningv2.inventory_process.messages;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;

/**
 * Created by Константин on 08.07.2017.
 */

public class DeletedAccountedRecord {
    private AccountedRecord accountedRecord;

    public DeletedAccountedRecord(AccountedRecord accountedRecord){
        this.accountedRecord = accountedRecord;
    }

    public AccountedRecord getAccountedRecord() {
        return accountedRecord;
    }
}
