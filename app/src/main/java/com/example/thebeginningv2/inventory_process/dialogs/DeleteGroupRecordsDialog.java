package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.adapters.AccountedRecordsAdapterV2;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.domains.DeleteAccountedRecordsDomain;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process.models.DeleteGroupRecordsModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordBodyV2;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Константин on 14.07.2017.
 */

public class DeleteGroupRecordsDialog extends SupportModelDialog<InventoryProcessRepository, DeleteGroupRecordsModel>
        implements  DeleteGroupRecordsModel.ViewInterface, AccountedRecordsAdapterV2.Interface{

    private ListView listView;
    private RecyclerView recyclerView;
    private AccountedRecordsAdapterV2 adapterV2;

    private RemainedRecordBodyV2 bodyV2;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.delete_group_records, null);

        listView = (ListView) view.findViewById(R.id.list);

        TextView groupRecordsDescription = (TextView) view.findViewById(R.id.group_records_description);
        groupRecordsDescription.setText(R.string.choose_group_record_to_delete);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        bodyV2 = new RemainedRecordBodyV2(view);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final AlertDialog alertDialog = builder.setTitle(R.string.group_records_deletion).
                setView(view).
                setNegativeButton(R.string.close, null).
                setPositiveButton(R.string.delete_all, null).
                create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProcessDialog dialog = new ProcessDialog();
                        dialog.show(getFragmentManager(), dialog.getTag());

                        getModel().deleteAll();
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            InventorySettings inventorySettings = getModel().getInventorySettings();

            bodyV2.setRemainedRecord(getModel().getRemainedRecord(),
                    inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS());
        }

        getModel().getGroupBarcodes();
    }

    @Override
    protected DeleteGroupRecordsModel createModel() {
        RemainedRecord remainedRecords = getArguments().getParcelable(DeleteGroupRecordsModel.REMAINED_RECORD_KEY);
        return new DeleteGroupRecordsModel(this, getRepository().getDatabaseManager(), remainedRecords, new DeleteAccountedRecordsDomain(getRepository().getDatabaseManager()));
    }

    @Override
    public void onGroupRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> groupRecords) {
        adapterV2 = new AccountedRecordsAdapterV2(AccountedRecordsAdapterV2.DELETE, groupRecords, inventorySettings, this);
        recyclerView.setAdapter(adapterV2);
    }

    @Override
    public void onGroupBarcodesReceived(List<String> groupBarcodes) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, groupBarcodes);
        listView.setAdapter(adapter);
    }

    @Override
    public void onRecordDeleted(int position) {
        EventBus.getDefault().post(new StopProcess());
        adapterV2.notifyItemRemoved(position);
    }

    @Override
    public void onAllRecordsDeleted() {
        EventBus.getDefault().post(new StopProcess());

        Toast.makeText(getActivity(), R.string.all_group_records_deleted, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFunctionalButtonPressed(Record record) {
        ProcessDialog dialog = new ProcessDialog();
        dialog.show(getFragmentManager(), dialog.getTag());

        getModel().delete((AccountedRecord) record);
    }
}
