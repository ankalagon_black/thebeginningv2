package com.example.thebeginningv2.inventory_process.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordViewHolder;

import java.util.List;

/**
 * Created by Константин on 18.07.2017.
 */

public class RemainedRecordsAdapterV2 extends RecyclerView.Adapter<RemainedRecordViewHolder> {

    private Interface anInterface;
    private List<RemainedRecord> remainedRecords;
    private InventorySettings inventorySettings;

    public RemainedRecordsAdapterV2(List<RemainedRecord> remainedRecords, Interface anInterface, InventorySettings inventorySettings){
        this.remainedRecords = remainedRecords;
        this.anInterface = anInterface;
        this.inventorySettings = inventorySettings;
    }

    @Override
    public RemainedRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MoveRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.remained_record, null));
    }

    @Override
    public void onBindViewHolder(RemainedRecordViewHolder holder, int position) {
        holder.setRecord(position, remainedRecords.get(position), inventorySettings.isAllowColorIdentification(),
                inventorySettings.isAllowGroupInventory(),
                inventorySettings.isAllowGPS());
    }

    @Override
    public int getItemCount() {
        return remainedRecords.size();
    }

    public class MoveRecordHolder extends RemainedRecordViewHolder{

        public MoveRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final RemainedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_arrow_back);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButtonPressed(record);
                }
            });
        }
    }

    public interface Interface{
        void onFunctionalButtonPressed(Record record);
    }
}
