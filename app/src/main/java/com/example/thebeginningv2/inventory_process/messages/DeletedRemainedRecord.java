package com.example.thebeginningv2.inventory_process.messages;

import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;

/**
 * Created by Константин on 08.07.2017.
 */

public class DeletedRemainedRecord {
    private RemainedRecord remainedRecord;

    public DeletedRemainedRecord(RemainedRecord remainedRecord){
        this.remainedRecord = remainedRecord;
    }

    public RemainedRecord getRemainedRecord() {
        return remainedRecord;
    }
}
