package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 14.06.2017.
 */

public class InputSettingsDialog extends DialogFragment {
    private RadioButton inputType1, inputType2;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View view = LayoutInflater.from(getContext()).inflate(R.layout.inventory_process_input_settings, null);

        inputType1 = (RadioButton) view.findViewById(R.id.input_type_1);
        inputType2 = (RadioButton) view.findViewById(R.id.input_type_2);

        return builder.setTitle(R.string.input_settings).
                setView(view).
                setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int inputType = inputType1.isChecked() ? InputSettings.INPUT_TYPE_1 : InputSettings.INPUT_TYPE_2;

                        SharedPreferences preferences = getActivity().getSharedPreferences(getString(R.string.input_settings_key), Context.MODE_PRIVATE);
                        preferences.edit().putInt(getString(R.string.input_type_key), inputType).apply();

                        EventBus.getDefault().post(new InputSettings(inputType));
                    }
                }).
                setNegativeButton(R.string.cancel, null).
                create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SharedPreferences preferences = getActivity().getSharedPreferences(getString(R.string.input_settings_key), Context.MODE_PRIVATE);

        int inputType = preferences.getInt(getString(R.string.input_type_key), InputSettings.DEFAULT_INPUT_TYPE);

        setInputSettings(inputType);
    }

    public void setInputSettings(int inputType) {
        switch (inputType){
            case InputSettings.INPUT_TYPE_1:
                inputType1.setChecked(true);
                break;
            case InputSettings.INPUT_TYPE_2:
                inputType2.setChecked(true);
                break;
        }
    }
}
