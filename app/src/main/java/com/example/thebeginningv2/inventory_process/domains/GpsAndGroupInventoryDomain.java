package com.example.thebeginningv2.inventory_process.domains;

import android.app.Activity;
import android.location.Location;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.util.List;

/**
 * Created by Константин on 16.07.2017.
 */

public class GpsAndGroupInventoryDomain {
    private DatabaseManager databaseManager;
    private Activity activity;

    public GpsAndGroupInventoryDomain(DatabaseManager databaseManager, Activity activity) {
        this.databaseManager = databaseManager;
        this.activity = activity;
    }

    public Result createRecord(AccountedRecord accountedRecord, Location location) {
        AccountedRecord result = new AccountedRecord(accountedRecord, accountedRecord.getAccountedQuantity(),
                location != null ? AccountedRecord.getGpsMark(location, activity) : null);

        databaseManager.addToAccountedRecords(result.getId(), result.getBarcode(),
                result.getAccountedQuantity(), result.getGPSMark());

        return new Result(result, false, Result.ADD);
    }

    public Result moveRecord(InventorySettings inventorySettings, RemainedRecord remainedRecord, float quantity, Location location){
        AccountedRecord accountedRecord = new AccountedRecord(remainedRecord, quantity,
                location != null ? AccountedRecord.getGpsMark(location, activity) : null);

        databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity(),
                accountedRecord.getGPSMark());

        if(inventorySettings.isAllowGroupInventory() && accountedRecord.isGroup()){
            List<ColumnValue> groupBarcodes = databaseManager.getGroupBarcodes(accountedRecord.getId());

            boolean hasGroupRecords = databaseManager.hasGroupRemainedRecords(groupBarcodes);

            return new Result(accountedRecord, hasGroupRecords, Result.MOVE);
        }
        else return new Result(accountedRecord, false, Result.MOVE);
    }

    public Result updateRecord(InventorySettings inventorySettings, AccountedRecord previousRecord, float quantity){
        AccountedRecord updatedRecord = new AccountedRecord(previousRecord, quantity);
        return updateRecord(inventorySettings, updatedRecord);
    }

    public Result updateRecord(InventorySettings inventorySettings, AccountedRecord updatedRecord){
        databaseManager.updateAccountedRecord(updatedRecord.getId(), updatedRecord.getBarcode(), updatedRecord.getAccountedQuantity());

        if(inventorySettings.isAllowGroupInventory() && updatedRecord.isGroup()){
            List<ColumnValue> groupBarcodes = databaseManager.getGroupBarcodes(updatedRecord.getId());

            boolean hasGroupRecords = databaseManager.hasGroupRemainedRecords(groupBarcodes);

            return new Result(updatedRecord, hasGroupRecords, Result.UPDATE);
        }
        else return new Result(updatedRecord, false, Result.UPDATE);
    }
}
