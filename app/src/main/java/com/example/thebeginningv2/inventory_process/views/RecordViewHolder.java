package com.example.thebeginningv2.inventory_process.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.thebeginningv2.inventory_process.data_types.Record;

/**
 * Created by Константин on 15.06.2017.
 */

public abstract class RecordViewHolder<R extends Record> extends RecyclerView.ViewHolder {

    public RecordViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void setRecord(int position, R record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS);
}
