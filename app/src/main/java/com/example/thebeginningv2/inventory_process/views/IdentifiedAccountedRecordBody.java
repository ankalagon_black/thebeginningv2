package com.example.thebeginningv2.inventory_process.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class IdentifiedAccountedRecordBody extends IdentifiedRecordBody {
    protected TextView outOf, difference;

    public IdentifiedAccountedRecordBody(View view) {
        super(view);
        outOf = (TextView) view.findViewById(R.id.out_of);
        difference = (TextView) view.findViewById(R.id.difference);
    }

    public void setAccountedRecord(AccountedRecord accountedRecord, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS){
        setRecord(accountedRecord, allowGroupInventory, allowGPS);
        Context context = view.getContext();

        outOf.setText(String.format(Locale.US, context.getString(R.string.out_of_stated),
                accountedRecord.getAccountedQuantity(), accountedRecord.getStatedQuantity()));

        switch (accountedRecord.getDifferenceType()){
            case AccountedRecord.MATCHING:
                difference.setVisibility(View.GONE);

                if(allowColorIdentification) outOf.setTextColor(context.getResources().getColor(R.color.green_color));
                break;

            case AccountedRecord.WITH_RESIDUE:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, context.getString(R.string.residue_difference),
                        accountedRecord.getDifference()));

                if(allowColorIdentification){
                    int color = context.getResources().getColor(R.color.blue_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;

            case AccountedRecord.WITH_SURPLUS:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, context.getString(R.string.surplus_difference),
                        accountedRecord.getDifference()));

                if(allowColorIdentification){
                    int color = context.getResources().getColor(R.color.red_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;
        }
    }

    public TextView getOutOf() {
        return outOf;
    }

    public TextView getDifference() {
        return difference;
    }
}
