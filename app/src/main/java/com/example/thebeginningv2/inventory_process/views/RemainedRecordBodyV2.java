package com.example.thebeginningv2.inventory_process.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.dialogs.AboutRecordDialog;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;

/**
 * Created by Константин on 17.07.2017.
 */

public class RemainedRecordBodyV2 extends RemainedRecordBody {
    protected ImageButton functionalButton;

    public RemainedRecordBodyV2(View view) {
        super(view);
        functionalButton = (ImageButton) view.findViewById(R.id.functional_button);
    }

    @Override
    public void setRemainedRecord(final RemainedRecord remainedRecord, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
        super.setRemainedRecord(remainedRecord, allowColorIdentification, allowGroupInventory, allowGPS);

        functionalButton.setImageResource(R.drawable.ic_action_info);
        functionalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AboutRecordDialog aboutRecordDialog = new AboutRecordDialog();

                Bundle bundle = new Bundle();
                bundle.putParcelable(AboutRecordModel.RECORD_KEY, remainedRecord);

                aboutRecordDialog.setArguments(bundle);
                aboutRecordDialog.show(((AppCompatActivity) view.getContext()).getSupportFragmentManager(), aboutRecordDialog.getTag());
            }
        });
    }
}
