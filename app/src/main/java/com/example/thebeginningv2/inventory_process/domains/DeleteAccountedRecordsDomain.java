package com.example.thebeginningv2.inventory_process.domains;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Константин on 14.07.2017.
 */

public class DeleteAccountedRecordsDomain {
    private DatabaseManager databaseManager;

    public DeleteAccountedRecordsDomain(DatabaseManager databaseManager){
        this.databaseManager = databaseManager;
    }

    public RemainedRecord delete(AccountedRecord accountedRecord){
        databaseManager.deleteFromAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode());
        if(accountedRecord.isIdentified()) return new RemainedRecord(accountedRecord);
        else return null;
    }

    public List<RemainedRecord> deleteAll(List<AccountedRecord> accountedRecords){
        List<RemainedRecord> remainedRecords = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();

        for (AccountedRecord i: accountedRecords){
            if(i.isIdentified()){
                ids.add(i.getId());
                remainedRecords.add(new RemainedRecord(i));
            }
        }

        if(ids.size() != 0) databaseManager.deleteAllAccountedRecords(ids);

        return remainedRecords;
    }
}
