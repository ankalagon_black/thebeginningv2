package com.example.thebeginningv2.inventory_process.presenters;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.thebeginningv2.core_v3.presenters.Presenter;
import com.example.thebeginningv2.inventory_process.data_types.Selection;
import com.example.thebeginningv2.inventory_process.messages.CloseSelection;
import com.example.thebeginningv2.inventory_process.messages.OpenSelection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 13.06.2017.
 */

public class AccountedRecordsPresenter extends Presenter<AccountedRecordsPresenter.StateModel> {
    public static final String SELECTION_KEY = "SelectionKey";

    private Interface anInterface;

    public AccountedRecordsPresenter(Interface anInterface){
        this.anInterface = anInterface;
    }

    @Override
    public void onActivityCreated(Bundle bundle, Bundle savedInstanceState) {
        super.onActivityCreated(bundle, savedInstanceState);
        EventBus.getDefault().register(this);

        if(getStateModel().isInSelectionMode()) anInterface.openSelection();
        else anInterface.closeSelection();
    }

    @Override
    protected StateModel createStateModel(Bundle bundle) {
        return new StateModel();
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
    }

    public Selection getSelection(){
        return getStateModel().getSelection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectionChanged(Selection newSelection){
        Selection oldSelection = getSelection();
        getStateModel().setSelection(newSelection);
        anInterface.changeSelection(oldSelection, newSelection);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenSelection(OpenSelection openSelection){
        getStateModel().setInSelectionMode(true);
        anInterface.openSelection();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCloseSelection(CloseSelection closeSelection){
        getStateModel().setInSelectionMode(false);
        anInterface.closeSelection();
    }

    public interface Interface{
        void changeSelection(Selection oldSelection, Selection newSelection);
        void openSelection();
        void closeSelection();
    }

    public static class StateModel implements Parcelable{
        private boolean isInSelectionMode = false;
        private Selection selection;

        public StateModel(){
            selection = new Selection();
        }

        protected StateModel(Parcel in) {
            isInSelectionMode = in.readByte() != 0;
            selection = in.readParcelable(Selection.class.getClassLoader());
        }

        public Selection getSelection() {
            return selection;
        }

        public void setSelection(Selection selection) {
            this.selection = selection;
        }

        public boolean isInSelectionMode() {
            return isInSelectionMode;
        }

        public void setInSelectionMode(boolean inSelectionMode) {
            isInSelectionMode = inSelectionMode;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isInSelectionMode ? 1 : 0));
            dest.writeParcelable(selection, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<StateModel> CREATOR = new Creator<StateModel>() {
            @Override
            public StateModel createFromParcel(Parcel in) {
                return new StateModel(in);
            }

            @Override
            public StateModel[] newArray(int size) {
                return new StateModel[size];
            }
        };
    }
}
