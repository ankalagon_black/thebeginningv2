package com.example.thebeginningv2.inventory_process.data_types;

import android.app.Activity;
import android.location.Location;
import android.os.Parcel;

import com.example.thebeginningv2.R;

import java.util.Locale;

/**
 * Created by Константин on 13.06.2017.
 */

public class AccountedRecord extends Record{
    public static final int NO_ID = -1;
    public static final int MATCHING = 0, WITH_RESIDUE = 1, WITH_SURPLUS = 2;

    public static int getDifferenceType(float accountedQuantity, float statedQuantity){
        if(Math.abs(accountedQuantity - statedQuantity) < 0.00001f) return MATCHING;
        else if(accountedQuantity < statedQuantity) return WITH_RESIDUE;
        else return WITH_SURPLUS;
    }

    public static String getGpsMark(Location location, Activity activity){
        return String.format(Locale.US, activity.getString(R.string.float_2_decimal), location.getLongitude()) + " " +
                String.format(Locale.US, activity.getString(R.string.float_2_decimal), location.getLatitude()) + " " +
                String.format(Locale.US, activity.getString(R.string.float_2_decimal), location.getAltitude());
    }

    private int id;
    private String barcode, subtitle;
    private float accountedQuantity, statedQuantity;
    private boolean isGroup;
    private String gpsMark = null;

    public AccountedRecord(String barcode, float accountedQuantity, String gpsMark) {
        this(NO_ID, barcode, null, accountedQuantity, 0f, false, gpsMark);
    }

    public AccountedRecord(int id, String barcode, String subtitle, float accountedQuantity, float statedQuantity, boolean isGroup, String gpsMark){
        this.id = id;
        this.barcode = barcode;
        this.subtitle = subtitle;
        this.accountedQuantity = accountedQuantity;
        this.statedQuantity = statedQuantity;
        this.isGroup = isGroup;
        this.gpsMark = gpsMark;
    }

    public AccountedRecord(Record record, float accountedQuantity){
        this(record.getId(), record.getBarcode(), record.getSubtitle(), accountedQuantity,
                record.getStatedQuantity(), record.isGroup(),
                record.isAccounted() ? ((AccountedRecord) record).getGPSMark() : null);
    }

    public AccountedRecord(Record record, float accountedQuantity, String gpsMark){
        this(record.getId(), record.getBarcode(), record.getSubtitle(), accountedQuantity,
                record.getStatedQuantity(), record.isGroup(),
                gpsMark);
    }

    protected AccountedRecord(Parcel in) {
        id = in.readInt();
        barcode = in.readString();
        subtitle = in.readString();
        accountedQuantity = in.readFloat();
        statedQuantity = in.readFloat();
        isGroup = in.readInt() == 1;
        gpsMark = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(barcode);
        dest.writeString(subtitle);
        dest.writeFloat(accountedQuantity);
        dest.writeFloat(statedQuantity);
        dest.writeInt(isGroup ? 1 : 0);
        dest.writeString(gpsMark);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AccountedRecord> CREATOR = new Creator<AccountedRecord>() {
        @Override
        public AccountedRecord createFromParcel(Parcel in) {
            return new AccountedRecord(in);
        }

        @Override
        public AccountedRecord[] newArray(int size) {
            return new AccountedRecord[size];
        }
    };

    public boolean isIdentified(){
        return id != NO_ID;
    }

    @Override
    public boolean isAccounted() {
        return true;
    }

    public int getId(){
        return id;
    }

    public String getBarcode() {
        return barcode;
    }

    public boolean hasSubtitle(){
        return subtitle != null;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public float getAccountedQuantity() {
        return accountedQuantity;
    }

    public float getStatedQuantity() {
        return statedQuantity;
    }

    @Override
    public boolean isGroup() {
        return isGroup;
    }

    public boolean hasGPSMark() {
        return gpsMark != null;
    }

    public String getGPSMark() {
        return gpsMark;
    }

    public float getDifference(){
        return Math.abs(accountedQuantity - statedQuantity);
    }

    public int getDifferenceType(){
        if(Math.abs(accountedQuantity - statedQuantity) < 0.00001f) return MATCHING;
        else if(accountedQuantity < statedQuantity) return WITH_RESIDUE;
        else return WITH_SURPLUS;
    }
}
