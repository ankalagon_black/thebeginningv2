package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 13.06.2017.
 */

public class RecordsCount {
    private int accountedRecordsCount, remainedRecordsCount;

    public RecordsCount(int accountedRecordsCount, int remainedRecordsCount){
        this.accountedRecordsCount = accountedRecordsCount;
        this.remainedRecordsCount = remainedRecordsCount;
    }

    public void addAccountedRecordsCount(int count){
        accountedRecordsCount += count;
    }

    public void setAccountedRecordsCount(int count){
        this.accountedRecordsCount = count;
    }

    public int getAccountedRecordsCount() {
        return accountedRecordsCount;
    }

    public void addRemainedRecordsCount(int count){
        remainedRecordsCount += count;
    }

    public int getRemainedRecordsCount() {
        return remainedRecordsCount;
    }
}
