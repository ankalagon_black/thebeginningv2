package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.messages.RecordUpdateFinished;
import com.example.thebeginningv2.inventory_process.models.EditQuantityModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordV2;
import com.example.thebeginningv2.inventory_process.views.UnidentifiedRecordBody;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

/**
 * Created by Константин on 14.06.2017.
 */

public class EditQuantityDialog extends SupportModelDialog<InventoryProcessRepository, EditQuantityModel> implements EditQuantityModel.ViewInterface{

    private IdentifiedAccountedRecordV2 identifiedAccountedRecordV2;
    private UnidentifiedRecordBody unidentifiedRecordBody;

    private EditText accountedQuantityEdit;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(R.layout.edit_record_quantity, null);

        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.accounted_record_container);

        final AccountedRecord accountedRecord = getArguments().getParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY);

        if(accountedRecord.isIdentified()){
            View identifiedRecord = layoutInflater.inflate(R.layout.identified_accounted_record_v2, frameLayout, true);
            identifiedAccountedRecordV2 = new IdentifiedAccountedRecordV2(identifiedRecord);
        }
        else {
            View unidentifiedRecord = layoutInflater.inflate(R.layout.unidentified_accounted_record_body_v2, frameLayout, true);
            unidentifiedRecordBody = new UnidentifiedRecordBody(unidentifiedRecord);
        }

        accountedQuantityEdit = (EditText) view.findViewById(R.id.accounted_quantity_edit);
        accountedQuantityEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentQuantity = editable.toString();
                if (NumberUtils.isCreatable(currentQuantity)) {
                    if(accountedRecord.isIdentified()){
                        updateDifference(accountedRecord, Float.parseFloat(currentQuantity));
                    }
                    else updateQuantity(Float.parseFloat(currentQuantity));
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.setTitle(R.string.edit_quantity).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);
                    }
                }).
                setPositiveButton(R.string.edit, null).
                create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String currentQuantity = accountedQuantityEdit.getEditableText().toString();
                        if(NumberUtils.isCreatable(currentQuantity)){
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);

                            float quantity = Float.parseFloat(currentQuantity);

                            if (!getModel().getInventorySettings().isAllowDuplicates() && quantity > 1.0f) {
                                Toast.makeText(getActivity(), R.string.duplicates_not_alllowed, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                AccountedRecordUpdateDialog dialog = new AccountedRecordUpdateDialog();
                                dialog.show(getFragmentManager(), dialog.getTag());

                                getModel().updateRecordQuantity(accountedRecord, quantity);
                            }
                        }
                        else Toast.makeText(getContext(), R.string.quantity_not_a_number, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            AccountedRecord accountedRecord = getArguments().getParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY);

            InventorySettings inventorySettings = getModel().getInventorySettings();

            if(identifiedAccountedRecordV2 != null) {
                identifiedAccountedRecordV2.setAccountedRecord(accountedRecord,
                        inventorySettings.isAllowColorIdentification(),
                        inventorySettings.isAllowGroupInventory(),
                        inventorySettings.isAllowGPS());

                updateDifference(accountedRecord, accountedRecord.getAccountedQuantity());
            }
            else{
                unidentifiedRecordBody.setAccountedRecord(accountedRecord, inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGPS());
                if(getModel().getInventorySettings().isAllowColorIdentification()) unidentifiedRecordBody.getQuantity().
                        setTextColor(getResources().getColor(R.color.purple_color));
            }

            accountedQuantityEdit.setText(String.format(Locale.US, getString(R.string.float_3_decimal), accountedRecord.getAccountedQuantity()));
        }
    }

    private void updateDifference(AccountedRecord accountedRecord, float accountedQuantity){
        TextView outOf = identifiedAccountedRecordV2.getOutOf(), difference = identifiedAccountedRecordV2.getDifference();

        outOf.setText(String.format(Locale.US, getString(R.string.out_of_stated),
                accountedQuantity, accountedRecord.getStatedQuantity()));

        switch (AccountedRecord.getDifferenceType(accountedQuantity, accountedRecord.getStatedQuantity())){
            case AccountedRecord.MATCHING:
                difference.setVisibility(View.INVISIBLE);

                if(getModel().getInventorySettings().isAllowColorIdentification())
                    outOf.setTextColor(getResources().getColor(R.color.green_color));
                break;

            case AccountedRecord.WITH_RESIDUE:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, getString(R.string.residue_difference),
                        accountedRecord.getStatedQuantity() - accountedQuantity));

                if(getModel().getInventorySettings().isAllowColorIdentification()){
                    int color = getResources().getColor(R.color.blue_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;

            case AccountedRecord.WITH_SURPLUS:
                difference.setVisibility(View.VISIBLE);
                difference.setText(String.format(Locale.US, getString(R.string.surplus_difference),
                        accountedQuantity - accountedRecord.getStatedQuantity()));

                if(getModel().getInventorySettings().isAllowColorIdentification()){
                    int color = getResources().getColor(R.color.red_color);
                    outOf.setTextColor(color);
                    difference.setTextColor(color);
                }
                break;
        }
    }

    private void updateQuantity(float newQuantity){
        TextView quantity = unidentifiedRecordBody.getQuantity();

        quantity.setText(String.format(Locale.US, getString(R.string.float_1_demical), newQuantity));
    }

    @Override
    protected EditQuantityModel createModel() {
        return new EditQuantityModel(this, getRepository().getDatabaseManager());
    }

    @Override
    public void onRecordQuantityEdited(AccountedRecord updatedRecord, boolean hasGroupRecords) {
        EventBus.getDefault().post(new RecordUpdateFinished());
        Toast.makeText(getActivity(), R.string.quantity_edited, Toast.LENGTH_SHORT).show();

        if(hasGroupRecords){
            EditGroupRecordsDialog dialog = EditGroupRecordsDialog.getInstance(updatedRecord);
            dialog.show(getFragmentManager(), dialog.getTag());
        }

        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new RecordUpdateFinished());
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public static class AccountedRecordUpdateDialog extends DialogFragment{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder.setCancelable(false).
                    setTitle(R.string.record_is_updating).
                    setView(LayoutInflater.from(getActivity()).inflate(R.layout.progress_bar, null)).
                    create();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            EventBus.getDefault().unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onAccountedRecordDeleted(RecordUpdateFinished recordUpdateFinished){
            dismiss();
        }
    }
}
