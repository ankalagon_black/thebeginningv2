package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 16.07.2017.
 */

public class Result {
    public static final int UPDATE = 0, MOVE = 1, ADD = 2, DUPLICATES_NOT_ALLOWED = 3, UNIDENTIFIED_RECORD_NOT_ALLOWED = 4;

    private AccountedRecord accountedRecord;
    private boolean hasGroupRecords;
    private int type = -1;

    public Result(AccountedRecord accountedRecord, boolean hasGroupRecords){
        this.accountedRecord = accountedRecord;
        this.hasGroupRecords = hasGroupRecords;
    }

    public Result(AccountedRecord accountedRecord, boolean hasGroupRecords, int type){
        this(accountedRecord, hasGroupRecords);
        this.type = type;
    }

    public boolean isHasGroupRecords() {
        return hasGroupRecords;
    }

    public AccountedRecord getAccountedRecord() {
        return accountedRecord;
    }

    public int getType() {
        return type;
    }
}
