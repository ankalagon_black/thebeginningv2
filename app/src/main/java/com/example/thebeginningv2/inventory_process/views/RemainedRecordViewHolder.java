package com.example.thebeginningv2.inventory_process.views;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class RemainedRecordViewHolder extends RecordViewHolder<RemainedRecord> {
    private TextView index;
    protected ImageButton functionalButton;

    private RemainedRecordBody recordBody;

    public RemainedRecordViewHolder(View itemView) {
        super(itemView);
        recordBody = new RemainedRecordBody(itemView);
        index = (TextView) itemView.findViewById(R.id.index);
        functionalButton = (ImageButton) itemView.findViewById(R.id.functional_button);
    }

    @Override
    public void setRecord(int position, RemainedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
        recordBody.setRemainedRecord(record, allowColorIdentification, allowGroupInventory, allowGPS);

        index.setText(String.format(Locale.US, itemView.getContext().getString(R.string.index), position + 1));
    }
}
