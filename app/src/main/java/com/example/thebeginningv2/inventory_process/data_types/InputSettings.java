package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 10.06.2017.
 */

public class InputSettings {
    public static final int INPUT_TYPE_1 = 0, INPUT_TYPE_2 = 1;

    public static final int DEFAULT_INPUT_TYPE = INPUT_TYPE_2;

    private int inputType;

    public InputSettings(int inputType){
        this.inputType = inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public int getInputType() {
        return inputType;
    }
}
