package com.example.thebeginningv2.inventory_process.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.ColumnInfo;

import java.util.List;

/**
 * Created by Константин on 17.06.2017.
 */

public class AboutRecordAdapter extends RecyclerView.Adapter<AboutRecordAdapter.ColumnViewHolder> {
    private List<ColumnInfo> columnValues;

    public AboutRecordAdapter(List<ColumnInfo> columnValues){
        this.columnValues = columnValues;
    }

    @Override
    public ColumnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ColumnViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.about_record_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ColumnViewHolder holder, int position) {
        holder.setColumnValue(columnValues.get(position));
    }

    @Override
    public int getItemCount() {
        return columnValues.size();
    }

    public class ColumnViewHolder extends RecyclerView.ViewHolder{

        private TextView title, value;

        public ColumnViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            value = (TextView) itemView.findViewById(R.id.value);
        }

        public void setColumnValue(ColumnInfo columnInfo){
            Context context = itemView.getContext();

            if(columnInfo.hasTitle()) title.setText(columnInfo.getTitle());
            else title.setText(context.getString(R.string.name));

            if(columnInfo.hasValue()) value.setText(columnInfo.getValue());
            else title.setText(context.getString(R.string.no_value));

            if(columnInfo.hasTitleBackGroundColor()){
                try {
                    int color = Color.parseColor(columnInfo.getTitleBackgroundColor());
                    title.setBackgroundColor(color);
                }
                catch (IllegalArgumentException ignored){

                }
            }

            if(columnInfo.hasTitleTextColor()){
                try {
                    int color = Color.parseColor(columnInfo.getTitleTextColor());
                    title.setTextColor(color);
                }
                catch (IllegalArgumentException ignored){

                }
            }

            if(columnInfo.isTitleUseBoldFont()){
                title.setTypeface(title.getTypeface(), Typeface.BOLD);
            }

            float titleTextSize = columnInfo.getTitleTextSize();
            if(titleTextSize != -1){
                title.setTextSize(titleTextSize);
            }

            if(columnInfo.hasDataBackgroundColor()){
                try {
                    int color = Color.parseColor(columnInfo.getDataBackgroundColor());
                    value.setBackgroundColor(color);
                }
                catch (IllegalArgumentException ignored){

                }
            }

            if(columnInfo.hasDataTextColor()){
                try {
                    int color = Color.parseColor(columnInfo.getDataTextColor());
                    value.setTextColor(color);
                }
                catch (IllegalArgumentException ignored){

                }
            }

            if(columnInfo.isDataUseBoldFont()){
                value.setTypeface(value.getTypeface(), Typeface.BOLD);
            }

            float dataTextSize = columnInfo.getDataTextSize();
            if(dataTextSize != -1){
                value.setTextSize(dataTextSize);
            }
        }
    }
}
