package com.example.thebeginningv2.inventory_process.domains;

import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

/**
 * Created by Константин on 14.07.2017.
 */

public class InventoryStatisticsDomain {
    private DatabaseManager databaseManager;

    public InventoryStatisticsDomain(DatabaseManager databaseManager){
       this.databaseManager = databaseManager;
    }

    public RecordsCount getRecordsCount(){
        return new RecordsCount(databaseManager.getAccountedRecordsCount(), databaseManager.getRemainedRecordsCount());
    }

    public ShortStatistics getShortStatistics(){
        return new ShortStatistics(databaseManager.getAccountedRecordSum(), databaseManager.getStatedRecordsSum(),
                databaseManager.getResidueQuantity(), databaseManager.getSurplusQuantity(), databaseManager.getUnidentifiedRecordsSum());
    }
}
