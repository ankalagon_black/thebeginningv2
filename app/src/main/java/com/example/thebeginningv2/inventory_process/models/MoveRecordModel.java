package com.example.thebeginningv2.inventory_process.models;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by apple on 30.06.17.
 */

public class MoveRecordModel extends Model {
    public static final String REMAINED_RECORD_KEY = "RemainedRecordKey";

    private ViewInterface viewInterface;

    private InventorySettings inventorySettings;
    private GpsAndGroupInventoryDomain domain;
    private InventoryProcessRepository repository;

    public MoveRecordModel(ViewInterface viewInterface, GpsAndGroupInventoryDomain domain, InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.domain = domain;
        this.repository = repository;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void move(final RemainedRecord remainedRecord, final float quantity, final Location location){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                EventBus.getDefault().post(new DeletedRemainedRecord(remainedRecord));
                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                viewInterface.onRecordMoved(value.getAccountedRecord(), value.isHasGroupRecords());
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                return domain.moveRecord(inventorySettings, remainedRecord, quantity, location);
            }
        });

        subscribe(single, observer);
    }

    public void move(final RemainedRecord remainedRecord, final float quantity, final boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    move(remainedRecord, quantity, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    viewInterface.onGPSNotAvailable();
                }
            });
        }
        else move(remainedRecord, quantity, null);
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public interface ViewInterface{
        void onRecordMoved(AccountedRecord movedRecord, boolean hasGroupRecords);
        void onGPSNotAvailable();
        void onError(Throwable e);
    }
}
