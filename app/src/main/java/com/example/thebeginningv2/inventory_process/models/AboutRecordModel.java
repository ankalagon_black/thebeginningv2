package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.ColumnInfo;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 17.06.2017.
 */

public class AboutRecordModel extends Model {
    public static final String RECORD_KEY = "RecordKey";

    private ViewInterface viewInterface;
    private InventorySettings inventorySettings;

    private DatabaseManager databaseManager;

    private Record record;

    public AboutRecordModel(ViewInterface viewInterface, Record record, DatabaseManager databaseManager){
        this.viewInterface = viewInterface;
        this.record = record;
        this.databaseManager = databaseManager;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }


    public void getGroupBarcodes(){
        DisposableSingleObserver<List<ColumnValue>> observer = new DisposableSingleObserver<List<ColumnValue>>() {
            @Override
            public void onSuccess(List<ColumnValue> value) {
                List<String> groupBarcodes = new ArrayList<>(value.size());

                for (ColumnValue i: value) groupBarcodes.add(i.getValue());

                viewInterface.onGroupBarcodes(groupBarcodes);

                getGroupRecords(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<ColumnValue>> single = Single.fromCallable(new Callable<List<ColumnValue>>() {
            @Override
            public List<ColumnValue> call() throws Exception {
                return databaseManager.getGroupBarcodes(record.getId());
            }
        });

        subscribe(single,observer);
    }

    private void getGroupRecords(final List<ColumnValue> values){
        DisposableSingleObserver<List<Record>> observer = new DisposableSingleObserver<List<Record>>() {
            @Override
            public void onSuccess(List<Record> value) {
                viewInterface.onGroupRecords(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<Record>> single = Single.fromCallable(new Callable<List<Record>>() {
            @Override
            public List<Record> call() throws Exception {
                return databaseManager.findGroupRecords(values);
            }
        });

        subscribe(single, observer);
    }

    public void getColumnValues(){
        DisposableSingleObserver<List<ColumnInfo>> observer = new DisposableSingleObserver<List<ColumnInfo>>() {
            @Override
            public void onSuccess(List<ColumnInfo> value) {
                viewInterface.onResults(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<ColumnInfo>> single = Single.fromCallable(new Callable<List<ColumnInfo>>() {
            @Override
            public List<ColumnInfo> call() throws Exception {
                return databaseManager.getColumnValues(record.getId());
            }
        });

        subscribe(single, observer);
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public interface ViewInterface{
        void onGroupBarcodes(List<String> groupBarcodes);
        void onGroupRecords(InventorySettings inventorySettings, List<Record> groupRecord);

        void onResults(List<ColumnInfo> columnInfos);

        void onError(Throwable e);
    }

}
