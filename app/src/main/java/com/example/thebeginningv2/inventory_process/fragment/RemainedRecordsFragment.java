package com.example.thebeginningv2.inventory_process.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.fragments.SupportModelFragment;
import com.example.thebeginningv2.inventories.dialogs.AboutInventoryDialog;
import com.example.thebeginningv2.inventory_process.SearchRecordActivity;
import com.example.thebeginningv2.inventory_process.adapters.RemainedRecordsAdapter;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.inventory_process.dialogs.AboutRecordDialog;
import com.example.thebeginningv2.inventory_process.dialogs.MoveRecordDialog;
import com.example.thebeginningv2.inventory_process.models.AboutRecordModel;
import com.example.thebeginningv2.inventory_process.models.MoveRecordModel;
import com.example.thebeginningv2.inventory_process.models.RemainedRecordsModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 10.06.2017.
 */

public class RemainedRecordsFragment extends SupportModelFragment<InventoryProcessRepository, RemainedRecordsModel>
        implements  RemainedRecordsModel.ViewInterface, RemainedRecordsAdapter.Interface{
    private TextView shortStatistics;

    private RecyclerView recyclerView;
    private RemainedRecordsAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_remained_records, container, false);

        shortStatistics = (TextView) view.findViewById(R.id.short_statistics);

        recyclerView = (RecyclerView) view.findViewById(R.id.remained_records_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(this);

        getModel().getRemainedRecords();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected RemainedRecordsModel createModel() {
        return new RemainedRecordsModel(this, getRepository().getDatabaseManager());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.inventory_process_remained_records, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                Intent intent = new Intent(getContext(), SearchRecordActivity.class);
                startActivity(intent);
                return true;
            case R.id.more_about_inventory:
                AboutInventoryDialog aboutInventory = new AboutInventoryDialog();
                aboutInventory.show(getChildFragmentManager(), aboutInventory.getTag());
                return true;
            default:
                return false;
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onShortStatisticsReceived(ShortStatistics shortStatistics){
        updateShortStatistics(shortStatistics);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShortStatisticsUpdated(ShortStatistics shortStatistics){
        updateShortStatistics(shortStatistics);
    }

    private void updateShortStatistics(ShortStatistics shortStatistics){
        this.shortStatistics.setText(String.format(Locale.US, getString(R.string.short_statistics),
                shortStatistics.getAccountedQuantity(), shortStatistics.getStatedQuantity(), shortStatistics.getResidueQuantity(), shortStatistics.getSurplusQuantity(),
                shortStatistics.getUnidentifiedQuantity()));
    }

    @Override
    public void showPopupMenu(final RemainedRecord remainedRecord, View view) {
        PopupMenu popupMenu = new PopupMenu(getContext(), view);
        popupMenu.getMenuInflater().inflate(R.menu.inventory_process_remained_record_popup, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.move:
                        MoveRecordDialog moveRecordDialog = new MoveRecordDialog();

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(MoveRecordModel.REMAINED_RECORD_KEY, remainedRecord);

                        moveRecordDialog.setArguments(bundle);
                        moveRecordDialog.show(getChildFragmentManager(), moveRecordDialog.getTag());
                        return true;
                    case R.id.more_about_record:
                        AboutRecordDialog aboutRecordDialog = new AboutRecordDialog();
                        bundle = new Bundle();
                        bundle.putParcelable(AboutRecordModel.RECORD_KEY, remainedRecord);

                        aboutRecordDialog.setArguments(bundle);
                        aboutRecordDialog.show(getChildFragmentManager(), aboutRecordDialog.getTag());
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }

    @Override
    public void onRemainedRecordsReceived(InventorySettings inventorySettings, List<RemainedRecord> remainedRecords) {
        adapter = new RemainedRecordsAdapter(remainedRecords,
                inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS(),
                this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRemainedRecordAdded() {
        adapter.notifyRecordAdded();
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void onRemainedRecordRemoved(int position) {
        adapter.notifyRecordRemoved(position);
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
