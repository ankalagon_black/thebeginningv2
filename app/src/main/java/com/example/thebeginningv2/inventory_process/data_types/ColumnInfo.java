package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 05.08.2017.
 */

public class ColumnInfo {
    private String title;
    private String value;

    private String dataBackgroundColor, dataTextColor;
    private boolean dataUseBoldFont;
    private float dataTextSize;

    private String titleBackgroundColor, titleTextColor;
    private boolean titleUseBoldFont;
    private float titleTextSize;

    public ColumnInfo(String title, String value,
                      String dataBackgroundColor, String dataTextColor, boolean dataUseBoldFont, float dataTextSize,
                      String titleBackgroundColor, String titleTextColor, boolean titleUseBoldFont, float titleTextSize){
        this.title = title;
        this.value = value;

        this.dataBackgroundColor = dataBackgroundColor;
        this.dataTextColor = dataTextColor;
        this.dataUseBoldFont = dataUseBoldFont;
        this.dataTextSize = dataTextSize;

        this.titleBackgroundColor = titleBackgroundColor;
        this.titleTextColor = titleTextColor;
        this.titleUseBoldFont = titleUseBoldFont;
        this.titleTextSize = titleTextSize;
    }

    public boolean hasTitle(){
        return title != null;
    }

    public String getTitle() {
        return title;
    }

    public boolean hasValue(){
        return value != null;
    }

    public String getValue() {
        return value;
    }

    public boolean hasDataBackgroundColor(){
        return dataBackgroundColor != null;
    }

    public String getDataBackgroundColor() {
        return dataBackgroundColor;
    }

    public boolean hasDataTextColor(){
        return dataTextColor != null;
    }

    public String getDataTextColor() {
        return dataTextColor;
    }

    public boolean isDataUseBoldFont() {
        return dataUseBoldFont;
    }

    public float getDataTextSize() {
        return dataTextSize;
    }

    public boolean hasTitleBackGroundColor(){
        return titleBackgroundColor != null;
    }

    public String getTitleBackgroundColor() {
        return titleBackgroundColor;
    }

    public boolean hasTitleTextColor(){
        return titleTextColor != null;
    }

    public String getTitleTextColor() {
        return titleTextColor;
    }

    public boolean isTitleUseBoldFont() {
        return titleUseBoldFont;
    }

    public float getTitleTextSize() {
        return titleTextSize;
    }
}
