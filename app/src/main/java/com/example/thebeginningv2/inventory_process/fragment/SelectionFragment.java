package com.example.thebeginningv2.inventory_process.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Selection;
import com.example.thebeginningv2.inventory_process.messages.AddGpsMark;
import com.example.thebeginningv2.inventory_process.messages.CloseSelection;
import com.example.thebeginningv2.inventory_process.presenters.AccountedRecordsPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 13.06.2017.
 */

public class SelectionFragment extends Fragment {
    public static final String TAG = "SelectionFragment";

    private CheckBox matching, withResidue, withSurplus, unidentified;
    private CheckBox gpsMark;

    private boolean multipleCallProtection = false;

    private AddGpsMark addGpsMark;

    private InventorySettings inventorySettings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_selection, container, false);

        Button allRecords = (Button) view.findViewById(R.id.all_records);
        allRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedWithoutNotification(true, true, true, true);
                EventBus.getDefault().post(new Selection());
            }
        });

        Button back = (Button) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new CloseSelection());
            }
        });

        matching = (CheckBox) view.findViewById(R.id.matching);
        matching.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!multipleCallProtection) EventBus.getDefault().post(new Selection(b, withResidue.isChecked(), withSurplus.isChecked(), unidentified.isChecked()));
            }
        });
        withResidue = (CheckBox) view.findViewById(R.id.with_residue);
        withResidue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!multipleCallProtection) EventBus.getDefault().post(new Selection(matching.isChecked(), b, withSurplus.isChecked(), unidentified.isChecked()));
            }
        });
        withSurplus = (CheckBox) view.findViewById(R.id.with_surplus);
        withSurplus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!multipleCallProtection) EventBus.getDefault().post(new Selection(matching.isChecked(), withResidue.isChecked(), b, unidentified.isChecked()));
            }
        });
        unidentified = (CheckBox) view.findViewById(R.id.unidentified);
        unidentified.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!multipleCallProtection) EventBus.getDefault().post(new Selection(matching.isChecked(), withResidue.isChecked(), withSurplus.isChecked(), b));
            }
        });

        gpsMark = (CheckBox) view.findViewById(R.id.add_gps_mark);
        gpsMark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                addGpsMark.setState(b);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        addGpsMark = new AddGpsMark(gpsMark.isChecked());

        if(inventorySettings.isAllowGPS()) gpsMark.setVisibility(View.VISIBLE);
        else gpsMark.setVisibility(View.GONE);

        EventBus.getDefault().postSticky(addGpsMark);

        if(savedInstanceState == null){
            Bundle bundle = getArguments();
            Selection selection = bundle.getParcelable(AccountedRecordsPresenter.SELECTION_KEY);
            if(selection != null)
                setSelectedWithoutNotification(selection.isMatching(), selection.isWithResidue(), selection.isWithSurplus(), selection.isUnidentified());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettingsReceived(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    private void setSelectedWithoutNotification(boolean matching, boolean withResidue, boolean withSurplus, boolean unidentified){
        multipleCallProtection = true;

        this.matching.setChecked(matching);
        this.withResidue.setChecked(withResidue);
        this.withSurplus.setChecked(withSurplus);
        this.unidentified.setChecked(unidentified);

        multipleCallProtection = false;
    }
}
