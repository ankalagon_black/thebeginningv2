package com.example.thebeginningv2.inventory_process.fragment;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.fragments.SupportModelFragment;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.dialogs.AddGroupRecordsDialog;
import com.example.thebeginningv2.inventory_process.dialogs.ManualInputDialog;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.InputFinished;
import com.example.thebeginningv2.inventory_process.models.InputTypeModel;
import com.example.thebeginningv2.inventory_process.models.ManualInputModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.BarcodeScannerReceiver;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 15.06.2017.
 */

public class InputType1Fragment extends SupportModelFragment<InventoryProcessRepository, InputTypeModel>
        implements BarcodeScannerReceiver.ReceiveInterface, InputTypeModel.ViewInterface{
    public static final String TAG = "InputType1Fragment";

    private EditText quantityEdit;

    private BarcodeScannerReceiver receiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_input_type_1, container, false);

        quantityEdit = (EditText) view.findViewById(R.id.quantity_edit);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        receiver = new BarcodeScannerReceiver(this);

        IntentFilter intentFilter = new IntentFilter(BarcodeScannerReceiver.INTENT);

        getContext().registerReceiver(receiver, intentFilter);

        if(savedInstanceState == null) setDefault();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(receiver);
    }

    @Override
    protected InputTypeModel createModel() {
        DatabaseManager databaseManager = getRepository().getDatabaseManager();
        return new InputTypeModel(this, databaseManager, new GpsAndGroupInventoryDomain(databaseManager, getActivity()), getRepository());
    }

    private void setDefault(){
        quantityEdit.setText("1");
    }

    @Override
    public void onBarcodeScanned(String barcode) {
        String quantity = quantityEdit.getText().toString();

        if(barcode.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)){
            float currentQuantity = Float.parseFloat(quantity);

            if(!getModel().getInventorySettings().isAllowDuplicates() && currentQuantity > 1.0f) onDuplicatesNotAllowed();
            else {
                InputType2Fragment.InputDialog inputDialog = new InputType2Fragment.InputDialog();
                inputDialog.show(getChildFragmentManager(), inputDialog.getTag());

                getModel().input(new InputValues(barcode, currentQuantity));
            }

        }
    }

    @Override
    public void onDuplicatesNotAllowed() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), R.string.duplicates_not_alllowed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnidentifiedRecordsNotAllowed() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), R.string.unidentified_records_not_allowed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAccountedRecordAdded(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        Toast.makeText(getActivity(), R.string.record_added, Toast.LENGTH_SHORT).show();

        if(result.isHasGroupRecords()){
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(AddGroupRecordsDialog.MOVE,
                    result.getAccountedRecord());
            dialog.show(getFragmentManager(), dialog.getTag());
        }
    }

    @Override
    public void onAccountedRecordUpdated(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        Toast.makeText(getActivity(), R.string.record_updated, Toast.LENGTH_SHORT).show();

        if(result.isHasGroupRecords()){
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(AddGroupRecordsDialog.UPDATE,
                    result.getAccountedRecord());
            dialog.show(getFragmentManager(), dialog.getTag());
        }
    }

    @Override
    public void onUnidentifiedRecordAdded(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        Toast.makeText(getActivity(), R.string.record_added, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnidentifiedRecordUpdated(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        Toast.makeText(getActivity(), R.string.record_updated, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGPSNotAvailable() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getContext(), R.string.gps_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAmbiguousValue(final InputValues inputValues) {
        EventBus.getDefault().post(new InputFinished());

        setDefault();

        ManualInputDialog manualInputDialog = new ManualInputDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ManualInputModel.INPUT_VALUES_KEY, inputValues);

        manualInputDialog.setArguments(bundle);
        manualInputDialog.show(getFragmentManager(), manualInputDialog.getTag());

        Toast.makeText(getActivity(), R.string.ambiguous_ids_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
