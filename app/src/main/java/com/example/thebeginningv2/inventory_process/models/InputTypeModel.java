package com.example.thebeginningv2.inventory_process.models;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.data_types.SearchResult;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.domains.InputIdentifiedRecordDomain;
import com.example.thebeginningv2.inventory_process.domains.InputUnidentifiedRecordsDomain;
import com.example.thebeginningv2.inventory_process.messages.AddGpsMark;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 08.07.2017.
 */

public class InputTypeModel extends Model {
    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private GpsAndGroupInventoryDomain domain;
    private InventoryProcessRepository repository;

    private InventorySettings inventorySettings;

    private AddGpsMark addGpsMark = new AddGpsMark(false);

    public InputTypeModel(ViewInterface viewInterface, DatabaseManager databaseManager, GpsAndGroupInventoryDomain domain,
                          InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.domain = domain;
        this.repository = repository;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettingsReceived(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void addGpsMark(AddGpsMark addGpsMark){
        this.addGpsMark = addGpsMark;
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public void input(final InputValues inputValues){
        if(addGpsMark.isState()){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    input(inputValues, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else input(inputValues, null);
    }

    public void input(final InputValues inputValues, final Location location){
        DisposableSingleObserver<SearchResult> observer = new DisposableSingleObserver<SearchResult>() {
            @Override
            public void onSuccess(SearchResult value) {
                if(value.getType() == SearchResult.AMBIGUOUS) viewInterface.onAmbiguousValue(inputValues);
                else input(inputValues, value.getRecord(), location);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<SearchResult> single = Single.fromCallable(new Callable<SearchResult>() {
            @Override
            public SearchResult call() throws Exception {
                List<Integer> ids = databaseManager.findIdentifiedRecordIds(inputValues.getValue(), false, "2");
                if(ids.size() != 0){
                    if(ids.size() == 1){
                        int id = ids.get(0);
                        if(databaseManager.isAccounted(id)){
                            AccountedRecord accountedRecord = databaseManager.getIdentifiedAccountedRecord(id);
                            return new SearchResult(SearchResult.SUCCESS, accountedRecord);
                        }
                        else {
                            RemainedRecord remainedRecord = databaseManager.getRemainedRecord(id);
                            return new SearchResult(SearchResult.SUCCESS, remainedRecord);
                        }
                    }
                    else return new SearchResult(SearchResult.AMBIGUOUS, null);
                }
                else {
                    List<AccountedRecord> unidentifiedRecords = databaseManager.findUnidentifiedRecords(inputValues.getValue(), false);
                    if(unidentifiedRecords.size() != 0){
                        return new SearchResult(SearchResult.SUCCESS, unidentifiedRecords.get(0));
                    }
                    else return new SearchResult(SearchResult.SUCCESS, null);
                }
            }
        });

        subscribe(single, observer);
    }

    private void input(final InputValues inputValues, final Record record, final Location location){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                int type = value.getType();
                if(type == Result.DUPLICATES_NOT_ALLOWED) viewInterface.onDuplicatesNotAllowed();
                else if(type == Result.UNIDENTIFIED_RECORD_NOT_ALLOWED) viewInterface.onUnidentifiedRecordsNotAllowed();
                else {
                    if(type == Result.ADD) viewInterface.onUnidentifiedRecordAdded(value);
                    else if(type == Result.MOVE){
                        EventBus.getDefault().post(new DeletedRemainedRecord((RemainedRecord) record));

                        viewInterface.onAccountedRecordAdded(value);
                    }
                    else{
                        EventBus.getDefault().post(new DeletedAccountedRecord((AccountedRecord) record));
                        if(value.getAccountedRecord().isIdentified()) viewInterface.onAccountedRecordUpdated(value);
                        else viewInterface.onUnidentifiedRecordUpdated(value);
                    }

                    EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));
                }
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                if(record == null){
                    if(!inventorySettings.isAllowUnidentifiedRecords()) return new Result(null, false, Result.UNIDENTIFIED_RECORD_NOT_ALLOWED);
                    else {
                        AccountedRecord accountedRecord = new AccountedRecord(inputValues.getValue(), inputValues.getQuantity(), null);
                        return domain.createRecord(accountedRecord, location);
                    }
                }
                else {
                    if (record.isAccounted()) {
                        AccountedRecord accountedRecord = (AccountedRecord) record;
                        if(!accountedRecord.isIdentified() &&
                                !inventorySettings.isAllowUnidentifiedRecords()) return new Result(null, false, Result.UNIDENTIFIED_RECORD_NOT_ALLOWED);

                        float quantity = accountedRecord.getAccountedQuantity() + inputValues.getQuantity();

                        if(!inventorySettings.isAllowDuplicates() && quantity > 1.0f) return new Result(null , false, Result.DUPLICATES_NOT_ALLOWED);
                        else return domain.updateRecord(inventorySettings, accountedRecord, quantity);
                    }
                    else {
                        RemainedRecord remainedRecord = (RemainedRecord) record;
                        if(!inventorySettings.isAllowDuplicates() && inputValues.getQuantity() > 1.0f) return new Result(null , false, Result.DUPLICATES_NOT_ALLOWED);
                        else return domain.moveRecord(inventorySettings, remainedRecord, inputValues.getQuantity(), location);
                    }
                }
            }
        });

        subscribe(single, observer);
    }

    private void input(final InputValues inputValues, final Record record, boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    input(inputValues, record, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else input(inputValues, record, null);
    }

    public interface ViewInterface{
        void onAmbiguousValue(InputValues inputValues);

        void onDuplicatesNotAllowed();
        void onUnidentifiedRecordsNotAllowed();

        void onAccountedRecordAdded(Result result);
        void onAccountedRecordUpdated(Result result);

        void onUnidentifiedRecordAdded(Result result);
        void onUnidentifiedRecordUpdated(Result result);

        void onGPSNotAvailable();

        void onError(Throwable e);
    }
}
