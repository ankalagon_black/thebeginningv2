package com.example.thebeginningv2.inventory_process.domains;

import android.os.Handler;
import android.os.Looper;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Константин on 15.07.2017.
 */

public class InputUnidentifiedRecordsDomain {
    private DatabaseManager databaseManager;
    private WeakReference<ViewInterface> reference;

    public InputUnidentifiedRecordsDomain(DatabaseManager databaseManager, WeakReference<ViewInterface> reference){
        this.databaseManager = databaseManager;
        this.reference = reference;
    }

    public void input(InventorySettings inventorySettings, InputValues inputValues){
        Handler handler = new Handler(Looper.getMainLooper());

        if(inventorySettings.isAllowUnidentifiedRecords()){
            List<AccountedRecord> accountedRecords = databaseManager.findUnidentifiedRecords(inputValues.getValue(), false);
            if(accountedRecords.size() != 0){
                final AccountedRecord previousRecord = accountedRecords.get(0),
                        updatedRecord = new AccountedRecord(inputValues.getValue(), previousRecord.getAccountedQuantity() + inputValues.getQuantity(), null);

                if(!inventorySettings.isAllowDuplicates() && updatedRecord.getAccountedQuantity() > 1.0f){
                    if(reference.get() != null){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                reference.get().onDuplicatesNotAllowed();
                            }
                        });
                    }
                }
                else {
                    databaseManager.updateAccountedRecord(updatedRecord.getId(), updatedRecord.getBarcode(), updatedRecord.getAccountedQuantity());
                    if(reference.get() != null){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                reference.get().onUnidentifiedRecordUpdated(previousRecord, updatedRecord);
                            }
                        });
                    }
                }
            }
            else {
                final AccountedRecord accountedRecord = new AccountedRecord(inputValues.getValue(), inputValues.getQuantity(), null);
                databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity(), null);
                if(reference.get() != null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onUnidentifiedRecordAdded(accountedRecord);
                        }
                    });
                }
            }
        }
        else{
            if(reference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        reference.get().onUnidentifiedRecordsNotAllowed();
                    }
                });
            }
        }
    }

    public interface ViewInterface extends DuplicatesInterface{
        void onUnidentifiedRecordAdded(AccountedRecord accountedRecord);
        void onUnidentifiedRecordUpdated(AccountedRecord previousRecord, AccountedRecord updatedRecord);

        void onUnidentifiedRecordsNotAllowed();
    }
}
