package com.example.thebeginningv2.inventory_process.data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Константин on 17.06.2017.
 */

public class InputValues implements Parcelable{
    private String value;
    private float quantity;

    public InputValues(String value, float quantity){
        this.value = value;
        this.quantity = quantity;
    }

    protected InputValues(Parcel in) {
        value = in.readString();
        quantity = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
        dest.writeFloat(quantity);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InputValues> CREATOR = new Creator<InputValues>() {
        @Override
        public InputValues createFromParcel(Parcel in) {
            return new InputValues(in);
        }

        @Override
        public InputValues[] newArray(int size) {
            return new InputValues[size];
        }
    };

    public String getValue() {
        return value;
    }

    public float getQuantity() {
        return quantity;
    }
}
