package com.example.thebeginningv2.inventory_process.domains;

/**
 * Created by Константин on 15.07.2017.
 */

public interface DuplicatesInterface {
    void onDuplicatesNotAllowed();
}
