package com.example.thebeginningv2.inventory_process.domains;

import android.os.Handler;
import android.os.Looper;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import java.lang.ref.WeakReference;

/**
 * Created by Константин on 15.07.2017.
 */

public class InputIdentifiedRecordDomain {
    private DatabaseManager databaseManager;
    private WeakReference<ViewInterface> reference;

    public InputIdentifiedRecordDomain(DatabaseManager databaseManager, WeakReference<ViewInterface> reference){
        this.databaseManager = databaseManager;
        this.reference = reference;
    }

    public void input(InventorySettings inventorySettings, int id, float quantity){
        Handler handler = new Handler(Looper.getMainLooper());

        if(databaseManager.isAccounted(id)){
            final AccountedRecord previousRecord = databaseManager.getIdentifiedAccountedRecord(id),
                    updatedRecord = new AccountedRecord(previousRecord, previousRecord.getAccountedQuantity() + quantity);

            if(!inventorySettings.isAllowDuplicates() && updatedRecord.getAccountedQuantity() > 1.0f){
                if(reference.get() != null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onDuplicatesNotAllowed();
                        }
                    });
                }
            }
            else {
                databaseManager.updateAccountedRecord(updatedRecord.getId(), updatedRecord.getBarcode(), updatedRecord.getAccountedQuantity());

                if(reference.get() != null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reference.get().onAccountedRecordUpdated(previousRecord, updatedRecord);
                        }
                    });
                }
            }
        }
        else {
            final RemainedRecord remainedRecord = databaseManager.getRemainedRecord(id);
            final AccountedRecord accountedRecord = new AccountedRecord(remainedRecord, quantity);

            databaseManager.addToAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity(), null);

            if(reference.get() != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        reference.get().onAccountedRecordAdded(accountedRecord, remainedRecord);
                    }
                });
            }
        }
    }

    public interface ViewInterface extends DuplicatesInterface{
        void onAccountedRecordAdded(AccountedRecord accountedRecord, RemainedRecord remainedRecord);
        void onAccountedRecordUpdated(AccountedRecord previousRecord, AccountedRecord updatedRecord);
    }
}
