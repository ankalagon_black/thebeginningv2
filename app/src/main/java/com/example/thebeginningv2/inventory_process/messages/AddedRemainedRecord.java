package com.example.thebeginningv2.inventory_process.messages;

import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;

/**
 * Created by Константин on 08.07.2017.
 */

public class AddedRemainedRecord {
    private RemainedRecord remainedRecord;

    public AddedRemainedRecord(RemainedRecord remainedRecord){
        this.remainedRecord = remainedRecord;
    }

    public RemainedRecord getRemainedRecord() {
        return remainedRecord;
    }
}
