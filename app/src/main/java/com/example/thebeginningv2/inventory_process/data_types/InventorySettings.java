package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 07.07.2017.
 */

public class InventorySettings {
    public static final String ALLOW_DUPLICATES = "AllowDuplicates";
    public static final String ALLOW_UNIDENTIFIED_RECORDS = "AllowUnidentifiedRecords";
    public static final String ALLOW_COLOR_IDENTIFICATION = "AllowColorIdentification";
    public static final String GROUP_INVENTORY = "GroupInventory";
    public static final String GPS = "GPS";

    private boolean allowDuplicates, allowUnidentifiedRecords,
            allowColorIdentification, allowGroupInventory, allowGPS;

    public InventorySettings(boolean allowDuplicates, boolean allowUnidentifiedRecords, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS){
        this.allowDuplicates = allowDuplicates;
        this.allowUnidentifiedRecords = allowUnidentifiedRecords;
        this.allowColorIdentification = allowColorIdentification;
        this.allowGroupInventory = allowGroupInventory;
        this.allowGPS = allowGPS;
    }

    public boolean isAllowDuplicates() {
        return allowDuplicates;
    }

    public boolean isAllowUnidentifiedRecords() {
        return allowUnidentifiedRecords;
    }

    public boolean isAllowColorIdentification() {
        return allowColorIdentification;
    }

    public boolean isAllowGroupInventory() {
        return allowGroupInventory;
    }

    public boolean isAllowGPS() {
        return allowGPS;
    }
}
