package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 10.06.2017.
 */

public class ShortStatistics {
    private float accountedQuantity, statedQuantity, residueQuantity, surplusQuantity, unidentifiedQuantity;

    public ShortStatistics(float accountedQuantity, float statedQuantity, float residueQuantity, float surplusQuantity, float unidentifiedQuantity){
        this.accountedQuantity = accountedQuantity;
        this.statedQuantity = statedQuantity;
        this.residueQuantity = residueQuantity;
        this.surplusQuantity = surplusQuantity;
        this.unidentifiedQuantity = unidentifiedQuantity;
    }

    public void addAccountedQuantity(float quantity){
        accountedQuantity += quantity;
    }

    public void setAccountedQuantity(float accountedQuantity){
        this.accountedQuantity = accountedQuantity;
    }

    public float getAccountedQuantity() {
        return accountedQuantity;
    }

    public float getStatedQuantity() {
        return statedQuantity;
    }

    public void addResidueQuantity(float quantity){
        residueQuantity += quantity;
    }

    public void setResidueQuantity(float residueQuantity){
        this.residueQuantity = residueQuantity;
    }

    public float getResidueQuantity() {
        return residueQuantity;
    }

    public void addSurplusQuantity(float quantity){
        surplusQuantity += quantity;
    }

    public void setSurplusQuantity(float surplusQuantity){
        this.surplusQuantity = surplusQuantity;
    }

    public float getSurplusQuantity() {
        return surplusQuantity;
    }

    public void addUnidentifiedQuantity(float quantity){
        unidentifiedQuantity += quantity;
    }

    public void setUnidentifiedQuantity(float unidentifiedQuantity){
        this.unidentifiedQuantity = unidentifiedQuantity;
    }

    public float getUnidentifiedQuantity() {
        return unidentifiedQuantity;
    }
}
