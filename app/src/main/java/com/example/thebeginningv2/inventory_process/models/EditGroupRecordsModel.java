package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 18.07.2017.
 */

public class EditGroupRecordsModel extends Model {
    public static final String ACCOUNTED_RECORD_KEY = "AccountedRecordKey";

    private InventorySettings inventorySettings;

    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private AccountedRecord accountedRecord;
    private List<AccountedRecord> groupRecords;

    private GpsAndGroupInventoryDomain domain;

    public EditGroupRecordsModel(ViewInterface viewInterface, DatabaseManager databaseManager, AccountedRecord accountedRecord, GpsAndGroupInventoryDomain domain){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.accountedRecord = accountedRecord;
        this.domain = domain;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void getGroupBarcodes(){
        DisposableSingleObserver<List<ColumnValue>> observer = new DisposableSingleObserver<List<ColumnValue>>() {
            @Override
            public void onSuccess(List<ColumnValue> value) {
                List<String> groupBarcodes = new ArrayList<>(value.size());

                for (ColumnValue i: value) groupBarcodes.add(i.getValue());

                viewInterface.onGroupBarcodesReceived(groupBarcodes);

                getGroupRecords(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<ColumnValue>> single = Single.fromCallable(new Callable<List<ColumnValue>>() {
            @Override
            public List<ColumnValue> call() throws Exception {
                return databaseManager.getGroupBarcodes(accountedRecord.getId());
            }
        });

        subscribe(single,observer);
    }

    private void getGroupRecords(final List<ColumnValue> values){
        DisposableSingleObserver<List<AccountedRecord>> observer = new DisposableSingleObserver<List<AccountedRecord>>() {
            @Override
            public void onSuccess(List<AccountedRecord> value) {
                groupRecords = value;

                for (AccountedRecord i: value){
                    if(i.getId() == accountedRecord.getId()){
                        value.remove(i);
                        break;
                    }
                }

                viewInterface.onGroupRecordsReceived(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<AccountedRecord>> single = Single.fromCallable(new Callable<List<AccountedRecord>>() {
            @Override
            public List<AccountedRecord> call() throws Exception {
                return databaseManager.findGroupAccountedRecords(values);
            }
        });

        subscribe(single, observer);
    }

    public void updateRecordQuantity(final AccountedRecord accountedRecord){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {

                int index  = groupRecords.indexOf(accountedRecord);

                EventBus.getDefault().post(new DeletedAccountedRecord(accountedRecord));
                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                groupRecords.remove(index);
                if(groupRecords.size() == 0) viewInterface.onAllRecordsEdited();
                else viewInterface.onRecordEdited(index);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                return domain.updateRecord(inventorySettings, accountedRecord, EditGroupRecordsModel.this.accountedRecord.getAccountedQuantity());
            }
        });

        subscribe(single, observer);
    }

    public void updateAllRecordsQuantity(){
        if(groupRecords != null){
            DisposableSingleObserver<List<Result>> observer = new DisposableSingleObserver<List<Result>>() {
                @Override
                public void onSuccess(List<Result> value) {

                    for (AccountedRecord i: groupRecords){
                        EventBus.getDefault().post(new DeletedAccountedRecord(i));
                    }

                    for (Result i: value){
                        EventBus.getDefault().post(new AddedAccountedRecord(i.getAccountedRecord()));
                    }

                    viewInterface.onAllRecordsEdited();
                }

                @Override
                public void onError(Throwable e) {
                    viewInterface.onError(e);
                }
            };

            Single<List<Result>> single = Single.fromCallable(new Callable<List<Result>>() {
                @Override
                public List<Result> call() throws Exception {
                    List<Result> results = new ArrayList<>(groupRecords.size());

                    for (AccountedRecord i: groupRecords){
                        results.add(domain.updateRecord(inventorySettings, i, EditGroupRecordsModel.this.accountedRecord.getAccountedQuantity()));
                    }

                    return results;
                }
            });

            subscribe(single, observer);
        }
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public AccountedRecord getAccountedRecord() {
        return accountedRecord;
    }

    public interface ViewInterface{
        void onGroupRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> groupRecords);
        void onGroupBarcodesReceived(List<String> groupBarcodes);

        void onRecordEdited(int position);
        void onAllRecordsEdited();

        void onError(Throwable e);
    }
}
