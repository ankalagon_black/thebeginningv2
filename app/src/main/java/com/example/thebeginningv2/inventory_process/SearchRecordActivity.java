package com.example.thebeginningv2.inventory_process;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.activities.AppCompatRepositoryActivity;
import com.example.thebeginningv2.inventory_process.adapters.RecordsAdapter;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.dialogs.AboutRecordDialog;
import com.example.thebeginningv2.inventory_process.models.SearchModel;
import com.example.thebeginningv2.inventory_process.repository.SearchRepository;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.util.List;

/**
 * Created by Константин on 18.06.2017.
 */

public class SearchRecordActivity extends AppCompatRepositoryActivity<SearchRepository> 
        implements SearchModel.ViewInterface, RecordsAdapter.Interface{

    private SearchView searchView;
    private RecyclerView recyclerView;
    private CheckBox partialMatch;

    private SearchModel searchModel;

    @Override
    protected SearchRepository createRepository() {
        return new SearchRepository(this, PathManager.getIntance());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.search);

        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String keyword = searchView.getQuery().toString();
                if(keyword.replace(" ", "").length() > 0){
                    searchModel.search(keyword, partialMatch.isChecked());
                    return true;
                }
                else return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.search_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        partialMatch = (CheckBox) findViewById(R.id.partial_match);
        partialMatch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String keyword = searchView.getQuery().toString();
                if(keyword.replace(" ", "").length() > 0) searchModel.search(keyword, b);
            }
        });

        searchModel = new SearchModel(this, getRepository().getDatabaseManager(), getRepository());
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchModel.getInventorySettings();
        if(searchModel.hasInventorySettings()) {
            String keyword = searchView.getQuery().toString();
            if (keyword.replace(" ", "").length() > 0)
                searchModel.search(keyword, partialMatch.isChecked());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        searchModel.disposeAllBackgroundTasks();
    }

    @Override
    public void onSearchResults(InventorySettings inventorySettings, List<Record> results) {
        RecordsAdapter recordsAdapter = new RecordsAdapter(results, inventorySettings, this);
        recyclerView.setAdapter(recordsAdapter);
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFunctionalButton(Record record) {
        AboutRecordDialog dialog = AboutRecordDialog.getInstance(record);
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }
}
