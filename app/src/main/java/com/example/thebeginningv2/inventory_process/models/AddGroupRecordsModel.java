package com.example.thebeginningv2.inventory_process.models;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 18.07.2017.
 */

public class AddGroupRecordsModel extends Model {
    public static final String ACCOUNTED_RECORD_KEY = "AccountedRecordKey";

    private InventorySettings inventorySettings;

    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private AccountedRecord accountedRecord;
    private List<RemainedRecord> groupRecords;

    private GpsAndGroupInventoryDomain domain;
    private InventoryProcessRepository repository;

    public AddGroupRecordsModel(ViewInterface viewInterface, DatabaseManager databaseManager, AccountedRecord accountedRecord,
                                GpsAndGroupInventoryDomain domain, InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.accountedRecord = accountedRecord;
        this.domain = domain;
        this.repository = repository;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void getGroupBarcodes(){
        DisposableSingleObserver<List<ColumnValue>> observer = new DisposableSingleObserver<List<ColumnValue>>() {
            @Override
            public void onSuccess(List<ColumnValue> value) {
                List<String> groupBarcodes = new ArrayList<>(value.size());

                for (ColumnValue i: value) groupBarcodes.add(i.getValue());

                viewInterface.onGroupBarcodesReceived(groupBarcodes);

                getGroupRecords(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<ColumnValue>> single = Single.fromCallable(new Callable<List<ColumnValue>>() {
            @Override
            public List<ColumnValue> call() throws Exception {
                return databaseManager.getGroupBarcodes(accountedRecord.getId());
            }
        });

        subscribe(single,observer);
    }

    private void getGroupRecords(final List<ColumnValue> values){
        DisposableSingleObserver<List<RemainedRecord>> observer = new DisposableSingleObserver<List<RemainedRecord>>() {
            @Override
            public void onSuccess(List<RemainedRecord> value) {
                groupRecords = value;
                viewInterface.onGroupRecordsReceived(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<RemainedRecord>> single = Single.fromCallable(new Callable<List<RemainedRecord>>() {
            @Override
            public List<RemainedRecord> call() throws Exception {
                return databaseManager.findGroupRemainedRecords(values);
            }
        });

        subscribe(single, observer);
    }

    public void move(final RemainedRecord remainedRecord, boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    move(remainedRecord, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else move(remainedRecord, null);
    }

    public void move(final RemainedRecord remainedRecord, final Location location){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                int index = groupRecords.indexOf(remainedRecord);
                EventBus.getDefault().post(new DeletedRemainedRecord(remainedRecord));
                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                groupRecords.remove(index);
                if (groupRecords.size() == 0) viewInterface.onAllRecordMoved();
                else viewInterface.onRecordMoved(index);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                return domain.moveRecord(inventorySettings, remainedRecord, accountedRecord.getAccountedQuantity(), location);
            }
        });

        subscribe(single, observer);
    }

    public void moveAll(boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    moveAll(location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else moveAll(null);
    }

    public void moveAll(final Location location){
        if(groupRecords != null){
            DisposableSingleObserver<List<Result>> observer = new DisposableSingleObserver<List<Result>>() {
                @Override
                public void onSuccess(List<Result> value) {
                    for (RemainedRecord i : groupRecords) {
                        EventBus.getDefault().post(new DeletedRemainedRecord(i));
                    }

                    for (Result i : value) {
                        EventBus.getDefault().post(new AddedAccountedRecord(i.getAccountedRecord()));
                    }

                    viewInterface.onAllRecordMoved();
                }

                @Override
                public void onError(Throwable e) {
                    viewInterface.onError(e);
                }
            };

            Single<List<Result>> single = Single.fromCallable(new Callable<List<Result>>() {
                @Override
                public List<Result> call() throws Exception {
                    List<Result> results = new ArrayList<>(groupRecords.size());

                    for (RemainedRecord i : groupRecords) {
                        results.add(domain.moveRecord(inventorySettings, i, accountedRecord.getAccountedQuantity(), location));
                    }

                    return results;
                }
            });

            subscribe(single, observer);
        }
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public AccountedRecord getAccountedRecord() {
        return accountedRecord;
    }

    public interface ViewInterface{
        void onGroupRecordsReceived(InventorySettings inventorySettings, List<RemainedRecord> groupRecords);
        void onGroupBarcodesReceived(List<String> groupBarcodes);

        void onRecordMoved(int position);
        void onAllRecordMoved();

        void onGPSNotAvailable();

        void onError(Throwable e);
    }
}
