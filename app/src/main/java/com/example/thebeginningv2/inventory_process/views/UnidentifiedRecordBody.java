package com.example.thebeginningv2.inventory_process.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class UnidentifiedRecordBody {
    protected View view;

    protected TextView barcode, quantity;
    protected TextView gpsMark;

    public UnidentifiedRecordBody(View view){
        this.view = view;
        barcode = (TextView) view.findViewById(R.id.barcode);
        quantity = (TextView) view.findViewById(R.id.quantity);
        gpsMark = (TextView) view.findViewById(R.id.gps_mark);
    }

    public void setAccountedRecord(AccountedRecord accountedRecord, boolean allowColorIdentification, boolean allowGPS){
        Context context = view.getContext();

        barcode.setText(accountedRecord.getBarcode());
        quantity.setText(String.format(Locale.US, context.getString(R.string.float_1_demical), accountedRecord.getAccountedQuantity()));
        if(allowColorIdentification) quantity.setTextColor(context.getResources().getColor(R.color.purple_color));

        if(allowGPS){
            gpsMark.setVisibility(View.VISIBLE);

            if(accountedRecord.hasGPSMark())
                gpsMark.setText(String.format(Locale.US, context.getString(R.string.gps_mark), accountedRecord.getGPSMark()));
            else gpsMark.setText(context.getString(R.string.no_gps_mark));
        }
        else gpsMark.setVisibility(View.GONE);
    }

    public TextView getQuantity() {
        return quantity;
    }
}
