package com.example.thebeginningv2.inventory_process.data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Константин on 13.06.2017.
 */

public class Selection implements Parcelable{
    private boolean matching, withResidue, withSurplus, unidentified;

    public Selection(){
        matching = true;
        withResidue = true;
        withSurplus = true;
        unidentified = true;
    }

    public Selection(boolean matching, boolean withResidue, boolean withSurplus, boolean unidentified){
        this.matching = matching;
        this.withResidue = withResidue;
        this.withSurplus = withSurplus;
        this.unidentified = unidentified;
    }

    protected Selection(Parcel in) {
        matching = in.readByte() != 0;
        withResidue = in.readByte() != 0;
        withSurplus = in.readByte() != 0;
        unidentified = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (matching ? 1 : 0));
        dest.writeByte((byte) (withResidue ? 1 : 0));
        dest.writeByte((byte) (withSurplus ? 1 : 0));
        dest.writeByte((byte) (unidentified ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Selection> CREATOR = new Creator<Selection>() {
        @Override
        public Selection createFromParcel(Parcel in) {
            return new Selection(in);
        }

        @Override
        public Selection[] newArray(int size) {
            return new Selection[size];
        }
    };

    public boolean isUnidentified() {
        return unidentified;
    }

    public boolean isWithSurplus() {
        return withSurplus;
    }

    public boolean isWithResidue() {
        return withResidue;
    }

    public boolean isMatching() {
        return matching;
    }
}
