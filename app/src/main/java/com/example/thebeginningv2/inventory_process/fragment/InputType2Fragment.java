package com.example.thebeginningv2.inventory_process.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.fragments.SupportModelFragment;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.dialogs.AddGroupRecordsDialog;
import com.example.thebeginningv2.inventory_process.dialogs.ManualInputDialog;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.InputFinished;
import com.example.thebeginningv2.inventory_process.models.InputTypeModel;
import com.example.thebeginningv2.inventory_process.models.ManualInputModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Константин on 10.06.2017.
 */

public class InputType2Fragment extends SupportModelFragment<InventoryProcessRepository, InputTypeModel>
        implements InputTypeModel.ViewInterface{
    public static final String TAG = "InputType2Fragment";

    private EditText editValue, editQuantity;

    private long startTime;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_input_type_2, container, false);

        Button button = (Button) view.findViewById(R.id.input);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = editValue.getText().toString(), quantity = editQuantity.getText().toString();

                if(value.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)){
                    float currentQuantity = Float.parseFloat(quantity);

                    if(!getModel().getInventorySettings().isAllowDuplicates() && currentQuantity > 1.0f) onDuplicatesNotAllowed();
                    else {
                        InputDialog inputDialog = new InputDialog();
                        inputDialog.show(getChildFragmentManager(), inputDialog.getTag());

                        getModel().input(new InputValues(value, currentQuantity));
                    }

                }
            }
        });

        editValue = (EditText) view.findViewById(R.id.value_edit);
        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                if (after == 1 || (start == 0 && after > 5)) startTime = System.currentTimeMillis();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    long diff = System.currentTimeMillis() - startTime;
                    if (diff < 300 && editValue.getText().length() > 5) {
                        String value = editValue.getText().toString(), quantity = editQuantity.getText().toString();

                        if(value.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)){
                            float currentQuantity = Float.parseFloat(quantity);

                            if(!getModel().getInventorySettings().isAllowDuplicates() && currentQuantity > 1.0f) onDuplicatesNotAllowed();
                            else {
                                InputDialog inputDialog = new InputDialog();
                                inputDialog.show(getChildFragmentManager(), inputDialog.getTag());

                                getModel().input(new InputValues(value, currentQuantity));
                            }

                        }
                    }
                }
            }
        });

        editQuantity = (EditText) view.findViewById(R.id.quantity_edit);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       if(savedInstanceState == null){
           setDefault();
       }
    }

    @Override
    protected InputTypeModel createModel() {
        DatabaseManager databaseManager = getRepository().getDatabaseManager();
        return new InputTypeModel(this, databaseManager, new GpsAndGroupInventoryDomain(databaseManager, getActivity()), getRepository());
    }

    private void setDefault(){
        editValue.setText("");
        editQuantity.setText("1");
    }

    @Override
    public void onDuplicatesNotAllowed() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), R.string.duplicates_not_alllowed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnidentifiedRecordsNotAllowed() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), R.string.unidentified_records_not_allowed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAccountedRecordAdded(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        Toast.makeText(getActivity(), R.string.record_added, Toast.LENGTH_SHORT).show();

        if(result.isHasGroupRecords()){
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(AddGroupRecordsDialog.MOVE,
                    result.getAccountedRecord());
            dialog.show(getFragmentManager(), dialog.getTag());
        }
    }

    @Override
    public void onAccountedRecordUpdated(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        Toast.makeText(getActivity(), R.string.record_updated, Toast.LENGTH_SHORT).show();

        if(result.isHasGroupRecords()){
            AddGroupRecordsDialog dialog = AddGroupRecordsDialog.getInstance(AddGroupRecordsDialog.UPDATE,
                    result.getAccountedRecord());
            dialog.show(getFragmentManager(), dialog.getTag());
        }
    }

    @Override
    public void onUnidentifiedRecordAdded(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        Toast.makeText(getActivity(), R.string.record_added, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnidentifiedRecordUpdated(Result result) {
        EventBus.getDefault().post(new InputFinished());
        setDefault();

        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        Toast.makeText(getActivity(), R.string.record_updated, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGPSNotAvailable() {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getContext(), R.string.gps_not_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAmbiguousValue(final InputValues inputValues) {
        EventBus.getDefault().post(new InputFinished());

        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        setDefault();

        ManualInputDialog manualInputDialog = new ManualInputDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ManualInputModel.INPUT_VALUES_KEY, inputValues);

        manualInputDialog.setArguments(bundle);
        manualInputDialog.show(getFragmentManager(), manualInputDialog.getTag());

        Toast.makeText(getActivity(), R.string.ambiguous_ids_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new InputFinished());
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public static class InputDialog extends DialogFragment{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder.setCancelable(false).
                    setTitle(R.string.input_in_progress).
                    setView(LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null)).
                    create();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            EventBus.getDefault().unregister(this);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onInputFinished(InputFinished inputFinished){
            dismiss();
        }
    }
}
