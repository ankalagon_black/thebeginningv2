package com.example.thebeginningv2.inventory_process.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
import com.example.thebeginningv2.inventory_process.messages.OpenSelection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

/**
 * Created by Константин on 14.06.2017.
 */

public class ShortStatisticsFragment extends Fragment {
    public static final String TAG = "ShortStatisticsFragment";

    private Button button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        button = (Button) inflater.inflate(R.layout.inventory_process_short_statistics, container, false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new OpenSelection());
            }
        });
        return button;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onShortStatisticsReceived(ShortStatistics shortStatistics){
        updateShortStatistics(shortStatistics);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShortStatisticsUpdated(ShortStatistics shortStatistics){
        updateShortStatistics(shortStatistics);
    }

    private void updateShortStatistics(ShortStatistics shortStatistics){
        button.setText(String.format(Locale.US, getString(R.string.short_statistics),
                shortStatistics.getAccountedQuantity(),
                shortStatistics.getStatedQuantity(),
                shortStatistics.getResidueQuantity(),
                shortStatistics.getSurplusQuantity(),
                shortStatistics.getUnidentifiedQuantity()));
    }
}
