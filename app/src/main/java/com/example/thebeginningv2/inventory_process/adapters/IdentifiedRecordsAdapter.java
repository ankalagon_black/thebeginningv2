package com.example.thebeginningv2.inventory_process.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.RecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordViewHolder;

import java.util.List;

/**
 * Created by Константин on 16.06.2017.
 */

public class IdentifiedRecordsAdapter extends RecyclerView.Adapter<RecordViewHolder> {
    public static final int IDENTIFIED_TYPE = 0, REMAINED_RECORD = 1;

    private List<Record> list;

    private Interface anInterface;

    private InventorySettings inventorySettings;

    public IdentifiedRecordsAdapter(List<Record> records, Interface anInterface, InventorySettings inventorySettings) {
        this.list = records;
        this.anInterface = anInterface;
        this.inventorySettings = inventorySettings;
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position).isAccounted()) return IDENTIFIED_TYPE;
        else return REMAINED_RECORD;
    }

    @Override
    public RecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case IDENTIFIED_TYPE:
                return new IdentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.clickable_identified_accounted_record, parent, false));
            case REMAINED_RECORD:
                return new RemainedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.clickable_remained_record, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecordViewHolder holder, int position) {
        holder.setRecord(position, list.get(position), inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class IdentifiedRecordHolder extends IdentifiedAccountedRecordViewHolder{

        public IdentifiedRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onClick(record);
                }
            });

            functionalButton.setImageResource(R.drawable.ic_action_search);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButtonPressed(record);
                }
            });
        }
    }

    public class RemainedRecordHolder extends RemainedRecordViewHolder{

        public RemainedRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final RemainedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onClick(record);
                }
            });

            functionalButton.setImageResource(R.drawable.ic_action_search);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButtonPressed(record);
                }
            });
        }
    }

    public interface Interface{
        void onClick(Record record);
        void onFunctionalButtonPressed(Record record);
    }
}
