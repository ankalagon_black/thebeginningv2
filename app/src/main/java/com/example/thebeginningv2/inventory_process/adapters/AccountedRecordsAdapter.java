package com.example.thebeginningv2.inventory_process.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Selection;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.RecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.UnidentifiedRecordBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 13.06.2017.
 */

public class AccountedRecordsAdapter extends RecyclerView.Adapter<RecordViewHolder> {
    public static final int IDENTIFIED_TYPE = 0, UNIDENTIFIED_TYPE = 1;

    private List<AccountedRecord> accountedRecords;

    private Selection selection;
    private boolean allowColorIdentification, allowGroupInventory, allowGPS;
    private List<AccountedRecord> list;

    private Interface anInterface;

    public AccountedRecordsAdapter(List<AccountedRecord> accountedRecords,
                                   Selection selection,
                                   boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS,
                                   Interface anInterface){
        this.accountedRecords = accountedRecords;
        this.selection = selection;

        this.allowColorIdentification = allowColorIdentification;
        this.allowGroupInventory = allowGroupInventory;
        this.allowGPS = allowGPS;
        
        list = new ArrayList<>();
        list.addAll(accountedRecords);
        
        this.anInterface = anInterface;
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position).isIdentified()) return IDENTIFIED_TYPE;
        else return UNIDENTIFIED_TYPE;
    }

    @Override
    public RecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case IDENTIFIED_TYPE:
                return new IdentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.identified_accounted_record, parent, false));
            case UNIDENTIFIED_TYPE:
                return new UnidentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.unidentified_accounted_record_with_functional_button, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecordViewHolder holder, int position) {
        holder.setRecord(position, list.get(position), allowColorIdentification, allowGroupInventory, allowGPS);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void changeSelection(Selection oldSelection, Selection newSelection){
        int oldCount = (oldSelection.isMatching() ? 1 : 0) + (oldSelection.isUnidentified() ? 1 : 0) +
                (oldSelection.isWithResidue() ? 1 : 0) + (oldSelection.isWithSurplus() ? 1 : 0),
                newCount = (newSelection.isMatching() ? 1 : 0) + (newSelection.isUnidentified() ? 1 : 0) +
                        (newSelection.isWithResidue() ? 1 : 0) + (newSelection.isWithSurplus() ? 1 : 0);

        if(newCount < oldCount){
            for (int i = list.size() - 1; i >= 0; i--) {
                AccountedRecord accountedRecord = list.get(i);
                if(accountedRecord.isIdentified()){
                    switch (accountedRecord.getDifferenceType()){
                        case AccountedRecord.MATCHING:
                            if(!newSelection.isMatching()){
                                list.remove(i);
                                notifyItemRemoved(i);
                            }
                            break;
                        case AccountedRecord.WITH_RESIDUE:
                            if(!newSelection.isWithResidue()){
                                list.remove(i);
                                notifyItemRemoved(i);
                            }
                            break;
                        case AccountedRecord.WITH_SURPLUS:
                            if(!newSelection.isWithSurplus()){
                                list.remove(i);
                                notifyItemRemoved(i);
                            }
                            break;
                    }
                }
                else if(!newSelection.isUnidentified()){
                    list.remove(i);
                    notifyItemRemoved(i);
                }
            }
        }
        else if(newCount > oldCount){
            int position = 0;
            int size = accountedRecords.size();
            for (int i = 0; i < size; i++){
                AccountedRecord accountedRecord = accountedRecords.get(i);

                if(accountedRecord.isIdentified()){
                    switch (accountedRecord.getDifferenceType()){
                        case AccountedRecord.MATCHING:
                            if(newSelection.isMatching() && !oldSelection.isMatching()){
                                if(position == list.size()){
                                    list.add(accountedRecord);
                                    notifyItemInserted(position);
                                }
                                else{
                                    list.add(position, accountedRecord);
                                    notifyItemInserted(position);
                                }
                            }
                            else if(!newSelection.isMatching()) --position;
                            break;
                        case AccountedRecord.WITH_RESIDUE:
                            if(newSelection.isWithResidue() && !oldSelection.isWithResidue()){
                                if(position == list.size()){
                                    list.add(accountedRecord);
                                    notifyItemInserted(position);
                                }
                                else{
                                    list.add(position, accountedRecord);
                                    notifyItemInserted(position);
                                }
                            }
                            else if(!newSelection.isWithResidue()) --position;
                            break;
                        case AccountedRecord.WITH_SURPLUS:
                            if(newSelection.isWithSurplus() && !oldSelection.isWithSurplus()){
                                if(position == list.size()){
                                    list.add(accountedRecord);
                                    notifyItemInserted(position);
                                }
                                else{
                                    list.add(position, accountedRecord);
                                    notifyItemInserted(position);
                                }
                            }
                            else if(!newSelection.isWithSurplus()) --position;
                            break;
                    }
                }
                else {
                    if(newSelection.isUnidentified() && !oldSelection.isUnidentified()){
                        if(position == list.size()){
                            list.add(accountedRecord);
                            notifyItemInserted(position);
                        }
                        else{
                            list.add(position, accountedRecord);
                            notifyItemInserted(position);
                        }
                    }
                    else if(!newSelection.isUnidentified()) --position;
                }

                ++position;
            }
        }

        selection = newSelection;
        notifyItemRangeChanged(0, list.size());
    }
    
    public void addRecord(AccountedRecord accountedRecord){
        if(accountedRecord.isIdentified()){
            switch (accountedRecord.getDifferenceType()){
                case AccountedRecord.MATCHING:
                    if(selection.isMatching()) {
                        list.add(0, accountedRecord);
                        notifyItemInserted(0);
                        notifyItemRangeChanged(0, list.size());
                    }
                    break;
                case AccountedRecord.WITH_RESIDUE:
                    if(selection.isWithResidue()) {
                        list.add(0, accountedRecord);
                        notifyItemInserted(0);
                        notifyItemRangeChanged(0, list.size());
                    }
                    break;
                case AccountedRecord.WITH_SURPLUS:
                    if(selection.isWithSurplus()) {
                        list.add(0, accountedRecord);
                        notifyItemInserted(0);
                        notifyItemRangeChanged(0, list.size());
                    }
                    break;
            }
        }
        else if(selection.isUnidentified()){
            list.add(0, accountedRecord);
            notifyItemInserted(0);
            notifyItemRangeChanged(0, list.size());
        }
    }

    public void removeRecord(AccountedRecord accountedRecord){
        int position;
        if(accountedRecord.isIdentified()){
            switch (accountedRecord.getDifferenceType()){
                case AccountedRecord.MATCHING:
                    if(selection.isMatching()) {
                        position = list.indexOf(accountedRecord);
                        list.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, list.size() - position);
                    }
                    break;
                case AccountedRecord.WITH_RESIDUE:
                    if(selection.isWithResidue()) {
                        position = list.indexOf(accountedRecord);
                        list.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, list.size() - position);
                    }
                    break;
                case AccountedRecord.WITH_SURPLUS:
                    if(selection.isWithSurplus()) {
                        position = list.indexOf(accountedRecord);
                        list.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, list.size() - position);
                    }
                    break;
            }
        }
        else if(selection.isUnidentified()){
            position = list.indexOf(accountedRecord);
            list.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list.size() - position);
        }
    }

    public void removeAll(){
        list.clear();
        notifyDataSetChanged();
    }

    public class IdentifiedRecordHolder extends IdentifiedAccountedRecordViewHolder{

        public IdentifiedRecordHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_more_vert);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPopupMenu(record, functionalButton);
                }
            });
        }
    }

    public class UnidentifiedRecordHolder extends RecordViewHolder<AccountedRecord> {
        private TextView index;
        private ImageButton functionalButton;

        private UnidentifiedRecordBody recordBody;

        public UnidentifiedRecordHolder(View itemView) {
            super(itemView);
            recordBody = new UnidentifiedRecordBody(itemView);

            index = (TextView) itemView.findViewById(R.id.index);
            functionalButton = (ImageButton) itemView.findViewById(R.id.functional_button);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            recordBody.setAccountedRecord(record, allowColorIdentification, allowGPS);
            Context context = itemView.getContext();
            index.setText(String.format(Locale.US, context.getString(R.string.index), position + 1));
            functionalButton.setImageResource(R.drawable.ic_more_vert);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPopupMenu(record, functionalButton);
                }
            });
        }
    }

    public interface Interface {
        void showPopupMenu(AccountedRecord accountedRecord, View view);
    }
}
