package com.example.thebeginningv2.inventory_process;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.activities.AppCompatRepositoryActivity;
import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
import com.example.thebeginningv2.inventory_process.domains.InventoryStatisticsDomain;
import com.example.thebeginningv2.inventory_process.fragment.AccountedRecordsFragment;
import com.example.thebeginningv2.inventory_process.fragment.RemainedRecordsFragment;
import com.example.thebeginningv2.inventory_process.models.WithAccountingDataInventoryModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.PathManager;

import java.util.Locale;

/**
 * Created by Константин on 09.06.2017.
 */

public class WithAccountingDataInventoryActivity extends AppCompatRepositoryActivity<InventoryProcessRepository> implements WithAccountingDataInventoryModel.ViewInterface{
    private WithAccountingDataInventoryModel model;

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_process_with_accounting_data);

        setTitle(R.string.inventory);

        getSupportActionBar().setHomeButtonEnabled(true);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(viewPager);

        InventoryStatisticsDomain statisticsDomain = new InventoryStatisticsDomain(getRepository().getDatabaseManager());

        model = new WithAccountingDataInventoryModel(this, getRepository(), statisticsDomain);

    }

    @Override
    protected InventoryProcessRepository createRepository() {
        PathManager pathManager = PathManager.getIntance();
        return new InventoryProcessRepository(this, pathManager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.registerEventBus();

        model.getInventorySettings();
        model.getRecordsCount();
        model.getShortStatistics();
    }

    @Override
    protected void onStop() {
        super.onStop();
        model.unregisterEventBus();
        model.disposeAllBackgroundTasks();
    }

    @Override
    public void onRecordsCountReceived(RecordsCount recordsCount) {
        String[] counts = new String[2];
        counts[0] = String.format(Locale.US, getString(R.string.accounted_records_count), recordsCount.getAccountedRecordsCount());
        counts[1] = String.format(Locale.US, getString(R.string.remained_records_count), recordsCount.getRemainedRecordsCount());

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), counts);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onRecordsCountUpdated(RecordsCount recordsCount) {
        String[] counts = new String[2];
        counts[0] = String.format(Locale.US, getString(R.string.accounted_records_count), recordsCount.getAccountedRecordsCount());
        counts[1] = String.format(Locale.US, getString(R.string.remained_records_count), recordsCount.getRemainedRecordsCount());

        adapter.setCounts(counts);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        public static final int FRAGMENTS_COUNT = 2;

        private String[] counts;

        public ViewPagerAdapter(FragmentManager fm, String[] counts) {
            super(fm);
            this.counts = counts;
        }

        public void setCounts(String[] counts){
            this.counts = counts;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new AccountedRecordsFragment();
                case 1:
                    return new RemainedRecordsFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return counts[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS_COUNT;
        }
    }
}
