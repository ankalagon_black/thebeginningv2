package com.example.thebeginningv2.inventory_process.data_types;

/**
 * Created by Константин on 17.07.2017.
 */

public class DeletionResult {
    private RemainedRecord remainedRecord;
    private boolean hasGroupRecords;

    public DeletionResult(RemainedRecord remainedRecord, boolean hasGroupRecords){
        this.remainedRecord = remainedRecord;
        this.hasGroupRecords = hasGroupRecords;
    }

    public RemainedRecord getRemainedRecord() {
        return remainedRecord;
    }

    public boolean isHasGroupRecords() {
        return hasGroupRecords;
    }
}
