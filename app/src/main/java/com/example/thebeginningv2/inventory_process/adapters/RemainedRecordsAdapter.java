package com.example.thebeginningv2.inventory_process.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.views.RemainedRecordViewHolder;

import java.util.List;

/**
 * Created by Константин on 14.06.2017.
 */

public class RemainedRecordsAdapter extends RecyclerView.Adapter<RemainedRecordsAdapter.Holder> {
    private List<RemainedRecord> list;
    private boolean allowColorIdentification, allowGroupInventory, allowGPS;

    private Interface anInterface;

    public RemainedRecordsAdapter(List<RemainedRecord> list,
                                  boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS,
                                  Interface anInterface){
        this.list = list;
        this.allowColorIdentification = allowColorIdentification;
        this.allowGroupInventory = allowGroupInventory;
        this.allowGPS = allowGPS;
        this.anInterface = anInterface;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.remained_record, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setRecord(position, list.get(position), allowColorIdentification, allowGroupInventory, allowGPS);
    }

    public void notifyRecordAdded(){
        notifyItemInserted(0);
        notifyItemRangeChanged(0, list.size());
    }

    public void notifyRecordRemoved(int position){
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RemainedRecordViewHolder {

        public Holder(View itemView) {
            super(itemView);
        }

        @Override
        public void setRecord(int position, final RemainedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            super.setRecord(position, record, allowColorIdentification, allowGroupInventory, allowGPS);

            functionalButton.setImageResource(R.drawable.ic_more_vert);
            functionalButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPopupMenu(record, functionalButton);
                }
            });
        }
    }

    public interface Interface {
        void showPopupMenu(RemainedRecord remainedRecord, View view);
    }
}
