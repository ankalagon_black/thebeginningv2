package com.example.thebeginningv2.inventory_process.models;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedRemainedRecord;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 08.07.2017.
 */

public class ConvertRecordModel extends Model {
    public static final String UNIDENTIFIED_RECORD_KEY = "UnidentifiedRecordKey";

    private ViewInterface viewInterface;

    private AccountedRecord unidentifiedRecord;
    private InventorySettings inventorySettings;

    private DatabaseManager databaseManager;

    private GpsAndGroupInventoryDomain domain;
    private InventoryProcessRepository repository;

    public ConvertRecordModel(ViewInterface viewInterface, DatabaseManager databaseManager,
                              GpsAndGroupInventoryDomain domain, AccountedRecord unidentifiedRecord, InventoryProcessRepository repository){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
        this.domain = domain;
        this.unidentifiedRecord = unidentifiedRecord;
        this.repository = repository;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void search(final boolean useLike){
        DisposableSingleObserver<List<Record>> observer = new DisposableSingleObserver<List<Record>>() {
            @Override
            public void onSuccess(List<Record> value) {
                viewInterface.onSearchResultsReceived(inventorySettings, value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<Record>> single = Single.fromCallable(new Callable<List<Record>>() {
            @Override
            public List<Record> call() throws Exception {
                return databaseManager.findIdentifiedRecords(unidentifiedRecord.getBarcode(), useLike);
            }
        });

        subscribe(single, observer);
    }

    public void convert(final Record target, final AccountedRecord result, final boolean addGpsMark){
        if(addGpsMark){
            repository.getLocation(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    convert(target, result, location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    if(i != LocationProvider.AVAILABLE) viewInterface.onGPSNotAvailable();
                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            });
        }
        else convert(target, result, null);
    }

    public void convert(final Record target, final AccountedRecord result, final Location location){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                EventBus.getDefault().post(new DeletedAccountedRecord(unidentifiedRecord));

                if(target.isAccounted()) EventBus.getDefault().post(new DeletedAccountedRecord((AccountedRecord) target));
                else EventBus.getDefault().post(new DeletedRemainedRecord((RemainedRecord) target));

                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                viewInterface.onConversionFinished(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onConversionError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                databaseManager.deleteFromAccountedRecords(unidentifiedRecord.getId(), unidentifiedRecord.getBarcode());

                if(target.isAccounted()) return domain.updateRecord(inventorySettings, result);
                else return domain.moveRecord(inventorySettings, (RemainedRecord) target, result.getAccountedQuantity(), location);
            }
        });

        subscribe(single, observer);
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public AccountedRecord getUnidentifiedRecord() {
        return unidentifiedRecord;
    }

    public interface ViewInterface{
        void onSearchResultsReceived(InventorySettings inventorySettings, List<Record> records);

        void onGPSNotAvailable();

        void onConversionFinished(Result result);
        void onConversionError(Throwable e);
        void onError(Throwable e);
    }
}
