package com.example.thebeginningv2.inventory_process.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v3.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.adapters.AccountedRecordsAdapterV2;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Record;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process.models.EditGroupRecordsModel;
import com.example.thebeginningv2.inventory_process.repository.InventoryProcessRepository;
import com.example.thebeginningv2.inventory_process.views.IdentifiedAccountedRecordV2;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Константин on 18.07.2017.
 */

public class EditGroupRecordsDialog extends SupportModelDialog<InventoryProcessRepository, EditGroupRecordsModel>
        implements EditGroupRecordsModel.ViewInterface, AccountedRecordsAdapterV2.Interface{

    private ListView listView;
    private RecyclerView recyclerView;
    private AccountedRecordsAdapterV2 adapterV2;

    private IdentifiedAccountedRecordV2 recordV2;

    public static EditGroupRecordsDialog getInstance(AccountedRecord accountedRecord){
        EditGroupRecordsDialog dialog = new EditGroupRecordsDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(EditGroupRecordsModel.ACCOUNTED_RECORD_KEY, accountedRecord);

        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.edit_group_records, null);

        listView = (ListView) view.findViewById(R.id.list);

        TextView groupRecordsDescription = (TextView) view.findViewById(R.id.group_records_description);
        groupRecordsDescription.setText(R.string.choose_record_to_update);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recordV2 = new IdentifiedAccountedRecordV2(view);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.setTitle(R.string.group_records_movement).
                setView(view).
                setNegativeButton(R.string.close, null).
                setPositiveButton(R.string.edit_all, null).
                create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProcessDialog dialog = new ProcessDialog();
                        dialog.show(getFragmentManager(), dialog.getTag());

                        getModel().updateAllRecordsQuantity();
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            InventorySettings inventorySettings = getModel().getInventorySettings();

            recordV2.setAccountedRecord(getModel().getAccountedRecord(),
                    inventorySettings.isAllowColorIdentification(), inventorySettings.isAllowGroupInventory(), inventorySettings.isAllowGPS());
        }

        getModel().getGroupBarcodes();
    }

    @Override
    protected EditGroupRecordsModel createModel() {
        AccountedRecord accountedRecord = getArguments().getParcelable(EditGroupRecordsModel.ACCOUNTED_RECORD_KEY);
        return new EditGroupRecordsModel(this, getRepository().getDatabaseManager(), accountedRecord, new GpsAndGroupInventoryDomain(getRepository().getDatabaseManager(), getActivity()));
    }


    @Override
    public void onGroupRecordsReceived(InventorySettings inventorySettings, List<AccountedRecord> groupRecords) {
        adapterV2 = new AccountedRecordsAdapterV2(AccountedRecordsAdapterV2.EDIT, groupRecords, inventorySettings, this);
        recyclerView.setAdapter(adapterV2);
    }

    @Override
    public void onGroupBarcodesReceived(List<String> groupBarcodes) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, groupBarcodes);
        listView.setAdapter(adapter);
    }

    @Override
    public void onRecordEdited(int position) {
        EventBus.getDefault().post(new StopProcess());
        adapterV2.notifyItemRemoved(position);
    }

    @Override
    public void onAllRecordsEdited() {
        EventBus.getDefault().post(new StopProcess());

        Toast.makeText(getActivity(), R.string.group_records_updated, Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFunctionalButtonPressed(Record record) {
        ProcessDialog dialog = new ProcessDialog();
        dialog.show(getFragmentManager(), dialog.getTag());

        getModel().updateRecordQuantity((AccountedRecord) record);
    }
}
