package com.example.thebeginningv2.inventory_process.repository;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;

import com.example.thebeginningv2.core_v3.repository.Repository;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;

import java.io.File;

/**
 * Created by apple on 23.07.17.
 */

public class SearchRepository extends Repository {
    private PathManager pathManager;
    private DatabaseManager databaseManager;

    public SearchRepository(Activity activity, PathManager pathManager) {
        super(activity);
        this.pathManager = pathManager;
        this.databaseManager = new DatabaseManager(SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildPath(PathManager.DB), null));
    }

    public DataModeParser getDataModeParser() {
        return new DataModeParser(new File(pathManager.getDataChildPath(PathManager.MODE)));
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    @Override
    public void releaseResources() {
        databaseManager.close();
    }
}
