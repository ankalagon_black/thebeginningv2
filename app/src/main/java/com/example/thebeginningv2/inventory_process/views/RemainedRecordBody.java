package com.example.thebeginningv2.inventory_process.views;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class RemainedRecordBody extends IdentifiedRecordBody {
    protected TextView statedQuantity;

    public RemainedRecordBody(View view) {
        super(view);
        statedQuantity = (TextView) view.findViewById(R.id.stated_quantity);
    }

    public void setRemainedRecord(RemainedRecord remainedRecord, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS){
        setRecord(remainedRecord, allowGroupInventory, allowGPS);
        Context context = view.getContext();

        statedQuantity.setText(String.format(Locale.US, context.getString(R.string.float_1_demical), remainedRecord.getStatedQuantity()));
        if(allowColorIdentification) statedQuantity.setTextColor(context.getResources().getColor(R.color.yellow_color));
    }
}
