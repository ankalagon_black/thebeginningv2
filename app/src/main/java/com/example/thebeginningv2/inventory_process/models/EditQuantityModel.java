package com.example.thebeginningv2.inventory_process.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.ColumnValue;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.inventory_process.data_types.Result;
import com.example.thebeginningv2.inventory_process.domains.GpsAndGroupInventoryDomain;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process.messages.DeletedAccountedRecord;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by apple on 30.06.17.
 */

public class EditQuantityModel extends Model {
    public static final String ACCOUNTED_RECORD_KEY = "AccountedRecordKey";

    private ViewInterface viewInterface;

    private InventorySettings inventorySettings;

    private DatabaseManager databaseManager;

    public EditQuantityModel(ViewInterface viewInterface, DatabaseManager databaseManager){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onInventorySettings(InventorySettings inventorySettings){
        this.inventorySettings = inventorySettings;
    }

    public void updateRecordQuantity(final AccountedRecord previousRecord, final float quantity){
        DisposableSingleObserver<Result> observer = new DisposableSingleObserver<Result>() {
            @Override
            public void onSuccess(Result value) {
                EventBus.getDefault().post(new DeletedAccountedRecord(previousRecord));
                EventBus.getDefault().post(new AddedAccountedRecord(value.getAccountedRecord()));

                viewInterface.onRecordQuantityEdited(value.getAccountedRecord(), value.isHasGroupRecords());
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Result> single = Single.fromCallable(new Callable<Result>() {
            @Override
            public Result call() throws Exception {
                AccountedRecord updatedRecord = new AccountedRecord(previousRecord, quantity);
                databaseManager.updateAccountedRecord(updatedRecord.getId(), updatedRecord.getBarcode(), updatedRecord.getAccountedQuantity());

                if(inventorySettings.isAllowGroupInventory() && previousRecord.isGroup()){
                    List<ColumnValue> groupBarcodes = databaseManager.getGroupBarcodes(updatedRecord.getId());

                    boolean hasGroupRecords = databaseManager.hasGroupAccountedRecords(groupBarcodes);

                    return new Result(updatedRecord, hasGroupRecords);
                }
                else return new Result(updatedRecord, false);
            }
        });

        subscribe(single, observer);
    }

    public InventorySettings getInventorySettings() {
        return inventorySettings;
    }

    public interface ViewInterface{
        void onRecordQuantityEdited(AccountedRecord updatedRecord, boolean hasGroupRecords);
        void onError(Throwable e);
    }
}
