package com.example.thebeginningv2.inventory_process.views;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;

import java.util.Locale;

/**
 * Created by Константин on 15.07.2017.
 */

public class IdentifiedAccountedRecordViewHolder extends RecordViewHolder<AccountedRecord> {
    private TextView index;
    protected ImageButton functionalButton;

    private IdentifiedAccountedRecordBody recordBody;

    public IdentifiedAccountedRecordViewHolder(View itemView) {
        super(itemView);
        recordBody = new IdentifiedAccountedRecordBody(itemView);
        index = (TextView) itemView.findViewById(R.id.index);
        functionalButton = (ImageButton) itemView.findViewById(R.id.functional_button);
    }

    @Override
    public void setRecord(int position, AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
        recordBody.setAccountedRecord(record, allowColorIdentification, allowGroupInventory, allowGPS);

        index.setText(String.format(Locale.US, itemView.getContext().getString(R.string.index), position + 1));
    }
}
