//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleObservableUseCase;
//import com.example.thebeginningv2.inventory_module.data_types.SmbTransmissionInfo;
//import com.example.thebeginningv2.inventory_module.data_types.TransmissionProgress;
//import com.example.thebeginningv2.repository.DownloadingRepositoryInterface;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Observable;
//import io.reactivex.ObservableEmitter;
//import io.reactivex.ObservableOnSubscribe;
//import jcifs.smb.NtlmPasswordAuthentication;
//import jcifs.smb.SmbException;
//import jcifs.smb.SmbFile;
//import jcifs.smb.SmbFileInputStream;
//
///**
// * Created by apple on 28.06.17.
// */
//
//public class DownloadingUseCase extends SingleObservableUseCase<SmbTransmissionInfo, TransmissionProgress> {
//    private DownloadingRepositoryInterface repositoryInterface;
//
//    private int count;
//
//    public DownloadingUseCase(Executor executor, DownloadingRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    public Observable<TransmissionProgress> buildUseCaseObservable(final SmbTransmissionInfo params) {
//        return Observable.create(new ObservableOnSubscribe<TransmissionProgress>() {
//            @Override
//            public void subscribe(ObservableEmitter<TransmissionProgress> e) throws Exception {
//
//                String path = params.getSmbProtocolUrl();
//
//                NtlmPasswordAuthentication authentication =
//                        new NtlmPasswordAuthentication("", params.getLogin(), params.getPassword());
//
//                int totalCount = getCount(path, authentication);
//
//                count = 0;
//
//                downloadAll(path, params.getTransmissionRoot(), authentication, e, totalCount);
//
//                e.onComplete();
//            }
//        });
//    }
//
//    private int getCount(String start, NtlmPasswordAuthentication authentication) throws MalformedURLException, SmbException {
//        SmbFile smbFile = new SmbFile(start, authentication);
//        if(smbFile.isDirectory()){
//            int count = 0;
//            SmbFile[] files = smbFile.listFiles();
//            for (SmbFile i: files){
//                count += getCount(i.getPath(), authentication);
//            }
//
//            return count;
//        }
//        else return 1;
//    }
//
//    private void downloadAll(String start, String transmissionRoot, NtlmPasswordAuthentication authentication,
//                             ObservableEmitter<TransmissionProgress> e, int totalCount) throws IOException {
//        SmbFile smbFile = new SmbFile(start, authentication);
//        if(smbFile.isDirectory()){
//            String innerPath = smbFile.getPath();
//            innerPath = innerPath.substring(innerPath.indexOf(transmissionRoot) + transmissionRoot.length());
//            repositoryInterface.createFolder(innerPath);
//            SmbFile[] files = smbFile.listFiles();
//            if(files != null) {
//                for (SmbFile i : files) {
//                    downloadAll(i.getPath(), transmissionRoot, authentication, e, totalCount);
//                }
//            }
//        }
//        else{
//            float fileSize = smbFile.length();
//            count += 1;
//            e.onNext(new TransmissionProgress(count, 0, totalCount, fileSize));
//            downloadFile(smbFile, transmissionRoot, e, count, totalCount, fileSize);
//            e.onNext(new TransmissionProgress(count, fileSize, totalCount, fileSize));
//        }
//    }
//
//    private void downloadFile(SmbFile smbFile, String transmissionRoot, ObservableEmitter<TransmissionProgress> e,
//                              int count, int totalCount, float fileSize) throws IOException {
//        SmbFileInputStream inputStream = new SmbFileInputStream(smbFile);
//
//        String innerPath = smbFile.getPath();
//        innerPath = innerPath.substring(innerPath.indexOf(transmissionRoot) + transmissionRoot.length());
//        File file = repositoryInterface.createFile(innerPath);
//
//        FileOutputStream outputStream = new FileOutputStream(file);
//
//        byte[] buffer = new byte[12288];
//        int n;
//        long downloaded = 0;
//        while ((n = inputStream.read(buffer)) != -1){
//            outputStream.write(buffer, 0, n);
//            downloaded += n;
//            e.onNext(new TransmissionProgress(count, downloaded, totalCount, fileSize));
//        }
//
//        outputStream.flush();
//        outputStream.close();
//
//        inputStream.close();
//    }
//}
