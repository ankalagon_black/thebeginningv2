//package com.example.thebeginningv2.deprecated.core.presenters;
//
//import android.os.Bundle;
//import android.os.Parcelable;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public abstract class FragmentStateModelPresenter<S extends Parcelable> extends FragmentBasePresenter {
//    public static final String STATE_MODEL = "StateModel";
//
//    protected S stateModel = null;
//
//    protected boolean hasStateModel(){
//        return stateModel != null;
//    }
//
//    protected void restoreStateModel(Bundle savedInstanceState){
//        if(savedInstanceState != null) stateModel = savedInstanceState.getParcelable(STATE_MODEL);
//    }
//
//    protected void saveStateModel(Bundle outState){
//        outState.putParcelable(STATE_MODEL, stateModel);
//    }
//}
