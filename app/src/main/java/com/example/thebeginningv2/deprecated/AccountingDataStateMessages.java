//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public class AccountingDataStateMessages extends MessageManager<AccountingDataStateMessages.Interface> {
//    public static final String REQUEST_ACCOUNTING_DATA_DATE = "RequestAccountingDataDate";
//    public static final String DELETE_ACCOUNTING_DATA = "DeleteAccountingData";
//
//    public AccountingDataStateMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void requestAccountingDataDate(){
//        sendMessage(new EventBus.Message(REQUEST_ACCOUNTING_DATA_DATE));
//    }
//
//    public void sendDeleteAccountingData(){
//        sendMessage(new EventBus.Message(DELETE_ACCOUNTING_DATA));
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case ThirdLevelModelMessages.ACCOUNTING_DATA_DATE:
//                receiveInterface.onAccountingDateReceived((String) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onAccountingDateReceived(String date);
//    }
//}
