//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//
///**
// * Created by Константин on 07.06.2017.
// */
//
//public class SentInventoryMessages extends MessageSender {
//    public static final String RETURN_TO_IN_PROGRESS = "ReturnToInProgress";
//
//    public SentInventoryMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendReturnToInProgress(){
//        sendMessage(new EventBus.Message(RETURN_TO_IN_PROGRESS));
//    }
//
//    public void sendDeleteInventory(){
//        sendMessage(new EventBus.Message(InProgressInventoryStateMessages.DELETE_INVENTORY));
//    }
//}
