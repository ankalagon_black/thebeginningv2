//package com.example.thebeginningv2.deprecated.core.communication;
//
//import android.util.Log;
//
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Consumer;
//
///**
// * Created by Константин on 05.06.2017.
// */
//
//public abstract class MessageManager<R> extends MessageSender{
//    private Disposable disposable;
//    protected R receiveInterface;
//
//    public MessageManager(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    protected abstract void onMessageReceived(EventBus.BaseMessage message);
//
//    public void subscribe() {
//        disposable = busInterface.subscribe(new Consumer<Object>() {
//            @Override
//            public void accept(Object o) throws Exception {
//                if(receiveInterface != null) onMessageReceived((EventBus.BaseMessage) o);
//                else Log.e(this.getClass().getSimpleName(), "Interface is not connected");
//            }
//        });
//    }
//
//    public void dispose() {
//        disposable.dispose();
//    }
//
//    public void setReceiveInterface(R receiveInterface) {
//        this.receiveInterface = receiveInterface;
//    }
//}
