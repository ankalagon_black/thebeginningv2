//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//
///**
// * Created by Константин on 10.06.2017.
// */
//
//public class InventoryProcessMessages extends MessageSender {
//    public static final String PAGE_CHANGED_TO_ACCOUNTED_RECORDS = "PageChangedToAccountedRecords";
//    public static final String PAGE_CHANGED_TO_REMAINED_RECORDS = "PageChangedToRemainedRecords";
//
//    public InventoryProcessMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void notifyPageChangedToAccountedRecords(){
//        sendMessage(new EventBus.Message(PAGE_CHANGED_TO_ACCOUNTED_RECORDS));
//    }
//
//    public void notifyPageChangedToRemainedRecords(){
//        sendMessage(new EventBus.Message(PAGE_CHANGED_TO_REMAINED_RECORDS));
//    }
//}
