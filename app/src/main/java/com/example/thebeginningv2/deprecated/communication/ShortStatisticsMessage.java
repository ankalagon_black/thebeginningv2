//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class ShortStatisticsMessage<I extends ShortStatisticsMessage.Interface> extends MessageManager<I>{
//    public static final String REQUEST_SHORT_STATISTICS = "RequestShortStatistics";
//
//    public ShortStatisticsMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void requestShortStatistics(){
//        sendMessage(new EventBus.Message(REQUEST_SHORT_STATISTICS));
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case InventoryProcessModelMessages.SHORT_STATISTICS:
//                receiveInterface.onShortStatisticsReceived((ShortStatistics) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onShortStatisticsReceived(ShortStatistics shortStatistics);
//    }
//}
