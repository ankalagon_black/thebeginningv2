//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.InputValues;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class CreateRecord extends SingleSingleUseCase<InputValues,AccountedRecord> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public CreateRecord(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<AccountedRecord> buildUseCaseSingle(final InputValues params) {
//        return Single.fromCallable(new Callable<AccountedRecord>() {
//            @Override
//            public AccountedRecord call() throws Exception {
//                return repositoryInterface.create(params);
//            }
//        });
//    }
//}
