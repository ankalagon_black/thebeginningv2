//package com.example.thebeginningv2.deprecated.core.communication;
//
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Consumer;
//
///**
// * Created by Константин on 05.06.2017.
// */
//
//public interface EventBusInterface {
//    Disposable subscribe(Consumer<Object> consumer);
//    <M extends EventBus.BaseMessage> void send(M message);
//}
