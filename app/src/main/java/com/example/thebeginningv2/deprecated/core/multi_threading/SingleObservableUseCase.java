//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.Observer;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Action;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by apple on 28.06.17.
// */
//
//public abstract class SingleObservableUseCase<P,R> extends ObservableUseCase<P,R> {
//    private Disposable disposable;
//
//    protected SingleObservableUseCase(Executor executor) {
//        super(executor);
//    }
//
//    public boolean hasProcessRunning() {
//        return disposable != null;
//    }
//
//    @Override
//    public void run(P params, Observer<R> observer) {
//        dispose();
//        buildUseCaseObservable(params).
//                subscribeOn(Schedulers.from(getExecutor())).
//                observeOn(AndroidSchedulers.mainThread()).
//                doFinally(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        disposable = null;
//                    }
//                }).subscribe(observer);
//    }
//
//    @Override
//    public void dispose() {
//        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
//    }
//}
