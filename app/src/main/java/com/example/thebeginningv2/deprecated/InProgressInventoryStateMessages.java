//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//
///**
// * Created by Константин on 07.06.2017.
// */
//
//public class InProgressInventoryStateMessages extends MessageSender {
//    public static final String DELETE_INVENTORY = "DeleteInventory";
//
//    public InProgressInventoryStateMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendDeleteInventory(){
//        sendMessage(new EventBus.Message(DELETE_INVENTORY));
//    }
//}
