//package com.example.thebeginningv2.inventory_module.dialogs;
//
//import android.app.Dialog;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DialogFragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//
//import com.example.thebeginningv2.R;
//import com.example.thebeginningv2.inventory_module.adapters.InventoryParametersAdapter;
//import com.example.thebeginningv2.inventories.data_types.InventoryParameter;
//import com.example.thebeginningv2.inventories.data_types.CreateInventorySettings;
//import com.example.thebeginningv2.inventory_module.models.InventoryParametersModel;
//
//import java.util.List;
//
///**
// * Created by Константин on 20.06.2017.
// */
//
//public class InventoryParametersDialog extends DialogFragment implements InventoryParametersModel.Interface {
//
//    private RecyclerView recyclerView;
//
//    private InventoryParametersAdapter adapter;
//
//    private InventoryParametersModel model;
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
//        return super.onCreateDialog(savedInstanceState);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.inventory_module_edit_inventory, container, false);
//
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        toolbar.setTitle(R.string.parameters_edit);
//        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InventoryParametersDialog.this.dismiss();
//            }
//        });
//
//        recyclerView = (RecyclerView) view.findViewById(R.id.inventory_parameters);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//        Button apply = (Button) view.findViewById(R.id.apply);
//        apply.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               if(adapter.allRequiredParametersFilled()) model.applyChanges();
//            }
//        });
//
//        return view;
//
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        model = new InventoryParametersModel();
//        model.connect(this);
//        model.loadData();
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        model.disconnect();
//
//    }
//
//    @Override
//    public void onInventorySettingsAndParametersReceived(CreateInventorySettings inventorySettings, List<InventoryParameter> inventoryParameters) {
//        adapter = new InventoryParametersAdapter(inventoryParameters);
//        recyclerView.setAdapter(adapter);
//    }
//}
