//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//
///**
// * Created by Константин on 07.06.2017.
// */
//
//public class DownloadingDataMessage extends MessageSender {
//    public static final String ACCOUNTING_DATA_DOWNLOADED = "AccountingDataDownloaded";
//
//    public DownloadingDataMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendAccountingDataDownloaded(){
//        sendMessage(new EventBus.Message(ACCOUNTING_DATA_DOWNLOADED));
//    }
//}
