//package com.example.thebeginningv2.deprecated.core.communication;
//
///**
// * Created by Константин on 05.06.2017.
// */
//
//public abstract class MessageSender {
//    protected EventBusInterface busInterface;
//
//    public MessageSender(EventBusInterface busInterface){
//        this.busInterface = busInterface;
//    }
//
//    protected final <M extends EventBus.BaseMessage> void sendMessage(M m){
//        busInterface.send(m);
//    }
//}
