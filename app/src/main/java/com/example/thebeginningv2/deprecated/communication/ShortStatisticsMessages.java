//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//
///**
// * Created by Константин on 14.06.2017.
// */
//
//public class ShortStatisticsMessages extends ShortStatisticsMessage<ShortStatisticsMessage.Interface> {
//    public static final String OPEN_SELECTION = "OpenSelection";
//
//    public ShortStatisticsMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendOpenSelection(){
//        sendMessage(new EventBus.Message(OPEN_SELECTION));
//    }
//}
