//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.Selection;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class SelectionMessages extends MessageSender {
//    public static final String SELECTION_CHANGED = "SelectionChanged";
//    public static final String CLOSE_SELECTION = "CloseSelection";
//
//    public SelectionMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendSelectionChanged(Selection selection){
//        sendMessage(new EventBus.DataMessage<>(SELECTION_CHANGED, selection));
//    }
//
//    public void sendCloseSelection(){
//        sendMessage(new EventBus.Message(CLOSE_SELECTION));
//    }
//}
