//package com.example.thebeginningv2.inventory_module.activities;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.PopupMenu;
//import android.support.v7.widget.RecyclerView;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//
//import com.example.thebeginningv2.R;
//import com.example.thebeginningv2.deprecated.core.multi_threading.TaskExecutor;
//import com.example.thebeginningv2.inentories.InventoryAdapter;
//import com.example.thebeginningv2.inventory_module.communication.SecondLevelMessages;
//import com.example.thebeginningv2.inentories.data_types.Inventory;
//import com.example.thebeginningv2.inventory_module.dialogs.AboutInventoryDialog;
//import com.example.thebeginningv2.inventory_module.dialogs.CreateInventoryDialogv1;
//import com.example.thebeginningv2.inventory_module.dialogs.InventoryParametersDialog;
//import com.example.thebeginningv2.inventory_module.dialogs.SendingInventoryDataDialog;
//import com.example.thebeginningv2.inventory_module.fragments.DownloadedDataStateFragment;
//import com.example.thebeginningv2.inventory_module.fragments.SecondLevelRetainingFragment;
//import com.example.thebeginningv2.inventory_module.fragments.NotDownloadedDataStateFragment;
//import com.example.thebeginningv2.inventory_module.models.SecondLevelModel;
//import com.example.thebeginningv2.inventory_module.models.SendingInventoryDataModel;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Константин on 20.06.2017.
// */
//
//public class SecondLevelActivity extends AppCompatActivity implements SecondLevelModel.Interface, InventoryAdapter.Interface{
//    private RecyclerView recyclerView;
//    private InventoryAdapter adapter;
//
//    private SecondLevelModel model;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.inventory_module_inventories);
//
//        setTitle(R.string.inventories_title);
//        getSupportActionBar().setHomeButtonEnabled(true);
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        SecondLevelRetainingFragment retainingFragment = (SecondLevelRetainingFragment) fragmentManager.findFragmentByTag(SecondLevelRetainingFragment.TAG);
//        if(retainingFragment == null){
//            retainingFragment = new SecondLevelRetainingFragment();
//            retainingFragment.initializeRepository();
//
//            fragmentManager.beginTransaction().add(retainingFragment, SecondLevelRetainingFragment.TAG).commit();
//        }
//
//        recyclerView = (RecyclerView) findViewById(R.id.second_level_catalogs_list);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        final Button createInventory = (Button) findViewById(R.id.create_inventory);
//        createInventory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //if(model.isAccountingDataDownloaded()){
//                    CreateInventoryDialogv1 createInventoryDialog = new CreateInventoryDialogv1();
//                    createInventoryDialog.show(getSupportFragmentManager(), createInventoryDialog.getTag());
//                //}
//                //else Toast.makeText(InventoriesActivity.this, R.string.create_inventory_warning, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        TaskExecutor taskExecutor = TaskExecutor.getInstance();
//        SecondLevelRepositoryInterface repositoryInterface = retainingFragment.getRepository();
//
//        model = new SecondLevelModel(new SecondLevelMessages(retainingFragment.getEventBus()), repositoryInterface);
//        model.connect(this);
//
//        model.loadData();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        model.disconnect();
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.inventory_module_inventories, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.send_all:
//                AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                builder.setTitle(R.string.action_confirmation).
//                        setMessage(R.string.send_all_confirmation_message).
//                        setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                SendingInventoryDataDialog dialog = new SendingInventoryDataDialog();
//
//                                Bundle bundle = new Bundle();
//                                bundle.putParcelableArrayList(SendingInventoryDataModel.INVENTORIES_TO_SEND, model.getNotSentInventories());
//
//                                dialog.setArguments(bundle);
//                                dialog.show(getSupportFragmentManager(), dialog.getTag());
//                            }
//                        }).
//                        setNegativeButton(R.string.cancel, null).
//                        create().
//                        show();
//                return true;
//            case android.R.id.home:
//                model.moveBackward();
//                return false;
//            default:
//                return false;
//        }
//    }
//
//    @Override
//    public void onSecondLevelCatalogsReceived(List<Inventory> inventories) {
//        adapter = new InventoryAdapter(inventories, this);
//        recyclerView.setAdapter(adapter);
//    }
//
//    @Override
//    public void onInventoryCreated(int position) {
//        adapter.notifyItemInserted(position);
//    }
//
//    @Override
//    public void onInventorySent(int position) {
//        adapter.updateInventory(position);
//    }
//    @Override
//    public void onInventoryDeleted(int position) {
//        adapter.updateRemovedInventory(position);
//    }
//
//    @Override
//    public void onAccountingDataDateReceived(String accountingDataDate) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        if(accountingDataDate == null){
//            NotDownloadedDataStateFragment fragment = (NotDownloadedDataStateFragment) fragmentManager.findFragmentByTag(NotDownloadedDataStateFragment.TAG);
//            if(fragment == null) fragment = new NotDownloadedDataStateFragment();
//            fragmentManager.beginTransaction().replace(R.id.accounting_data_container, fragment, NotDownloadedDataStateFragment.TAG).commit();
//        }
//        else {
//            DownloadedDataStateFragment fragment = (DownloadedDataStateFragment) fragmentManager.findFragmentByTag(DownloadedDataStateFragment.TAG);
//            if(fragment == null) fragment = new DownloadedDataStateFragment();
//            fragmentManager.beginTransaction().replace(R.id.accounting_data_container, fragment, DownloadedDataStateFragment.TAG).commit();
//        }
//    }
//
//    @Override
//    public void showPopupMenu(View view, final Inventory inventory) {
//        PopupMenu popupMenu = new PopupMenu(this, view);
//
//        if(inventory.getState() == Inventory.IN_PROGRESS) popupMenu.getMenuInflater().inflate(R.menu.inventory_module_inventory_in_progress_popup, popupMenu.getMenu());
//        else popupMenu.getMenuInflater().inflate(R.menu.inventory_module_inventory_sent_popup, popupMenu.getMenu());
//
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.send:
//                    case R.id.send_again:
//                        AlertDialog.Builder builder = new AlertDialog.Builder(SecondLevelActivity.this);
//                        builder.setTitle(R.string.action_confirmation).
//                                setMessage(R.string.send_inventory_confirmation_message).
//                                setNegativeButton(R.string.cancel, null).
//                                setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        SendingInventoryDataDialog dialog = new SendingInventoryDataDialog();
//
//                                        Bundle bundle = new Bundle();
//                                        ArrayList<Inventory> inventories = new ArrayList<>();
//                                        inventories.add(inventory);
//                                        bundle.putParcelableArrayList(SendingInventoryDataModel.INVENTORIES_TO_SEND, inventories);
//
//                                        dialog.setArguments(bundle);
//                                        dialog.show(getSupportFragmentManager(), dialog.getTag());
//                                    }
//                                }).
//                                create().
//                                show();
//                        return true;
//                    case R.id.more_about_inventory:
//                        AboutInventoryDialog aboutInventoryDialog = new AboutInventoryDialog();
//                        aboutInventoryDialog.show(getSupportFragmentManager(), aboutInventoryDialog.getTag());
//                        return true;
//                    case R.id.inventory_parameters:
//                        InventoryParametersDialog inventoryParametersDialog = new InventoryParametersDialog();
//                        //// TODO: 20.06.2017 do not forget to put inventory name
//                        inventoryParametersDialog.show(getSupportFragmentManager(), inventoryParametersDialog.getTag());
//                        return true;
//                    case R.id.remove:
//                        builder = new AlertDialog.Builder(SecondLevelActivity.this);
//                        builder.setTitle(R.string.action_confirmation).
//                                setMessage(R.string.delete_inventory_confirmation_message).
//                                setNegativeButton(R.string.cancel, null).
//                                setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        model.delete(inventory);
//                                    }
//                                }).
//                                create().
//                                show();
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//        });
//
//        popupMenu.show();
//    }
//
//    @Override
//    public void showInventory(Inventory inventory) {
//        model.moveForward(inventory.getName());
//
//        Intent intent = new Intent(this, ThirdLevelActivity.class);
//        startActivity(intent);
//    }
//}
