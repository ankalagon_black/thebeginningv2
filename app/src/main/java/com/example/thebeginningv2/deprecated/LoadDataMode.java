//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
//import com.example.thebeginningv2.inventories.data_types.CreateInventorySettings;
//import com.example.thebeginningv2.inventories.data_types.InventoryParameter;
//import com.example.thebeginningv2.inventory_module.data_types.InventoryParameterBoolean;
//import com.example.thebeginningv2.inventory_module.data_types.InventoryParameterDate;
//import com.example.thebeginningv2.inventory_module.data_types.InventoryParameterFloat;
//import com.example.thebeginningv2.inventory_module.data_types.InventoryParameterString;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//import com.example.thebeginningv2.repository.data_types.xml.SettingsModeField;
//import com.example.thebeginningv2.repository.data_types.xml.SettingsModeRoot;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by Константин on 24.06.2017.
// */
//
//public class LoadDataMode extends SingleSingleUseCase<Object, CreateInventoryPattern> {
//    private SecondLevelRepositoryInterface repositoryInterface;
//
//    public LoadDataMode(Executor executor, SecondLevelRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<CreateInventoryPattern> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<CreateInventoryPattern>() {
//            @Override
//            public CreateInventoryPattern call() throws Exception {
//                SettingsModeRoot root = repositoryInterface.getDataMode();
//
//                com.example.thebeginningv2.repository.data_types.xml.SettingsModeCommon common = root.getCommon();
//
//                String dbPatternFile = common.getDbPatternFile();
//                if(dbPatternFile == null) throw new Exception("DbPattern file is not specified");
//
//                String divideByDates = common.getDivideByDates(), rewriteCatalogMode = common.getRewriteCatalogMode();
//
//                CreateInventorySettings modeSettings = new CreateInventorySettings(
//                        common.getTitle(),
//                        dbPatternFile,
//                        divideByDates != null && divideByDates.equals("1"),
//                        rewriteCatalogMode != null && rewriteCatalogMode.equals("1"));
//
//                List<SettingsModeField> fields = root.getFields(), pakets = root.getPakets();
//                List<InventoryParameter> inventoryParameters = new ArrayList<>();
//
//                for (SettingsModeField i: fields){
//                    String tag = i.getTag();
//
//                    if(tag == null) throw new Exception("No tag");
//
//                    String title = i.getTitle(), defaultValue = i.getDefaultValue();
//
//                    String partOfName = i.getPartOfName(), required = i.getRequired(), editable = i.getEditable();
//                    String type = i.getType();
//
//                    InventoryParameter inventoryParameter;
//
//                    if(type != null){
//                        switch (type){
//                            case SettingsModeField.STRING:
//                                inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        false);
//                                break;
//                            case SettingsModeField.BOOLEAN:
//                                inventoryParameter = new InventoryParameterBoolean(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        false);
//                                break;
//                            case SettingsModeField.DATE:
//                                inventoryParameter = new InventoryParameterDate(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        false);
//                                break;
//                            case SettingsModeField.FLOAT:
//                                inventoryParameter = new InventoryParameterFloat(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        false);
//                                break;
//                            default:
//                                inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        false);
//                                break;
//                        }
//                    }
//                    else inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                            (partOfName != null && partOfName.equals("1")),
//                            (required != null && required.equals("1")),
//                            (editable != null && editable.equals("1")),
//                            false);
//
//                    inventoryParameters.add(inventoryParameter);
//                }
//
//                for (SettingsModeField i: pakets){
//                    String tag = i.getTag();
//
//                    if(tag == null) throw new Exception("No tag");
//
//                    String title = i.getTitle(), defaultValue = i.getDefaultValue();
//
//                    String partOfName = i.getTitle(), required = i.getRequired(), editable = i.getEditable();
//                    String type = i.getType();
//
//                    InventoryParameter inventoryParameter;
//
//                    if(type != null){
//                        switch (type){
//                            case SettingsModeField.STRING:
//                                inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        true);
//                                break;
//                            case SettingsModeField.BOOLEAN:
//                                inventoryParameter = new InventoryParameterBoolean(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        true);
//                                break;
//                            case SettingsModeField.DATE:
//                                inventoryParameter = new InventoryParameterDate(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        true);
//                                break;
//                            case SettingsModeField.FLOAT:
//                                inventoryParameter = new InventoryParameterFloat(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        true);
//                                break;
//                            default:
//                                inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                                        (partOfName != null && partOfName.equals("1")),
//                                        (required != null && required.equals("1")),
//                                        (editable != null && editable.equals("1")),
//                                        true);
//                                break;
//                        }
//                    }
//                    else inventoryParameter = new InventoryParameterString(title, tag, defaultValue,
//                            (partOfName != null && partOfName.equals("1")),
//                            (required != null && required.equals("1")),
//                            (editable != null && editable.equals("1")),
//                            true);
//
//                    inventoryParameters.add(inventoryParameter);
//                }
//
//                return new CreateInventoryPattern(modeSettings, inventoryParameters);
//            }
//        });
//    }
//}
