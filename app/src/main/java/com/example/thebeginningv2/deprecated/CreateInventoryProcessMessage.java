//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
//
///**
// * Created by Константин on 24.06.2017.
// */
//
//public class CreateInventoryProcessMessage extends MessageManager<CreateInventoryProcessMessage.Interface> {
//    public static final String REQUEST_CREATION_PARAMETERS = "RequestCreationParameters";
//    public static final String INVENTORY_CREATED = "INVENTORY_CREATED";
//
//    public CreateInventoryProcessMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendRequestCreationParameters(){
//        sendMessage(new EventBus.Message(REQUEST_CREATION_PARAMETERS));
//    }
//
//    public void sendCreatedInventory(String name){
//        sendMessage(new EventBus.DataMessage<>(INVENTORY_CREATED, name));
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case CreateInventoryMessage.CREATION_PARAMETERS:
//                receiveInterface.onCreationParametersReceived((CreateInventoryPattern) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onCreationParametersReceived(CreateInventoryPattern dataMode);
//    }
//}
