//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Action;
//import io.reactivex.observers.DisposableCompletableObserver;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by Константин on 10.03.2017.
// */
//
//public abstract class SingleCompletableUseCase<P> extends CompletableUseCase<P>{
//    private Disposable disposable = null;
//
//    protected SingleCompletableUseCase(Executor executor) {
//        super(executor);
//    }
//
//    @Override
//    public void run(P params, DisposableCompletableObserver observer) {
//        dispose();
//        buildUseCaseCompletable(params).subscribeOn(Schedulers.from(getExecutor())).
//                observeOn(AndroidSchedulers.mainThread()).
//                doFinally(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        disposable = null;
//                    }
//                }).subscribe(observer);
//        disposable = observer;
//    }
//
//    @Override
//    public void dispose() {
//        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
//    }
//}
