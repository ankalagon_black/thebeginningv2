//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ConvertedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.CreatedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
//
//import java.util.List;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class InventoryProcessModelMessages extends MessageManager<InventoryProcessModelMessages.Interface>{
//    public static final String SHORT_STATISTICS = "ShortStatistics";
//
//    public InventoryProcessModelMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendShortStatistics(ShortStatistics shortStatistics){
//        sendMessage(new EventBus.DataMessage<>(SHORT_STATISTICS, shortStatistics));
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case ShortStatisticsMessage.REQUEST_SHORT_STATISTICS:
//                receiveInterface.onRequestShortStatisticsReceived();
//                break;
//            case AccountedRecordsModelMessages.REMOVED_ACCOUNTED_RECORDS:
//                receiveInterface.onRemovedAccountedRecordsReceived((List<AccountedRecord>) message.getData());
//                break;
//            case AccountedRecordsModelMessages.REMOVED_ACCOUNTED_RECORD:
//                receiveInterface.onRemovedAccountedRecordReceived((AccountedRecord) message.getData());
//                break;
//            case AccountedRecordsModelMessages.ADDED_UNIDENTIFIED_RECORD:
//                receiveInterface.onCreatedUnidentifiedRecordReceived((AccountedRecord) message.getData());
//                break;
//            case EditQuantityMessage.EDITED_QUANTITY:
//                receiveInterface.onEditedQuantityReceived((EditedQuantity) message.getData());
//                break;
//            case MoveRecordMessage.MOVED_RECORD:
//                receiveInterface.onMovedRecordReceived((EditedQuantity) message.getData());
//                break;
//            case ConvertMessage.CONVERTED_RECORD:
//                receiveInterface.onConvertedRecordReceived((ConvertedRecord) message.getData());
//                break;
//            case ManualInputMessages.CREATED_RECORD:
//                receiveInterface.onCreatedRecordReceived((CreatedRecord) message.getData());
//                break;
//            case ManualInputMessages.CREATED_UNIDENTIFIED_RECORD:
//                receiveInterface.onCreatedUnidentifiedRecordReceived((AccountedRecord) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onRequestShortStatisticsReceived();
//        void onRemovedAccountedRecordsReceived(List<AccountedRecord> accountedRecords);
//        void onRemovedAccountedRecordReceived(AccountedRecord accountedRecord);
//        void onEditedQuantityReceived(EditedQuantity editedQuantity);
//        void onMovedRecordReceived(EditedQuantity editedQuantity);
//        void onConvertedRecordReceived(ConvertedRecord convertedRecord);
//        void onCreatedRecordReceived(CreatedRecord createdRecord);
//        void onCreatedUnidentifiedRecordReceived(AccountedRecord accountedRecord);
//    }
//}
