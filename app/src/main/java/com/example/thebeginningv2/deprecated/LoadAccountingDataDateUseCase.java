//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by Константин on 23.06.2017.
// */
//
//public class LoadAccountingDataDateUseCase extends SingleSingleUseCase<Object, String> {
//    private SecondLevelRepositoryInterface repositoryInterface;
//
//    public LoadAccountingDataDateUseCase(Executor executor, SecondLevelRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<String> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return repositoryInterface.getAccountingDataDate();
//            }
//        });
//    }
//}
