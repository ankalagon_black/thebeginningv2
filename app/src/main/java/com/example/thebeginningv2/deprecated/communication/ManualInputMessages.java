//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.CreatedRecord;
//
///**
// * Created by Константин on 17.06.2017.
// */
//
//public class ManualInputMessages extends MessageSender {
//    public static final String CREATED_RECORD = "CreatedRecord";
//    public static final String CREATED_UNIDENTIFIED_RECORD = "CreatedUnidentifiedRecord";
//
//    public ManualInputMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendCreatedRecord(CreatedRecord createdRecord){
//        sendMessage(new EventBus.DataMessage<>(CREATED_RECORD, createdRecord));
//    }
//
//    public void sendCreatedUnidentifiedRecord(AccountedRecord accountedRecord){
//        sendMessage(new EventBus.DataMessage<>(CREATED_UNIDENTIFIED_RECORD, accountedRecord));
//    }
//}
