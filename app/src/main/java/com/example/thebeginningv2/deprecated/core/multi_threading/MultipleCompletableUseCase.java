//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Executor;
//
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Action;
//import io.reactivex.observers.DisposableCompletableObserver;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by Константин on 10.03.2017.
// */
//
//public abstract class MultipleCompletableUseCase<P> extends CompletableUseCase<P> {
//    private List<Disposable> disposables = new ArrayList<>();
//
//    protected MultipleCompletableUseCase(Executor executor) {
//        super(executor);
//    }
//
//    @Override
//    public void run(P params, final DisposableCompletableObserver observer) {
//
//        buildUseCaseCompletable(params).subscribeOn(Schedulers.from(getExecutor())).
//                observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
//            @Override
//            public void run() throws Exception {
//                disposables.remove(observer);
//            }
//        }).subscribe(observer);
//
//        disposables.add(observer);
//    }
//
//    @Override
//    public void dispose() {
//        for (Disposable i : disposables) if(!i.isDisposed()) i.dispose();
//        disposables.clear();
//    }
//}
