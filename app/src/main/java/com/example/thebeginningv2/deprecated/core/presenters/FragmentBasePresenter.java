//package com.example.thebeginningv2.deprecated.core.presenters;
//
//import android.os.Bundle;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public abstract class FragmentBasePresenter {
//    public abstract void onActivityCreated(Bundle bundle, Bundle savedInstanceState);
//
//    public abstract void onSaveInstanceState(Bundle outState);
//
//    public abstract void onDestroyView();
//}
