//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ConvertedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.CreatedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
//import com.example.thebeginningv2.inventory_process.data_types.InputValues;
//
//import java.util.List;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class AccountedRecordsModelMessages extends MessageManager<AccountedRecordsModelMessages.Interface> {
//    public static final String REMOVED_ACCOUNTED_RECORDS = "RemovedAccountedRecords";
//    public static final String REMOVED_ACCOUNTED_RECORD = "RemovedAccountedRecord";
//    public static final String ADDED_UNIDENTIFIED_RECORD = "AddedUnidentifiedRecord";
//
//    public AccountedRecordsModelMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case InputSettingsMessage.INPUT_SETTINGS_CHANGED:
//                receiveInterface.onInputSettingChangedReceived((InputSettings) message.getData());
//                break;
//            case EditQuantityMessage.EDITED_QUANTITY:
//                receiveInterface.onEditedQuantityReceived((EditedQuantity) message.getData());
//                break;
//            case MoveRecordMessage.MOVED_RECORD:
//                receiveInterface.onMovedRecordReceived((EditedQuantity) message.getData());
//                break;
//            case ConvertMessage.CONVERTED_RECORD:
//                receiveInterface.onConvertedRecordReceived((ConvertedRecord) message.getData());
//                break;
//            case ManualInputMessages.CREATED_RECORD:
//                receiveInterface.onCreatedRecordReceived((CreatedRecord) message.getData());
//                break;
//            case ManualInputMessages.CREATED_UNIDENTIFIED_RECORD:
//                receiveInterface.onCreatedUnidentifiedRecordReceived((AccountedRecord) message.getData());
//                break;
//            case InputTypeMessage.INPUT_VALUES:
//                receiveInterface.onInputValuesReceived((InputValues) message.getData());
//                break;
//        }
//    }
//
//    public void sendRemovedAccountedRecords(List<AccountedRecord> removedAccountedRecords){
//        sendMessage(new EventBus.DataMessage<>(REMOVED_ACCOUNTED_RECORDS, removedAccountedRecords));
//    }
//
//    public void sendRemovedAccountedRecord(AccountedRecord accountedRecord){
//        sendMessage(new EventBus.DataMessage<>(REMOVED_ACCOUNTED_RECORD, accountedRecord));
//    }
//
//    public void sendAddUnidentifiedRecord(AccountedRecord accountedRecord){
//        sendMessage(new EventBus.DataMessage<>(ADDED_UNIDENTIFIED_RECORD, accountedRecord));
//    }
//
//    public interface Interface{
//        void onInputSettingChangedReceived(InputSettings inputSettings);
//        void onEditedQuantityReceived(EditedQuantity editedQuantity);
//        void onMovedRecordReceived(EditedQuantity editedQuantity);
//        void onConvertedRecordReceived(ConvertedRecord convertedRecord);
//        void onCreatedRecordReceived(CreatedRecord createdRecord);
//        void onCreatedUnidentifiedRecordReceived(AccountedRecord accountedRecord);
//        void onInputValuesReceived(InputValues inputValues);
//    }
//}
