//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public class ThirdLevelModelMessages extends MessageManager<ThirdLevelModelMessages.Interface>{
//    public static final String ACCOUNTING_DATA_DATE = "AccountingDataDate";
//
//    public ThirdLevelModelMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendAccountingDataDate(String accountingDatadate){
//        sendMessage(new EventBus.DataMessage<>(ACCOUNTING_DATA_DATE, accountingDatadate));
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case SentInventoryMessages.RETURN_TO_IN_PROGRESS:
//                receiveInterface.onReturnToInProgressReceived();
//                break;
//            case SendingInventoryDataMessage.SENDING_FINISHED:
//                receiveInterface.onSendingFinished();
//                break;
//            case InProgressInventoryStateMessages.DELETE_INVENTORY:
//                receiveInterface.onDeleteInventoryReceived();
//                break;
//
//            case AccountingDataStateMessages.REQUEST_ACCOUNTING_DATA_DATE:
//                receiveInterface.onRequestAccountingDataDateReceived();
//                break;
//            case DownloadingDataMessage.ACCOUNTING_DATA_DOWNLOADED:
//                receiveInterface.onAccountingDataDownloadedReceived();
//                break;
//            case AccountingDataStateMessages.DELETE_ACCOUNTING_DATA:
//                receiveInterface.onDeleteAccountingDataReceived();
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onReturnToInProgressReceived();
//        void onSendingFinished();
//        void onDeleteInventoryReceived();
//
//        void onRequestAccountingDataDateReceived();
//        void onAccountingDataDownloadedReceived();
//        void onDeleteAccountingDataReceived();
//    }
//}
