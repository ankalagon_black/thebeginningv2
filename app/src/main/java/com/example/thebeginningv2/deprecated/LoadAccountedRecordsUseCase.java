//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class LoadAccountedRecordsUseCase extends SingleSingleUseCase<Object, List<AccountedRecord>> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public LoadAccountedRecordsUseCase(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<List<AccountedRecord>> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<List<AccountedRecord>>() {
//            @Override
//            public List<AccountedRecord> call() throws Exception {
//                return repositoryInterface.getAccountedRecords();
//            }
//        });
//    }
//}
