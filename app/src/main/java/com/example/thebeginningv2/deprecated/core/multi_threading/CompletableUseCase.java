//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.Completable;
//import io.reactivex.observers.DisposableCompletableObserver;
//
///**
// * Created by Константин on 10.03.2017.
// */
//
//public abstract class CompletableUseCase<P> {
//    private Executor executor;
//
//    protected CompletableUseCase(Executor executor) {
//        this.executor = executor;
//    }
//
//    public Executor getExecutor() {
//        return executor;
//    }
//
//    protected abstract Completable buildUseCaseCompletable(P params);
//
//    public abstract void run(P params, DisposableCompletableObserver observer);
//
//    public abstract void dispose();
//}
