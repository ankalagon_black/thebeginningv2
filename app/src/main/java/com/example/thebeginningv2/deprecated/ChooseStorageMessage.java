//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//
///**
// * Created by Константин on 22.06.2017.
// */
//
//public class ChooseStorageMessage extends MessageSender {
//    public static final String CHOSEN_STORAGE = "ChosenStorage";
//
//    public ChooseStorageMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendChosenStorage(String storage){
//        sendMessage(new EventBus.DataMessage<>(CHOSEN_STORAGE, storage));
//    }
//}
