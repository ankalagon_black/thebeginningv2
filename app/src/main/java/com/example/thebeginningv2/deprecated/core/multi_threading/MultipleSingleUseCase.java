//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Executor;
//
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Action;
//import io.reactivex.observers.DisposableSingleObserver;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by apple on 07.03.17.
// */
//
//public abstract class MultipleSingleUseCase<P,R> extends SingleUseCase<P,R> {
//    private List<Disposable> disposables = new ArrayList<>();
//
//    protected MultipleSingleUseCase(Executor executor) {
//        super(executor);
//    }
//
//    @Override
//    public void run(P params, final DisposableSingleObserver<R> observer) {
//        buildUseCaseSingle(params).subscribeOn(Schedulers.from(getExecutor())).
//                observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
//            @Override
//            public void run() throws Exception {
//                disposables.remove(observer);
//            }
//        }).subscribe(observer);
//
//        disposables.add(observer);
//    }
//
//    @Override
//    public void dispose() {
//        for (Disposable i : disposables) if(!i.isDisposed()) i.dispose();
//        disposables.clear();
//    }
//}
