////package com.example.thebeginningv2.inventory_module.communication;
////
////import com.example.thebeginningv2.deprecated.core.communication.EventBus;
////import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
////import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
////import com.example.thebeginningv2.inventories.data_types.Inventory;
////
/////**
//// * Created by Константин on 20.06.2017.
//// */
////
////public class SecondLevelMessages extends MessageManager<SecondLevelMessages.Interface> {
////    public static final String ACCOUNTING_DATA_DATE = "AccountingDataDate";
////
////    public SecondLevelMessages(EventBusInterface busInterface) {
////        super(busInterface);
////    }
////
////    public void sendAccountingDataDate(String accountingDataDate){
////        sendMessage(new EventBus.DataMessage<>(ACCOUNTING_DATA_DATE, accountingDataDate));
////    }
////
////    @Override
////    protected void onMessageReceived(EventBus.BaseMessage message) {
////        switch (message.getType()){
////            case SendingInventoryDataMessage.INVENTORY_SENT:
////                receiveInterface.onInventorySent((Inventory) message.getData());
////                break;
////
////            case AccountingDataStateMessages.REQUEST_ACCOUNTING_DATA_DATE:
////                receiveInterface.onRequestAccountingDataDateReceived();
////                break;
////            case DownloadingDataMessage.ACCOUNTING_DATA_DOWNLOADED:
////                receiveInterface.onAccountingDataDownloadedReceived();
////                break;
////            case AccountingDataStateMessages.DELETE_ACCOUNTING_DATA:
////                receiveInterface.onDeleteAccountingDataReceived();
////                break;
////            case CreateInventoryProcessMessage.INVENTORY_CREATED:
////                receiveInterface.onInventoryCreated((String) message.getData());
////                break;
////        }
////    }
//
//    public interface Interface{
//        void onInventorySent(Inventory inventory);
//
//        void onInventoryCreated(String name);
//
//        void onRequestAccountingDataDateReceived();
//        void onAccountingDataDownloadedReceived();
//        void onDeleteAccountingDataReceived();
//    }
//}
