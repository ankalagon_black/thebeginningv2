//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.ConvertedRecord;
//
///**
// * Created by Константин on 16.06.2017.
// */
//
//public class ConvertMessage extends MessageSender {
//    public static final String CONVERTED_RECORD = "ConvertedRecord";
//
//    public ConvertMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendConvertedRecord(ConvertedRecord convertedRecord){
//        sendMessage(new EventBus.DataMessage<>(CONVERTED_RECORD, convertedRecord));
//    }
//}
