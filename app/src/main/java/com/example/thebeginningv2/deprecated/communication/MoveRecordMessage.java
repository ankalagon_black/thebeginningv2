//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//
///**
// * Created by Константин on 16.06.2017.
// */
//
//public class MoveRecordMessage extends MessageSender{
//    public static final String MOVED_RECORD = "MovedRecord";
//
//    public MoveRecordMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendMovedRecord(EditedQuantity editedQuantity){
//        sendMessage(new EventBus.DataMessage<>(MOVED_RECORD, editedQuantity));
//    }
//}
