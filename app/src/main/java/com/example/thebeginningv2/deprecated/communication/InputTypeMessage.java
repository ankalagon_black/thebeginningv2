//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.InputValues;
//
///**
// * Created by Константин on 19.06.2017.
// */
//
//public class InputTypeMessage extends MessageSender {
//    public static final String INPUT_VALUES = "InputValues";
//
//    public InputTypeMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendInputValues(InputValues inputValues){
//        sendMessage(new EventBus.DataMessage<>(INPUT_VALUES, inputValues));
//    }
//}
