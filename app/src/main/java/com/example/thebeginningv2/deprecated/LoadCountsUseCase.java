//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class LoadCountsUseCase extends SingleSingleUseCase<Object, RecordsCount> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public LoadCountsUseCase(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<RecordsCount> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<RecordsCount>() {
//            @Override
//            public RecordsCount call() throws Exception {
//                return repositoryInterface.getRecordsCount();
//            }
//        });
//    }
//}
