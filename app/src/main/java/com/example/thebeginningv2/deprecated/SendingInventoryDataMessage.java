//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventories.data_types.Inventory;
//
///**
// * Created by Константин on 07.06.2017.
// */
//
//public class SendingInventoryDataMessage extends MessageSender {
//    public static final String INVENTORY_SENT = "InventorySent";
//    public static final String SENDING_FINISHED = "SendingFinished";
//
//    public SendingInventoryDataMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void inventorySent(Inventory inventory){
//        sendMessage(new EventBus.DataMessage<>(INVENTORY_SENT, inventory));
//    }
//
//    public void sendingFinished(){
//        sendMessage(new EventBus.Message(SENDING_FINISHED));
//    }
//}
