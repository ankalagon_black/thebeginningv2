//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//
///**
// * Created by Константин on 15.06.2017.
// */
//
//public class EditQuantityMessage extends MessageSender {
//    public static final String EDITED_QUANTITY = "EditedQuantity";
//
//    public EditQuantityMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendEditedQuantity(EditedQuantity editedQuantity){
//        sendMessage(new EventBus.DataMessage<>(EDITED_QUANTITY, editedQuantity));
//    }
//}
