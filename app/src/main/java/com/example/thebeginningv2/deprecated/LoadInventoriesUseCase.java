//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventories.data_types.Inventory;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by Константин on 23.06.2017.
// */
//
//public class LoadInventoriesUseCase extends SingleSingleUseCase<Object, List<Inventory>>{
//    private SecondLevelRepositoryInterface repositoryInterface;
//
//    public LoadInventoriesUseCase(Executor executor, SecondLevelRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<List<Inventory>> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<List<Inventory>>() {
//            @Override
//            public List<Inventory> call() throws Exception {
//                return repositoryInterface.getInventories();
//            }
//        });
//    }
//}
