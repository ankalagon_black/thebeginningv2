//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ConvertedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.CreatedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//
//import java.util.List;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class RemainedRecordsModelMessages extends ShortStatisticsMessage<RemainedRecordsModelMessages.Interface> {
//
//    public RemainedRecordsModelMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        super.onMessageReceived(message);
//        switch (message.getType()){
//            case AccountedRecordsModelMessages.REMOVED_ACCOUNTED_RECORDS:
//                receiveInterface.onRemovedAccountedRecordsReceived((List<AccountedRecord>) message.getData());
//                break;
//            case AccountedRecordsModelMessages.REMOVED_ACCOUNTED_RECORD:
//                receiveInterface.onRemovedAccountedRecordReceived((AccountedRecord) message.getData());
//                break;
//            case MoveRecordMessage.MOVED_RECORD:
//                receiveInterface.onMovedRecordReceived((EditedQuantity) message.getData());
//                break;
//            case ConvertMessage.CONVERTED_RECORD:
//                receiveInterface.onConvertedRecordReceived((ConvertedRecord) message.getData());
//                break;
//            case ManualInputMessages.CREATED_RECORD:
//                receiveInterface.onCreatedRecordReceived((CreatedRecord) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface extends ShortStatisticsMessage.Interface{
//        void onRemovedAccountedRecordsReceived(List<AccountedRecord> accountedRecords);
//        void onRemovedAccountedRecordReceived(AccountedRecord accountedRecord);
//        void onMovedRecordReceived(EditedQuantity editedQuantity);
//        void onConvertedRecordReceived(ConvertedRecord convertedRecord);
//        void onCreatedRecordReceived(CreatedRecord createdRecord);
//    }
//}
