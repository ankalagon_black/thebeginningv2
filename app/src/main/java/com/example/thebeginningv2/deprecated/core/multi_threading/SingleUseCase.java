//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//import io.reactivex.observers.DisposableSingleObserver;
//
///**
// * Created by apple on 07.03.17.
// */
//
//public abstract class SingleUseCase<P,R> {
//    private Executor executor;
//
//    protected SingleUseCase(Executor executor){
//        this.executor = executor;
//    }
//
//    public Executor getExecutor() {
//        return executor;
//    }
//
//    public abstract boolean hasProcessRunning();
//
//    protected abstract Single<R> buildUseCaseSingle(P params);
//
//    public abstract void run(P params, DisposableSingleObserver<R> observer);
//
//    public abstract void dispose();
//}
