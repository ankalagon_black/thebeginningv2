//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class LoadShortStatistics extends SingleSingleUseCase<Object, ShortStatistics> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public LoadShortStatistics(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<ShortStatistics> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<ShortStatistics>() {
//            @Override
//            public ShortStatistics call() throws Exception {
//                return repositoryInterface.getShortStatistics();
//            }
//        });
//    }
//}
