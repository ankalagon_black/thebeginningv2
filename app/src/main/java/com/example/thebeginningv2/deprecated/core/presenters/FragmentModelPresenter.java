//package com.example.thebeginningv2.deprecated.core.presenters;
//
//import com.example.thebeginningv2.deprecated.core.models.Model;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public abstract class FragmentModelPresenter<M extends Model> extends FragmentBasePresenter {
//
//    public abstract void attachModel(M model);
//
//    public abstract void detachModel();
//}
