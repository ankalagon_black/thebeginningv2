//package com.example.thebeginningv2.inventory_process.models;
//
//import com.example.thebeginningv2.deprecated.core.models.Model;
//import com.example.thebeginningv2.deprecated.LoadCountsUseCase;
//import com.example.thebeginningv2.deprecated.LoadShortStatistics;
//import com.example.thebeginningv2.deprecated.communication.InventoryProcessModelMessages;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.ConvertedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.CreatedRecord;
//import com.example.thebeginningv2.inventory_process.data_types.EditedQuantity;
//import com.example.thebeginningv2.inventory_process.data_types.Record;
//import com.example.thebeginningv2.inventory_process.data_types.RecordsCount;
//import com.example.thebeginningv2.inventory_process.data_types.ShortStatistics;
//
//import java.util.List;
//
//import io.reactivex.observers.DisposableSingleObserver;
//
///**
// * Created by Константин on 10.06.2017.
// */
//
//public class InventoryProcessActivityModel extends Model<InventoryProcessActivityModel.Interface> implements InventoryProcessModelMessages.Interface{
//    private RecordsCount recordsCount = null;
//    private ShortStatistics shortStatistics = null;
//
//    private InventoryProcessModelMessages modelMessages;
//
//    private LoadCountsUseCase loadCountsUseCase;
//    private LoadShortStatistics loadShortStatistics;
//
//    public InventoryProcessActivityModel(InventoryProcessModelMessages modelMessages,
//                                         LoadCountsUseCase loadCountsUseCase, LoadShortStatistics loadShortStatistics){
//        this.modelMessages = modelMessages;
//        this.modelMessages.setReceiveInterface(this);
//        this.modelMessages.subscribe();
//
//        this.loadCountsUseCase = loadCountsUseCase;
//        this.loadShortStatistics = loadShortStatistics;
//    }
//
//    public boolean isRecordsCountLoaded(){
//        return recordsCount != null;
//    }
//
//    private boolean isShortStatisticsLoaded(){
//        return shortStatistics != null;
//    }
//
//    public void loadData(){
//        loadCountsUseCase.run(null, new DisposableSingleObserver<RecordsCount>() {
//            @Override
//            public void onSuccess(RecordsCount value) {
//                recordsCount = value;
//                if(isConnected()) anInterface.onRecordsCountChanged();
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                if(isConnected()) anInterface.onLoadingError(e);
//            }
//        });
//
//        loadShortStatistics.run(null, new DisposableSingleObserver<ShortStatistics>() {
//            @Override
//            public void onSuccess(ShortStatistics value) {
//                shortStatistics = value;
//                modelMessages.sendShortStatistics(value);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                if(isConnected()) anInterface.onLoadingError(e);
//            }
//        });
//    }
//
//    public RecordsCount getRecordsCount() {
//        return recordsCount;
//    }
//
//    @Override
//    public void onRequestShortStatisticsReceived() {
//        if(isShortStatisticsLoaded()) modelMessages.sendShortStatistics(shortStatistics);
//    }
//
//    @Override
//    public void onEditedQuantityReceived(EditedQuantity editedQuantity) {
//        AccountedRecord accountedRecord = (AccountedRecord) editedQuantity.getRecord();
//
//        float newQuantity = editedQuantity.getNewQuantity();
//
//        shortStatistics.addAccountedQuantity(-accountedRecord.getAccountedQuantity());
//        shortStatistics.addAccountedQuantity(newQuantity);
//
//        if(accountedRecord.isIdentified()){
//            switch (accountedRecord.getDifferenceType()){
//                case AccountedRecord.WITH_RESIDUE:
//                    shortStatistics.addResidueQuantity(-accountedRecord.getDifference());
//                    break;
//                case AccountedRecord.WITH_SURPLUS:
//                    shortStatistics.addSurplusQuantity(-accountedRecord.getDifference());
//                    break;
//            }
//
//            int type = AccountedRecord.getDifferenceType(newQuantity, accountedRecord.getStatedQuantity());
//            switch (type){
//                case AccountedRecord.WITH_RESIDUE:
//                    float difference = accountedRecord.getStatedQuantity() - newQuantity;
//                    shortStatistics.addResidueQuantity(difference);
//                    break;
//                case AccountedRecord.WITH_SURPLUS:
//                    difference = newQuantity - accountedRecord.getStatedQuantity();
//                    shortStatistics.addSurplusQuantity(difference);
//                    break;
//            }
//        }
//        else{
//            shortStatistics.addUnidentifiedQuantity(-accountedRecord.getAccountedQuantity());
//            shortStatistics.addUnidentifiedQuantity(newQuantity);
//        }
//
//        modelMessages.sendShortStatistics(shortStatistics);
//    }
//
//    @Override
//    public void onMovedRecordReceived(EditedQuantity editedQuantity) {
//        recordsCount.addRemainedRecordsCount(-1);
//        recordsCount.addAccountedRecordsCount(1);
//
//        AccountedRecord accountedRecord = new AccountedRecord(editedQuantity.getRecord(), editedQuantity.getNewQuantity());
//
//        shortStatistics.addAccountedQuantity(accountedRecord.getAccountedQuantity());
//        switch (accountedRecord.getDifferenceType()){
//            case AccountedRecord.WITH_RESIDUE:
//                shortStatistics.addResidueQuantity(accountedRecord.getDifference());
//                break;
//            case AccountedRecord.WITH_SURPLUS:
//                shortStatistics.addSurplusQuantity(accountedRecord.getDifference());
//                break;
//        }
//
//        modelMessages.sendShortStatistics(shortStatistics);
//
//        if(isConnected()) anInterface.onRecordsCountChanged();
//    }
//
//    @Override
//    public void onConvertedRecordReceived(ConvertedRecord convertedRecord) {
//        float unidentifiedQuantity = convertedRecord.getUnidentifiedRecord().getAccountedQuantity();
//        shortStatistics.addAccountedQuantity(-unidentifiedQuantity);
//        shortStatistics.addUnidentifiedQuantity(-unidentifiedQuantity);
//
//
//        Record targetRecord = convertedRecord.getTargetRecord();
//        AccountedRecord resultRecord = convertedRecord.getResult();
//        if(targetRecord.isAccounted()){
//            AccountedRecord accountedRecord = (AccountedRecord) targetRecord;
//            shortStatistics.addAccountedQuantity(-accountedRecord.getAccountedQuantity());
//            switch (accountedRecord.getDifferenceType()){
//                case AccountedRecord.WITH_RESIDUE:
//                    shortStatistics.addResidueQuantity(-accountedRecord.getDifference());
//                    break;
//                case AccountedRecord.WITH_SURPLUS:
//                    shortStatistics.addSurplusQuantity(-accountedRecord.getDifference());
//                    break;
//            }
//        }
//        else {
//            recordsCount.addRemainedRecordsCount(-1);
//            recordsCount.addAccountedRecordsCount(1);
//
//            if(isConnected()) anInterface.onRecordsCountChanged();
//        }
//
//        shortStatistics.addAccountedQuantity(resultRecord.getAccountedQuantity());
//        switch (resultRecord.getDifferenceType()){
//            case AccountedRecord.WITH_RESIDUE:
//                shortStatistics.addResidueQuantity(resultRecord.getDifference());
//                break;
//            case AccountedRecord.WITH_SURPLUS:
//                shortStatistics.addSurplusQuantity(resultRecord.getDifference());
//                break;
//        }
//
//        modelMessages.sendShortStatistics(shortStatistics);
//    }
//
//    @Override
//    public void onCreatedRecordReceived(CreatedRecord createdRecord) {
//        Record targetRecord = createdRecord.getTargetRecord();
//        AccountedRecord resultRecord = createdRecord.getResultRecord();
//
//        if(targetRecord.isAccounted()){
//            AccountedRecord accountedRecord = (AccountedRecord) targetRecord;
//            shortStatistics.addAccountedQuantity(-accountedRecord.getAccountedQuantity());
//            if(targetRecord.isIdentified()) {
//                switch (accountedRecord.getDifferenceType()) {
//                    case AccountedRecord.WITH_RESIDUE:
//                        shortStatistics.addResidueQuantity(-accountedRecord.getDifference());
//                        break;
//                    case AccountedRecord.WITH_SURPLUS:
//                        shortStatistics.addSurplusQuantity(-accountedRecord.getDifference());
//                        break;
//                }
//            }
//            else {
//                shortStatistics.addUnidentifiedQuantity(-accountedRecord.getAccountedQuantity());
//            }
//        }
//        else {
//            recordsCount.addRemainedRecordsCount(-1);
//            recordsCount.addAccountedRecordsCount(1);
//
//            if(isConnected()) anInterface.onRecordsCountChanged();
//        }
//
//        shortStatistics.addAccountedQuantity(resultRecord.getAccountedQuantity());
//        if(resultRecord.isIdentified()) {
//            switch (resultRecord.getDifferenceType()) {
//                case AccountedRecord.WITH_RESIDUE:
//                    shortStatistics.addResidueQuantity(resultRecord.getDifference());
//                    break;
//                case AccountedRecord.WITH_SURPLUS:
//                    shortStatistics.addSurplusQuantity(resultRecord.getDifference());
//                    break;
//            }
//        }
//        else {
//            shortStatistics.addUnidentifiedQuantity(resultRecord.getAccountedQuantity());
//        }
//
//        modelMessages.sendShortStatistics(shortStatistics);
//    }
//
//    @Override
//    public void onCreatedUnidentifiedRecordReceived(AccountedRecord accountedRecord) {
//        shortStatistics.addAccountedQuantity(accountedRecord.getAccountedQuantity());
//        shortStatistics.addUnidentifiedQuantity(accountedRecord.getAccountedQuantity());
//
//        modelMessages.sendShortStatistics(shortStatistics);
//    }
//
//    public interface Interface{
//        void onRecordsCountChanged();
//        void onLoadingError(Throwable e);
//    }
//}
