//package com.example.thebeginningv2.inventories.dialogs;
//
//import android.app.Dialog;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DialogFragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.example.thebeginningv2.R;
//import com.example.thebeginningv2.deprecated.core.multi_threading.TaskExecutor;
//import com.example.thebeginningv2.deprecated.LoadDataMode;
//import com.example.thebeginningv2.inventories.adapters.InventoryParametersAdapter;
//import com.example.thebeginningv2.inventory_module.communication.CreateInventoryMessage;
//import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
//import com.example.thebeginningv2.inventory_module.models.CreateInventoryModel;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//
///**
// * Created by Константин on 20.06.2017.
// */
//
//public class CreateInventoryDialogv1 extends DialogFragment implements CreateInventoryModel.Interface{
//    private Toolbar toolbar;
//    private RecyclerView recyclerView;
//
//    private InventoryParametersAdapter adapter;
//
//    private CreateInventoryModel model;
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
//        return super.onCreateDialog(savedInstanceState);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.inventory_module_create_inventory, container, false);
//
//        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                CreateInventoryDialogv1.this.dismiss();
//            }
//        });
//
//        recyclerView = (RecyclerView) view.findViewById(R.id.inventory_parameters);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//        Button button = (Button) view.findViewById(R.id.create);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(adapter.allRequiredParametersFilled()){
//                    CreateInventoryProcessDialog dialog = new CreateInventoryProcessDialog();
//                    dialog.show(getFragmentManager(), dialog.getTag());
//                }
//                else Toast.makeText(getActivity(), R.string.create_inventory_waring, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        SecondLevelRetainingFragment fragment = (SecondLevelRetainingFragment) getFragmentManager().findFragmentByTag(SecondLevelRetainingFragment.TAG);
//        SecondLevelRepositoryInterface repositoryInterface = fragment.getRepository();
//
//        CreateInventoryMessage message = new CreateInventoryMessage(fragment.getEventBus());
//
//        model = new CreateInventoryModel(new LoadDataMode(TaskExecutor.getInstance(), repositoryInterface), message);
//        model.connect(this);
//        model.loadData();
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        model.disconnect();
//    }
//
//    @Override
//    public void onDataModeReceived(CreateInventoryPattern dataMode) {
//        String title = dataMode.getModeSettings().getTitle();
//        if(title != null) toolbar.setTitle(title);
//        else toolbar.setTitle(R.string.create_inventory_default_title);
//
//        adapter = new InventoryParametersAdapter(dataMode.getInventoryParameters());
//        recyclerView.setAdapter(adapter);
//    }
//
//    @Override
//    public void onModeLoadingError(Throwable e) {
//
//    }
//
//    @Override
//    public void close() {
//        dismiss();
//    }
//}
