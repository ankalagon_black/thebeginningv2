//package com.example.thebeginningv2.deprecated;
//
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
//import com.example.thebeginningv2.inventories.data_types.CreateInventorySettings;
//import com.example.thebeginningv2.inventories.data_types.InventoryParameter;
//import com.example.thebeginningv2.repository.SecondLevelRepositoryInterface;
//import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
//import com.example.thebeginningv2.repository.data_types.xml.PakModeCommon;
//import com.example.thebeginningv2.repository.data_types.xml.PakModeField;
//import com.example.thebeginningv2.repository.data_types.xml.PakModeRoot;
//import com.example.thebeginningv2.repository.data_types.xml.PakRoot;
//import com.example.thebeginningv2.repository.data_types.xml.Element;
//import com.example.thebeginningv2.repository.data_types.xml.DataModeAdditionalData;
//import com.example.thebeginningv2.repository.data_types.xml.DataModeSystemData;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by Константин on 24.06.2017.
// */
//
//public class CreateInventoryUseCase extends SingleSingleUseCase<CreateInventoryPattern, String> {
//    private SecondLevelRepositoryInterface repositoryInterface;
//
//    public CreateInventoryUseCase(Executor executor, SecondLevelRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<String> buildUseCaseSingle(final CreateInventoryPattern params) {
//        return Single.fromCallable(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                String name = "";
//                List<InventoryParameter> inventoryParameters = params.getInventoryParameters();
//
//                List<Element> mainData = new ArrayList<>(), paketsData = new ArrayList<>();
//
//                for(InventoryParameter i: inventoryParameters) {
//                    String value = i.getValueAsString();
//                    if(value == null || value.equals("")) value = "-";
//
//                    if(i.isPartOfName()) name += value;
//
//                    Element element = new Element(i.getTag(), value);
//                    if(i.isInPakets()) paketsData.add(element);
//                    else mainData.add(element);
//                }
//
//                if(name.replace(" ", "").length() == 0) name = "Инвентаризация";
//
//                Date date = Calendar.getInstance().getTime();
//
//                name += ", " + new SimpleDateFormat("dd-MM-yyyy").format(date);
//
//                DataModeSystemData systemData = new DataModeSystemData(name, new SimpleDateFormat("dd-MM-yyyy HH-mm").format(date));
//
//                CreateInventorySettings modeSettings = params.getModeSettings();
//
//                String dbPatternFile = modeSettings.getDbPatternFile();
//
//                DataModeAdditionalData additionalData = new DataModeAdditionalData(dbPatternFile,
//                        modeSettings.isDivideByDates() ? "1" : "0",
//                        modeSettings.isRewriteCatalogMode() ? "1" : "0");
//
//                repositoryInterface.createInventoryFolder(name, new DataModeRoot(additionalData, mainData, systemData, paketsData));
//
//                PakModeRoot pakModeRoot = repositoryInterface.getDataPakMode(additionalData.getDbPatternFile());
//
//                PakModeCommon pakModeCommon = pakModeRoot.getCommon();
//                List<PakModeField> pakModeFields = pakModeRoot.getFields();
//
//                List<String> tags = new ArrayList<>(pakModeFields.size());
//
//                for (PakModeField i: pakModeFields) tags.add(i.getTag());
//                if(pakModeCommon.getGroupField() != null){
//                    tags.add(pakModeCommon.getGroupField());
//                    tags.addAll(pakModeCommon.getGroupBarcodeFields());
//                    tags.addAll(pakModeCommon.getGroupRFIDFields());
//                }
//
//                PakRoot pakRoot = repositoryInterface.getDataPak(pakModeCommon.getDbDataFile(), tags);
//
//                repositoryInterface.createInventoryDatabase(name, pakModeRoot, pakRoot);
//
//                return name;
//            }
//        });
//    }
//}
