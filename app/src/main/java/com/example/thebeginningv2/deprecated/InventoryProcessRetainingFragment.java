//package com.example.thebeginningv2.deprecated;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.models.Model;
//import com.example.thebeginningv2.repository.InventoryProcessRepository;
//import com.example.thebeginningv2.repository.PathManager;
//import com.example.thebeginningv2.repository.database.DatabaseManager;
//
//import java.util.HashMap;
//
///**
// * Created by Константин on 09.06.2017.
// */
//
//public class InventoryProcessRetainingFragment extends Fragment {
//    public static final String TAG = "InventoryProcessRetainingFragment";
//
//    private HashMap<String, Model> hashMap = new HashMap<>();
//
//    private EventBus eventBus = new EventBus();
//
//    private InventoryProcessRepository repository;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
//    }
//
//    public void initializeRepository(){
//        PathManager pathManager = PathManager.getIntance();
//        //// TODO: 05.07.2017 nu ti ponyal
//        DatabaseManager databaseManager = new DatabaseManager(null);
//
//        repository = new InventoryProcessRepository(pathManager, databaseManager);
//    }
//
//    public EventBus getEventBus() {
//        return eventBus;
//    }
//
//    public void saveModel(String tag, Model model){
//        hashMap.put(tag, model);
//    }
//
//    public Model restoreModel(String tag){
//        return hashMap.remove(tag);
//    }
//
//    public InventoryProcessRepository getRepository() {
//        return repository;
//    }
//}
