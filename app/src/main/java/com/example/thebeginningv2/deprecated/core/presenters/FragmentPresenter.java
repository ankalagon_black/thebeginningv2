//package com.example.thebeginningv2.deprecated.core.presenters;
//
//import android.os.Parcelable;
//
//import com.example.thebeginningv2.deprecated.core.models.Model;
//
///**
// * Created by Константин on 06.06.2017.
// */
//
//public abstract class FragmentPresenter<M extends Model, S extends Parcelable> extends FragmentStateModelPresenter<S> {
//
//    public abstract void attachModel(M model);
//
//    public abstract void detachModel();
//}
