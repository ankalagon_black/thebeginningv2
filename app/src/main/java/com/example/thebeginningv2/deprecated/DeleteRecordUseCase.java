//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleCompletableUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.Completable;
//import io.reactivex.functions.Action;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class DeleteRecordUseCase extends SingleCompletableUseCase<AccountedRecord> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public DeleteRecordUseCase(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Completable buildUseCaseCompletable(final AccountedRecord params) {
//        return Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//                repositoryInterface.deleteAccountedRecord(params);
//            }
//        });
//    }
//}
