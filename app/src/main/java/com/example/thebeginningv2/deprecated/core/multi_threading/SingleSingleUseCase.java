//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Action;
//import io.reactivex.observers.DisposableSingleObserver;
//import io.reactivex.schedulers.Schedulers;
//
///**
// * Created by apple on 07.03.17.
// */
//
//public abstract class SingleSingleUseCase<P,R> extends SingleUseCase<P,R> {
//    private Disposable disposable = null;
//
//    protected SingleSingleUseCase(Executor executor) {
//        super(executor);
//    }
//
//    @Override
//    public boolean hasProcessRunning() {
//        return disposable != null;
//    }
//
//    @Override
//    public void run(P params, DisposableSingleObserver<R> observer) {
//        dispose();
//        buildUseCaseSingle(params).subscribeOn(Schedulers.from(getExecutor())).
//                observeOn(AndroidSchedulers.mainThread()).
//                doFinally(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        disposable = null;
//                    }
//                }).subscribe(observer);
//
//        disposable = observer;
//    }
//
//    @Override
//    public void dispose() {
//        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
//    }
//}
