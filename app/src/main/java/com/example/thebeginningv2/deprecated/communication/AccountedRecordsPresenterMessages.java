//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventory_process.data_types.Selection;
//
///**
// * Created by Константин on 13.06.2017.
// */
//
//public class AccountedRecordsPresenterMessages extends MessageManager<AccountedRecordsPresenterMessages.Interface> {
//
//    public AccountedRecordsPresenterMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case SelectionMessages.SELECTION_CHANGED:
//                receiveInterface.onSelectionChangedReceived((Selection) message.getData());
//                break;
//            case ShortStatisticsMessages.OPEN_SELECTION:
//                receiveInterface.onOpenSelectionReceived();
//                break;
//            case SelectionMessages.CLOSE_SELECTION:
//                receiveInterface.onCloseSelectionReceived();
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onSelectionChangedReceived(Selection selection);
//        void onOpenSelectionReceived();
//        void onCloseSelectionReceived();
//    }
//}
