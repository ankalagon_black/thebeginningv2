//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleCompletableUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.List;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Completable;
//import io.reactivex.functions.Action;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class DeleteRecordsUseCase extends SingleCompletableUseCase<List<AccountedRecord>> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public DeleteRecordsUseCase(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Completable buildUseCaseCompletable(final List<AccountedRecord> params) {
//        return Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//                repositoryInterface.deleteAll(params);
//            }
//        });
//    }
//}
