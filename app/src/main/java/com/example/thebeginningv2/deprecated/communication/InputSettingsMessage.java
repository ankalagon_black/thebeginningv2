//package com.example.thebeginningv2.deprecated.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageSender;
//import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
//
///**
// * Created by Константин on 15.06.2017.
// */
//
//public class InputSettingsMessage extends MessageSender {
//    public static final String INPUT_SETTINGS_CHANGED = "InputSettingsChanged";
//
//    public InputSettingsMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    public void sendInputSettingsChanged(InputSettings inputSettings){
//        sendMessage(new EventBus.DataMessage<>(INPUT_SETTINGS_CHANGED, inputSettings));
//    }
//}
