//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//import com.example.thebeginningv2.inventories.data_types.CreateInventoryPattern;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class CreateInventoryMessage extends MessageManager<CreateInventoryMessage.Interface> {
//    public static final String CREATION_PARAMETERS = "CreationParameters";
//
//    public CreateInventoryMessage(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case CreateInventoryProcessMessage.REQUEST_CREATION_PARAMETERS:
//                receiveInterface.onCreationParametersRequested();
//                break;
//        }
//    }
//
//    public void sendCreationParameters(CreateInventoryPattern dataMode){
//        sendMessage(new EventBus.DataMessage<>(CREATION_PARAMETERS, dataMode));
//    }
//
//    public interface Interface{
//        void onCreationParametersRequested();
//    }
//}
