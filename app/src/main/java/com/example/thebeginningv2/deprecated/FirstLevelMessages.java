//package com.example.thebeginningv2.inventory_module.communication;
//
//import com.example.thebeginningv2.deprecated.core.communication.EventBus;
//import com.example.thebeginningv2.deprecated.core.communication.EventBusInterface;
//import com.example.thebeginningv2.deprecated.core.communication.MessageManager;
//
///**
// * Created by Константин on 22.06.2017.
// */
//
//public class FirstLevelMessages extends MessageManager<FirstLevelMessages.Interface> {
//
//    public FirstLevelMessages(EventBusInterface busInterface) {
//        super(busInterface);
//    }
//
//    @Override
//    protected void onMessageReceived(EventBus.BaseMessage message) {
//        switch (message.getType()){
//            case ChooseStorageMessage.CHOSEN_STORAGE:
//                receiveInterface.onStorageChosen((String) message.getData());
//                break;
//        }
//    }
//
//    public interface Interface{
//        void onStorageChosen(String storage);
//    }
//}
