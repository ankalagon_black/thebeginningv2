//package com.example.thebeginningv2.deprecated.core.communication;
//
//import io.reactivex.disposables.Disposable;
//import io.reactivex.functions.Consumer;
//import io.reactivex.subjects.PublishSubject;
//import io.reactivex.subjects.Subject;
//
///**
// * Created by Константин on 05.06.2017.
// */
//
//public class EventBus implements EventBusInterface {
//    private Subject<Object> subject = PublishSubject.create();
//
//    public Disposable subscribe(Consumer<Object> consumer){
//        return subject.subscribe(consumer);
//    }
//
//    public <M extends BaseMessage> void send(M m){
//        subject.onNext(m);
//    }
//
//    public void finish(){
//        subject.onComplete();
//    }
//
//    public static abstract class BaseMessage<D>{
//
//        private String type;
//
//        public BaseMessage(String type){
//            this.type = type;
//        }
//
//        public String getType(){
//            return type;
//        }
//
//        public abstract boolean hasData();
//
//        public abstract D getData();
//    }
//
//    public static class Message extends BaseMessage{
//
//        public Message(String type) {
//            super(type);
//        }
//
//        @Override
//        public boolean hasData() {
//            return false;
//        }
//
//        @Override
//        public Object getData() {
//            return null;
//        }
//    }
//
//    public static class DataMessage<D> extends BaseMessage<D>{
//        private D data;
//
//        public DataMessage(String type, D data) {
//            super(type);
//            this.data = data;
//        }
//
//        @Override
//        public boolean hasData() {
//            return true;
//        }
//
//        public D getData(){
//            return data;
//        }
//    }
//}
