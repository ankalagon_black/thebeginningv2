//package com.example.thebeginningv2.deprecated;
//
//import com.example.thebeginningv2.deprecated.core.multi_threading.SingleSingleUseCase;
//import com.example.thebeginningv2.inventory_process.data_types.RemainedRecord;
//import com.example.thebeginningv2.repository.InventoryProcessRepositoryInterface;
//
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.concurrent.Executor;
//
//import io.reactivex.Single;
//
///**
// * Created by apple on 30.06.17.
// */
//
//public class LoadRemainedRecordsUseCase extends SingleSingleUseCase<Object, List<RemainedRecord>> {
//    private InventoryProcessRepositoryInterface repositoryInterface;
//
//    public LoadRemainedRecordsUseCase(Executor executor, InventoryProcessRepositoryInterface repositoryInterface) {
//        super(executor);
//        this.repositoryInterface = repositoryInterface;
//    }
//
//    @Override
//    protected Single<List<RemainedRecord>> buildUseCaseSingle(Object params) {
//        return Single.fromCallable(new Callable<List<RemainedRecord>>() {
//            @Override
//            public List<RemainedRecord> call() throws Exception {
//                return repositoryInterface.getRemainedRecords();
//            }
//        });
//    }
//}
