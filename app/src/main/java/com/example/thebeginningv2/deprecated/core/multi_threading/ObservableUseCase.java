//package com.example.thebeginningv2.deprecated.core.multi_threading;
//
//import java.util.concurrent.Executor;
//
//import io.reactivex.Observable;
//import io.reactivex.Observer;
//
///**
// * Created by apple on 28.06.17.
// */
//
//public abstract class ObservableUseCase<P,R> {
//    private Executor executor;
//
//    protected ObservableUseCase(Executor executor){
//        this.executor = executor;
//    }
//
//    public Executor getExecutor() {
//        return executor;
//    }
//
//    public abstract Observable<R> buildUseCaseObservable(P params);
//
//    public abstract void run(P params, Observer<R> observer);
//
//    public abstract void dispose();
//}
