package com.example.thebeginningv2.inventory_process_without_data;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.inventory_process.dialogs.InputSettingsDialog;
import com.example.thebeginningv2.inventory_process.dialogs.ProcessDialog;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process_without_data.adapters.WithoutDataAdapter;
import com.example.thebeginningv2.inventory_process_without_data.dialogs.EditQuantityDialog;
import com.example.thebeginningv2.inventory_process_without_data.domains.MainDomain;
import com.example.thebeginningv2.inventory_process_without_data.fragments.InputType1Fragment;
import com.example.thebeginningv2.inventory_process_without_data.fragments.InputType2Fragment;
import com.example.thebeginningv2.inventory_process_without_data.models.WithoutDataInventoryModel;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 07.07.2017.
 */

public class WithoutAccountingDataInventoryActivity extends AppCompatActivity
        implements WithoutDataAdapter.Interface, WithoutDataInventoryModel.ViewInterface{

    private TextView count;
    private TextView quantity;
    private RecyclerView recyclerView;

    private WithoutDataAdapter adapter;

    private WithoutDataInventoryModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_process_without_data);

        setTitle(R.string.inventory);
        getSupportActionBar().setHomeButtonEnabled(true);

        count = (TextView) findViewById(R.id.count);
        quantity = (TextView) findViewById(R.id.quantity);

        recyclerView = (RecyclerView) findViewById(R.id.accounted_records_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        PathManager pathManager = PathManager.getIntance();
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(pathManager.getDataChildPath(PathManager.DB), null);

        model = new WithoutDataInventoryModel(this, new MainDomain(pathManager, new DatabaseManager(db), this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.registerEventBus();

        model.getRecordsCount();
        model.getQuantity();
        model.getAccountedRecords();

        model.getInputMode();
    }

    @Override
    protected void onStop() {
        super.onStop();
        model.unregisterEventBus();
        model.disposeAllBackgroundTasks();
    }

    @Override
    public void onCountReceived(int count) {
        this.count.setText(String.format(Locale.US, getString(R.string.accounted_records_count), count));
    }

    @Override
    public void onQuantityReceived(float quantity) {
        this.quantity.setText(String.format(Locale.US, getString(R.string.quantity_unidentified), quantity));
    }

    @Override
    public void onAccountedRecordsReceived(List<AccountedRecord> accountedRecords) {
        adapter = new WithoutDataAdapter(accountedRecords, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onInputModeChanged(InputSettings inputSettings) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (inputSettings.getInputType()){
            case InputSettings.INPUT_TYPE_1:
                InputType1Fragment inputType1Fragment = (InputType1Fragment) fragmentManager.findFragmentByTag(InputType1Fragment.TAG);
                if(inputType1Fragment == null) inputType1Fragment = new InputType1Fragment();
                fragmentManager.beginTransaction().replace(R.id.input_mode_container, inputType1Fragment, InputType1Fragment.TAG).commit();
                break;
            case InputSettings.INPUT_TYPE_2:
                InputType2Fragment inputType2Fragment = (InputType2Fragment) fragmentManager.findFragmentByTag(InputType2Fragment.TAG);
                if(inputType2Fragment == null) inputType2Fragment = new InputType2Fragment();
                fragmentManager.beginTransaction().replace(R.id.input_mode_container, inputType2Fragment, InputType2Fragment.TAG).commit();
                break;
        }
    }

    @Override
    public void onAllRecordsDeleted() {
        adapter.notifyDataSetChanged();
        EventBus.getDefault().post(new StopProcess());
    }

    @Override
    public void onRecordAdded() {
        recyclerView.scrollToPosition(0);
        adapter.onRecordAdded();
    }

    @Override
    public void onRecordDeleted(int position) {
        adapter.onRecordDeleted(position);
        EventBus.getDefault().post(new StopProcess());
    }

    @Override
    public void onDeleteRecordError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new StopProcess());
    }

    @Override
    public void onError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.inventory_without_data, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.remove_all:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.action_confirmation).
                        setMessage(R.string.remove_all_records_confirmation).
                        setNegativeButton(R.string.cancel, null).
                        setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ProcessDialog processDialog = new ProcessDialog();
                                processDialog.show(getSupportFragmentManager(), processDialog.getTag());

                                model.deleteAll();
                            }
                        }).
                        create().show();
                return true;
            case R.id.input_settings:
                InputSettingsDialog inputSettingsDialog = new InputSettingsDialog();
                inputSettingsDialog.show(getSupportFragmentManager(), inputSettingsDialog.getTag());
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onFunctionalButton(final AccountedRecord accountedRecord, View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.getMenuInflater().inflate(R.menu.no_data_unidentified_record_popup, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.edit:
                        EditQuantityDialog dialog = EditQuantityDialog.getInstance(accountedRecord);
                        dialog.show(getSupportFragmentManager(), dialog.getTag());
                        return true;
                    case R.id.remove:
                        AlertDialog.Builder builder = new AlertDialog.Builder(WithoutAccountingDataInventoryActivity.this);
                        builder.setTitle(R.string.action_confirmation).
                                setMessage(R.string.remove_record_confirmation).
                                setNegativeButton(R.string.cancel, null).
                                setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        ProcessDialog processDialog = new ProcessDialog();
                                        processDialog.show(getSupportFragmentManager(), processDialog.getTag());

                                        model.deleteRecord(accountedRecord);
                                    }
                                }).
                                create().show();
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }
}
