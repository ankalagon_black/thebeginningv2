package com.example.thebeginningv2.inventory_process_without_data.fragments;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.fragments.SupportModelFragment;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.dialogs.ProcessDialog;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process_without_data.models.InputTypeModel;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 26.07.2017.
 */

public class InputType2Fragment extends SupportModelFragment<InputTypeModel> implements InputTypeModel.ViewInterface{
    public static final String TAG = "InputType2Fragment";

    private EditText editValue, editQuantity;

    private long startTime;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_input_type_2, container, false);

        Button button = (Button) view.findViewById(R.id.input);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = editValue.getText().toString(), quantity = editQuantity.getText().toString();

                if(value.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)) {
                    ProcessDialog processDialog = new ProcessDialog();
                    processDialog.show(getFragmentManager(), processDialog.getTag());

                    float currentQuantity = Float.parseFloat(quantity);
                    getModel().input(new InputValues(value, currentQuantity));
                }
            }
        });

        editValue = (EditText) view.findViewById(R.id.value_edit);
        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                if (after == 1 || (start == 0 && after > 5)) startTime = System.currentTimeMillis();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    long diff = System.currentTimeMillis() - startTime;
                    if (diff < 300 && editValue.getText().length() > 5) {
                        String value = editValue.getText().toString(), quantity = editQuantity.getText().toString();

                        if(value.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)){
                            ProcessDialog processDialog = new ProcessDialog();
                            processDialog.show(getFragmentManager(), processDialog.getTag());

                            float currentQuantity = Float.parseFloat(quantity);
                            getModel().input(new InputValues(value, currentQuantity));
                        }
                    }
                }
            }
        });

        editQuantity = (EditText) view.findViewById(R.id.quantity_edit);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            setDefault();
        }
    }

    private void setDefault(){
        editValue.setText("");
        editQuantity.setText("1");
    }

    @Override
    protected InputTypeModel createModel() {
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(PathManager.getIntance().getDataChildPath(PathManager.DB), null);
        return new InputTypeModel(new DatabaseManager(db),this);
    }

    @Override
    public void onInputFinished() {
        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        setDefault();
        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), R.string.input_finished, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        View view = getActivity().getCurrentFocus();
        if(view != null) view.clearFocus();
        editValue.requestFocus();

        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
