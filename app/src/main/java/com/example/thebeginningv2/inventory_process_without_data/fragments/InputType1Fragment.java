package com.example.thebeginningv2.inventory_process_without_data.fragments;

import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.fragments.SupportModelFragment;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.dialogs.ProcessDialog;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process_without_data.models.InputTypeModel;
import com.example.thebeginningv2.repository.BarcodeScannerReceiver;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Константин on 26.07.2017.
 */

public class InputType1Fragment extends SupportModelFragment<InputTypeModel> implements InputTypeModel.ViewInterface, BarcodeScannerReceiver.ReceiveInterface{
    public static final String TAG = "InputType1Fragment";
    private EditText quantityEdit;

    private BarcodeScannerReceiver receiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_process_input_type_1, container, false);

        quantityEdit = (EditText) view.findViewById(R.id.quantity_edit);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        receiver = new BarcodeScannerReceiver(this);

        IntentFilter intentFilter = new IntentFilter(BarcodeScannerReceiver.INTENT);

        getContext().registerReceiver(receiver, intentFilter);

        if(savedInstanceState == null) setDefault();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(receiver);
    }

    @Override
    protected InputTypeModel createModel() {
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(PathManager.getIntance().getDataChildPath(PathManager.DB), null);
        return new InputTypeModel(new DatabaseManager(db),this);
    }

    private void setDefault(){
        quantityEdit.setText("1");
    }

    @Override
    public void onBarcodeScanned(String barcode) {
        String quantity = quantityEdit.getText().toString();

        if(barcode.replace(" ", "").length() > 0 && NumberUtils.isCreatable(quantity)){
            ProcessDialog processDialog = new ProcessDialog();
            processDialog.show(getFragmentManager(), processDialog.getTag());

            float currentQuantity = Float.parseFloat(quantity);
            getModel().input(new InputValues(barcode, currentQuantity));
        }
    }

    @Override
    public void onInputFinished() {
        setDefault();
        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), R.string.input_finished, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
