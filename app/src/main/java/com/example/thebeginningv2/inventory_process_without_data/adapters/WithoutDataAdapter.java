package com.example.thebeginningv2.inventory_process_without_data.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.views.RecordViewHolder;
import com.example.thebeginningv2.inventory_process.views.UnidentifiedRecordBody;

import java.util.List;
import java.util.Locale;

/**
 * Created by Константин on 26.07.2017.
 */

public class WithoutDataAdapter extends RecyclerView.Adapter<WithoutDataAdapter.UnidentifiedRecordHolder> {
    private List<AccountedRecord> accountedRecords;
    private Interface anInterface;

    public WithoutDataAdapter(List<AccountedRecord> accountedRecords, Interface anInterface){
        this.accountedRecords = accountedRecords;
        this.anInterface = anInterface;
    }

    @Override
    public UnidentifiedRecordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UnidentifiedRecordHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.unidentified_accounted_record_with_functional_button, parent, false));
    }

    @Override
    public void onBindViewHolder(UnidentifiedRecordHolder holder, int position) {
        holder.setRecord(position, accountedRecords.get(position),
                false,
                false,
                false);
    }

    @Override
    public int getItemCount() {
        return accountedRecords.size();
    }


    public void onRecordDeleted(int position){
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, accountedRecords.size() - position);
    }

    public void onRecordAdded(){
        notifyItemInserted(0);
        notifyItemRangeChanged(0, accountedRecords.size());
    }

    public class UnidentifiedRecordHolder extends RecordViewHolder<AccountedRecord>{
        private UnidentifiedRecordBody body;

        private TextView index;
        private ImageButton button;

        public UnidentifiedRecordHolder(View itemView) {
            super(itemView);
            body = new UnidentifiedRecordBody(itemView);
            index = (TextView) itemView.findViewById(R.id.index);
            button = (ImageButton) itemView.findViewById(R.id.functional_button);
        }

        @Override
        public void setRecord(int position, final AccountedRecord record, boolean allowColorIdentification, boolean allowGroupInventory, boolean allowGPS) {
            body.setAccountedRecord(record, allowColorIdentification, allowGPS);

            index.setText(String.format(Locale.US, itemView.getContext().getString(R.string.index), position + 1));
            button.setImageResource(R.drawable.ic_more_vert);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.onFunctionalButton(record, button);
                }
            });
        }
    }

    public interface Interface{
        void onFunctionalButton(AccountedRecord accountedRecord, View view);
    }
}
