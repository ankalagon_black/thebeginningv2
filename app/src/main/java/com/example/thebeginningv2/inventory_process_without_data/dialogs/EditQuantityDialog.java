package com.example.thebeginningv2.inventory_process_without_data.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.core_v4.dialogs.SupportModelDialog;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.dialogs.ProcessDialog;
import com.example.thebeginningv2.inventory_process.messages.StopProcess;
import com.example.thebeginningv2.inventory_process.views.UnidentifiedRecordBody;
import com.example.thebeginningv2.inventory_process_without_data.models.EditQuantityModel;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

/**
 * Created by Константин on 26.07.2017.
 */

public class EditQuantityDialog extends SupportModelDialog<EditQuantityModel> implements EditQuantityModel.ViewInterface{

    private UnidentifiedRecordBody unidentifiedRecordBody;
    private EditText accountedQuantityEdit;

    public static EditQuantityDialog getInstance(AccountedRecord accountedRecord){
        EditQuantityDialog dialog = new EditQuantityDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY, accountedRecord);

        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(R.layout.edit_record_quantity, null);

        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.accounted_record_container);

        View unidentifiedRecord = layoutInflater.inflate(R.layout.unidentified_accounted_record_body_v2, frameLayout, true);
        unidentifiedRecordBody = new UnidentifiedRecordBody(unidentifiedRecord);

        accountedQuantityEdit = (EditText) view.findViewById(R.id.accounted_quantity_edit);
        accountedQuantityEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentQuantity = editable.toString();
                if (NumberUtils.isCreatable(currentQuantity)) {
                    updateQuantity(Float.parseFloat(currentQuantity));
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog alertDialog = builder.setTitle(R.string.edit_quantity).
                setView(view).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);
                    }
                }).
                setPositiveButton(R.string.edit, null).
                create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String currentQuantity = accountedQuantityEdit.getEditableText().toString();
                        if(NumberUtils.isCreatable(currentQuantity)){
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(accountedQuantityEdit.getWindowToken(), 0);

                            float quantity = Float.parseFloat(currentQuantity);

                            ProcessDialog processDialog = new ProcessDialog();
                            processDialog.show(getFragmentManager(), processDialog.getTag());

                            getModel().update(quantity);
                        }
                        else Toast.makeText(getContext(), R.string.quantity_not_a_number, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    protected EditQuantityModel createModel() {
        AccountedRecord accountedRecord = getArguments().getParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY);

        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(PathManager.getIntance().getDataChildPath(PathManager.DB), null);

        return new EditQuantityModel(this, new DatabaseManager(db), accountedRecord);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState == null){
            AccountedRecord accountedRecord = getArguments().getParcelable(EditQuantityModel.ACCOUNTED_RECORD_KEY);

            unidentifiedRecordBody.setAccountedRecord(accountedRecord, false, false);

            accountedQuantityEdit.setText(String.format(Locale.US, getString(R.string.float_3_decimal), accountedRecord.getAccountedQuantity()));
        }
    }

    private void updateQuantity(float newQuantity){
        TextView quantity = unidentifiedRecordBody.getQuantity();

        quantity.setText(String.format(Locale.US, getString(R.string.float_1_demical), newQuantity));
    }

    @Override
    public void onQuantityEdited() {
        EventBus.getDefault().post(new StopProcess());
        dismiss();
    }

    @Override
    public void onError(Throwable e) {
        EventBus.getDefault().post(new StopProcess());
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
