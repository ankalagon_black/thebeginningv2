package com.example.thebeginningv2.inventory_process_without_data.models;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputValues;
import com.example.thebeginningv2.inventory_process.messages.AddedAccountedRecord;
import com.example.thebeginningv2.inventory_process_without_data.messages.ListChanges;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 26.07.2017.
 */

public class InputTypeModel extends BaseModel {


    private DatabaseManager databaseManager;
    private ViewInterface viewInterface;

    public InputTypeModel(DatabaseManager databaseManager, ViewInterface viewInterface){
        this.databaseManager = databaseManager;
        this.viewInterface = viewInterface;
    }

    public void input(final InputValues inputValues){
        DisposableSingleObserver<ListChanges> observer = new DisposableSingleObserver<ListChanges>() {

            @Override
            public void onSuccess(ListChanges value) {
                viewInterface.onInputFinished();

                EventBus.getDefault().post(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<ListChanges> single = Single.fromCallable(new Callable<ListChanges>() {
            @Override
            public ListChanges call() throws Exception {
                List<AccountedRecord> accountedRecords = databaseManager.findUnidentifiedRecords(inputValues.getValue(), false);
                if(accountedRecords.size() != 0){
                    AccountedRecord oldRecord = accountedRecords.get(0);
                    AccountedRecord accountedRecord = new AccountedRecord(oldRecord, oldRecord.getAccountedQuantity() + inputValues.getQuantity());
                    databaseManager.updateAccountedRecord(AccountedRecord.NO_ID, accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity());

                    return new ListChanges(ListChanges.UPDATE, accountedRecord);
                }
                else{
                    AccountedRecord accountedRecord = new AccountedRecord(inputValues.getValue(), inputValues.getQuantity(), null);
                    databaseManager.addToAccountedRecords(AccountedRecord.NO_ID, accountedRecord.getBarcode(), accountedRecord.getAccountedQuantity(), accountedRecord.getGPSMark());

                    return new ListChanges(ListChanges.ADD, accountedRecord);
                }
            }
        });

        subscribe(single, observer);
    }

    public interface ViewInterface{
        void onInputFinished();
        void onError(Throwable e);
    }
}
