package com.example.thebeginningv2.inventory_process_without_data.models;

import com.example.thebeginningv2.core_v3.models.Model;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.inventory_process_without_data.domains.MainDomain;
import com.example.thebeginningv2.inventory_process_without_data.messages.ListChanges;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 26.07.2017.
 */

public class WithoutDataInventoryModel extends Model {
    private ViewInterface viewInterface;

    private MainDomain domain;

    private List<AccountedRecord> accountedRecords;
    private int count;
    private float quantity;

    public WithoutDataInventoryModel(ViewInterface viewInterface, MainDomain domain){
        this.viewInterface = viewInterface;
        this.domain = domain;
    }

    public void getRecordsCount(){
        DisposableSingleObserver<Integer> observer = new DisposableSingleObserver<Integer>() {
            @Override
            public void onSuccess(Integer value) {
                viewInterface.onCountReceived(value);
                count = value;
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Integer> single = Single.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return domain.getRecordsCount();
            }
        });

        subscribe(single, observer);
    }

    public void getQuantity(){
        DisposableSingleObserver<Float> observer = new DisposableSingleObserver<Float>() {
            @Override
            public void onSuccess(Float value) {
                quantity = value;
                viewInterface.onQuantityReceived(quantity);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<Float> single = Single.fromCallable(new Callable<Float>() {
            @Override
            public Float call() throws Exception {
                return domain.getAccountedRecordsQuantity();
            }
        });

        subscribe(single, observer);
    }

    public void getAccountedRecords(){
        DisposableSingleObserver<List<AccountedRecord>> observer = new DisposableSingleObserver<List<AccountedRecord>>() {
            @Override
            public void onSuccess(List<AccountedRecord> value) {
                accountedRecords = value;
                viewInterface.onAccountedRecordsReceived(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<List<AccountedRecord>> single = Single.fromCallable(new Callable<List<AccountedRecord>>() {
            @Override
            public List<AccountedRecord> call() throws Exception {
                return domain.getAccountedRecords();
            }
        });

        subscribe(single, observer);
    }

    public void getInputMode(){
        viewInterface.onInputModeChanged(domain.getInputSettings());
    }

    public void deleteRecord(final AccountedRecord accountedRecord){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                int index = accountedRecords.indexOf(accountedRecord);
                accountedRecords.remove(index);
                viewInterface.onRecordDeleted(index);

                --count;
                viewInterface.onCountReceived(count);
                quantity -= accountedRecord.getAccountedQuantity();
                viewInterface.onQuantityReceived(quantity);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onDeleteRecordError(e);
            }
        };

        Completable completable = Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domain.delete(accountedRecord);
            }
        });

        subscribe(completable, observer);
    }

    public void deleteAll(){
        DisposableCompletableObserver observer = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                accountedRecords.clear();
                viewInterface.onAllRecordsDeleted();

                count = 0;
                viewInterface.onCountReceived(count);

                quantity = 0;
                viewInterface.onQuantityReceived(quantity);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onDeleteRecordError(e);
            }
        };

        Completable completable = Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                domain.deleteAllRecods();
            }
        });

        subscribe(completable, observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onListChanges(ListChanges listChanges){
        if(listChanges.getType() == ListChanges.ADD){
            AccountedRecord accountedRecord = listChanges.getRecord();
            accountedRecords.add(0, accountedRecord);
            viewInterface.onRecordAdded();

            ++count;
            viewInterface.onCountReceived(count);

            quantity += accountedRecord.getAccountedQuantity();
            viewInterface.onQuantityReceived(quantity);
        }
        else {
            AccountedRecord accountedRecord = listChanges.getRecord();
            int index = findRecord(accountedRecord.getBarcode());
            if(index != - 1) {
                quantity -= accountedRecords.get(index).getAccountedQuantity();

                accountedRecords.remove(index);
                viewInterface.onRecordDeleted(index);

                accountedRecords.add(0, accountedRecord);
                viewInterface.onRecordAdded();

                quantity += accountedRecord.getAccountedQuantity();
                viewInterface.onQuantityReceived(quantity);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInputSettings(InputSettings inputSettings){
        viewInterface.onInputModeChanged(inputSettings);
    }

    private int findRecord(String barcode){
        int size = accountedRecords.size();
        for (int i = 0; i < size; i++){
            if(accountedRecords.get(i).getBarcode().equals(barcode)) return i;
        }

        return -1;
    }

    public interface ViewInterface{
        void onCountReceived(int count);
        void onQuantityReceived(float quantity);
        void onAccountedRecordsReceived(List<AccountedRecord> accountedRecords);

        void onInputModeChanged(InputSettings inputSettings);

        void onAllRecordsDeleted();

        void onRecordAdded();
        void onRecordDeleted(int position);

        void onDeleteRecordError(Throwable e);

        void onError(Throwable e);
    }
}
