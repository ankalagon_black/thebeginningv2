package com.example.thebeginningv2.inventory_process_without_data.models;

import com.example.thebeginningv2.core_v3.models.BaseModel;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process_without_data.messages.ListChanges;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Callable;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Константин on 26.07.2017.
 */

public class EditQuantityModel extends BaseModel{
    public static final String ACCOUNTED_RECORD_KEY = "AccountedRecordKey";

    private ViewInterface viewInterface;
    private DatabaseManager databaseManager;

    private AccountedRecord accountedRecord;

    public EditQuantityModel(ViewInterface viewInterface, DatabaseManager databaseManager, AccountedRecord accountedRecord){
        this.viewInterface = viewInterface;
        this.databaseManager = databaseManager;

        this.accountedRecord = accountedRecord;
    }

    public void update(final float quantity){
        DisposableSingleObserver<ListChanges> observer = new DisposableSingleObserver<ListChanges>() {
            @Override
            public void onSuccess(ListChanges value) {
                viewInterface.onQuantityEdited();
                EventBus.getDefault().post(value);
            }

            @Override
            public void onError(Throwable e) {
                viewInterface.onError(e);
            }
        };

        Single<ListChanges> single = Single.fromCallable(new Callable<ListChanges>() {
            @Override
            public ListChanges call() throws Exception {
                AccountedRecord result = new AccountedRecord(accountedRecord, quantity);

                databaseManager.updateAccountedRecord(AccountedRecord.NO_ID, result.getBarcode(), result.getAccountedQuantity());

                return new ListChanges(ListChanges.UPDATE, result);
            }
        });

        subscribe(single, observer);
    }

    public interface ViewInterface{
        void onQuantityEdited();
        void onError(Throwable e);
    }
}
