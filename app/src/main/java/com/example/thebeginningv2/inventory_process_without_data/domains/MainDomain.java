package com.example.thebeginningv2.inventory_process_without_data.domains;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.thebeginningv2.R;
import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;
import com.example.thebeginningv2.inventory_process.data_types.InputSettings;
import com.example.thebeginningv2.inventory_process.data_types.InventorySettings;
import com.example.thebeginningv2.repository.data_types.xml.DataModeRoot;
import com.example.thebeginningv2.repository.data_types.xml.Element;
import com.example.thebeginningv2.repository.managers.PathManager;
import com.example.thebeginningv2.repository.managers.database_managers.DatabaseManager;
import com.example.thebeginningv2.repository.managers.xml_managers.DataModeParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Константин on 26.07.2017.
 */

public class MainDomain {
    private PathManager pathManager;
    private DatabaseManager databaseManager;
    private Activity activity;

    public MainDomain(PathManager pathManager, DatabaseManager databaseManager, Activity activity){
        this.pathManager = pathManager;
        this.databaseManager = databaseManager;
        this.activity = activity;
    }

    public InventorySettings getInventorySettings() throws IOException, XmlPullParserException {
        DataModeParser dataModeParser = new DataModeParser(new File(pathManager.getDataChildPath(PathManager.MODE)));

        DataModeRoot root = dataModeParser.getDataModeRoot();
        List<Element> pakets = root.getPaketsData();

        boolean allowDuplicates = true, allowUnidentifiedRecords = true,
                allowColorIdentification = true, allowGroupInventory = false, allowGPS = false;

        for (Element i: pakets){
            switch (i.getTag()){
                case InventorySettings.ALLOW_DUPLICATES:
                    allowDuplicates = !i.getValue().equals("0");
                    break;
                case InventorySettings.ALLOW_UNIDENTIFIED_RECORDS:
                    allowUnidentifiedRecords = !i.getValue().equals("0");
                    break;
                case InventorySettings.ALLOW_COLOR_IDENTIFICATION:
                    allowColorIdentification = !i.getValue().equals("0");
                    break;
                case InventorySettings.GROUP_INVENTORY:
                    allowGroupInventory = i.getValue().equals("1");
                    break;
                case InventorySettings.GPS:
                    allowGPS = i.getValue().equals("1");
                    break;
            }
        }

        return new InventorySettings(allowDuplicates, allowUnidentifiedRecords, allowColorIdentification, allowGroupInventory, allowGPS);
    }

    public int getRecordsCount(){
        return databaseManager.getAccountedRecordsCount();
    }

    public float getAccountedRecordsQuantity(){
        return databaseManager.getAccountedRecordSum();
    }

    public List<AccountedRecord> getAccountedRecords(){
        return databaseManager.getAccountedRecordsAsUnidentified();
    }

    public InputSettings getInputSettings(){
        SharedPreferences preferences = activity.getSharedPreferences(activity.getString(R.string.input_settings_key), Context.MODE_PRIVATE);

        int inputType = preferences.getInt(activity.getString(R.string.input_type_key), InputSettings.DEFAULT_INPUT_TYPE);

        return new InputSettings(inputType);
    }

    public void deleteAllRecods(){
        databaseManager.deleteAllAccountedRecords();
    }

    public void delete(AccountedRecord accountedRecord){
        databaseManager.deleteFromAccountedRecords(accountedRecord.getId(), accountedRecord.getBarcode());
    }
}
