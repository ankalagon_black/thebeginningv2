package com.example.thebeginningv2.inventory_process_without_data.messages;

import com.example.thebeginningv2.inventory_process.data_types.AccountedRecord;

/**
 * Created by Константин on 27.07.2017.
 */

public class ListChanges {
    public static final int ADD = 0, UPDATE = 1;

    private int type;
    private AccountedRecord record;

    public ListChanges(int type, AccountedRecord record){
        this.type = type;
        this.record = record;
    }

    public int getType() {
        return type;
    }

    public AccountedRecord getRecord() {
        return record;
    }
}
